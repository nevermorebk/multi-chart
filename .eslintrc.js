module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
  },
  extends: [
    'plugin:@typescript-eslint/recommended'
  ],
  rules: {
    // place to specify ESLint rules - can be used to overwrite rules specified from the extended configs
    // e.g.
    'quotes': [2, 'single', { 'avoidEscape': true }],
    // temp off rules
    '@typescript-eslint/no-unused-vars': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/no-namespace': 'off',
    '@typescript-eslint/no-this-alias': 'off',
    'prefer-const': 'off',
    'prefer-spread': 'off',
    'prefer-rest-params': 'off',
  },
  overrides: [
    {
      files: ['**/config/*.js'],
      env: { browser: false },
      rules: {
        '@typescript-eslint/no-var-requires': 'off'
      }
    }
  ]
};
