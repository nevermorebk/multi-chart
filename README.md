# Multi-Chart

a stock/fx chart component.

## Usage

### Run the demo 

- `yarn`（recommended) or `npm install`
- `npm start`
- Open `http://localhost:3000/`

### Develop

- npm i -g commitizen
- ** git cz ** # replace `git commit` for conventional commit message


### Run test 

- `npm test`

> pls use `vscode` to debug testcase 

## [ChangeLog](./CHANGELOG.md)
