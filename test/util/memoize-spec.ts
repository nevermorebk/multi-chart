import * as path from 'path';
import { expect } from 'chai';
import * as sinon from 'sinon';
import { memoizeTimes } from '../../src/util/index';

function origin(x, y) {
  return {
    x, y, z: x + y
  };
}

describe('memoize', () => {
  before(() => {});

  it('should call the original function', () => {
    let callback = sinon.spy(origin);
    let proxy = memoizeTimes(callback);
    proxy();
    expect(callback.called).to.be.true;
  });

  it('should call the original function only once', function () {
    let callback = sinon.spy(origin);
    let proxy = memoizeTimes(callback);
    proxy();
    proxy();
    expect(callback.calledOnce).to.be.true;
  });

  it('should call the original function only once with same args', function () {
    let callback = sinon.spy(origin);
    let proxy = memoizeTimes(callback);
    proxy(1, 2);
    proxy(1, 2);
    expect(callback.calledOnce).to.be.true;
  });

  it('should returns the return value from the original function', function () {
    let callback = sinon.stub().returns(42);
    let proxy = memoizeTimes(callback);
    expect(proxy()).to.be.equal(42);
  });

});
