import * as path from 'path';
import { expect } from 'chai';
import { copy, deepMergeObject } from '../../src/util/index';
import defaultConfig from '../../src/config/defaultConfig';


describe('copy', () => {
  before(() => {});

  it('should return a new object', () => {
    let origin = {str: 'a', obj: {k1: 'k1'}, callback: () => {} };
    let cpy = copy(origin);
    expect(cpy).to.not.equal(origin);
    expect(cpy).to.eql(origin);

    expect(cpy.str).to.equal(origin.str);

    expect(cpy.obj).to.not.equal(origin.obj);
    expect(cpy.obj).to.eql(origin.obj);

    expect(cpy.callback).to.equal(origin.callback);
  });

  it('should return a new array', () => {
    let origin = [{str: 'a', obj: {k1: 'k1'}, callback: () => {} }];
    let cpy = copy(origin);

    expect(Array.isArray(cpy)).to.be.true;
    expect(cpy).to.not.equal(origin);
    expect(cpy[0].str).to.equal(origin[0].str);
  });

  it('should return new object using "copy" method', () => {
    let origin = {
      model: {k: 'v'},
      copy: function() {
        let model = JSON.parse(JSON.stringify(this.model));
        let bak = {model, copied: true} as any;
        bak.copy = this.copy;
        return bak;
      }
    };
    let updateCfg = {
      copykey: origin
    };
    let cpy = copy(updateCfg);
    expect(cpy.copykey).to.be.not.equal(origin);
    expect(cpy.copykey.copied).to.be.true;

  });

});


describe('deepMergeObject', () => {
  let defaultCfg;
  beforeEach(() => {
    defaultCfg = JSON.parse(JSON.stringify(defaultConfig));
    defaultCfg.callback = () => {};
    defaultCfg.symbol = {
      test: 1, // invalid
    };
    Object.freeze(defaultCfg);
  });

  it('should return a new object', () => {
    let updateCfg = {};
    let cpy = deepMergeObject(defaultCfg, updateCfg);
    expect(cpy).to.not.equal(defaultCfg);
    expect(cpy).to.be.eql(defaultCfg);
  });

  it('should merge primitive value', () => {
    let updateCfg = {
      lang: 'cn',
      chartType: 10
    };
    let cpy = deepMergeObject(defaultCfg, updateCfg);
    expect(cpy.lang).to.be.equal(updateCfg.lang);
    expect(cpy.chartType).to.be.equal(updateCfg.chartType);

    updateCfg.lang = 'en';
    expect(cpy.lang).to.be.not.equal('en');
  });

  it('should merge each key for object value', () => {
    let updateCfg = {
      sizeOption: {
        width: 1000
      },
    };
    let cpy = deepMergeObject(defaultCfg, updateCfg);
    expect(cpy.sizeOption).to.be.not.eql(updateCfg.sizeOption);
    expect(cpy.sizeOption.width).to.be.equal(updateCfg.sizeOption.width);
    expect(cpy.sizeOption.height).to.be.equal(defaultCfg.sizeOption.height);

    updateCfg.sizeOption.width = 2000;
    expect(cpy.sizeOption.width).to.be.not.equal(2000);
  });

  it('should merge new entry to return value', () => {
    let updateCfg = {
      newPrimitive: 'string',
      newFunction: () => {},
      newObject: {a: 'a'},
      newComplexObj: {
        c: 'c',
        f: function f() {return this.c;}
      }
    };
    let cpy = deepMergeObject(defaultCfg, updateCfg);
    expect(cpy.newPrimitive).to.be.equal(updateCfg.newPrimitive);
    expect(cpy.newFunction).to.be.equal(updateCfg.newFunction);

    expect(cpy.newObject).to.be.eql(updateCfg.newObject);
    expect(cpy.newObject).to.be.not.equal(updateCfg.newObject);

    expect(cpy.newComplexObj).to.be.eql(updateCfg.newComplexObj);
    expect(cpy.newComplexObj).to.be.not.equal(updateCfg.newComplexObj);

    updateCfg.newComplexObj.c = 'c2';

    expect(cpy.newComplexObj.f()).to.be.equal('c');

  });

  it('should return origin function', () => {
    let updateCfg = {
      callback: () => {}
    };
    let cpy = deepMergeObject(defaultCfg, updateCfg);
    expect(cpy.callback).to.be.equal(updateCfg.callback);
  });

  it('should return new array for array like source', () => {
    let updateCfg = {
      newArray: [1]
    };
    let cpy = deepMergeObject(defaultCfg, updateCfg);
    expect(cpy.newArray).to.be.eql(updateCfg.newArray);
    expect(cpy.newArray).to.be.not.equal(updateCfg.newArray);
  });


  it('should replace key which marked', () => {
    let updateCfg = {} as any;
    updateCfg.symbol = {
      id: 1,
      name: 'usdjpy',
      scale: 3,
      own: updateCfg
    };
    let cpy = deepMergeObject(defaultCfg, updateCfg, ['symbol']);
    expect(cpy.symbol).to.be.eql(updateCfg.symbol);
    expect(cpy.symbol).to.be.equal(updateCfg.symbol);
    expect(cpy.symbol.test).to.be.not.exist;
  });



});
