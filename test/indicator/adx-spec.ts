import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { adx } from '../../src/indicator';

describe('adx', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/adx.csv'))
      .then(data => {
        const period = 14;
        const adx0 = adx()
          .accessor(d => d).period(period);
        const result = adx0(data);
        let forward = adx0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(2 * period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 2)).to.equal(data[i].adx);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
