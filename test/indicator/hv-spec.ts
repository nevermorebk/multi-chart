import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { hv, config } from '../../src/indicator';

describe('hv', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/hv.csv'))
      .then(data => {
        let period = 8;
        const hv0 = hv()
          .period(period)
          .total(251)
          .accessor(d => d.close);
        const result = hv0(data);
        let forward = hv0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 5)).to.equal(toFixed(data[i].hv, 5));
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
