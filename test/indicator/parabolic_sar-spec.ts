import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { parabolic_sar } from '../../src/indicator';

describe('parabolic sar', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/parabolic_sar.csv'))
      .then(data => {
        const parabolicSar = parabolic_sar()
          .accessor(d => d)
          .factor([0.02, 0.2]);
        const result = parabolicSar(data);
        let forward = parabolicSar.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(100);
        for (let i = 0; i < result.length; i++) {
          expect(data[i].up - result[i][0]).to.lt(0.001);
          expect(data[i].down - result[i][1]).to.lt(0.001);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
