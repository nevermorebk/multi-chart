import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { atr } from '../../src/indicator';

describe('atr', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/atr.csv'))
      .then(data => {
        let period = 10;
        const atr0 = atr()
          .accessor(d => d).period(period);
        const result = atr0(data);
        let forward = atr0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(Math.abs(result[i] - data[i].atr), 5)).to.lte(0.00001);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
