import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { ikh } from '../../src/indicator';

const dataAccessor = d => d; //
describe('ikh', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/ikh.csv'))
      .then(ikhData => {
        let ikhSource = ikhData.slice(0, 600);
        const period = [9, 26, 52];
        const ikh9 = ikh()
          .accessor(dataAccessor).period(period);
        const result = ikh9(ikhSource);
        let forward = ikh9.forward();
        expect(result.length).to.equal(ikhSource.length + period[1] - 1);
        expect(forward).to.equal(period[1] - 1 + period[2] - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i][0], 3)).to.equal(ikhData[i].tenkan);
          expect(toFixed(result[i][1], 3)).to.equal(ikhData[i].kijun);
          expect(toFixed(result[i][2], 3)).to.equal(ikhData[i].chikou);
          expect(toFixed(result[i][3], 3)).to.equal(ikhData[i].senkouSpanA);
          expect(toFixed(result[i][4], 3)).to.equal(ikhData[i].senkouSpanB);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
