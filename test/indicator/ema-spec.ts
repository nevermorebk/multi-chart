import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { ema } from '../../src/indicator';

const emaAccessor = d => d.close; // get close price
describe('ema', () => {
  it('should has expected result ema10', done => {
    readCsv(path.resolve(__dirname, './data/ema.csv'))
      .then(emaData => {
        const period = 10;
        const ema10 = ema()
          .accessor(emaAccessor)
          .period(period);
        const result = ema10(emaData);
        let forward = ema10.forward();
        expect(result.length).to.equal(emaData.length);
        expect(forward).to.equal(2 * period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 4)).to.equal(toFixed(emaData[i].ema10, 4));
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });

  it('should has expected result ema10(2)', done => {
    readCsv(path.resolve(__dirname, './data/ema.csv'))
      .then(emaData => {
        emaData.splice(0, 1);
        const period = 10;
        const ema10 = ema()
          .accessor(emaAccessor)
          .period(period);
        const result = ema10(emaData);
        let forward = ema10.forward();
        expect(result.length).to.equal(emaData.length);
        expect(forward).to.equal(2 * period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 4)).to.equal(toFixed(emaData[i].ema10_2, 4));
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
