import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { kdj } from '../../src/indicator';

describe('kdj', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/kdj.csv'))
      .then(data => {
        let period = [14, 3, 3];
        const kdj0 = kdj()
          .accessor(d => d)
          .period(period);
        const result = kdj0(data);
        let forward = kdj0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period[0] - 1 + period[1] - 1 + period[2] - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i][0], 2)).to.equal(data[i].k);
          expect(toFixed(result[i][1], 2)).to.equal(data[i].d);
          expect(toFixed(result[i][2], 2)).to.equal(data[i].sd);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
