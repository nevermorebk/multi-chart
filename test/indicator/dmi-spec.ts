import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { dmi } from '../../src/indicator';

describe('dmi', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/dmi.csv'))
      .then(data => {
        let period = 14;
        const dmi0 = dmi()
          .period(period)
          .accessor(d => d);
        const result = dmi0(data);
        let forward = dmi0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(2 * period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i][0], 2)).to.equal(data[i].adx);
          expect(toFixed(result[i][1], 2)).to.equal(data[i].di_plus);
          expect(toFixed(result[i][2], 2)).to.equal(data[i].di_minus);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
