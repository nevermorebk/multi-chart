
import { csvParse } from 'd3-dsv';
import * as fsp from 'fs-promise';

const readCsv = file =>
  fsp.readFile(file, 'utf8').then(csvParse).then(data => {
    data.map(row => {
      Object.keys(row).forEach(column => {
        let value = row[column];
        if (value) {
          if(column === 'date') {
            row[column] = new Date(value);
          }else {
            row[column] = Number(value);
          }
        } else {
          row[column] = null;
        }
      });
    });
    return data;
  });

export {
  readCsv
};
