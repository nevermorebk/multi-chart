


export function toFixed(value , scale) {
  if(isNaN(value) || value === null || value === undefined) return null;
  if(value === -0) value = 0;
  value = parseFloat(value);
  let delta = Math.pow(10, scale);
  let val = Math.round(value * delta);
  let result = +((val / delta).toFixed(scale));
  return result;
}
