import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { momentum } from '../../src/indicator';

describe('momentum', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/momentum.csv'))
      .then(data => {
        let period = 10;
        const momentum10 = momentum()
          .accessor(d => d.close).period(period);
        const result = momentum10(data);
        let forward = momentum10.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 3)).to.equal(data[i].ma);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
