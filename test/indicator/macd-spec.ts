import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { macd } from '../../src/indicator';

const emaAccessor = d => d.close; // get close price
describe('macd', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/macd.csv'))
      .then(macdData => {
        macdData = macdData.slice(0, 40);
        const period = [12, 26, 9];
        const macd12 = macd()
          .accessor(emaAccessor)
          .period(period);
        const result = macd12(macdData);
        let forward = macd12.forward();
        expect(result.length).to.equal(macdData.length);
        expect(forward).to.equal( 2 *  Math.max(period[0], period[1]) - 1 + period[2] - 1);
        for (let i = forward; i < result.length; i++) {
          let [macd, signal, osci] = result[i];
          expect(toFixed(macd, 3)).to.equal(macdData[i].macd);
          expect(toFixed(signal, 3)).to.equal(macdData[i].macdSignal);
          expect(toFixed(osci, 3)).to.equal(macdData[i].macdOsci);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
