import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { span_model } from '../../src/indicator';

describe('span model', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/span_model.csv'))
      .then(source => {
        let period = [9, 20, 52];
        let data = source.slice(19);
        const spanModel = span_model()
          .accessor(d => d).period(period);
        const result = spanModel(data);
        let forward = spanModel.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(Math.max.apply(null, period) - 1);
        for (let i = period[0]; i < result.length - period[1]; i++) {
          if (result[i][0]) expect(data[i].late - result[i][0]).to.lte(0.00001);
          if (result[i][1]) expect(data[i].span1 - result[i][1]).to.lte(0.00001);
          if (result[i][2]) expect(data[i].span2 - result[i][2]).to.lte(0.00001);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
