import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { pivot_point } from '../../src/indicator';

describe('pivot point', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/pivot_point.csv'))
      .then(data => {
        const pivotPoint = pivot_point().accessor(d => d);
        const result = pivotPoint(data);
        expect(result.length).to.equal(data.length);
        for (let i = 0; i < result.length; i++) {
          expect(toFixed(result[i][0], 3)).to.equal(data[i].R4);
          expect(toFixed(result[i][1], 3)).to.equal(data[i].R3);
          expect(toFixed(result[i][2], 3)).to.equal(data[i].R2);
          expect(toFixed(result[i][3], 3)).to.equal(data[i].R1);
          expect(toFixed(result[i][4], 3)).to.equal(data[i].PP);
          expect(toFixed(result[i][5], 3)).to.equal(data[i].S1);
          expect(toFixed(result[i][6], 3)).to.equal(data[i].S2);
          expect(toFixed(result[i][7], 3)).to.equal(data[i].S3);
          expect(toFixed(result[i][8], 3)).to.equal(data[i].S4);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
