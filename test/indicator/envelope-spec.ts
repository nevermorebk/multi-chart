import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { envelope } from '../../src/indicator';

describe('envelope', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/envelope.csv'))
      .then(data => {
        let period = 20;
        const envelopex = envelope()
          .period(period)
          .deviation(0.0002)
          .accessor(d => d.close);
        const result = envelopex(data);
        let forward = envelopex.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i][0], 3)).to.equal(data[i].upper);
          expect(toFixed(result[i][1], 3)).to.equal(data[i].ma);
          expect(toFixed(result[i][2], 3)).to.equal(data[i].lower);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
