import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { sma } from '../../src/indicator';

const smaAccessor = d => d.close; // get close price
describe('sma', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/sma10.csv'))
      .then(smaData => {
        const period = 10;
        const sma10 = sma()
          .accessor(smaAccessor)
          .period(period);
        const result = sma10(smaData);
        let forward = sma10.forward();
        expect(result.length).to.equal(smaData.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 4)).to.equal(toFixed(smaData[i].sma, 4));
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
