import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { rci } from '../../src/indicator';

describe('rci', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/rci2.csv'))
      .then(data => {
        let period = 9;
        const rci0 = rci()
          .accessor(d => d.close).period(period);
        const result = rci0(data);
        let forward = rci0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 1)).to.equal(data[i].rci);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
