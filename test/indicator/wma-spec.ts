import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { wma } from '../../src/indicator';

const wmaAccessor = d => d.close; // get close price
describe('wma', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/wma10.csv'))
      .then(wmaData => {
        const period = 10;
        const wma10 = wma()
          .accessor(wmaAccessor).period(period);
        const result = wma10(wmaData);
        let forward = wma10.forward();
        expect(result.length).to.equal(wmaData.length);
        expect(forward).to.equal(period - 1);
        for (let i = 0; i < result.length; i++) {
          expect(toFixed(result[i], 3)).to.equal(toFixed(wmaData[i].wma, 3));
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
