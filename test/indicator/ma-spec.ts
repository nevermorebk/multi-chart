import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { ma } from '../../src/indicator';

const maAccessor = d => d.close; // get close price
describe('ma', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/ma10.csv'))
      .then(maData => {
        const period = 10;
        const ma10 = ma().accessor(maAccessor).period(period);
        const result = ma10(maData);
        let forward = ma10.forward();
        expect(result.length).to.equal(maData.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 3)).to.equal(toFixed(maData[i].ma, 3));
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
