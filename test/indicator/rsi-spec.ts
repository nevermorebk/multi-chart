import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { rsi, config } from '../../src/indicator';

describe('rsi', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/rsi.csv'))
      .then(data => {
        let period = 14;
        const rsi0 = rsi()
          .accessor(d => d.close).period(period);
        const result = rsi0(data);
        let forward = rsi0.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(toFixed(result[i], 3)).to.equal(data[i].rsi);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
