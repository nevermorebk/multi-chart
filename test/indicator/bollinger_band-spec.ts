import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { bollinger_band } from '../../src/indicator';

describe('bollinger band', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/bollinger_band.csv'))
      .then(data => {
        let period = 20;
        const bollingerBand = bollinger_band()
          .variance(2)
          .accessor(d => d.close)
          .period(period);
        const result = bollingerBand(data);
        let forward = bollingerBand.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(period - 1);
        for (let i = forward; i < result.length; i++) {
          expect(data[i].upper - toFixed(result[i][0], 3)).to.lte(0.00101);
          expect(data[i].ma - toFixed(result[i][1], 3)).to.lte(0.00101);
          expect(data[i].lower - toFixed(result[i][2], 3)).to.lte(0.00101);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
