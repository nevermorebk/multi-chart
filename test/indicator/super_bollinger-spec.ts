import * as path from 'path';
import { expect } from 'chai';
import { readCsv } from './lib/readcsv';
import { toFixed } from './lib/fn';

import { super_bollinger, config } from '../../src/indicator';

describe('super bollinger', () => {
  it('should has expected result', done => {
    readCsv(path.resolve(__dirname, './data/super_bollinger.csv'))
      .then(data => {
        let period = [21, 21];
        const superBollinger = super_bollinger()
          .accessor(d => d.close).variance([1, 2, 3]).period(period);
        const result = superBollinger(data);
        let forward = superBollinger.forward();
        expect(result.length).to.equal(data.length);
        expect(forward).to.equal(Math.max.apply(null, period) - 1);
        for (let i = 0; i < result.length; i++) {
          expect(data[i].upper3 - result[i][0]).to.lte(0.001);
          expect(data[i].upper2 - result[i][1]).to.lte(0.001);
          expect(data[i].upper1 - result[i][2]).to.lte(0.001);
          expect(data[i].ma - result[i][3]).to.lte(0.001);
          expect(data[i].lower1 - result[i][4]).to.lte(0.001);
          expect(data[i].lower2 - result[i][5]).to.lte(0.001);
          expect(data[i].lower3 - result[i][6]).to.lte(0.001);
          expect(data[i].late - result[i][7]).to.lte(0.001);
        }
      })
      .then(done)
      .catch(e => console.error(e));
  });
});
