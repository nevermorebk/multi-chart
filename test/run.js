require('ts-node').register({
  compilerOptions: {
    module: 'commonjs',
    target: 'es2015',
    inlineSourceMap: true,
    sourceMap: true,
    allowJs: true,
  },
  skipIgnore: true,
  files: ['./tsconfig.json'],
});

require('jsdom-global/register');
