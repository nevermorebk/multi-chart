# Change Log

## [2.5.1] - 2018-04-26

### Added

- Support modify params and styles for drawline and indicator (without depends host app)
- Limited pan and zoom (more)
- Generate single d.ts

### Changed

- Refactor eagel to magnifier and some fixes
- Refactor some components (tooltip / loading)  using react
- Refactor cache (memoize)
- Optimize angle calc using mathematics vector
- Optimize redraw when new data appended
- Use `ready` api  instead ready event
- Update tick & label for y axis (floor or round)
- Update cursor style in axis

### BugFix

- Indicator line not redraw when loadmore
- Label text (order line , position summary line ) overflow
- Fix adx / dmi config , ikh / super_bollinger / ema tooltip
- Fix loadmore triggered more than once
- Fix fibo-timezone line draw incorrectly
- Fix trigger drawline saved event when mouse down (add `dragcancel` event)
- Fix y scale domain incorrect when ohlc is same
- Fix chart not resize in safari when drp changed



## [2.3.0] - 2017-11-21

### Added

- Support zoom event : `zoomChanged`
- Support interaction and drawline in touch device
- Support toggle visible for indicator line
- Support toggle maximize chart
- Support EagleView for drawing / dragging line in touch device

### Changed

- Refactor position & parallel line
- remove updateDisplayOption / updateSize api, use update instead
- refactor draw / drag line  for mobile
- Show Tooltip in future time for IKH(indicator)
- Update canvas usage for optimize performance

### BugFix

- Fix calculation error in position line
- Fix some incompatible problems  in IE (TouchEvent  / isFinite / context.fillText ...)
- Fix order line & position line not update when symbol changed
- Fix axia label not update when symbol changed
- Fix ohlc calc error in avg style
- Fix appendData bug
- Fix zoom bug when mouse wheeling
- Fix load line when data is  outside viewport
- Fix part indicator line not sync with data (ikh / super_model / ..)


## [2.2.0] - 2017-10-18

### Added

- Add displayOption in template config
- Support show same indicator with different parmaters
- Support `initZoomLevel` config and `zoomChanged` event

### Changed

- Refactor chart capacity monitor
- Update some theme config
- Hide indicator labels when resizing chart

### BugFix

- Fix can't resize mainchart
- Fix merge array object bug
- Fix crosshair line not move when drawline
- Fix ParallelLine draw bug when dragging
- Fix incorrect xAxis tick format

## [2.1.1] - 2017-10-05

### Added

- optimize font rendering

### BugFix

- Fix crosshair line not move when drag drawedLines

## [2.1.0] - 2017-10-03

### Added

- Add idle interval timer for slice data outside viewport
- Add sort chart , toggle maximize chart
- Add setting & remove indicator chart
- load / export template config for restore / store chart state
### Changed

- Check loadmore more intelligently
- Update default dpr for performance
- Render Crosshair & IndicatorLabel in Dom
- Clear Chart when loading data
- Support deep merge chart config on initialize

### BugFix

- Fix IE not support functions(Number.isFinite)

## [2.0.0] - 2017-09-12

### Added

- Add i18n support
- Add theme support
- Add pause state and do not redraw when page is hidden
- Add showlast and whether or not redraw
- Add Loadingbar and loading state control
- Support chart top / left padding
- Support series left / right margin
- Support loadmore after drag or wheel
- Add data capacity (default value is 3000)
- Support import / export draw line objects
- Support show order & position line


### Changed

- Only calculate indicators in viewport data , not all data
- Support load & draw identical indicator many times with separate config
- clear d3-selection
- automatic reset chart when idle

### Bugfix

- Fix draw crosshair when dragging
- Fix ghost texture in canvas
- fix destroy error because of async redraw
- fix compatibility questions in IE
- fix some draw line bugs

## [1.4.0] - 2017-08-21

### Added
- Add Tool / Command interface
- Support draw and update fx analytical lines!
- Add Tooltip to show mouse information

### Changed
- refactor Feature interface and implements

### Bugfix
- fix resize bug
- fix yaxis not draw when fliped


## 【1.3.0】 - 2017-07-27

### Added
- Support remaining indicator calc

### Changed
- Optimize resize chart
- Refactor draw api
- Update babel config use `babel-preset-env`
- Support event in context


### Bugfix
- fix zoom
- fix mouse when out main area

## [1.2.0] - 2017-07-06

### Added
- Refact Event, support gesture (pinch, dbltouch, drag) and wheel without d3.zoom
- Support indicator calc: `ma` , `sma` , `wma` , `macd` , `ikh`
- Support display crosshair label in axis
- Support resize chart
- Support sortable series

### Bugfix
- error in computing mouse when no data
- fix mergeToStandard

## [1.1.0] - 2017-06-20

- Update renderer to Canvas
- Fix crosshair render bug when mouse is outside area
- Refact Area and Chart

## [1.0.0] - 2017-06-07
### Added
- Init project config about `webpack`, `typescript`, `d3`.
- Support draw chart with `d3.svg` Api.
- Support three chart styles: `Line`, `Candle`, `Bar`.
- Support zoom settings and change zoom methods.
- Support resize chart.
- Support update chart props: `chartType`, `data`, `indicatorProps`.
- Support indicator calc : `ema` ,  `macd`.
- Support draw indicator chart.
- Unit test for indicators.
