import { getParallelPoints, getPointInLine } from '../../util/geometry';
import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType, ICoord } from '../../lib';
import BaseLine  from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { forEach } from '../../util/array';
import { getDataItemByX, getCenterPoint, isFinite } from '../../util/index';

const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;


// tslint:disable:jsdoc-format
/**
p1------------------+-------------------p2



q1------------------p3------------------q2
*/
export default class ParallelLine extends BaseLine {
  public ctrlPointSize = 3;
  protected distance = 0;

  getName(): ToolLineType {
    return ToolLineType.PARALLEL_LINE;
  }

  setCtrlPoint(ctrlPoint: ICtrlPoint, index: number): void {
    super.setCtrlPoint(ctrlPoint, index);
    let [cp1, cp2, cp3] = this.ctrlPoints;
    if (index === 2) {
      if (cp3 && this.drawingIndex === 2 && (cp3.isEditing || this.isRestoring)) {
        this.distance = this._calcDistance();
      }
    }
    if (cp3) this._resetCtrlPoint3();
  }

  getDrawEndPoint(): Point[][] {
    let [p1, p2, p3] = this.getPixelCtrlPoints();
    let scaleY = this.context.getScaleY();
    let distancePoint = scaleY(this.distance) - scaleY(0);
    let [q1, q2] = getParallelPoints(p1, p2, distancePoint);
    let endPoints = [[p1, p2], [q1, p3, q2]];
    return endPoints;
  }

  _getCenterX(): number {
    let [cp1, cp2] = this.ctrlPoints;
    let index = (cp1.coord.index + cp2.coord.index) / 2;
    return index;
  }

  _resetCtrlPoint3() {
    let [cp1, cp2, cp3] = this.ctrlPoints;
    let x3 = this._getCenterX();
    let index3 = Math.floor(x3);
    let centerPrice = (cp1.coord.value + cp2.coord.value) / 2;
    let distance = this.distance;
    let cp3Price = centerPrice + distance;
    let { data, date } = getDataItemByX(this.context.allData, index3);
    let newCoord = {
      index: index3,
      displayIndex: x3,
      value: cp3Price,
      data: data,
      date: date,
    } as ICoord;
    cp3.move(newCoord);
  }

  _calcDistance() {
    let distance = 0;
    let [, , cp3] = this.ctrlPoints;
    let [p1, p2] = this.getPixelCtrlPoints();

    let index = cp3.coord.index;
    let x = this.context.getScaleX()(index);
    let p = getPointInLine([p1, p2], x);
    let price = this.context.getScaleY().invert(p.y);
    if (cp3.coord.value && price) distance = cp3.coord.value - price;
    return distance;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let line1Points = [points[0][0], points[0][1]];
    let line2Points = [points[1][0], points[1][2]];
    return (
      (this.isInBoundingBox(xy, line1Points) && this.isNearLine(xy, line1Points)) ||
      (this.isInBoundingBox(xy, line2Points) && this.isNearLine(xy, line2Points))
    );
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let endPoints = this.getDrawEndPoint();
    let [p1, p2] = endPoints[0];
    let [q1, p3, q2] = endPoints[1];
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let areaMetaInfo = stylesObj.area;
    let lineStyle = draw.getCanvasLineStyle(lineMetaInfo, this.state);
    let areaStyle = { fillStyle: areaMetaInfo };
    draw.drawLine(ctx, [p1, p2], lineStyle);
    draw.drawLine(ctx, [q1, q2], lineStyle);
    draw.drawArea(ctx, [p1, p2, q2, q1], areaStyle);
    let points = [p1, p2, p3];
    forEach(this.ctrlPoints, (point, i) => point.draw(ctx, points[i]));
    return true;
  }
}
