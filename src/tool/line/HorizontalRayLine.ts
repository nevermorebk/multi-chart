import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { distanceToLine } from '../../util';

export default class HorizontalRayLine extends BaseLine {
  public ctrlPointSize = 1;

  getName(): ToolLineType {
    return ToolLineType.HORIZONTAL_RAY_LINE;
  }

  _getIntersectionPoint(p1: Point): Point {
    let box = this.getTargetBox();
    let width = box[2];
    return {
      x: width,
      y: p1.y,
    };
  }

  getDrawEndPoint(): Point[] {
    let points = this.getPixelCtrlPoints();
    let extentionPoint = this._getIntersectionPoint(points[0]);
    if (extentionPoint) {
      points.push(extentionPoint);
    }
    return points;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let inBox = this.isInBoundingBox(xy, points);
    return inBox;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getDrawEndPoint();
    let { stylesObj } = this.metaCfg;
    let metaInfo = stylesObj.line;
    let lineStyle = draw.getCanvasLineStyle(metaInfo, this.state);
    draw.drawLine(ctx, points, lineStyle);
    this.ctrlPoints[0].draw(ctx, points[0]); // on ctrl point
    return true;
  }
}
