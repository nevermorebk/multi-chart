import TrendLine from './TrendLine';
import RayLine from './RayLine';
import HorizontalLine from './HorizontalLine';
import HorizontalRayLine from './HorizontalRayLine';
import ParallelLine from './ParallelLine';
import VerticalLine from './VerticalLine';
import FibonacciLine from './FibonacciLine';
import GannFanLine from './GannFanLine';
import LongPositionLine from './LongPositionLine';
import ShortPositionLine from './ShortPositionLine';
import FibonacciTrendBasedLine from './FibonacciTrendBasedLine';
import FibonacciTimeZoneLine from './FibonacciTimeZoneLine';
import FibonacciTimeZoneTrendBasedLine from './FibonacciTimeZoneTrendBasedLine';
import DateRangeLine from './DateRangeLine';
import PriceRangeLine from './PriceRangeLine';
import { ToolLineType, IToolLine, IChartContext } from '../../lib';
import CtrlPoint from '../CtrlPoint';

export {
  TrendLine,
  RayLine,
  HorizontalLine,
  HorizontalRayLine,
  ParallelLine,
  VerticalLine,
  FibonacciLine,
  GannFanLine,
  LongPositionLine,
  ShortPositionLine,
  FibonacciTrendBasedLine,
  FibonacciTimeZoneLine,
  FibonacciTimeZoneTrendBasedLine,
  DateRangeLine,
  PriceRangeLine,
};

export function createToolline(context: IChartContext, lineType: ToolLineType): IToolLine {
  let line;
  switch (lineType) {
    case ToolLineType.TREND_LINE: {
      line = new TrendLine(context);
      break;
    }
    case ToolLineType.RAY_LINE: {
      line = new RayLine(context);
      break;
    }
    case ToolLineType.HORIZONTAL_LINE: {
      line = new HorizontalLine(context);
      break;
    }
    case ToolLineType.HORIZONTAL_RAY_LINE: {
      line = new HorizontalRayLine(context);
      break;
    }
    case ToolLineType.VERTICAL_LINE: {
      line = new VerticalLine(context);
      break;
    }
    case ToolLineType.PARALLEL_LINE: {
      line = new ParallelLine(context);
      break;
    }
    case ToolLineType.FIBONACCI_LINE: {
      line = new FibonacciLine(context);
      break;
    }
    case ToolLineType.GANN_FAN_LINE: {
      line = new GannFanLine(context);
      break;
    }
    case ToolLineType.LONG_POSITION_LINE: {
      line = new LongPositionLine(context);
      break;
    }
    case ToolLineType.SHORT_POSITION_LINE: {
      line = new ShortPositionLine(context);
      break;
    }
    case ToolLineType.FIBONACCI_TREND_BASED_LINE: {
      line = new FibonacciTrendBasedLine(context);
      break;
    }
    case ToolLineType.FIBONACCI_TIMEZONE_LINE: {
      line = new FibonacciTimeZoneLine(context);
      break;
    }
    case ToolLineType.FIBONACCI_TIMEZONE_TREND_BASED_LINE: {
      line = new FibonacciTimeZoneTrendBasedLine(context);
      break;
    }
    case ToolLineType.DATE_RANGE_LINE: {
      line = new DateRangeLine(context);
      break;
    }
    case ToolLineType.PRICE_RANGE_LINE: {
      line = new PriceRangeLine(context);
      break;
    }
    default: {
      throw new TypeError(`line type: ${lineType} is not supported!`);
    }
  }
  return line;
}
