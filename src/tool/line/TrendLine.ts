import { BoundingBox, Point, ICoord, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { forEach } from '../../util/array';

export default class TrendLine extends BaseLine {
  public ctrlPointSize = 2;

  getName(): ToolLineType {
    return ToolLineType.TREND_LINE;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getPixelCtrlPoints();
    let inBox = this.isInBoundingBox(xy, points);
    if (inBox) {
      return this.isNearLine(xy, points);
    }
    return false;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getPixelCtrlPoints();
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let showArrow = stylesObj.showArrow;
    let lineStyle = draw.getCanvasLineStyle(lineMetaInfo, this.state);
    if (showArrow) draw.drawArrowLine(ctx, points, lineStyle);
    else draw.drawLine(ctx, points, lineStyle);
    forEach(this.ctrlPoints, point => point.draw(ctx));
    return true;
  }
}
