import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { distanceToLine } from '../../util';

// TODO (2018/03/02 15:31) show price line !!
export default class HorizontalLine extends BaseLine {
  public ctrlPointSize = 1;

  getName(): ToolLineType {
    return ToolLineType.HORIZONTAL_LINE;
  }

  _getIntersectionPoints(p1: Point): Point[] {
    let box = this.getTargetBox();
    let width = box[2];
    return [
      {
        x: 0,
        y: p1.y,
      },
      {
        x: width,
        y: p1.y,
      },
    ];
  }

  getDrawEndPoint(): Point[] {
    let points = this.getPixelCtrlPoints();
    let extentionPoints = this._getIntersectionPoints(points[0]);
    if (extentionPoints) {
      extentionPoints.splice(1, 0, ...points);
    }
    return extentionPoints;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let inBox = this.isInBoundingBox(xy, points);
    return inBox;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getDrawEndPoint();
    let { stylesObj } = this.metaCfg;
    let metaInfo = stylesObj.line;
    let lineStyle = draw.getCanvasLineStyle(metaInfo, this.state);
    draw.drawLine(ctx, points, lineStyle);
    this.ctrlPoints[0].draw(ctx, points[1]); // middle ctrl point
    return true;
  }
}
