import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { distanceToLine } from '../../util';

export default class VerticalLine extends BaseLine {
  public ctrlPointSize = 1;

  getName(): ToolLineType {
    return ToolLineType.VERTICAL_LINE;
  }

  _getIntersectionPoints(p1: Point): Point[] {
    let box = this.getTargetBox();
    let height = box[3];
    return [
      {
        x: p1.x,
        y: 0,
      },
      {
        x: p1.x,
        y: height,
      },
    ];
  }

  getDrawEndPoint(): Point[] {
    let points = this.getPixelCtrlPoints();
    let extentionPoints = this._getIntersectionPoints(points[0]);
    if (extentionPoints) {
      extentionPoints.splice(1, 0, ...points);
    }
    return extentionPoints;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let inBox = this.isInBoundingBox(xy, points);
    return inBox;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getDrawEndPoint();
    let { stylesObj } = this.metaCfg;
    let metaInfo = stylesObj.line;
    let lineStyle = draw.getCanvasLineStyle(metaInfo, this.state);
    draw.drawLine(ctx, points, lineStyle);
    this.ctrlPoints[0].draw(ctx, points[1]); // middle ctrl point
    return true;
  }
}
