import { numberFormat, forEach, rgba } from '../../util';
import { Point, SelectionState, ToolLineType, LevelColorMetaInfo } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';

const FONT_STYLE = {
    textAlign: 'right',
    textBaseline: 'bottom',
};

export default class FibonacciLine extends BaseLine {
  public ctrlPointSize = 2;

  getName(): ToolLineType {
    return ToolLineType.FIBONACCI_LINE;
  }

  getDrawEndPoint(): Point[][] {
    let [pStart, pEnd] = this.getPixelCtrlPoints();
    let { stylesObj: {fibos, reverse, extendLine} } = this.metaCfg;
    if (reverse === true) {
      [pStart, pEnd] = [pEnd, pStart];
    }
    let distanceY = pStart.y - pEnd.y;
    let levels = fibos.levels;
    let points = levels.filter(l => !l.disable).map(level => {
      let y = pEnd.y + distanceY * level.val;
      let start = {
        x: pStart.x,
        y,
      };
      let end = {
        x: pEnd.x,
        y,
      };
      return [start, end];
    });
    if (extendLine === true) {
      let box = this.getTargetBox();
      let maxX = box[2];
      let minX = 0;
      points = points.map(([start, end]) => {
        return [{
          x: Math.max(minX, Math.min(start.x, end.x)),
          y: start.y
        }, {
          x: Math.max(start.x, end.x),
          y: start.y
        }, {
          x: Math.max(maxX, start.x, end.x),
          y: start.y
        }];
      });
    }
    return points;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let inBox = !!points.find(linePoints => this.isInBoundingBox(xy, linePoints));
    if (!inBox) {
      let points = this.getPixelCtrlPoints();
      return this.isInBoundingBox(xy, points) && this.isNearLine(xy, points);
    }
    return inBox;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let ctrlPoints = this.ctrlPoints;
    let points = this.getPixelCtrlPoints();
    let endPoints = this.getDrawEndPoint();
    let scaleY = this.context.getScaleY();
    let format = this.tickFormat();
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let trendLineMetaInfo = stylesObj.trendLine;
    let isDisplayPrice = stylesObj.displayPrice;
    let fibosMetaInfo: LevelColorMetaInfo = stylesObj.fibos;
    let trendLineStyle = draw.getCanvasLineStyle(trendLineMetaInfo, this.state);

    if (stylesObj.reverse === true) {
      points.reverse();
    }
    if (this.drawingIndex >= 1) {
      let alpha = stylesObj.background;
      let levels = fibosMetaInfo.levels;
      levels = levels.filter(l => !l.disable);
      forEach(endPoints, (line, i) => {
        line.sort((p1, p2) => p1.x - p2.x);
        let start = line[0];
        let price = format(scaleY.invert(start.y));
        let lineStyle = draw.getCanvasLineStyle(Object.assign({}, lineMetaInfo, { color: levels[i].color }), this.state);
        draw.drawLine(ctx, [line[0], line[1]], lineStyle);
        if (stylesObj.extendLine === true) {
          draw.drawLine(ctx, [line[1], line[2]], lineStyle);
        }
        if (i > 0) {
          let points = [endPoints[i - 1][0], line[line.length - 1]];
          let backgroundColor = rgba(levels[i].color, alpha);
          let areaStyle = { fillStyle: backgroundColor };
          draw.drawRect(ctx, points, areaStyle);
        }
      });

      if (stylesObj.label !== false) {
        forEach(endPoints, (line, i) => {
          line.sort((p1, p2) => p1.x - p2.x);
          let start = line[0];
          let end = line[line.length - 1];
          let price = format(scaleY.invert(start.y));
          let levelLabel = levels[i].label || levels[i].val;
          let label = isDisplayPrice ? `${levelLabel} (${price})` : `${levelLabel}`;
          let fontStyle = {
            fillStyle: levels[i].color,
            ...FONT_STYLE
          };
          let textPoint = {
            x: start.x > 0 ? start.x - 6 : end.x - 6,
            y: start.x > 0 ? start.y + 6 : start.y - 6
          };
          draw.drawText(ctx, label,textPoint.x, textPoint.y, fontStyle);
        });
      }
    }

    draw.drawLine(ctx, points, trendLineStyle);

    forEach(ctrlPoints, (point, i) => point.draw(ctx, points[i]));
    return true;
  }

  tickFormat() {
    let { precisionScale } = this.context;
    let format = numberFormat(precisionScale, false);
    return d => format(d);
  }
}
