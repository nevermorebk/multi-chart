import BasePositionLine, { Position } from './BasePositionLine';
import { ToolLineType } from '../../lib';

export default class LongPositionLine extends BasePositionLine {
  position = Position.LONG;

  getName(): ToolLineType {
    return ToolLineType.LONG_POSITION_LINE;
  }
}
