import { forEach, rgba } from '../../util';
import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType, LevelColorMetaInfo } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';


const FONT_STYLE = {
  textAlign: 'left',
  textBaseline: 'bottom',
};

export default class FibonacciTimeZoneTrendBasedLine extends BaseLine {
  public ctrlPointSize = 3;

  getName(): ToolLineType {
    return ToolLineType.FIBONACCI_TIMEZONE_TREND_BASED_LINE;
  }

  getDrawEndPoint(): Point[][] {
    let [cpStartIndex,  cpPostIndex, cpEndIndex] = this.ctrlPoints.map(cp => cp.coord.index);
    let box = this.getTargetBox();
    let scaleX = this.context.getScaleX();
    let levels = this.metaCfg.stylesObj.fibos.levels;
    let endPoints = levels.filter(l => !l.disable).map(level => {
      let index = Math.floor(cpEndIndex + (cpPostIndex - cpStartIndex) * level.val);
      let x = scaleX(index);
      return [{
        x, y: 0
      }, {
        x, y: box[3]
      }];
    });
    return endPoints;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let inBox = !!points.find(linePoints => this.isInBoundingBox(xy, linePoints));
    if (!inBox) {
      let points = this.getPixelCtrlPoints();
      let line1Points = [points[0], points[1]];
      let line2Points = [points[1], points[2]];
      return (
        (this.isInBoundingBox(xy, line1Points) && this.isNearLine(xy, line1Points)) ||
        (this.isInBoundingBox(xy, line2Points) && this.isNearLine(xy, line2Points))
      );
    }
    return inBox;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getPixelCtrlPoints();
    let endPoints = this.getDrawEndPoint();
    let theme = this.getChartTheme();
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let trendLineMetaInfo = stylesObj.trendLine;
    let isDisplayPrice = stylesObj.displayPrice;
    let fibosMetaInfo: LevelColorMetaInfo = stylesObj.fibos;
    let trendLineStyle = draw.getCanvasLineStyle(trendLineMetaInfo, this.state);

    if (this.drawingIndex > 1) {
      let alpha = stylesObj.background;
      let levels = fibosMetaInfo.levels;
      levels = levels.filter(l => !l.disable);
      forEach(endPoints, (line, i) => {
        let bottomPoint = line[1];
        let lineStyle = draw.getCanvasLineStyle(Object.assign({}, lineMetaInfo, { color: levels[i].color }), this.state);
        draw.drawLine(ctx, line, lineStyle);
        if (i > 0) {
          let points = [endPoints[i - 1][0], line[1]];
          let backgroundColor = rgba(levels[i].color, alpha);
          let areaStyle = { fillStyle: backgroundColor };
          draw.drawRect(ctx, points, areaStyle);
        }
      });
      if (stylesObj.label !== false) {
        forEach(endPoints, (line, i) => {
          line.sort((p1, p2) => p1.x - p2.x);
          let start = line[0];
          let bottomPoint = line[1];
          let label = `${levels[i].val}`;
          let fontStyle = {
            fillStyle: levels[i].color,
            ...FONT_STYLE
          };
          draw.drawText(ctx, label, bottomPoint.x + 5, bottomPoint.y - 5, fontStyle);
        });
      }

    }

    draw.drawArrowLine(ctx, points, trendLineStyle);

    forEach(this.ctrlPoints, (point, i) => point.draw(ctx, points[i]));
    return true;
  }
}
