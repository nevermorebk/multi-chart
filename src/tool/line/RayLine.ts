import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { intersectionOnBoundingBox, distanceToLine, forEach } from '../../util';

export default class RayLine extends BaseLine {
  public ctrlPointSize = 2;

  getName(): ToolLineType {
    return ToolLineType.RAY_LINE;
  }

  _getIntersectionPoint(pixPoints: Point[]): Point {
    let [p1, p2] = pixPoints;
    let box = this.getTargetBox();
    let interPoints = intersectionOnBoundingBox(p1, p2, box, true);
    return interPoints[0];
  }

  getDrawEndPoint(): Point[] {
    let points = this.getPixelCtrlPoints();
    let extentionPoint = this._getIntersectionPoint(points);
    if (extentionPoint) {
      points.push(extentionPoint);
    }
    return points;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let inBox = this.isInBoundingBox(xy, points);
    if (inBox) {
      return this.isNearLine(xy, points);
    }
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getDrawEndPoint();
    let { stylesObj } = this.metaCfg;
    let metaInfo = stylesObj.line;
    let lineStyle = draw.getCanvasLineStyle(metaInfo, this.state);
    draw.drawLine(ctx, points, lineStyle);
    forEach(this.ctrlPoints, (point, i) => point.draw(ctx, points[i]));
    return true;
  }
}
