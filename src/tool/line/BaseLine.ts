import { ICoord, BoundingBox, Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType, ITheme } from '../../lib';
import EventAdapter from '../../event/EventAdapter';
import CtrlPoint from '../CtrlPoint';
import { contains2, distanceToLine, getBoundingBox2 } from '../../util/geometry';
import { forEach } from '../../util/array';
import { getTheme } from '../../config/index';
import { first, last, uuidGenerator, memoizeOnce } from '../../util/index';
import { IMetaInfoCfg, MetaInfo } from '../../config/meta';
import { Vector } from '../../util/math';
import Chart from '../../chart/Chart';

const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;

const lineIdGenerator = uuidGenerator();
export const STYLES = {
  [SHOW]: {
    strokeStyle: '#6b7b87',
    lineWidth: 1,
    // lineDash: [4, 4]
  },
  [HIGHLIGHT]: {
    strokeStyle: 'rgba(255,0,0,0.6)',
    lineWidth: 1,
    // lineDash: [4, 4]
  },
  [SELECTED]: {
    strokeStyle: 'red',
    lineWidth: 1,
    // lineDash: [4, 4]
  },
};

export const BUFFER = 8;

export default abstract class BaseLine extends EventAdapter implements IToolLine {
  protected _savedPoints: any[];
  protected metaCfg: Readonly<IMetaInfoCfg>;
  id: number;
  owner: Chart;
  context: IChartContext;
  state: SelectionState;
  ctrlPoints: ICtrlPoint[];
  ctrlPointSize = 2;
  isEditing: boolean;
  isRestoring: boolean;
  drawingIndex: number;

  anthor: Vector; // touch point when dragging (vector from box 's left top )

  constructor(context: IChartContext, ctrlPoints?: ICtrlPoint[]) {
    super();
    this.id = Date.now(); // make sure identify util removed
    this.owner = context.getMainChart();
    this.context = context;
    this.ctrlPoints = ctrlPoints || [];
    this.state = SelectionState.HIDE;
    if (this.ctrlPointSize === undefined) {
      throw new Error('ctrlPointSize need a default value!');
    }
    if (!this._checkCtrlPoints()) {
      console.warn('ctrlPoints is invalid!');
    }

    this.getPixelCtrlPoints = memoizeOnce(this.getPixelCtrlPoints.bind(this), () => context.timestamp + '');

    // TODO (2018/03/07 16:04) if line style has to use last settings , get last settings from cache?
    this.setMetaCfg();
  }


  setMetaCfg(cfg?: Readonly<IMetaInfoCfg>): void {
    let meta = MetaInfo.drawed(this.getName());
    this.metaCfg = meta && meta.merge(cfg);
  }

  getMetaCfg(): Readonly<IMetaInfoCfg> {
    return this.metaCfg;
  }

  abstract getName(): ToolLineType;

  getTargetBox(): BoundingBox {
    let { chartWidth, chartHeight } = this.context;
    return [0, 0, chartWidth, chartHeight];
  }

  draw(context2d: Context2D): boolean {
    if (this.state === HIDE) return false;
    if (this.ctrlPoints.length === 0) return false;
    return true;
  }

  setCtrlPoint(ctrlPoint: ICtrlPoint, index: number): void {
    this.ctrlPoints[index] = ctrlPoint;
  }

  hasCtrlPoint(ctrlPoint: ICtrlPoint): boolean {
    return this.ctrlPoints.indexOf(ctrlPoint) > -1;
  }

  _checkCtrlPoints(): boolean {
    return this.ctrlPoints.length <= this.ctrlPointSize;
  }

  setState(state: SelectionState): void {
    this.state = state;
  }
  getState(): SelectionState {
    return this.state;
  }

  startEdit() {
    this.isEditing = true;
  }

  stopEdit() {
    this.isEditing = false;
    this.ctrlPoints.forEach(cp => (cp.isEditing = false));
  }

  move(coords: ICoord[]) {
    if (coords.length === this.ctrlPoints.length) {
      forEach(this.ctrlPoints, (p, i) => {
        p.move(coords[i]);
        this.setCtrlPoint(p, i);
      });
    }
  }

  moveCtrlPoint(coord: ICoord, index: number) {
    let cp = this.ctrlPoints[index];
    if (cp) {
      cp.move(coord);
      // if cp in line, need invoke setCtrlPoint
      this.setCtrlPoint(cp, index);
    }
  }

  getBoundingBox(): BoundingBox {
    let points = this.getPixelCtrlPoints();
    return getBoundingBox2(points);
  }

  getPixelCtrlPoints(): Point[] {
    return this.ctrlPoints.map(p => p.getPoint());
  }


  getBufferBoundingBox(points: Point[], buf: number = BUFFER): BoundingBox {
    return getBoundingBox2(points, buf);
  }

  isInBoundingBox(xy: Point, points: Point[]): boolean {
    let box = this.getBufferBoundingBox(points);
    return contains2(xy, box);
  }

  isNearLine(xy: Point, line: Point[]): boolean {
    return distanceToLine(line[0], line[1], xy) < BUFFER;
  }

  abstract isHover(xy: Point): boolean;

  highlight(): void {
    this.setState(HIGHLIGHT);
    forEach(this.ctrlPoints, ctrl => ctrl.highlight());
  }

  select(): void {
    this.setState(SELECTED);
    forEach(this.ctrlPoints, ctrl => ctrl.select());
  }

  normal(): void {
    this.setState(SHOW);
    forEach(this.ctrlPoints, ctrl => ctrl.normal());
  }

  getChartTheme(): ITheme {
    return getTheme(this.context.theme);
  }

  toJSON(): object {
    let name = this.getName();
    let meta = this.metaCfg;
    let ctrlPointsJSON;
    if (this.ctrlPoints.length) {
      ctrlPointsJSON = this.ctrlPoints.map(p => p.toJSON());
    } else if (this._savedPoints) {
      ctrlPointsJSON = this._savedPoints;
    } else {
      console.warn('line to json error');
    }
    return {
      n: name,
      p: ctrlPointsJSON,
      m: meta
    };
  }

  fromJSON(points: any, meta?: Readonly<IMetaInfoCfg>) {
    let ctrlPoints = CtrlPoint.create(this.context, points, this);
    if (ctrlPoints.length === 0) {
      this._savedPoints = points;
      this.setMetaCfg(meta);
      this.setState(HIDE);
    } else {
      this.id = lineIdGenerator();
      this._restoreLine(ctrlPoints);
      this.setMetaCfg(meta);
      this.setState(SHOW);
    }
  }

  // TODO (2018/05/09 16:51) rename onDataLoaded
  refresh() {
    let points = this._savedPoints;
    if (points && this.state === HIDE && this.ctrlPoints.length === 0) {
      let ctrlPoints = CtrlPoint.create(this.context, points, this);
      if (ctrlPoints.length) {
        this._restoreLine(ctrlPoints);
        this.setState(SHOW);
        this._savedPoints = null;
      }
    }
    if (!points && this.state !== HIDE && this.ctrlPoints.length) {
      let { allData, xAccessor } = this.context;
      let [firstX, lastX] = [xAccessor(first(allData)), xAccessor(last(allData))];
      let hasCtrlPoint = !!this.ctrlPoints.find(cp => cp.coord.index < firstX || cp.coord.index > lastX);
      if (hasCtrlPoint) {
        let json = this.toJSON() as any;
        this._savedPoints = json.p;
        this.setState(HIDE);
      }
    }
  }

  _restoreLine(ctrlPoints: ICtrlPoint[]) {
    this.isRestoring = true;
    this.drawingIndex = this.ctrlPointSize - 1;
    forEach(ctrlPoints, (cp, i) => {
      this.setCtrlPoint(cp, i);
    });
    this.isRestoring = false;
  }
}
