import { calcPositionEntryICoord, calcPositionEndICoord, forEach, sliceDataByX, rgba } from '../../util';
import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { roundDown, roundUp } from '../../util/math';
import { getI18n } from '../../config/index';

const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;

// for rect style
const TP_TEXT_BG_COLOR = 'rgb(0, 128, 0)';
const SL_TEXT_BG_COLOR = 'rgb(255, 0, 0)';


export enum Position {
  LONG,
  SHORT,
}

// tslint:disable:jsdoc-format
/**
p3(q3)--------------(q5)
|                   |
|                   |
|                   |
p1(q1)--------------p2(q2)
|                   |
|                   |
|                   |
|                   |
p4(q4)--------------(q6)

ref:  https://blog.tradingview.com/en/estimate-profits-new-long-short-position-drawing-tool-4239/
 */
export default abstract class BasePositionLine extends BaseLine {
  public ctrlPointSize = 2;
  private _delta: number[];
  protected position: Position;
  constructor(context: IChartContext, ctrlPoints?: ICtrlPoint[]) {
    super(context, ctrlPoints);
    this._delta = [];
  }
  _checkCtrlPoints() {
    return true;
  }

  setCtrlPoint(ctrlPoint: ICtrlPoint, index: number): void {
    super.setCtrlPoint(ctrlPoint, index);
    let [cp1, cp2, cp3, cp4] = this.ctrlPoints;
    if (index === 0 && this.isEditing && this._delta.length === 0 && !this.isRestoring) {
      this._initLimitedCtrlPoints();
    }

    if (cp1 && cp2) this._resetCtrlPoint2();
    if (cp1 && cp3) this._resetCtrlPoint3();
    if (cp1 && cp4) this._resetCtrlPoint4();
  }

  _initLimitedCtrlPoints() {
    let coord1 = this.ctrlPoints[0].coord;
    // set p3 p4
    let domainy = this.context.getScaleY().domain();
    let delta = Math.abs(domainy[0] - domainy[1]) / 10;
    let coord3 = {
      index: coord1.index,
      displayIndex: coord1.index,
      date: coord1.date,
      data: coord1.data,
      value: coord1.value + delta,
    };
    let coord4 = {
      index: coord1.index,
      displayIndex: coord1.index,
      date: coord1.date,
      data: coord1.data,
      value: coord1.value - delta,
    };
    let p3 = new CtrlPoint(this.context, this, coord3);
    let p4 = new CtrlPoint(this.context, this, coord4);
    this.ctrlPoints[2] = p3;
    this.ctrlPoints[3] = p4;
    this._delta = [delta, -delta];
  }

  getDrawEndPoint(): Point[] {
    let points = this.getPixelCtrlPoints();
    let [p1, p2, p3, p4] = points;
    let endPoints;
    // reset q
    let q1 = p1;
    let q2 = { x: p2.x, y: p1.y };
    let q3 = { x: p1.x, y: p3.y };
    let q4 = { x: p1.x, y: p4.y };
    let q5 = { x: p2.x, y: p3.y };
    let q6 = { x: p2.x, y: p4.y };
    endPoints = [q1, q2, q3, q4, q5, q6];

    return endPoints;
  }

  _resetCtrlPoint2() {
    let [cp1, cp2] = this.ctrlPoints;
    let [p1, p2] = [cp1, cp2].map(cp => CtrlPoint.calcPixelPoint(this.context, cp.coord));
    let coord = CtrlPoint.calcClosestCoord(this.context, { x: p2.x, y: p1.y });
    cp2.move(coord);
  }

  _resetCtrlPoint3() {
    let [cp1, , cp3] = this.ctrlPoints;
    let [p1, p3] = [cp1, cp3].map(cp => CtrlPoint.calcPixelPoint(this.context, cp.coord));
    if (p3.y >= p1.y) {
      if (__DEV__) console.log('positionline: p3 limited drag!');
      p3.y = p1.y - 10;
    }
    let coord = CtrlPoint.calcClosestCoord(this.context, p3);
    let value = coord.value;
    if (cp1.isEditing) {
      value = cp1.coord.value + this._delta[0];
    }
    let coord3 = {
      index: cp1.coord.index,
      displayIndex: cp1.coord.index,
      date: cp1.coord.date,
      data: cp1.coord.data,
      value: value,
    };
    cp3.move(coord3);
    this._delta[0] = cp3.coord.value - cp1.coord.value;
  }

  _resetCtrlPoint4() {
    let [cp1, , , cp4] = this.ctrlPoints;
    let [p1, p4] = [cp1, cp4].map(cp => CtrlPoint.calcPixelPoint(this.context, cp.coord));
    if (p4.y <= p1.y) {
      if (__DEV__) console.log('positionline: p4 limited drag!');
      p4.y = p1.y + 10;
    }
    let coord = CtrlPoint.calcClosestCoord(this.context, p4);
    let value = coord.value;
    if (cp1.isEditing) {
      value = cp1.coord.value + this._delta[1];
    }
    let coord4 = {
      index: cp1.coord.index,
      displayIndex: cp1.coord.index,
      date: cp1.coord.date,
      data: cp1.coord.data,
      value: value,
    };
    cp4.move(coord4);
    this._delta[1] = cp4.coord.value - cp1.coord.value;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getPixelCtrlPoints();
    return this.isInBoundingBox(xy, points);
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let endPoints = this.getDrawEndPoint();
    let [q1, q2] = endPoints;
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let lineStyle = draw.getCanvasLineStyle(lineMetaInfo, this.state);
    draw.drawLine(ctx, [q1, q2], lineStyle);
    this.drawTargetArea(ctx, endPoints);
    this.drawStopArea(ctx, endPoints);
    if (this.drawingIndex >= 1 && q1.x !== q2.x) this.drawOrderAndLabel(ctx, endPoints);
    forEach(this.ctrlPoints, (point, i) => point.draw(ctx, endPoints[i]));
    return true;
  }

  drawTargetArea(ctx: Context2D, qPoints: Point[]) {
    let { stylesObj } = this.metaCfg;
    let targetMetaInfo = stylesObj.target;
    let style = { fillStyle: targetMetaInfo };
    let [q1, q2, q3, q4, q5, q6] = qPoints;
    let points = [q1, q2, q5, q3]; // LONG
    if (this.position === Position.SHORT) {
      points = [q1, q2, q6, q4];
    }
    draw.drawArea(ctx, points, style);
  }

  drawStopArea(ctx: Context2D, qPoints: Point[]) {
    let { stylesObj } = this.metaCfg;
    let stopMetaInfo = stylesObj.stop;
    let style = { fillStyle: stopMetaInfo };

    let [q1, q2, q3, q4, q5, q6] = qPoints;
    let points = [q1, q2, q6, q4]; // LONG
    if (this.position === Position.SHORT) {
      points = [q1, q2, q5, q3];
    }
    draw.drawArea(ctx, points, style);
  }

  drawOrderAndLabel(ctx: Context2D, qPoints: Point[]) {
    const long = this.position === Position.LONG;
    const [coord1, coord2, coord3, coord4] = this.ctrlPoints.map(cp => cp.coord);
    const entryPrice = coord1.value;
    const profitLevel = long ? coord3.value : coord4.value;
    const stopLevel = long ? coord4.value : coord3.value;
    const stopPrice = stopLevel;
    let { inputsObj } = this.metaCfg;
    let inputParams = {
      accountSize: inputsObj.accountSize,
      riskSize: inputsObj.riskSize
    };
    let endPrice = coord2.data.close;
    let entryCoord, endCoord;
    let result;
    let range = [coord1.index, coord2.index];
    let rangeData = sliceDataByX(this.context.allData, range);
    entryCoord = calcPositionEntryICoord(rangeData, coord1.value);
    if (entryCoord) {
      range = [entryCoord.index, coord2.index];
      rangeData = sliceDataByX(this.context.allData, range);
      endCoord = calcPositionEndICoord(rangeData, profitLevel, stopLevel);
      if (endCoord) endPrice = endCoord.value;
    }

    result = calc(inputParams, this.position, entryPrice, stopPrice, endPrice, profitLevel, stopLevel, this.context.precisionScale);
    // draw arraw dashed line  & area
    if (entryCoord && endCoord) {
      let points = [CtrlPoint.calcPixelPoint(this.context, entryCoord), CtrlPoint.calcPixelPoint(this.context, endCoord)];

      let { stylesObj } = this.metaCfg;
      let lineMetaInfo = stylesObj.line;
      let targetMetaInfo = stylesObj.target;
      let stopMetaInfo = stylesObj.stop;
      let arrowlineMetaInfo = stylesObj.arrowLine;
      let theme = this.getChartTheme();
      arrowlineMetaInfo.color = theme.chart.foregroundColor;
      let lineStyle = draw.getCanvasLineStyle(arrowlineMetaInfo, this.state);
      let rectStyle = {
        fillStyle: rgba(result.isProfit ? targetMetaInfo : stopMetaInfo),
      };
      draw.drawRect(ctx, points, rectStyle);
      draw.drawArrowLine(ctx, points, lineStyle);
    }

    // draw label
    this.drawLabel(ctx, qPoints, result);
  }

  drawLabel(ctx: Context2D, points: Point[], result: any) {
    if (this.state !== SELECTED && this.state !== HIGHLIGHT) return;
    const t = getI18n(this.context.lang).prefix('label.common');
    const long = this.position === Position.LONG;
    const [q1, q2, q3, q4, q5, q6] = points;
    let topXY = { x: (q3.x + q5.x) / 2, y: q3.y - 10 };
    let bottomXY = { x: (q4.x + q6.x) / 2, y: q4.y + 10 };
    let centerXY = { x: (q1.x + q2.x) / 2, y: q1.y };

    let tpLabel = `${t('target')}: ${result.tpRatio1} (${result.tpRatio2}) ${result.tpRatio3}, ${t('amount')}: ${result.tpAmount}`;
    let slLabel = `${t('stop')}: ${result.slRatio1} (${result.slRatio2}) ${result.slRatio3}, ${t('amount')}: ${result.slAmount}`;
    let centerLabel = `${result.orderClosed ? t('close') : t('open')} ${t('pnl')}: ${result.pl}, ${t('qty')}: ${result.qty}  ${t(
      'rrratio'
    )}: ${result.riskRewardRatio}`;
    let tpXY = topXY,
      slXY = bottomXY;
    if (!long) {
      tpXY = bottomXY;
      slXY = topXY;
    }
    draw.drawText(ctx, tpLabel, tpXY.x, tpXY.y, {
      fillStyle: '#fff',
      bgColor: TP_TEXT_BG_COLOR,
      textAlign: 'center',
      textBaseline: 'middle',
    });
    draw.drawText(ctx, slLabel, slXY.x, slXY.y, {
      fillStyle: '#fff',
      bgColor: SL_TEXT_BG_COLOR,
      textAlign: 'center',
      textBaseline: 'middle',
    });

    draw.drawText(ctx, centerLabel, centerXY.x, centerXY.y, {
      fillStyle: '#fff',
      bgColor: result.isProfit ? TP_TEXT_BG_COLOR : SL_TEXT_BG_COLOR,
      textAlign: 'center',
      textBaseline: 'middle',
    });
  }
}

function calc(input: any, position: Position, entryPrice, stopPrice, endPrice, profitLevel, stopLevel, scale: number): any {
  const long = position === Position.LONG;
  const accountSize = input.accountSize;
  const riskSize = input.riskSize * accountSize;
  let qty = riskSize / (entryPrice - stopPrice);
  let tpAmount: any = accountSize + (profitLevel - entryPrice) * qty;
  let slAmount: any = accountSize - (entryPrice - stopLevel) * qty;
  let pl: any = endPrice - entryPrice;
  let tpRatio1: any = Math.abs(profitLevel - entryPrice);
  let slRatio1: any = Math.abs(entryPrice - stopLevel);
  let riskRewardRatio: any = (profitLevel - entryPrice) / (entryPrice - stopLevel);
  let orderClosed = endPrice === profitLevel || endPrice === stopLevel;
  let isProfit = pl >= 0;
  let scaleRatio = Math.pow(10, scale - 1);
  if (!long) {
    qty = riskSize / (stopPrice - entryPrice);
    tpAmount = accountSize + (entryPrice - profitLevel) * qty;
    slAmount = accountSize - (stopLevel - entryPrice) * qty;
    pl = entryPrice - endPrice;
    isProfit = pl >= 0;
  }
  tpAmount = roundDown(tpAmount, 2);
  slAmount = roundUp(slAmount, 2);
  tpRatio1 = roundDown(tpRatio1, scale);
  slRatio1 = roundUp(slRatio1, scale);
  pl = roundDown(pl, scale);
  riskRewardRatio = roundDown(riskRewardRatio, 2);
  let tpRatio2 = (tpRatio1 / entryPrice * 100).toFixed(2) + '%';
  let slRatio2 = (slRatio1 / entryPrice * 100).toFixed(2) + '%';
  let tpRatio3 = (tpRatio1 * scaleRatio).toFixed(1);
  let slRatio3 = (slRatio1 * scaleRatio).toFixed(1);
  tpRatio1 = tpRatio1.toFixed(scale);
  slRatio1 = slRatio1.toFixed(scale);
  tpAmount = tpAmount.toFixed(2);
  slAmount = slAmount.toFixed(2);
  riskRewardRatio = riskRewardRatio.toFixed(2);
  pl = pl.toFixed(scale);
  qty = qty | 0;
  return {
    qty,
    tpAmount,
    slAmount,
    orderClosed,
    isProfit,
    tpRatio1,
    tpRatio2,
    tpRatio3,
    slRatio1,
    slRatio2,
    slRatio3,
    pl,
    riskRewardRatio,
  };
}
