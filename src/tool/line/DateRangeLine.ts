import { getParallelPoints } from '../../util/geometry';
import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { roundUp } from '../../util/math';
import { getHumanDeltaTime } from '../../util/format';
import { forEach } from '../../util/array';
import { getI18n } from '../../config/index';

const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;


// tslint:disable:jsdoc-format
/**
p1(q1)-------------+------------------q3

q5                                    q6

q4------------------------------------p2(q2)
*/
export default class DateRangeLine extends BaseLine {
  public ctrlPointSize = 2;

  getName(): ToolLineType {
    return ToolLineType.DATE_RANGE_LINE;
  }

  getDrawEndPoint(): Point[][] {
    let [q1, q2] = this.getPixelCtrlPoints();
    let q3 = { x: q2.x, y: q1.y };
    let q4 = { x: q1.x, y: q2.y };
    let q5 = { x: q1.x, y: (q1.y + q4.y) / 2 };
    let q6 = { x: q2.x, y: q5.y };
    let endPoints = [[q1, q4], [q3, q2], [q5, q6]];
    return endPoints;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    return !!points.find(linePoints => this.isInBoundingBox(xy, linePoints));
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let endPoints = this.getDrawEndPoint();
    let points = this.getPixelCtrlPoints();
    let theme = this.getChartTheme();
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let areaMetaInfo = stylesObj.area;
    let arrowLineMetaInfo = stylesObj.arrowLine;
    arrowLineMetaInfo.color = theme.chart.foregroundColor;
    let lineStyle = draw.getCanvasLineStyle(lineMetaInfo, this.state);
    let arrowLineStyle = draw.getCanvasLineStyle(arrowLineMetaInfo, this.state);
    let areaStyle = { fillStyle: areaMetaInfo };

    draw.drawRect(ctx, points, areaStyle);
    draw.drawLine(ctx, endPoints[0], lineStyle);
    draw.drawLine(ctx, endPoints[1], lineStyle);
    draw.drawArrowLine(ctx, endPoints[2], arrowLineStyle);
    if (this.drawingIndex >= 1) this.drawLabel(ctx, endPoints);
    forEach(this.ctrlPoints, (point, i) => point.draw(ctx, points[i]));
    return true;
  }

  drawLabel(ctx: Context2D, endPoints: Point[][]) {
    let [q1, q4] = endPoints[0];
    let chartHeight = this.context.chartHeight;
    let q2 = endPoints[1][1];
    let offset = 10;
    let xy = { x: (q4.x + q2.x) / 2, y: q2.y };
    let deltaY = q2.y > q1.y ? offset : -offset;
    xy.y += deltaY;
    if (xy.y < 0) {
      xy.y = offset;
    } else if (xy.y > chartHeight) {
      xy.y = chartHeight - offset;
    }
    if ((q1.y < 0 && q2.y < 0) || (q1.y > chartHeight && q2.y > chartHeight)) return;
    const t = getI18n(this.context.lang).t;
    let coords = this.ctrlPoints.map(p => p.coord);
    let deltaIndex = coords[1].index - coords[0].index;
    let deltaData = coords[1].date - coords[0].date;
    let label = `${deltaIndex} ${t('label.common.bar')}, ${getHumanDeltaTime(deltaData, t)}`;
    draw.drawText(ctx, label, xy.x, xy.y, {
      bgColor: '#999',
      fillStyle: '#fff',
      textAlign: 'center',
      textBaseline: 'middle',
    });
  }
}
