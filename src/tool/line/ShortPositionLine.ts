import BasePositionLine, { Position } from './BasePositionLine';
import { ToolLineType } from '../../lib';

export default class ShortPositionLine extends BasePositionLine {
  position = Position.SHORT;

  getName(): ToolLineType {
    return ToolLineType.SHORT_POSITION_LINE;
  }
}
