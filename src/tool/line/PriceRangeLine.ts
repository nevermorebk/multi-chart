import { getParallelPoints } from '../../util/geometry';
import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { roundUp } from '../../util/math';
import { forEach } from '../../util/array';

const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;

// tslint:disable:jsdoc-format
/**
p1(q1)-------------q5------------------q3



q4-----------------q6-----------------p2(q2)
*/
export default class PriceRangeLine extends BaseLine {
  public ctrlPointSize = 2;

  getName(): ToolLineType {
    return ToolLineType.PRICE_RANGE_LINE;
  }

  getDrawEndPoint(): Point[][] {
    let [q1, q2] = this.getPixelCtrlPoints();
    let q3 = { x: q2.x, y: q1.y };
    let q4 = { x: q1.x, y: q2.y };
    let q5 = { x: (q1.x + q2.x) / 2, y: q1.y };
    let q6 = { x: q5.x, y: q2.y };
    let endPoints = [[q1, q3], [q4, q2], [q5, q6]];
    return endPoints;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    return !!points.find(linePoints => this.isInBoundingBox(xy, linePoints));
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let endPoints = this.getDrawEndPoint();
    let points = this.getPixelCtrlPoints();
    let theme = this.getChartTheme();
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let areaMetaInfo = stylesObj.area;
    let arrowLineMetaInfo = stylesObj.arrowLine;
    arrowLineMetaInfo.color = theme.chart.foregroundColor;
    let lineStyle = draw.getCanvasLineStyle(lineMetaInfo, this.state);
    let arrowLineStyle = draw.getCanvasLineStyle(arrowLineMetaInfo, this.state);
    let areaStyle = { fillStyle: areaMetaInfo };


    draw.drawRect(ctx, points, areaStyle);
    draw.drawLine(ctx, endPoints[0], lineStyle);
    draw.drawLine(ctx, endPoints[1], lineStyle);
    draw.drawArrowLine(ctx, endPoints[2], arrowLineStyle);
    if (this.drawingIndex >= 1) this.drawLabel(ctx, endPoints);
    forEach(this.ctrlPoints, (point, i) => point.draw(ctx, points[i]));
    return true;
  }

  drawLabel(ctx: Context2D, endPoints: Point[][]) {
    let chartHeight = this.context.chartHeight;
    let q1 = endPoints[0][0];
    let q2 = endPoints[1][1];
    let q6 = endPoints[2][1];
    let offset = 10;
    let deltaY = q2.y > q1.y ? offset : -offset;
    let xy = q6;
    xy.y += deltaY;
    if (xy.y < 0) {
      xy.y = offset;
    } else if (xy.y > chartHeight) {
      xy.y = chartHeight - offset;
    }
    if ((q1.y < 0 && q2.y < 0) || (q1.y > chartHeight && q2.y > chartHeight)) return;
    let scale = this.context.precisionScale;
    let scaleRatio = Math.pow(10, scale - 1);
    let prices = this.ctrlPoints.map(p => p.coord.value);
    let deltaPrice = prices[1] - prices[0];
    let ratio1: any = roundUp(deltaPrice, scale);
    let ratio2 = (deltaPrice / prices[0] * 100).toFixed(2) + '%';
    let ratio3 = (ratio1 * scaleRatio).toFixed(1);
    ratio1 = ratio1.toFixed(scale);
    let label = `${ratio1} (${ratio2}) ${ratio3}`;
    draw.drawText(ctx, label, xy.x, xy.y, {
      bgColor: '#999',
      fillStyle: '#fff',
      textAlign: 'center',
      textBaseline: 'middle',
    });
  }
}
