import { isFinite, distanceBetweenPoint, rgba, maxDistance, getAngle } from '../../util';
import { Vector } from '../../util/math';
import { contains2 } from '../../util/geometry';
import { Point, IChartContext, IToolLine, ICtrlPoint, SelectionState, ToolLineType, LevelColorMetaInfo, BoundingBox } from '../../lib';
import BaseLine from './BaseLine';
import * as draw from '../../canvas/draw';
import CtrlPoint from '../CtrlPoint';
import { intersectionOnBoundingBox, distanceToLine, isValidPoint, forEach } from '../../util';

export default class GannFanLine extends BaseLine {
  public ctrlPointSize = 2;

  getName(): ToolLineType {
    return ToolLineType.GANN_FAN_LINE;
  }

  _getIntersectionPoint(pixPoints: Point[], box: BoundingBox): Point {
    let [p1, p2] = pixPoints;
    let interPoints = intersectionOnBoundingBox(p1, p2, box, true);
    return interPoints[0];
  }

  getDrawEndPoint(): Point[][] {
    let box = this.getTargetBox();
    let [p1, p2] = this.getPixelCtrlPoints();
    let v = Vector.from(p1, p2);
    let levels = this.metaCfg.stylesObj.ratios.levels;
    let endPoints = levels.filter(l => !l.disable).map(({ val }) => {
      if (val >= 1) {
        return [p1, new Vector(v.x / val, v.y, p1).end()];
      } else {
        return [p1, new Vector(v.x, v.y * val, p1).end()];
      }
    });

    forEach(endPoints, (points: Point[]) => {
      let ePoint = this._getIntersectionPoint(points, box);
      if (ePoint) {
        points.push(ePoint);
      }
    });
    return endPoints;
  }

  isHover(xy: Point): boolean {
    if (this.state === SelectionState.HIDE) return false;
    let points = this.getDrawEndPoint();
    let found = points.filter(linePoints => this.isInBoundingBox(xy, linePoints)).find(ps => {
      if (ps.length === 2) {
        return this.isNearLine(xy, ps);
      }
      if (ps.length === 3) {
        return this.isNearLine(xy, [ps[0], ps[1]]) || this.isNearLine(xy, [ps[0], ps[2]]);
      }
    });
    return !!found;
  }

  draw(ctx: Context2D): boolean {
    if (!super.draw(ctx)) return false;
    let points = this.getDrawEndPoint();
    let { stylesObj } = this.metaCfg;
    let lineMetaInfo = stylesObj.line;
    let fibosMetaInfo: LevelColorMetaInfo = stylesObj.ratios;

    if (this.drawingIndex >= 1) {
      let [p1, p2] = this.getPixelCtrlPoints();
      let alpha = stylesObj.background;
      let radius = maxDistance(p1, this.getTargetBox());
      let levels = fibosMetaInfo.levels.filter((level) => !level.disable);
      let angles = points.reduce<any>((result, p, i) => {
        if (i === 0) {
          result[i] = getAngle(p[0], { x: p[1].x, y: p[0].y }, p[1]);
          if (p2.x < p1.x) result[i] += Math.PI;
        } else {
          result[i] = result[i - 1] + getAngle(p[0], points[i - 1][1], p[1]);
        }
        return result;
      }, []);

      forEach(points, (linePoint, i) => {
        let lineStyle = draw.getCanvasLineStyle(Object.assign({}, lineMetaInfo, { color: levels[i].color }), this.state);
        draw.drawLine(ctx, linePoint, lineStyle);
        if (i > 0) {
          let backgroundColor = rgba(levels[i].color, alpha);
          let areaStyle = { fillStyle: backgroundColor };

          draw.drawFan(ctx, p1, radius, angles[i - 1], angles[i], areaStyle);
        }
      });
      if (stylesObj.label !== false) {
        forEach(points, (linePoint, i) => {
          let fontStyle = {
            textAlign: 'center',
            textBaseline: 'middle',
            fillStyle: levels[i].color,
          };
          this.drawLabel(ctx, levels[i].label, linePoint, fontStyle);
        });
      }
    }

    forEach(this.ctrlPoints, (point, i) => point.draw(ctx));
    return true;
  }

  drawLabel(ctx: Context2D, label: string, linePoint: Point[], fontStyle) {
    let [origin, center, interPoint] = linePoint;
    if (!interPoint) return;
    let offset = 15;
    let point = center;
    if (!contains2(point, this.getTargetBox())) {
      point = interPoint;
    }
    if (origin.x < point.x) point.x -= offset;
    else point.x += offset;
    if (origin.y < point.y) point.y -= offset;
    else point.y += offset;
    draw.drawText(ctx, label, point.x, point.y, fontStyle);
  }
}
