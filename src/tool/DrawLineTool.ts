import { SelectionState, DrawOpt, ChartStyle, IBaseContext, OHLC, DragPoint } from '../lib';
import { IDrawLineTool, Point, ICtrlPoint, IToolLine, ChartMode, ToolLineType, CommandState, ContextState } from '../lib';
import { getMagnetValue, getDataItemByX } from '../util/data';
import { contains } from '../util/geometry';
import MainArea from '../area/MainArea';
import ITool from './ITool';
import CtrlPoint from './CtrlPoint';
import * as EventUtil from '../util/event';
import { noevent } from '../util/event';
import { createToolline } from './line';

const { BEFORE, PROCESS, FINISH } = CommandState;

const { NORMAL, DRAWTOOL } = ChartMode;

export enum ProcessMode {
  REPLACE,
  GHOST,
  ADD,
}

export default class DrawLineTool extends ITool implements IDrawLineTool {
  protected step: number;
  protected ctrlPoints: ICtrlPoint[];
  toolLine: IToolLine;
  protected area: MainArea;
  protected lineType: ToolLineType;

  constructor(context: IBaseContext, lineType: ToolLineType, config?: object) {
    super(context, ToolLineType[lineType]);
    let mainChart = context.getMainChart();
    this.lineType = lineType;
    this.area = mainChart.mainArea as MainArea;

    this._handleKeyUp = this._handleKeyUp.bind(this);
    this._handleMouseDown = this._handleMouseDown.bind(this);
  }

  active(): void {
    super.active();
    if (this.context.getChartMode() !== DRAWTOOL) {
      this.context.setChartMode(DRAWTOOL);
      // TODO (2018/03/16 18:35) cursor manager
      // this.context.setCursor(CursorStyle.CROSSHAIR);
    }
    this.resetToolLine();
    // register key event
    window.addEventListener('keyup', this._handleKeyUp, false);
    let canvas = this.context.overlayCanvas.element;
    canvas.addEventListener('mousedown', this._handleMouseDown, true);
  }

  deactive(): void {
    super.deactive();
    if (this.context.getChartMode() === DRAWTOOL) {
      this.context.setChartMode(NORMAL);
      // this.context.setCursor(CursorStyle.DEFAULT);
    }
    // off key event
    window.removeEventListener('keyup', this._handleKeyUp, false);
    let canvas = this.context.overlayCanvas.element;
    canvas.removeEventListener('mousedown', this._handleMouseDown, true);
  }

  _handleKeyUp(e) {
    if (EventUtil.isEscKey(e)) {
      this.doAbort();
      this.context.redraw(DrawOpt.overlay);
    }
  }

  _handleMouseDown(e) {
    if (EventUtil.isRightButton(e)) {
      if (this.state === BEFORE) {
        noevent(e);
      } else if (this.state === PROCESS) {
        this.doAbort();
        this.context.redraw(DrawOpt.overlay);
      }
    }
  }

  resetToolLine() {
    this.toolLine = createToolline(this.context, this.lineType);
    this.toolLine.startEdit();
    this.step = this.toolLine.ctrlPointSize;
  }

  setPoint(point: Point, fromIndex = 0): boolean {
    let pointsSize = this.toolLine.ctrlPointSize;
    let coord = CtrlPoint.calcClosestCoord(this.context, point);
    if (coord && this.context.magnetMode) {
      let ohlc = coord.data;
      if (this.context.chartStyle === ChartStyle.Avg) {
        let item = getDataItemByX(this.context.data, coord.index);
        if (item[ChartStyle.Avg]) ohlc = item[ChartStyle.Avg];
      } else if (this.context.chartStyle === ChartStyle.Line) {
        ohlc = { close: ohlc.close } as OHLC;
      }
      let magnetValue = getMagnetValue(ohlc, this.context.getScaleY(), point.y, this.context.magnetDistance);
      if (magnetValue) coord.value = magnetValue;
    }
    if (!coord) return false;

    this.toolLine.drawingIndex = fromIndex;
    this.toolLine.ctrlPoints.forEach(cp => (cp.isEditing = false));
    for (let i = fromIndex; i < pointsSize; i++) {
      let ctrlPoint = new CtrlPoint(this.context, this.toolLine, coord);
      if (i === fromIndex) ctrlPoint.isEditing = true;
      this.toolLine.setCtrlPoint(ctrlPoint, i);
      ctrlPoint.select();
    }
    if (fromIndex === 0) {
      this.toolLine.setState(SelectionState.SELECTED);
    }
    return true;
  }

  getDrawIndex() {
    return this.toolLine.ctrlPointSize - this.step;
  }

  isPointInArea(point: Point) {
    let rect = this.area.getRect();
    return contains(point, rect);
  }


  doAbort() {
    this.context.state.drawedDragging = ContextState.DrawedDragging.EMPTY;
    // rm toolLine from lineGroup group
    let group = this.area.lineGroup;
    let removed = group.remove(this.toolLine);
    if (removed) {
      // make state not process
      this.setState(BEFORE); // or FINISH
      this.context.setCurTool(null);
      this.deactive();
    }
    return removed;
  }

  doBefore(point: Point): void {
    this.context.state.drawedDragging = ContextState.DrawedDragging.CTRL_POINT;
    let lineGroup = this.area.lineGroup;
    lineGroup.setSelectedPoint(null);
    lineGroup.setSelectedLine(null);
  }

  doProcess(point: Point, mode: ProcessMode = ProcessMode.GHOST): boolean {
    let index;
    let flag = false;
    let lineGroup = this.area.lineGroup;
    if (this.step < 0) return false;
    index = this.getDrawIndex();
    if (mode === ProcessMode.REPLACE) {
      index--;
    }
    flag = this.setPoint(point, index);
    if (mode === ProcessMode.ADD && flag && this.step > 0) {
      this.step--;
      if (index === 0) {
        // first point
        lineGroup.add(this.toolLine);
        lineGroup.setSelectedLine(this.toolLine);
      }
    }
    return flag;
  }

  doFinish(): void {
    this.context.state.drawedDragging = ContextState.DrawedDragging.EMPTY;
    if (this.step > 0) {
      this.step--;
    }
    if (this.step === 0) {
      this.toolLine.stopEdit();
      this.area.lineGroup.setSelectedLine(this.toolLine);
      this.area.lineGroup.changed();
      this.context.setCurTool(null);
      this.deactive();
      this.context.redraw(DrawOpt.overlay);
    }
  }

  onMouseDown(point: Point, event?: MouseEvent): boolean {
    if (!this.isPointInArea(point)) return false;

    switch (this.state) {
      case BEFORE: {
        this.doBefore(point);
        let result = this.doProcess(point, ProcessMode.ADD);
        if (result) {
          this.setState(PROCESS);
        }
        return true;
      }
      case PROCESS: {
        let result = this.doProcess(point, ProcessMode.ADD);
        return true;
      }
      case FINISH:
        return true;
    }
  }

  onMouseMove(point: Point, event?: MouseEvent): boolean {
    // if (!this.isPointInArea(point)) return false;
    switch (this.state) {
      case BEFORE: {
        return true;
      }
      case PROCESS: {
        this.doProcess(point);
        return true;
      }
    }
  }

  onMouseUp(point: Point, event?: MouseEvent): boolean {
    if (!this.isPointInArea(point)) return false;
    switch (this.state) {
      case PROCESS: {
        if (this.step === 0) {
          this.setState(FINISH);
          this.doFinish();
        }
        return true;
      }
      case FINISH: {
        this.doFinish();
        return true;
      }
    }
  }

  onDrag(point: DragPoint): boolean {
    // if (!this.isPointInArea(point)) return false;
    switch (this.state) {
      case BEFORE: {
        return true;
      }
      case PROCESS: {
        this.doProcess(point, ProcessMode.REPLACE);
        return true;
      }
    }
  }
}
