import ICommand from './ICommand';
import { CommandState, Point, IBaseContext, DragPoint } from '../lib';

export default abstract class ITool extends ICommand {
  constructor(context: IBaseContext, name: string, config?: object) {
    super(context, name);
  }

  setState(state: CommandState) {
    super.setState(state);
    if (state === CommandState.PROCESS) {
      this.context.state.cmdProcing = true;
    } else {
      this.context.state.cmdProcing = false;
    }
  }

  active(): void {
    super.active();
  }

  deactive(): void {
    // change tool
    if (this.state === CommandState.PROCESS) {
      this.doAbort();
    }
    super.deactive();
  }

  // event adapter
  onMouseDown(point: Point, event?: MouseEvent): boolean {
    return false;
  }
  onMouseMove(point: Point, event?: MouseEvent): boolean {
    return false;
  }
  onMouseUp(point: Point, event?: MouseEvent): boolean {
    return false;
  }
  onClick(point: Point, event?: MouseEvent): boolean {
    return false;
  }
  onDblClick(point: Point): boolean {
    return false;
  }
  onDrag(point: DragPoint, event?: any): boolean {
    return false;
  }
}
