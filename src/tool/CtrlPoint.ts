import { contains2, getBoundingBox } from '../util/geometry';
import { BoundingBox, IToolLine, Point, IChartContext, ICoord, ICtrlPoint, SelectionState } from '../lib';
import { calcClosestData, xAccessor, dateAccessor, getClosestItemIndex } from '../util/data';
import * as draw from '../canvas/draw';
import EventAdapter from '../event/EventAdapter';
import { isTouchDevice } from '../util/is';
import * as color from '../util/color';
import { getTheme } from '../config/index';
import { clamp } from '../util/math';
import { memoize } from '../util';

const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;


const STYLES = memoize(function memoizedStyle(themeBgColor: string) {
  let invertColor = color.invertColor(themeBgColor);
  let style = {
    // [SHOW] :      { strokeStyle: invertColor, lineWidth: 1, fillStyle: themeBgColor },
    [HIGHLIGHT]: { strokeStyle: invertColor, lineWidth: 1, fillStyle: themeBgColor },
    [SELECTED]: { strokeStyle: invertColor, lineWidth: 2, fillStyle: themeBgColor },
  };
  return style;
});

export default class CtrlPoint extends EventAdapter implements ICtrlPoint {
  static MARGIN = isTouchDevice() ? 16 : 8; // double for pad
  static MIN_RADIUS = 5;
  static MAX_RADIUS = 6;
  coord: ICoord;
  line: IToolLine;
  state: SelectionState;
  context: IChartContext;
  isEditing = false;
  constructor(context: IChartContext, line: IToolLine, coord?: ICoord) {
    super();
    this.context = context;
    this.line = line;
    this.coord = coord;
    this.state = HIDE;
  }
  setState(state: SelectionState): void {
    this.state = state;
  }
  getState(): SelectionState {
    return this.state;
  }
  // TODO: getPoint cache by timestamp
  getPoint(): Point {
    return CtrlPoint.calcPixelPoint(this.context, this.coord);
  }
  move(coord: ICoord): void {
    if (coord) this.coord = coord;
  }
  isHover(mouseXY: Point): boolean {
    return contains2(mouseXY, this.getBoundingBox());
  }
  highlight(): void {
    this.setState(HIGHLIGHT);
  }
  select(): void {
    this.setState(SELECTED);
  }
  normal(): void {
    this.setState(HIDE);
  }

  getBoundingBox(): BoundingBox {
    return getBoundingBox(this.getPoint(), CtrlPoint.MARGIN);
  }

  draw(ctx: Context2D, point?: Point): void {
    if (this.state === HIDE) return;
    let barWidth = this.context.getBandWidth();
    let theme = this.context.theme;
    let bgColor = getTheme(theme).chart.backgroundColor;
    let radius = clamp(barWidth / 2 | 0, CtrlPoint.MIN_RADIUS, CtrlPoint.MAX_RADIUS);
    const styles = STYLES(bgColor);
    point = point || this.getPoint();
    draw.drawCircle(ctx, point, radius, styles[this.state]);
  }

  toJSON(): object {
    let { date, value } = this.coord; // index is optional
    return {
      d: date,
      v: value,
    };
  }

  static create(context: IChartContext, points: any[], line: IToolLine): CtrlPoint[] {
    let ctrlPoints = [];
    for (let i = 0; i < points.length; i++) {
      let point = points[i];
      let { d: date, v: value } = point;
      let allData = context.allData;
      let j = getClosestItemIndex(allData, dateAccessor, date);
      if (j >= 0 && j < allData.length) {
        let { x: index, data } = allData[j];
        let coord = { displayIndex: index, index, data, date, value };
        ctrlPoints.push(new CtrlPoint(context, line, coord));
      } else {
        return [];
      }
    }
    return ctrlPoints;
  }

  // calc tool
  // TODO (2018/05/09 14:24) mv to model
  static calcClosestCoord(context: IChartContext, point: Point, nostrict = false): ICoord {
    let scaleX = context.getScaleX();
    let scaleY = context.getScaleY();
    let strict = !nostrict;
    let closest = calcClosestData(scaleX, context.allData, xAccessor, point.x, strict);
    if (!closest) {
      return null;
    }

    let index = closest.x;
    let date = closest.date;
    let data = closest.data;
    let value = scaleY.invert(point.y);

    return { displayIndex: index, index, value, date, data };
  }

  static calcPixelPoint(context: IChartContext, coord: ICoord): Point {
    let scaleX = context.getScaleX();
    let scaleY = context.getScaleY();
    let x = scaleX(coord.displayIndex || coord.index);
    let y = scaleY(coord.value);
    return { x, y };
  }
}
