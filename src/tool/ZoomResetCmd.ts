import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';

export default class ZoomResetCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.ZOOM_RESET];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    this.context.resetZoom();
    return true;
  }
}
