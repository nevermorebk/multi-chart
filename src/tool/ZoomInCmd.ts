import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';

export default class ZoomInCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.ZOOM_IN];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    this.context.zoomIn();
    return true;
  }
}
