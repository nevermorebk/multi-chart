import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';

export default class ClearIndicatorsCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.CLEAR_INDICATORS];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    this.context.clearIndicators();
    return true;
  }
}
