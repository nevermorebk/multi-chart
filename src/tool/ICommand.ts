import { ChartMode, IChartContext, CursorStyle, CommandState, IBaseContext } from '../lib';
import Chart from '../chart/Chart';

export default abstract class ICommand {
  name: string;
  state: CommandState;
  cursorStyle: CursorStyle;
  protected context: IChartContext;
  constructor(context: IBaseContext, name: string, config?: object) {
    this.name = name;
    let mainChart = context.getMainChart();
    this.context = mainChart.context;
  }

  init() {}

  setState(state: CommandState) {
    this.state = state;
  }

  getState() {
    return this.state;
  }

  active(): void {
    this.state = CommandState.BEFORE;
  }

  deactive(): void {
    this.state = CommandState.FINISH;
  }

  abstract doBefore(arg?: object);

  abstract doProcess(arg?: object);

  abstract doFinish(arg?: object);

  abstract doAbort(arg?: object);
}
