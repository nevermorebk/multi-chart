import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';
import { DrawLineTool } from './index';

export default class DeleteLineCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.DELETE_LINE];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    let curTool = this.context.getCurTool();
    if (curTool && curTool instanceof DrawLineTool) {
      this.context.setCurTool(null);
    }
    let lineGroup = this.area.lineGroup;
    let result = lineGroup.remove(lineGroup.selectedLine);
    return result;
  }
}
