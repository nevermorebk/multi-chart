import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';

export default class ZoomOutCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.ZOOM_OUT];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    this.context.zoomOut();
    return true;
  }
}
