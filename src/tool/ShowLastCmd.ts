import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';

export default class ShowLastCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.SHOW_LAST];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    if (this.context.state.preventMore === true) {
      this.context.state.preventMore = false;
    }
    return this.context.showLast({ resetZoom: true });
  }
}
