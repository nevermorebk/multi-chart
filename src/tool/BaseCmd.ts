import { IBaseContext } from '../lib';
import ICommand from './ICommand';
import { MainArea } from '../area/index';

export default abstract class BaseCmd extends ICommand {
  area: MainArea;
  constructor(context: IBaseContext, name: string, config?: object) {
    super(context, name, config);
    let mainChart = context.getMainChart();
    this.area = mainChart.mainArea as MainArea;
  }

  active(): void {
    super.active();
    let result = this.doProcess();
    if (result) {
      this.context.redraw();
    }
    this.deactive();
  }

  doBefore(arg?: object) {}

  abstract doProcess(arg?: object): boolean;

  doFinish(arg?: object) {}

  doAbort(arg?: object) {}
}
