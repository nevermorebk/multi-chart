import { BoundingBox, IDrawable, IChartContext, IToolLine, ICtrlPoint } from '../lib';
import { DragPoint, Point, KEY_CODE, ChartStyle, ToolLineType, DrawOpt, IMouseXY, OHLC, ContextState, SetConfigParam } from '../lib';
import EventAdapter from '../event/EventAdapter';
import CtrlPoint from './CtrlPoint';
import { getMagnetValue, forEach, debounce } from '../util';
import { getDataItemByX } from '../util/data';
import { timeout } from '../util/index';
import { createToolline } from './line/index';
import { IMetaInfoCfg } from '../config/meta';

type V = {
  dx: number; // drag: 累加dx
  dy: number;
  left: number; // 拖动点与拖动目标boundingBox的左边距
  right: number;
  top: number;
  bottom: number;
};

const { CTRL_POINT, OBJECT, EMPTY } = ContextState.DrawedDragging;
const { HIDDEN, LOCKED } = ContextState.Drawed;

// Delegate & Manager
export default class SelectionGroup<T extends IToolLine> extends EventAdapter implements IDrawable {
  context: IChartContext;
  groupName: string;
  selectedLine: IToolLine;
  selectedPoint: ICtrlPoint;
  private _list: IToolLine[];
  private matchedPoint: ICtrlPoint;
  private matchedLine: IToolLine;
  private dragTargetMap = new WeakMap<IToolLine, V>();

  constructor(context: IChartContext, groupName: string) {
    super();
    this.context = context;
    this.groupName = groupName;
    this._list = [];
    this.context.selectedLine = null;
  }

  get(index: number): IToolLine {
    return this._list[index];
  }

  size(): number {
    return this._list.length;
  }

  add(line: IToolLine): void {
    this.matchedLine = line;
    this._list.push(line);
    // this.changed(); // not draw start, but draw finished
  }

  remove(line: IToolLine): boolean {
    let index = this._list.indexOf(line);
    if (this.matchedLine === line) {
      this.matchedLine = null;
    }
    if (this.selectedLine === line) {
      this.setSelectedLine(null);
    }
    if (index > -1) {
      this._list.splice(index, 1);
      this.changed();
      return true;
    }
    return false;
  }

  each(fn) {
    forEach(this._list, fn);
  }

  clear(): boolean {
    if (this._list.length === 0) return false;
    let result = !!this._list.length;
    this._list = [];
    this.matchedPoint = null;
    this.matchedLine = null;
    this.selectedPoint = null;
    this.setSelectedLine(null);
    this.changed();
    return result;
  }

  draw(ctx?: Context2D) {
    ctx = ctx || this.context.overlayCanvas.ctx;
    forEach(this._list, item => item.draw(ctx));
  }

  resetMatchedObj() {
    let { matchedPoint, matchedLine, selectedLine } = this;
    if (matchedPoint && matchedPoint.line !== selectedLine) {
      matchedPoint.normal();
      matchedPoint.line.normal();
      this.matchedPoint = null;
    }
    if (matchedLine && matchedLine !== selectedLine) {
      matchedLine.normal();
      this.matchedLine = null;
    }
  }

  _matchedPoint(point): ICtrlPoint {
    let drawed = this.context.state.drawed;
    if (drawed & HIDDEN || drawed & LOCKED) return null;
    let points = this._list.reduce((points, line) => {
      points.push(...line.ctrlPoints);
      return points;
    }, []);
    return points.find(p => p.isHover(point));
  }

  _matchedLine(point): IToolLine {
    let drawed = this.context.state.drawed;
    if (drawed & HIDDEN || drawed & LOCKED) return null;
    return this._list.find(line => line.isHover(point));
  }

  setSelectedLine(line: IToolLine) {
    let selected = this.selectedLine;
    if (selected) {
      selected.normal();
    }
    if (line) {
      line.select();
      // mv line to end of list
      this._sortLines();
      this.context.trigger('drawedLineSelected', line);
    }
    this.selectedLine = line;
    this.context.selectedLine = line;
  }

  setSelectedPoint(point: ICtrlPoint) {
    let selected = this.selectedPoint;
    if (selected) {
      selected.normal();
    }

    if (point) {
      point.select();
    }
    this.selectedPoint = point;
  }

  _resetDragTargetMap(key: IToolLine, xy: Point) {
    let box = key.getBoundingBox();
    this.dragTargetMap.set(key, {
      dx: 0,
      dy: 0,
      left: xy.x - box[0],
      right: box[2] - xy.x,
      top: xy.y - box[1],
      bottom: box[3] - xy.y,
    });
  }

  _sortLines() {
    this._list.sort((l1, l2) => l1.state - l2.state);
  }

  setHighlightLine(line: IToolLine) {
    let { selectedLine } = this;
    if (selectedLine !== line) {
      line.highlight();
      return true;
    }
    return false;
  }

  _canMove(point: DragPoint): any {
    let hDir = null;
    let vDir = null;
    let box: BoundingBox;
    let { selectedLine } = this;
    let result = { x: false, y: false };
    let delta = this.dragTargetMap.get(selectedLine);
    if (point.dx === 0 && point.dy === 0) return false;
    if (point.dx !== 0) hDir = point.dx > 0 ? 'right' : 'left';
    if (point.dy !== 0) vDir = point.dy > 0 ? 'bottom' : 'top';

    if (selectedLine) {
      box = selectedLine.getBoundingBox();
    }
    if (!box || !delta) return false;

    if (hDir) {
      result.x = hDir === 'left' ? point.x - box[0] <= delta.left : box[2] - point.x <= delta.right;
    }
    if (vDir) {
      result.y = vDir === 'top' ? point.y - box[1] <= delta.top : box[3] - point.y <= delta.bottom;
    }

    return result;
  }

  _moveSelectedLine(line: IToolLine, point: DragPoint): boolean {
    let movedCoords = [];
    let ctrlPoints = line.ctrlPoints;
    let delta = this.dragTargetMap.get(line);

    // +dx
    let width = this.context.getBandWidth(1);
    delta.dx += point.dx;
    let dx = ((delta.dx / width) | 0) * width;
    delta.dx = delta.dx % width;
    // +dy
    delta.dy += point.dy;
    let dy = delta.dy;
    delta.dy = 0;

    let canMove = this._canMove(point);
    if (!canMove) return false;
    // move
    for (let i = 0, len = ctrlPoints.length; i < len; i++) {
      let ctrlPoint = ctrlPoints[i];
      let p = ctrlPoint.getPoint();
      let x = p.x;
      let y = p.y;
      if (canMove.x) x += dx;
      if (canMove.y) y += dy;
      let movedCoord = CtrlPoint.calcClosestCoord(this.context, { x, y });
      // outside data area
      if (!movedCoord) break;
      movedCoords.push(movedCoord);
    }

    if (movedCoords.length === ctrlPoints.length) {
      // todo need check anyone not change
      line.move(movedCoords);
      return true;
    }

    return false;
  }

  _moveSelectedPoint(ctrlPoint: ICtrlPoint, point: DragPoint): boolean {
    // move
    let line = ctrlPoint.line;
    let cpIndex = line.ctrlPoints.indexOf(ctrlPoint);
    let coord = CtrlPoint.calcClosestCoord(this.context, point, true);
    let isDragLine = this.selectedLine && !this.selectedPoint;
    let { y } = point;
    if (coord && !isDragLine) {
      if (this.context.magnetMode) {
        let scaleY = this.context.getScaleY();
        let ohlc = coord.data;
        if (this.context.chartStyle === ChartStyle.Avg) {
          let item = getDataItemByX(this.context.data, coord.index);
          if (item[ChartStyle.Avg]) ohlc = item[ChartStyle.Avg];
        } else if (this.context.chartStyle === ChartStyle.Line) {
          ohlc = { close: ohlc.close } as OHLC;
        }
        let magnetValue = getMagnetValue(ohlc, scaleY, y, this.context.magnetDistance);
        if (magnetValue) {
          coord.value = magnetValue;
        }
      }
      line.moveCtrlPoint(coord, cpIndex);
    }
    return !!coord;
  }

  changed() {
    timeout(() => {
      this.context.trigger('objectDrawed');
    }, 0);
  }

  toJSON(): object {
    return this._list.map(line => line.toJSON());
  }

  // event delegate

  onClick(point: Point): boolean {
    // todo: contextmenu show
    return false;
  }

  onDblClick(point: Point): boolean {
    let { selectedLine } = this;
    this.context.state.drawedDragging = ContextState.DrawedDragging.EMPTY;
    if (selectedLine) {
      let fn = this.context.setDrawedConfig;
      if (fn) {
        let id = selectedLine.id;
        let cfg = selectedLine.getMetaCfg();
        let props: SetConfigParam<IMetaInfoCfg> = {
          id,
          cfg,
          update: (nextCfg?: IMetaInfoCfg, preview?: boolean) => {
            let cfg = selectedLine.getMetaCfg();
            if (nextCfg !== cfg) {
              nextCfg && (selectedLine.setMetaCfg(nextCfg));
              if (!preview) {  this.changed(); }
              this.context.redraw(DrawOpt.overlay);
            }
            return (void 0);
          }
        };
        timeout(() => fn(props), 100);
      }
    }
    return !!selectedLine;
  }

  onMouseDown(point: Point): boolean {
    this.onMouseMove(point);
    let { matchedPoint, matchedLine } = this;
    let isMatched = !!matchedPoint || !!matchedLine;
    let selectedLine;
    if (matchedPoint) {
      matchedPoint.select();
      selectedLine = matchedPoint.line;
    }

    if (matchedLine) {
      matchedLine.select();
      selectedLine = matchedLine;
    }

    this.setSelectedPoint(matchedPoint); // 顺序？
    this.setSelectedLine(selectedLine);
    if (selectedLine && point) {
      this._resetDragTargetMap(selectedLine, point);
    }
    // flag selected
    selectedLine && (selectedLine.isEditing = true);
    matchedPoint && (matchedPoint.isEditing = true);
    return isMatched;
  }

  onMouseMove(point: Point): boolean {
    let isMatched = false;
    this.resetMatchedObj();
    let matchedPoint = this._matchedPoint(point);
    this.matchedPoint = matchedPoint;
    isMatched = !!matchedPoint;
    if (isMatched) {
      let highlight = this.setHighlightLine(matchedPoint.line);
      if (highlight) {
        matchedPoint.highlight();
      }
    } else {
      let matchedLine = this._matchedLine(point);
      this.matchedLine = matchedLine;
      isMatched = !!matchedLine;
      if (isMatched) {
        this.setHighlightLine(matchedLine);
      }
    }
    return isMatched;
  }

  onMouseUp(point: Point): boolean {
    let { selectedPoint, selectedLine } = this;
    let isMatched = !!selectedPoint || !!selectedLine;
    // reset selected
    selectedLine && (selectedLine.isEditing = false);
    selectedPoint && (selectedPoint.isEditing = false);
    if (isMatched) return true;
    // else return this.onMouseMove(point);
  }

  onDragStart(point: DragPoint): boolean {
    let { selectedPoint, selectedLine } = this;
    let isMatched = !!selectedPoint || !!selectedLine;
    let state = selectedPoint ? CTRL_POINT : selectedLine ? OBJECT : EMPTY;
    if (state) {
      let currentMouse = this.context.mouse();
      this._onDelayedDragStart.cancel();
      this._onDelayedDragStart(currentMouse);
    }
    return isMatched;
  }

  _onDelayedDragStart = debounce((lastMouse: IMouseXY) => {
    let mouse = this.context.mouse();
    let { magnifier } = this.context.displayOption;
    let {touching, drawedDragging} = this.context.state;
    if ((touching === ContextState.Touching.START || (touching === undefined && magnifier)) && !drawedDragging) {
      if (mouse === lastMouse ||(mouse.x === lastMouse.x && mouse.y === lastMouse.y )) {
        let { selectedPoint, selectedLine } = this;
        let state = selectedPoint ? CTRL_POINT : selectedLine ? OBJECT : EMPTY;
        this.context.state.drawedDragging = state;
        if (state) {
          this.context.redraw(DrawOpt.overlay);
        }
      }
    }
  }, 300)

  onDrag(point: DragPoint): boolean {
    let { selectedPoint, selectedLine } = this;
    let state = selectedPoint ? CTRL_POINT : selectedLine ? OBJECT : EMPTY;
    this.context.state.drawedDragging = state;
    let moved;
    if (selectedPoint) {
      this._moveSelectedPoint(selectedPoint, point);
      moved = true;
    } else if (selectedLine) {
      this._moveSelectedLine(selectedLine, point);
      moved = true;
    }
    return moved;
  }

  onDragCancel(point): boolean {
    let { selectedPoint, selectedLine } = this;
    // reset selected
    selectedLine && (selectedLine.isEditing = false);
    selectedPoint && (selectedPoint.isEditing = false);


    this._onDelayedDragStart.cancel();
    if (this.context.state.drawedDragging) {
      this.context.state.drawedDragging = EMPTY;
    }
    return false;
  }

  onDragEnd(point): boolean {
    let { selectedPoint, selectedLine } = this;
    // reset selected
    selectedLine && (selectedLine.isEditing = false);
    selectedPoint && (selectedPoint.isEditing = false);

    if (this.context.state.drawedDragging) {
      this.changed();
      this.context.state.drawedDragging = EMPTY;
    }
    return false;
  }

  onKeyUp(event): boolean {
    let code = event.keyCode;
    if (code === KEY_CODE.DELETE || code === KEY_CODE.BACKSPACE) {
      let line = this.selectedLine;
      if (line) {
        return this.remove(line);
      }
    }
    return false;
  }

  // load
  load(linesJSON: any[]) {
    if (!linesJSON) return;
    let scaleY = this.context.getScaleY();
    let yDomain = scaleY.domain();
    if (!yDomain[0] && !yDomain[1]) { // TODO (2018/03/07 17:38) chart.ready
      // domainy is not init
      timeout(() => {
        this.load(linesJSON);
        this.context.redraw(DrawOpt.overlay);
      }, 100);
      return;
    }

    let list = linesJSON.map((lineJSON) => {
      let points = lineJSON.p;
      let name = lineJSON.n as ToolLineType;
      let meta = lineJSON.m as IMetaInfoCfg;
      let line = createToolline(this.context, name);
      line.fromJSON(points, meta);
      return line;
    });
    this._list = list;
  }

  refreshLines() {
    this.each(line => line.refresh());
  }
}
