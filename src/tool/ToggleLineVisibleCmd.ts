import { Command, IBaseContext, ContextState } from '../lib';
import BaseCmd from './BaseCmd';

export default class ToggleLineVisibleCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.TOGGLE_LINE_VISIBLE];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    let state = this.context.state;
    let Drawed = ContextState.Drawed;
    if (!state.drawed) state.drawed = Drawed.NORMAL;
    state.drawed ^= Drawed.HIDDEN;
    return true;
  }
}
