import { Command, IBaseContext } from '../lib';
import BaseCmd from './BaseCmd';

export default class FlipYCmd extends BaseCmd {
  constructor(context: IBaseContext, config?: object) {
    let name = Command[Command.FLIP_Y];
    super(context, name, config);
  }

  doProcess(arg?: object): boolean {
    let chart = this.context.getMainChart();
    let flipY = chart.flipY();
    chart.flipY(!flipY);
    return true;
  }
}
