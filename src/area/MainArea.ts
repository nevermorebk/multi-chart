import SeriesArea from './SeriesArea';
import { AreaProps } from './Area';
import { IToolLine, ContextState } from '../lib';
import SelectionGroup from '../tool/SelectionGroup';
import { ChartMode, CursorStyle, DrawOpt, IChartContext, Layout } from '../lib';
import SelectionLabelGroup from '../feature/SelectionLabelGroup';
import EventLabel from '../feature/EventLabel';
import { Feature } from '../feature/Feature';

export default class MainArea extends SeriesArea {
  lineGroup: SelectionGroup<IToolLine>;
  labelGroup: SelectionLabelGroup<EventLabel>;
  constructor(context: IChartContext, props: AreaProps, layout: Layout) {
    super(context, props, layout);
    this._createGroup();

    this.context.on('moreDataLoaded', () => {
      if (this.lineGroup) {
        this.lineGroup.refreshLines();
        this.context.redraw();
      }
    });
  }

  _createGroup() {
    this.lineGroup = new SelectionGroup<IToolLine>(this.context, 'tool_lines_group');
    // this.labelGroup = new SelectionLabelGroup<EventLabel>(this.context, 'label_group');
  }

  clearGroup() {
    this._createGroup();
  }
  loadLines(json) {
    this.lineGroup.load(json);
  }

  saveLines() {
    return this.lineGroup.toJSON();
  }

  drawFeatures(ctx: Context2D, isOverlay = false) {
    // this.labelGroup.clear();
    return super.drawFeatures(ctx, isOverlay);
  }

  drawFeature(ctx: Context2D, feature: Feature): void {
    feature.draw(ctx);

    // todo 2020年08月10日 next time
    // if (feature.isEventable) {
    //   let labels = feature.getEventlabels();
    //   if (labels) labels.forEach(label => this.labelGroup.add(label));
    // }
  }

  draw(ctx?: Context2D): void {
    ctx = ctx || this.context.canvas.ctx;
    this.preDraw(ctx);
    this.drawGridline(ctx);
    this.drawSeries(ctx);
    this.drawFeatures(ctx);
    this.drawBorder(ctx);
    this.postDraw(ctx);
  }

  redraw(drawOpt: DrawOpt): void {
    if (drawOpt.isAll) {
      let ctx = this.context.canvas.ctx;
      this.draw(ctx);
    }
    let ctx = this.context.overlayCanvas.ctx;
    this.preDraw(ctx);
    this.drawLines(ctx);
    this.drawFeatures(ctx, true);
    this.drawBorder(ctx); // TODO
    this.postDraw(ctx);
  }

  // TODO: MV TO mainarea , need refactor draw/redraw
  drawLines(ctx: Context2D) {
    let loading = this.context.state.loading;
    let hasData = this.context.allData.length > 0;
    let drawed = this.context.state.drawed || ContextState.Drawed.NORMAL;
    let drawedHidden = drawed & ContextState.Drawed.HIDDEN;
    if (hasData && !loading && !drawedHidden && this.lineGroup.size()) {
      this.lineGroup.draw(ctx);
    }
  }

  _passEventToSelection(method: string, ...args: any[]): boolean {
    let needRedraw = false;
    // if (this.labelGroup[method]) {
    //   needRedraw = this.labelGroup[method](...args);
    // }
    if (!needRedraw && this.lineGroup[method]) {
      needRedraw = this.lineGroup[method](...args);
    }
    if (needRedraw) {
      this.context.redraw(DrawOpt.overlay); // TODO: refactor redraw
    }
    return needRedraw;
  }

  onClick(point) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      let tool = context.getCurTool();
      if (tool) {
        let redraw = tool.onClick(point);
        if (!redraw) return;
      }
    }
    super.onClick(point);
  }

  onDblClick(point, event?: MouseEvent) {
    let redraw = false;
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      let tool = context.getCurTool();
      if (tool) {
        let redraw = tool.onDblClick(point);
        if (!redraw) return;
      }
    } else {
      redraw = this._passEventToSelection('onDblClick', point, event);
    }
    context.setCursor(CursorStyle.DEFAULT);
    !redraw && super.onDblClick(point);
  }

  onMouseMove(point, event?: MouseEvent) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      let tool = context.getCurTool();
      if (tool) {
        let redraw = tool.onMouseMove(point, event);
        if (!redraw) return;
      }
    } else {
      let redraw = this._passEventToSelection('onMouseMove', point, event);
      context.setCursor(redraw ? CursorStyle.POINTER : CursorStyle.DEFAULT);
    }
    super.onMouseMove(point);
  }

  onMouseDown(point, event?: MouseEvent) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      let tool = context.getCurTool();
      if (tool) {
        let redraw = tool.onMouseDown(point, event);
        if (!redraw) return;
      }
    } else {
      let redraw = this._passEventToSelection('onMouseDown', point, event);
      if (redraw) {
        context.setCursor(CursorStyle.MOVE);
      }
    }
    super.onMouseDown(point);
  }

  onMouseUp(point, event?: MouseEvent) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      let tool = context.getCurTool();
      if (tool) {
        let redraw = tool.onMouseUp(point, event);
        if (!redraw) return;
      }
    } else {
      let redraw = this._passEventToSelection('onMouseUp', point, event);
      context.setCursor(redraw ? CursorStyle.MOVE : CursorStyle.DEFAULT);
    }
    super.onMouseUp(point);
  }

  onDragStart(point) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      if (context.state.cmdProcing) return;
      // same as mousedown
    } else {
      let redraw = this._passEventToSelection('onDragStart', point);
    }
    super.onDragStart(point);
  }

  onDrag(point, event) {
    let redraw = false;
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      let tool = context.getCurTool();
      if (tool) {
        redraw = tool.onDrag(point, event);
      }
    } else {
      redraw = this._passEventToSelection('onDrag', point);
    }
    if (redraw) {
      context.mouse({
        chartId: this.context.chartId,
        ...point,
      });
      context.redraw(DrawOpt.overlay);
    }
    !redraw && super.onDrag(point, event);
  }

  onDragCancel(point) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      if (context.state.cmdProcing) return;
      // same as mouseup
    } else {
      let redraw = this._passEventToSelection('onDragCancel', point);
    }
    super.onDragCancel(point);
  }

  onDragEnd(point) {
    let context = this.context;
    if (context.getChartMode() === ChartMode.DRAWTOOL) {
      if (context.state.cmdProcing) return;
      // same as mouseup
    } else {
      let redraw = this._passEventToSelection('onDragEnd', point);
    }
    super.onDragEnd(point);
  }

  onKeyUp(event) {
    let redraw = this._passEventToSelection('onKeyUp', event);
    if (redraw) {
      this.context.redraw(DrawOpt.overlay);
    }
  }
}
