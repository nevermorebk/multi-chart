import { DragPoint, Point, DrawOpt, Layout } from '../lib';
import EventAdapter from '../event/EventAdapter';
import {IChartContext, Rect, IArea, RelationPoint } from '../lib';
import { Feature } from '../feature/Feature';
import { contains } from '../util/geometry';
import { forEach } from '../util/array';

export type AreaProps = {
  features?: Feature[];
};

export type AreaPartialProps = Partial<AreaProps>;

export default abstract class Area extends EventAdapter implements IArea {
  features: Feature[];
  protected rect: Rect;
  layout: Layout;
  readonly context: IChartContext;
  constructor(context: IChartContext, props: AreaProps, layout: Layout) {
    super();
    this.context = context;
    this.features = props.features || [];
    this.layout = layout;
  }

  setRect(rect: Rect) {
    this.rect = rect;
  }

  getRect() {
    return this.rect;
  }

  contains(xy) {
    return contains(xy, this.rect);
  }

  getRelationPoint(point: Point & DragPoint): RelationPoint {
    let rp: RelationPoint = {
      x: point.x - this.rect.left,
      y: point.y - this.rect.top,
    };
    if (point.dx !== undefined) {
      rp.dx = point.dx;
      rp.dy = point.dy;
    }
    return rp;
  }

  // TODO: maybe need a static util method across transaction
  // TODO: try stay scale = 1, width *= dpr
  preDraw(ctx: Context2D) {
    let scale = ctx.getDPR();
    let { left, top, width, height } = this.rect;
    ctx.save();
    ctx.beginPath();
    ctx.rect(left, top, width, height);
    ctx.clip();
    ctx.clearRect(left, top, width, height);
    if (scale >= 1.5) {
      ctx.translate(left, top);
    } else {
      ctx.translate(left + 0.5, top + 0.5);
    }
  }

  postDraw(ctx: Context2D) {
    ctx.restore();
  }

  abstract draw(ctx?: Context2D): void;

  abstract redraw(drawOpt: DrawOpt): void;

  abstract drawFeature(ctx: Context2D, feature: Feature): void;

  // TODO: rm to Chart?
  drawFeatures(ctx: Context2D, isOverlay = false) {
    let features = this.features;
    if (!features) return;
    forEach(features.filter(f => f.isOverlay === isOverlay), feature => {
      if (feature.shouldDisplay()) this.drawFeature(ctx, feature);
    });
  }

  update(props: AreaPartialProps = {}): void {
    if (props.features) {
      this.features = props.features;
    }
  }

  addFeature(feature) {
    this.features.push(feature);
  }

  removeFeature(feature) {
    let index = this.features.indexOf(feature);
    if (index >= 0) {
      this.features.splice(index, 1);
    }
  }

  destroy() {
    this.features = [];
  }
}
