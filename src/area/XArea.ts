import { identity as d3ZoomIdentity } from 'd3-zoom/src/transform';
import Area, { AreaPartialProps } from './Area';
import { CursorStyle, DrawOpt, DragPoint, ZoomPoint } from '../lib';

import XAxis from './axis/XAxis';
import { Feature } from '../feature/Feature';

export default class XArea extends Area {
  draw(ctx?: Context2D): void {
    ctx = ctx || this.context.canvas.ctx;
    super.preDraw(ctx);
    this.drawXAxis(ctx);
    this.drawFeatures(ctx);
    super.postDraw(ctx);
  }

  redraw(drawOpt: DrawOpt): void {
    if (drawOpt.isAll) {
      let ctx = this.context.canvas.ctx;
      this.draw(ctx);
    }
    let ctx = this.context.overlayCanvas.ctx;
    this.preDraw(ctx);
    this.drawFeatures(ctx, true);
    this.postDraw(ctx);
  }

  drawFeature(ctx: Context2D, feature: Feature): void {
    feature.drawXArea(ctx);
  }

  update(props: AreaPartialProps = {}): void {
    super.update(props);
  }

  drawXAxis(ctx: Context2D) {
    let at = this.layout;
    let xAxis = new XAxis(this.context, this.rect, at);
    xAxis.draw();
  }

  onMouseMove(point) {
    this.context.mouse(null);
    this.context.setCursor(CursorStyle.EW_RESIZE);
  }

  onWheel(point: ZoomPoint) {
    let { x, k } = point;
    if (k <= 0) return this._onDrag(point);
    let { rezoom, tryUpdateZoom, getScaleX } = this.context;
    let scaleX = getScaleX();
    let newK = tryUpdateZoom(k);
    let t = d3ZoomIdentity
      .translate(x, 0)
      .scale(newK)
      .translate(-x, 0);
    rezoom(t, { scaleX });
  }

  onDragStart(point) {
    // this.context.setCursor(CursorStyle.EW_RESIZE);
  }

  onDrag(point: DragPoint) {
    return this._onDrag(point);
  }

  _onDrag(point: DragPoint) {
    // how to limit scale extent here
    let context = this.context;
    let { dx: delta, x } = point;
    if (delta === 0) return;
    let { chartWidth: width, rezoom, getScaleX, tryUpdateZoom } = context;
    let scaleX = getScaleX();
    let k = 1;
    // let qx = Math.abs(delta) / Math.abs(width - x) * width;
    let qx = Math.abs(delta) * 3;
    if (delta < 0) {
      k = width / Math.abs(width - qx);
    } else {
      k = Math.abs(width - qx) / width;
    }
    let newK = tryUpdateZoom(k);
    let t = d3ZoomIdentity
      .translate(x, 0)
      .scale(newK)
      .translate(-x, 0);
    rezoom(t, { scaleX });
  }

  onDragCancel(point) {
    // this.context.setCursor(CursorStyle.DEFAULT);
  }

  onDragEnd(point) {
    // this.context.setCursor(CursorStyle.DEFAULT);
  }

  // onDblClick(event: any) {
  //   console.log('todo zoom to default level');
  // }
}
