import Area from './Area';
import { IChartContext, Layout, DrawOpt } from '../lib';

export default class EmptyArea extends Area {
  constructor(context: IChartContext, layout: Layout) {
    super(context, {}, layout);
  }

  draw(ctx?: Context2D): void {}

  redraw(drawOpt: DrawOpt): void {}

  drawFeature(ctx: Context2D, feature): void {}
}
