import { identity as d3ZoomIdentity } from 'd3-zoom/src/transform';
import Area, { AreaProps, AreaPartialProps } from './Area';
import { CursorStyle, Point, ScaleOpts, IChartContext, SeriesParams, DrawOpt, DragPoint } from '../lib';

import { Multi } from '../series/Multi';
import { isEmptyValue, isNumber } from '../util/is';
import { Gridline } from '../annotation/Gridline';
import { dom } from '../util';
import { YAxis } from './axis';
import { timeout } from '../util/index';
import { Feature } from '../feature/Feature';
import { last } from '../util/data';
import { isTouchDevice } from '../util';

export default class YArea extends Area {
  private _clickCount = 1;
  draw(ctx?: Context2D): void {
    ctx = ctx || this.context.canvas.ctx;
    super.preDraw(ctx);
    this.drawXAxis(ctx);
    this.drawFeatures(ctx);
    super.postDraw(ctx);
  }

  redraw(drawOpt: DrawOpt): void {
    if (drawOpt.isAll) {
      let ctx = this.context.canvas.ctx;
      this.draw(ctx);
    }
    let ctx = this.context.overlayCanvas.ctx;
    this.preDraw(ctx);
    this.drawFeatures(ctx, true);
    this.postDraw(ctx);
  }

  drawFeature(ctx: Context2D, feature: Feature): void {
    feature.drawYArea(ctx);
  }

  // update(props: AreaPartialProps = {}): void {
  //   super.update(props);
  // }

  drawXAxis(ctx: Context2D) {
    let at = this.layout;
    let yAxis = new YAxis(this.context, this.rect, at);
    yAxis.draw();
  }

  onMouseMove(point: Point) {
    this.context.mouse(null);
    this.context.setCursor(CursorStyle.NS_RESIZE);
  }

  onClick(point) {
    this._clickCount++;
  }

  onDragStart(point) {
    // let _clickCount = this._clickCount;
    // timeout(() => {
    //   if (_clickCount === this._clickCount && this.context.state.dragging) this.context.setCursor(CursorStyle.NS_RESIZE);
    // }, 200);
  }

  onDrag(point: DragPoint) {
    let { dy: delta } = point;
    if (delta === 0) return;
    if (isTouchDevice()) return;
    let { chartHeight: height, rezoom, getScaleY, flipY } = this.context;
    let k = 1;
    // check delta ,can't gt half of height
    let _delta = Math.min(Math.floor(height / 2) - 1, Math.abs(delta));
    if (delta < 0) {
      // zoomin
      k = height / (height - _delta * 2);
    } else {
      // zoomout
      k = (height - _delta * 2) / height;
    }
    let t = d3ZoomIdentity
      .translate(0, height / 2)
      .scale(k)
      .translate(0, -height / 2);

    let scaleY = getScaleY();

    if (!flipY()) {
      flipY(true);
    }
    rezoom(t, { scaleY });
  }

  onDragCancel(point) {
    // this.context.setCursor(CursorStyle.DEFAULT);
  }

  onDragEnd(point) {
    // this.context.setCursor(CursorStyle.DEFAULT);
  }

  onDblClick(point) {
    this.context.flipY(false);
    this.context.redraw();
  }

  onWheel(point) {
    let { k } = point;
    if (k <= 0) return;
    let { chartWidth, rezoom, getScaleX, tryUpdateZoom, data, xAccessor } = this.context;
    let scaleX = getScaleX();
    let dx = chartWidth;
    let newK = tryUpdateZoom(k);
    let t = d3ZoomIdentity
      .translate(dx, 0)
      .scale(newK)
      .translate(-dx, 0);
    rezoom(t, { scaleX });
  }
}
