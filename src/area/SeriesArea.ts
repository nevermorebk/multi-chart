import { identity as d3ZoomIdentity } from 'd3-zoom/src/transform';
import Area from './Area';
import { CursorStyle, IChartContext, SeriesParams, DrawOpt, ISeries } from '../lib';
import { Multi } from '../series/Multi';
import { forEach } from '../util';
import { Gridline } from '../annotation/Gridline';
import { Feature } from '../feature/Feature';
import * as draw from '../canvas/draw';
import { getTheme } from '../config/index';
import { KV } from '../config/meta';

export default class SeriesArea extends Area {
  draw(ctx?: Context2D): void {
    ctx = ctx || this.context.canvas.ctx;
    this.preDraw(ctx);
    this.drawGridline(ctx);
    this.drawSeries(ctx);
    this.drawFeatures(ctx);
    this.drawBorder(ctx);
    this.postDraw(ctx);
  }

  redraw(drawOpt: DrawOpt): void {
    if (drawOpt.isAll) {
      let ctx = this.context.canvas.ctx;
      this.draw(ctx);
    }
    let ctx = this.context.overlayCanvas.ctx;
    this.preDraw(ctx);
    this.drawFeatures(ctx, true);
    this.drawBorder(ctx); // TODO
    this.postDraw(ctx);
  }

  drawBorder(ctx: Context2D) {
    let { width, height } = this.rect;
    let top = 0,
      left = 0;
    let p1 = { x: left, y: top };
    let p2 = { x: left, y: height - top };
    let p3 = { x: width - left, y: top };
    let p4 = { x: width - left, y: height - top };
    let theme = getTheme(this.context.theme);
    let lines = [];
    if (left < width / 2) {
      // draw left border
      lines.push([p1, p2]);
    } else {
      // draw right border
      lines.push([p3, p4]);
    }

    if (top < height / 2) {
      // draw top border
      lines.push([p1, p3]);
    } else {
      // draw bottom border
      lines.push([p2, p4]);
    }

    draw.drawLine2(ctx, lines, draw.getCanvasLineStyle(theme.chart.border));
  }

  drawFeature(ctx: Context2D, feature: Feature): void {
    feature.draw(ctx);
  }

  drawGridline(ctx: Context2D) {
    let showGridline = this.context.displayOption.gridline;
    if (showGridline) {
      new Gridline(this.context, ctx).draw();
    }
  }

  drawSeries(ctx: Context2D) {
    let allData = this.context.allData;
    let series = this.context.series;
    if (allData && allData.length === 0) return;
    if (!series) return;
    let found = series
      .filter(_serier => _serier.seriesType && !_serier.config.hidden)
      .map((_series: SeriesParams) => {
        let { seriesType: SeriesCtrors, config, accessor: yAccessor } = _series;
        let styles: ReadonlyArray<KV>, key: string;
        let context = this.context;
        let seriesStyles;
        if (config) {
          styles = config.styles || [];
          key = config.name || 'data';
        }
        if (yAccessor) {
          context = Object.setPrototypeOf({ yAccessor }, context) as IChartContext;
        }
        seriesStyles = (styles || []).map(style => ({
          ...style, label: config.name
        })) as any;
        if (SeriesCtrors.length === 1) {
          // TODO: save series instance , not constructor
          return new SeriesCtrors[0](context, key, seriesStyles[0]);
        } else if (SeriesCtrors.length > 1) {
          return new Multi(context, key, seriesStyles).series(SeriesCtrors);
        }
      })
      .sort((s1: ISeries, s2: ISeries) => s1.order - s2.order);

    forEach(found, (s: ISeries) => s.draw(ctx));
  }

  onClick(point) {
    let context = this.context;
    context.mouse({ chartId: context.chartId, ...point });
    context.redraw(DrawOpt.overlay); // TODO: highlight indicator line ?
  }

  onDblClick(point) {
    let chartId = this.context.chartId;
    this.context.toggleMaximizeChart(chartId);
  }

  onMouseDown(point) {
    let context = this.context;
    context.mouse({ chartId: context.chartId, ...point });
    context.redraw(DrawOpt.overlay); // TODO: highlight indicator line ?
  }

  onMouseMove(point) {
    let context = this.context;
    context.mouse({ chartId: context.chartId, ...point });
    context.redraw(DrawOpt.overlay); // TODO: highlight indicator line ?
  }

  onMouseLeave() { }

  onDragStart(point) {
    let context = this.context;
    context.mouse({ chartId: context.chartId, ...point });
    context.setCursor(CursorStyle.MOVE);
  }

  onDragCancel(point) {
    this.context.mouse(null);
    this.context.setCursor(CursorStyle.DEFAULT);
  }

  onDragEnd(point) {
    this.context.mouse(null);
    this.context.setCursor(CursorStyle.DEFAULT);
  }

  onDrag(point, event) {
    return this._onDrag(point);
  }

  _onDrag(point) {
    let { x, y, dx, dy } = point;
    if (dx === 0 && dy === 0) return;
    let { height } = this.rect;
    let { rezoom, getScaleX, getScaleY, flipY, chartId } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    this.context.mouse({ chartId, x, y });

    let t = d3ZoomIdentity.scale(1).translate(dx, dy);

    rezoom(t, { scaleX, scaleY });
  }

  onWheel(point) {
    let { x, dx, k } = point;
    if (k <= 0) return this._onDrag(point);
    // x = this.rect.width / 2;
    x += dx;
    // normalized
    // k = k > 1 ? Math.max(1.1, k) : Math.min(0.9, k);
    let { rezoom, tryUpdateZoom, getScaleX } = this.context;
    let scaleX = getScaleX();
    let newK = tryUpdateZoom(k);
    let t = d3ZoomIdentity
      .translate(x, 0)
      .scale(newK)
      .translate(-x, 0);
    rezoom(t, { scaleX });
  }

  onPinch(point) {
    this.context.mouse(null);
    this.onWheel(point);
  }
}
