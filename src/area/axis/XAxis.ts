import { dom, forEach2, FnTickFormatter } from '../../util';
import { first, last, xAccessor, dateAccessor } from '../../util';
import { xTicks, xTickFormat } from '../../util';
import { ScaleOpts, ChartType, Layout } from '../../lib';
import Axis from './Axis';
import { getTheme } from '../../config/index';
import * as draw from '../../canvas/draw';
import { forEach, extent } from '../../util/array';

export default class XAxis extends Axis {
  getY: Function;

  initXYAccessor() {
    let at = this.at;
    let { height } = this.rect;
    this.getY =
      at === Layout.TOP
        ? y => {
            y = height - y;
            if (y >= height) y = height - 1;
            return y;
          }
        : y => y;
  }

  createTickFormat(): FnTickFormatter {
    let { chartType, tzOffset } = this.context;
    return xTickFormat(chartType, tzOffset);
  }

  drawLine() {
    let theme = getTheme(this.context.theme);
    let width = this.context.chartWidth;
    let ctx = this.context.canvas.ctx;
    let lineStyle = draw.getCanvasLineStyle(theme.chart.border);
    let lines = [];
    let y1 = this.getY(0);
    lines.push([{ x: 0, y: y1 }, { x: width, y: y1 }]);
    draw.drawLine2(ctx, lines, lineStyle);
  }

  drawTicks() {
    let data = this.context.data;
    let theme = getTheme(this.context.theme);
    let scaleX = this.context.getScaleX();
    let width = this.context.chartWidth;
    let height = this.context.chartHeight;
    let ticks = scaleX.ticks(xTicks(width));
    let ctx = this.context.canvas.ctx;
    let lineXs = ticks.map(t => scaleX(t));

    let fontStyle = {
      fillStyle: theme.chart.fontColor,
      font: theme.chart.baseFont,
      textAlign: 'center'
    };
    forEach(ticks, (d, i) => {
      draw.drawText(ctx, this.tickFormat(d, data), Math.round(lineXs[i]), height / 2, fontStyle);
    });
  }
}
