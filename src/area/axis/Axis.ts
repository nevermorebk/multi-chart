import { dom, timeout, FnTickFormatter } from '../../util';
import { IComponent, ScaleOpts, IChartContext, Rect, IDrawable, Layout } from '../../lib';

export default abstract class Axis implements IDrawable {
  tickFormat: FnTickFormatter;
  protected rect: Rect;
  readonly context: IChartContext;
  at: Layout;
  constructor(context: IChartContext, rect: Rect, at: Layout) {
    this.context = context;
    this.rect = rect;
    this.at = at;
    this.initXYAccessor();
    this.tickFormat = this.createTickFormat();
  }

  draw() {
    this.drawLine();
    let allData = this.context.allData;
    if (allData && allData.length === 0) return;
    this.drawTicks();
  }

  abstract initXYAccessor(): void;

  abstract drawLine(): void;

  abstract drawTicks(): void;

  abstract createTickFormat(): FnTickFormatter;
}
