import { dom, FnTickFormatter } from '../../util';
import { yTicks, yTickFormat } from '../../util';
import { forEach } from '../../util/array';
import { ScaleOpts, ChartType, Layout } from '../../lib';
import { getTheme } from '../../config/index';
import * as draw from '../../canvas/draw';
import Axis from './Axis';
import { drawLine } from '../../canvas/draw';

const MIN_PADDING = 8;

export default class YAxis extends Axis {
  getX: Function;

  initXYAccessor() {
    let at = this.at;
    let { width } = this.rect;
    this.getX = at === Layout.RIGHT ? x => x : x => width - x;
  }

  createTickFormat(): FnTickFormatter {
    let { precisionScale } = this.context;
    return yTickFormat(precisionScale);
  }

  drawLine() {
    let { height } = this.rect;
    let theme = getTheme(this.context.theme);
    let ctx = this.context.canvas.ctx;
    let lineStyle = draw.getCanvasLineStyle(theme.chart.border);
    let lines = [];
    let x1 = this.getX(0);
    lines.push([{ x: x1, y: 0 }, { x: x1, y: height }]);

    draw.drawLine2(ctx, lines, lineStyle);
  }

  drawTicks() {
    let { width, height } = this.rect;
    let theme = getTheme(this.context.theme);
    let { getScaleY, precisionScale } = this.context;
    let tickSize = 2;
    let tickPadding = 6;
    let ctx = this.context.canvas.ctx;
    let scaleY = getScaleY();
    let ticks = scaleY.ticks(yTicks(height, scaleY.domain(), precisionScale));

    let fontStyle = {
      textAlign: 'right',
      textBaseline: 'middle',
      fillStyle: theme.chart.fontColor,
      font: theme.chart.baseFont,
      maxWidth: width - tickPadding - tickSize,
    };
    forEach(ticks, (d, i) => {
      let _y = Math.round(scaleY(d));
      if (_y < MIN_PADDING || _y + MIN_PADDING > height) return;
      let _x = width - tickPadding;
      draw.drawText(ctx, this.tickFormat(d), _x, _y, fontStyle);
    });
  }
}
