import ctrlSvgString from 'raw-loader!./assets/ctrl-sprite.svg';
import { dom } from '../util/dom';
import { IElement } from '../util/index';

export enum SpritesMap {
  close = 'icon-closesquareo',
  setting = 'icon-setting',
  arrowdown = 'icon-arrowdownsquare',
  arrowup = 'icon-arrowupsquare',
  resize02 = 'icon-resize_to_max',
  resize03 = 'icon-resize_to_normal',
}

export const ctrlSvg: IElement = dom.fromString(ctrlSvgString);

const defaultStyle = {
  display: 'inline-block',
  width: '1em',
  height: '1em',
  fill: 'currentColor',
  overflow: 'visible',
  'font-size': '14px',
  'pointer-events': 'bounding-box',
};
function createSvgIconString(icon: SpritesMap, styleObj = {}): string {
  styleObj = { ...defaultStyle, ...styleObj };
  let style = Object.keys(styleObj).reduce((styleStr, key) => {
    styleStr += `${key}: ${styleObj[key]};`;
    return styleStr;
  }, '');
  let svg = `<svg style="${style}">
    <defs><style type="text/css">
    .${icon} {
      transform-origin: center
    }
    .${icon}:hover {
      transform: scale3d(1.2, 1.2, 1)
    }
    .${icon}:active {
      transform: scale3d(1, 1, 1)
    }
    </style></defs>
    <use xlink:href="#${icon}" class="${icon}"></use>
  </svg>`;
  return svg;
}

export function createSvgIcon(icon: SpritesMap, styleObj = {}): HTMLElement {
  let svg = createSvgIconString(icon, styleObj);
  return dom.fromString(svg).node();
}

function createSvgIconFromString(svg: string): HTMLElement {
  return dom.fromString(svg).node();
}

const SpritesSvg = {
  close: createSvgIconString(SpritesMap.close),
  setting: createSvgIconString(SpritesMap.setting),
  arrowdown: createSvgIconString(SpritesMap.arrowdown),
  arrowup: createSvgIconString(SpritesMap.arrowup),
  resize02: createSvgIconString(SpritesMap.resize02),
  resize03: createSvgIconString(SpritesMap.resize03),
};

