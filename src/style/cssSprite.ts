import iconfontCss from '!!raw-loader!./assets/iconfont.css';
import { dom } from '../util/dom';

enum SpritesMap {
  close = 'chart-icon-close',
  setting = 'chart-icon-setting',
  arrowdown = 'chart-icon-arrowdown',
  arrowup = 'chart-icon-arrowup',
  resizeToMax = 'chart-icon-resize_to_max',
  resizeToNormal = 'chart-icon-resize_to_normal',
  eye = 'chart-icon-eye',
  closeEye = 'chart-icon-eye_close',
}

const getIconfontStyle = () =>
  dom
    .create('style')
    .html(iconfontCss)
    .node();

const defaultStyle = {};

function createFontIconString(icon: SpritesMap, styleObj = {}): string {
  styleObj = { ...defaultStyle, ...styleObj };
  let style = Object.keys(styleObj).reduce((styleStr, key) => {
    styleStr += `${key}: ${styleObj[key]};`;
    return styleStr;
  }, '');
  let html = `<i class="${icon}" style="${style}"></i>`;
  return html;
}

function createFontIcon(icon: SpritesMap, styleObj = {}): HTMLElement {
  let style = createFontIconString(icon, styleObj);
  return dom.fromString(style).node();
}

export { SpritesMap, createFontIcon, getIconfontStyle };
