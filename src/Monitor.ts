import { MultiChart } from './MultiChart';

export interface IMonitor {
  start(): void;
  stop(): void;
}


// capacity  need refactor
export class CapacityMonitor implements IMonitor {
  mchart: MultiChart;
  capacity: number;
  timer: number;
  constructor(chart: MultiChart, capacity = 3000) {
    this.mchart = chart;
    this.capacity = capacity;
  }

  stop() {
    this.mchart.off('moreDataLoaded', this._checkCapacity);
    clearInterval(this.timer);
  }

  start() {
    let mchart = this.mchart;
    mchart.on('moreDataLoaded', this._checkCapacity);
    this.timer = window.setTimeout(this._checkCapacity, 60000);
    this.start = () => { };
    mchart.on('destroy', () => this.stop());
  }

  _checkCapacity = () => {
    let mchart = this.mchart;
    let context = mchart.context;
    let length = context.allData.length;
    let capacity = this.capacity;
    if (length >= capacity) {
      // context.state.preventMore = true;
      if (context.isAutoShowLast()) {
        this._removeOverflowData();
        mchart.redraw();
      }
    }
    this.timer = window.setTimeout(this._checkCapacity, 60000);
  }

  _removeOverflowData() {
    // clip capacity * 0.2 from head
    let mchart = this.mchart;
    let capacity = this.capacity;
    let removeSize = Math.ceil(capacity * 0.2);
    // update standard data
    mchart.context.chartDataModel.initOrUpdateStandardData([], -removeSize);
    console.warn(`chart data is overflow! removed size: ${removeSize}.`);
  }

}
