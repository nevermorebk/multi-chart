import { IDrawable, IChartContext, Layout } from '../lib';
import EventLabel from './EventLabel';
export abstract class Feature {
  isOverlay = true;
  isEventable = false;

  readonly context: IChartContext;

  constructor(context: IChartContext) {
    this.context = context;
  }

  getEventlabels(ctx?: Context2D): EventLabel[] {
    return null;
  }

  abstract shouldDisplay(): boolean;

  abstract draw(ctx: Context2D): void;

  drawXArea(ctx: Context2D): void { }

  drawYArea(ctx: Context2D): void { }
}
