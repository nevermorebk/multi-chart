import Chart from '../chart/Chart';
import { DrawOpt, IChartContext } from '../lib';

// TODO (2018/03/29 21:16) react
export abstract class DomFeature {
  readonly context: IChartContext;
  readonly panel: HTMLElement;
  private hidden: boolean;
  constructor(context: IChartContext, panel: HTMLElement) {
    this.context = context;
    this.panel = panel;
    this.registerEvent(this.panel);
  }

  getChart(): Chart {
    let chartId = this.context.chartId;
    let chart = this.context.getChart(chartId);
    return chart;
  }

  draw() {
    this.drawHTML();
    // this.draw = () => {};
  }

  redraw(drawOpt: DrawOpt) {
    if (this.shouldDisplay()) {
      if (this.hidden || this.shouldRedraw(drawOpt)) {
        this.drawHTML();
      }
      this.hidden = false;
    } else {
      this.hide();
      this.hidden = true;
    }
  }

  shouldDisplay(): boolean {
    let allData = this.context.allData;
    let state = this.context.state;
    let enoughSpace = this.context.sizeOption.width > 250;
    return enoughSpace && allData && !!allData.length && !state.resizing;
  }

  abstract shouldRedraw(drawOpt: DrawOpt): boolean;

  abstract drawHTML(): void;

  abstract hide(): void;

  abstract destroy(): void;

  abstract registerEvent(panel: HTMLElement): void;
}
