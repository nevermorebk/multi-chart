import { DataItem, IChartContext, IDrawable, Point, Rect, DrawOpt, SeriesParams, ChartType, SetConfigParam } from '../lib';
import { dataAccessor } from '../util/data';
import { isDefined } from '../util/is';
import { numberFormat } from '../util/format';
import { getTheme, getI18n } from '../config';
import * as draw from '../canvas/draw';
import { forEach } from '../util/array';
import { dom, timeout } from '../util/index';
import { DomFeature } from './DomFeature';
import Chart from '../chart/Chart';
import { IndicatorProps, IndicatorConfig } from '../model/Template';
import { createFontIcon, SpritesMap } from '../style/cssSprite';
import * as indicator from '../indicator';

export type LabelConfig = {
  label: string;
  id: number;
  config: IndicatorConfig;
};
export class IndicatorLabel extends DomFeature {
  indicators: IndicatorProps[];
  theme: string;
  lang: string;
  container: HTMLElement;

  shouldDisplay() {
    let display = super.shouldDisplay();
    let indicatorLabel = this.context.displayOption.indicatorLabel;
    return display && indicatorLabel && this.context.chartType !== ChartType.TICK && this.context.series.length > 0;
  }

  shouldRedraw(drawOpt: DrawOpt): boolean {
    let context = this.context;
    let series = context.series;
    let theme = context.theme;
    let lang = context.lang;
    let chart = this.getChart();
    let chartConfig = context.template.getChartConfig(chart);
    if (drawOpt.isOverlay) return false;
    if (lang !== this.lang || theme !== this.theme || chartConfig.indicators !== this.indicators) {
      this.indicators = chartConfig.indicators;
      this.theme = theme;
      this.lang = lang;
      return true;
    }
    return false;
  }

  drawHTML(): void {
    let context = this.context;
    let panel = this.panel;
    let $panel = dom(panel);
    let $label = $panel.find('.chart-indicator-label');
    let theme = getTheme(context.theme);
    let t = getI18n(context.lang).t;
    let { color } = theme.label;
    let chart = this.getChart();

    if ($label) {
      $label.empty();
    } else {
      let top = this.getChart().isMainChart ? 16 : 0;
      $label = dom
        .create('ul')
        .addClass('chart-indicator-label')
        .css({
          display: 'inline-block',
          position: 'relative',
          'list-style': 'none',
          margin: '6px',
          padding: 0,
          top: `${top}px`,
          'line-height': '22px',
        })
        .appendTo(panel);

      this.container = $label.node();
    }

    let indicators = (this.indicators = context.template.getChartConfig(chart).indicators);
    let labels = indicators.reduce((r, prop) => {
      let config = prop.config || {};
      let name = prop.name;
      if (name === 'data') return r;
      let indicatorF = indicator[name];
      let label = indicatorF && typeof indicatorF.label === 'function' ? indicatorF.label(config, context) : '';
      // TODO: show every style key with color
      r.push({
        label: `${t(`indicators.${name}.name`)}  ${label}`,
        id: prop.id,
        config,
      });
      return r;
    }, []);
    let container = $label.node();
    forEach(labels, labelConfig => {
      this._drawIndicatorLabel(container, labelConfig);
    });
  }

  _drawIndicatorLabel(container: HTMLElement, labelConfig: LabelConfig): void {
    let { label, id, config } = labelConfig;
    let theme = getTheme(this.context.theme);
    let { color, backgroundColor } = theme.label;
    let hidden = labelConfig.config.hidden;
    let t = getI18n(this.context.lang).t;
    let settingIcon = createFontIcon(SpritesMap.setting, { 'background-color': backgroundColor });
    let closeIcon = createFontIcon(SpritesMap.close, { 'background-color': backgroundColor });
    let eyeIcon = createFontIcon(hidden ? SpritesMap.closeEye : SpritesMap.eye, { 'background-color': backgroundColor });

    dom
      .create('li')
      .addClass('indicator-label-item')
      .css({
        position: 'relative',
        color: `${color}`,
      })
      .append(
        dom
          .create('span')
          .css({
            display: 'inline-block',
            'margin-right': '80px',
            padding: '0 4px',
            'background-color': `${backgroundColor}`,
          })
          .html(label)
          .node(),
        dom
          .create('span')
          .addClass('label-controls')
          .css({
            display: 'inline-block',
            color: `${color}`,
            position: 'absolute',
            right: 0,
            top: '2px',
          })
          .append(
            dom
              .create('i')
              .addClass('setting')
              .append(settingIcon)
              .attr({
                id: id,
              })
              .css({
                'margin-right': '2px',
                cursor: 'pointer',
              })
              .attr({ title: t('label.indicatorLabel.setting') })
              .node(),
            dom
              .create('i')
              .addClass('visible')
              .append(eyeIcon)
              .attr({ id })
              .css({
                'margin-right': '2px',
                cursor: 'pointer',
              })
              .attr({ title: t('label.indicatorLabel.visible') })
              .node(),
            dom
              .create('i')
              .addClass('remove')
              .append(closeIcon)
              .attr({
                id: id,
              })
              .css({
                'margin-right': '2px',
                cursor: 'pointer',
              })
              .attr({ title: t('label.indicatorLabel.remove') })
              .node()
          )
          .node()
      )
      .appendTo(container);
  }

  hide() {
    let container = this.container;
    if (container) {
      dom(container).empty();
    }
  }

  destroy() {
    let container = this.container;
    if (container) {
      dom(container).remove();
    }
  }

  registerEvent(panel: HTMLElement): void {
    let chartId = this.context.chartId;
    let chart = this.context.getChart(chartId);
    let $panel = dom(panel);
    $panel
      .on('click', '.chart-indicator-label .setting', event => {
        let node = dom(event.target as HTMLElement).closest('setting');
        let id = Number(node.getAttribute('id'));
        let prop = this.indicators.find(config => config.id === id);
        if (prop && prop.config) {
          let fn = this.context.setIndicatorConfig;
          if (fn) {
            let props: SetConfigParam<IndicatorConfig> = {
              id,
              cfg: prop.config,
              update: this.context.updateIndicator.bind(this.context, id),
            };
            timeout(() => fn(props), 100);
          }
        }
      })
      .on('click', '.chart-indicator-label .remove', event => {
        let node = dom(event.target as HTMLElement).closest('remove');
        let id = Number(node.getAttribute('id'));
        timeout(() => {
          this.context.removeIndicator(id);
        }, 0);
      })
      .on('click', '.chart-indicator-label .visible', event => {
        let node = dom(event.target as HTMLElement).closest('visible');
        let id = Number(node.getAttribute('id'));
        let prop = this.indicators.find(config => config.id === id);
        if (prop) {
          let config = prop.config;
          config.hidden = !config.hidden;
          this.context.updateIndicator(id, config);
          this.context.redraw();
        }
      });
  }
}
