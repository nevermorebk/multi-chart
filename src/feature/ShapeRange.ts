import { DataItem, IChartContext, IDrawable, Point, Layout, ChartType, DrawedLineDashedStyle } from '../lib';
import { Feature } from './Feature';
import { xTickFormat, yTickFormat, extent, forEach, union } from '../util';
import { dataAccessor } from '../util/data';
import { getTheme, getI18n } from '../config/index';
import * as draw from '../canvas/draw';
import { timeFormat2, timeFormat } from '../util/format';
import { clamp } from '../util/math';

const MIN_COORD = -10;

export class ShapeRange extends Feature {
  createXTickFormat() {
    let { chartType, tzOffset } = this.context;
    return xTickFormat(chartType, tzOffset);
  }

  createYTickFormat() {
    let { precisionScale } = this.context;
    return yTickFormat(precisionScale, false);
  }

  createXLabelTimeFormat() {
    let { chartType, tzOffset } = this.context;
    return timeFormat2(chartType, tzOffset);
  }

  shouldDisplay() {
    return true; // this.context.displayOption.crosshair;
  }

  draw(ctx: Context2D): void {}

  drawXArea(ctx): void {
    let line = this.context.selectedLine;
    if (!line) return;
    let {
      data,
      sizeOption: { xHeight: height },
    } = this.context;
    let xTickFormat = this.createXTickFormat();
    let theme = getTheme(this.context.theme);
    let values = line.ctrlPoints.map(cp => cp.coord.displayIndex);
    let xScale = this.context.getScaleX();
    let [xmin, xmax] = xScale.range();
    if (values.length > 1) {
      let style = Object.assign({ fillStyle: 'rgba(110, 167, 247, 0.5)' });
      let [left, right] = extent(values)
        .map(xScale)
        .map(v => clamp(v, xmin, xmax));
      if (left === right) return;
      let rect = {
        top: 0,
        left: left,
        width: Math.abs(left - right),
        height: 20,
      };
      draw.drawRect2(ctx, [rect], style);
    }

    let textStyle = {
      textAlign: 'center',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
    };
    forEach(union(values), val => {
      let x = xScale(val);
      if (x !== clamp(x, xmin, xmax)) return;
      let labelText = xTickFormat(val, data, this.createXLabelTimeFormat());
      let width = labelText.length * theme.chart.baseFontSize * 0.7; // for number rect width
      let padding = 10;
      let rect = {
        left: x - width / 2,
        top: 0.5,
        width,
        height,
      };
      draw.drawRect2(ctx, [rect], { fillStyle: 'rgb(110, 167, 247)' });
      draw.drawText(ctx, labelText, x, height / 2, textStyle);
    });
  }

  drawYArea(ctx): void {
    let line = this.context.selectedLine;
    if (!line) return;
    let yTickFormat = this.createYTickFormat();
    let { yWidth: width } = this.context.sizeOption;
    let theme = getTheme(this.context.theme);
    let values = line.ctrlPoints.map(cp => cp.coord.value);
    let yScale = this.context.getScaleY();
    let [ymax, ymin] = yScale.range();
    if (values.length > 1) {
      let style = Object.assign({ fillStyle: 'rgba(110, 167, 247, 0.5)' });
      let [bottom, top] = extent(values)
        .map(yScale)
        .map(v => clamp(v, ymin, ymax));
      if (top === bottom) return;
      let rect = {
        left: 0.5,
        top: top,
        width,
        height: Math.abs(bottom - top),
      };
      draw.drawRect2(ctx, [rect], style);
    }

    let tickSize = 2;
    let tickPadding = 6;
    let height = 20;
    let textStyle = {
      textAlign: 'right',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
      maxWidth: width - tickSize - tickPadding,
    };

    forEach(union(values), val => {
      let y = yScale(val);
      if (y !== clamp(y, ymin, ymax)) return;
      let text = yTickFormat(val);
      let rect = {
        left: 0.5,
        top: y - height / 2,
        width,
        height,
      };

      draw.drawRect2(ctx, [rect], { fillStyle: 'rgb(110, 167, 247)' });
      draw.drawText(ctx, text, width - tickPadding, y, textStyle);
    });
  }
}
