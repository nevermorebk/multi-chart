import EventAdapter from '../event/EventAdapter';
import { IChartContext, IDrawable, Point } from '../lib';
import { forEach } from '../util';
import { timeout } from '../util/index';
import EventLabel from './EventLabel';


// Delegate & Manager
export default class SelectionLabelGroup<T extends EventLabel> extends EventAdapter implements IDrawable {
  context: IChartContext;
  groupName: string;
  selectedLabel: T;
  private matchedLabel: T;
  private labels: T[];

  constructor(context: IChartContext, groupName: string) {
    super();
    this.context = context;
    this.groupName = groupName;
    this.labels = [];
    this.selectedLabel = null;
    this.matchedLabel = null;
  }

  get(index: number): T {
    return this.labels[index];
  }

  size(): number {
    return this.labels.length;
  }

  add(label: T): void {
    this.labels.push(label);
  }

  remove(label: T): boolean {
    let index = this.labels.indexOf(label);
    if (this.selectedLabel === label) {
    }
    if (index > -1) {
      this.labels.splice(index, 1);
      this.changed();
      return true;
    }
    return false;
  }

  each(fn) {
    forEach(this.labels, fn);
  }

  clear(): boolean {
    if (this.labels.length === 0) return false;
    let result = !!this.labels.length;
    this.labels = [];
    if (this.selectedLabel) {
      this.selectedLabel = null;
      this.changed();
    }
    this.matchedLabel = null;
    return result;
  }

  draw(ctx?: Context2D) {
    ctx = ctx || this.context.overlayCanvas.ctx;
    forEach(this.labels, item => item.draw(ctx));
  }

  _matchedLabel(point): T {
    return this.labels.find(label => label.isHover(point));
  }

  // _matchedLine(point): T { }

  setSelectedLabel(label: T) {
    let selected = this.selectedLabel;
    if (selected) {
      selected.normal();
    }
    if (label) {
      label.select();
      this._sortLabels();
    }
    this.selectedLabel = label;
  }

  _sortLabels() {
    this.labels.sort((l1, l2) => l1.state - l2.state);
  }

  setHighlightLabel(label: T) {
    let { selectedLabel } = this;
    if (selectedLabel !== label) {
      label.highlight();
      return true;
    }
    return false;
  }

  changed() {
    timeout(() => {
      this.context.trigger('labelSelected', this.selectedLabel);
    }, 0);
  }

  // event delegate

  onClick(point: Point): boolean {
    // todo: contextmenu show
    return false;
  }

  onMouseDown(point: Point): boolean {
    this.onMouseMove(point);
    if (this.matchedLabel) {
      this.matchedLabel.select();
      timeout(() => {
        alert('todo action');
      });
    }
    return !!this.matchedLabel;
  }

  onMouseMove(point: Point): boolean {
    let isMatched = false;
    let matchedLabel = this._matchedLabel(point);
    isMatched = !!matchedLabel;
    if (isMatched) {
      this.matchedLabel = matchedLabel;
      matchedLabel.highlight();
    } else {
      this.matchedLabel = null;
    }
    return isMatched;
  }

  refreshLabels() {
    this.each(label => label.refresh());
  }
}
