import { DataItem, IChartContext, IDrawable, Point, Rect, DrawOpt } from '../lib';
import { dataAccessor } from '../util/data';
import { isDefined } from '../util/is';
import { numberFormat } from '../util/format';
import { getTheme, getI18n } from '../config';
import * as draw from '../canvas/draw';
import { forEach } from '../util/array';
import { dom, timeout } from '../util/index';
import { DomFeature } from './DomFeature';
import Chart from '../chart/Chart';
import { createFontIcon, SpritesMap } from '../style/cssSprite';

export class SwitchChart extends DomFeature {
  prevChart: Chart;
  nextChart: Chart;
  chartHeight: number;
  theme: string;
  lang: string;
  container: HTMLElement;

  shouldRedraw(drawOpt: DrawOpt): boolean {
    let context = this.context;
    let chartId = context.chartId;
    let theme = context.theme;
    let lang = context.lang;
    let prevChart = context.getPrevChart(chartId);
    let nextChart = context.getNextChart(chartId);
    let chartHeight = context.chartHeight;
    if (drawOpt.isOverlay) return false;
    // tslint:disable-next-line:max-line-length
    if (
      this.theme !== theme ||
      this.lang !== lang ||
      this.prevChart !== prevChart ||
      this.nextChart !== nextChart ||
      this.chartHeight !== chartHeight
    ) {
      this.prevChart = prevChart;
      this.nextChart = nextChart;
      this.chartHeight = chartHeight;
      this.theme = theme;
      this.lang = lang;
      return true;
    }
    return false;
  }

  drawHTML(): void {
    let panel = this.panel;
    let $panel = dom(panel);
    let theme = getTheme(this.context.theme);
    let $switch = $panel.find('.chart-panel-switch');
    let hasMaxChart = !!this.context.template.getMaximizeChartConfig();
    let hasManyChart = this.context.template.hasManyConfigCharts();
    let { color, backgroundColor } = theme.label;
    let t = getI18n(this.context.lang).t;
    if ($switch) {
      $switch
        .css({
          color: `${color}`,
        })
        .empty();
    } else {
      $switch = dom
        .create('div')
        .addClass('chart-panel-switch')
        .css({
          position: 'absolute',
          top: 0,
          right: 0,
          margin: '2px 2px 0 0',
          'font-size': '18px',
          'z-index': '3',
          color: `${color}`,
        })
        .appendTo(panel);

      this.container = $switch.node();
    }
    // create
    if (!hasMaxChart && this.prevChart && !this.prevChart.isTimelineChart) {
      let upIcon = createFontIcon(SpritesMap.arrowup, { 'background-color': backgroundColor });
      dom
        .create('i')
        .addClass('moveup')
        .append(upIcon)
        .css({
          'margin-right': '2px',
          cursor: 'pointer',
        })
        .attr({ title: t('label.switch.up') })
        .appendTo($switch.node());
    }
    if (!hasMaxChart && this.nextChart && !this.nextChart.isTimelineChart) {
      let downIcon = createFontIcon(SpritesMap.arrowdown, { 'background-color': backgroundColor });
      dom
        .create('i')
        .addClass('movedown')
        .append(downIcon)
        .css({
          'margin-right': '2px',
          cursor: 'pointer',
        })
        .attr({ title: t('label.switch.down') })
        .appendTo($switch.node());
    }
    if (hasManyChart) {
      if (hasMaxChart) {
        let resizeToNormalIcon = createFontIcon(SpritesMap.resizeToNormal, { 'background-color': backgroundColor });
        dom
          .create('i')
          .addClass('toggle')
          .append(resizeToNormalIcon)
          .css({
            'margin-right': '2px',
            cursor: 'pointer',
          })
          .attr({ title: t('label.switch.toggle') })
          .appendTo($switch.node());
      } else {
        let resizeToMaxIcon = createFontIcon(SpritesMap.resizeToMax, { 'background-color': backgroundColor });
        dom
          .create('i')
          .addClass('toggle')
          .append(resizeToMaxIcon)
          .css({
            'margin-right': '2px',
            cursor: 'pointer',
          })
          .attr({ title: t('label.switch.toggle') })
          .appendTo($switch.node());
      }
    }
  }

  hide() {
    let container = this.container;
    if (container) {
      dom(container).empty();
    }
  }

  destroy() {
    let container = this.container;
    if (container) {
      dom(container).remove();
    }
  }

  registerEvent(panel: HTMLElement): void {
    let chartId = this.context.chartId;
    let chart = this.context.getChart(chartId);
    let $panel = dom(panel);
    $panel
      .on('click', '.chart-panel-switch > .moveup', () => {
        timeout(() => {
          this.context.moveUpChart(chartId);
        }, 0);
      })
      .on('click', '.chart-panel-switch > .movedown', () => {
        timeout(() => {
          this.context.moveDownChart(chartId);
        }, 0);
      })
      .on('click', '.chart-panel-switch > .toggle', () => {
        timeout(() => {
          this.context.toggleMaximizeChart(chartId);
        }, 0);
      });
  }
}
