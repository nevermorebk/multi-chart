import { DataItem, IChartContext, IDrawable, Point, Layout, Domain } from '../lib';
import { Feature } from './Feature';
import { xTickFormat, yTickFormat, numberFormat } from '../util';
import { dataAccessor } from '../util/data';
import { getTheme, getI18n } from '../config/index';
import * as draw from '../canvas/draw';
import { forEach } from '../util/array';
import { isEmpty } from '../util/is';

export class PositionSummaryLines extends Feature {
  constructor(context: IChartContext) {
    super(context);
  }

  createYTickFormat() {
    let { precisionScale } = this.context;
    return yTickFormat(precisionScale);
  }

  shouldDisplay() {
    let { displayOption, symbol, chartDataModel } = this.context;
    let symbolId = chartDataModel.positionSummary && chartDataModel.positionSummary.symbolId;
    return displayOption.positionSummaryLine && symbol.id === symbolId;
  }

  _getPositionSummary() {
    let chartDataModel = this.context.chartDataModel;
    return chartDataModel.positionSummary;
  }

  _getUpArrowPoints() {
    let { chartWidth } = this.context;
    let x = chartWidth - 50,
      y = 10;
    let p0 = { x, y };
    let p1 = { x: x - 8, y: y + 12 };
    let p2 = { x: x - 8, y: y + 20 };
    let p3 = { x: x + 8, y: y + 20 };
    let p4 = { x: x + 8, y: y + 12 };
    return [p0, p1, p2, p3, p4];
  }

  _getDownArrowPoints() {
    let { chartWidth, chartHeight } = this.context;
    let x = chartWidth - 50,
      y = chartHeight - 10;
    let p0 = { x, y };
    let p1 = { x: x - 8, y: y - 12 };
    let p2 = { x: x - 8, y: y - 20 };
    let p3 = { x: x + 8, y: y - 20 };
    let p4 = { x: x + 8, y: y - 12 };
    return [p0, p1, p2, p3, p4];
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let [left, right] = scaleX.domain();
    let [bottom, top] = scaleY.domain();
    let prices = this._getPositionSummary();
    if (!prices) {
      return;
    }
    let x1 = Math.round(scaleX(left));
    let x2 = Math.round(scaleX(right));
    let t = getI18n(this.context.lang).prefix('lines');
    let theme = getTheme(this.context.theme);
    let tuple: Array<[number[], string, any]> = [];
    if (prices.sell) {
      tuple.push([[prices.sell], t('sellPositionSummary'), draw.getCanvasLineStyle(theme.sellPositionSummaryLine)]);
    }
    if (prices.buy) {
      tuple.push([[prices.buy], t('buyPositionSummary'), draw.getCanvasLineStyle(theme.buyPositionSummaryLine)]);
    }
    // set canvas font first
    ctx.font = theme.chart.baseFont;
    forEach(tuple, ([prices, label, lineStyle]) => {
      let lines = [];
      let labelWidth = ctx.measureText(label).width + 12;
      forEach(prices, price => {
        if (isEmpty(price)) {
          return console.warn(`invalid position summary : value is empty: ${price}! `);
        }
        let pointY = scaleY(price);
        pointY = Math.round(pointY);
        lines.push([{ x: x1, y: pointY }, { x: x2, y: pointY }]);
      });
      let price = prices[0];
      if (price >= top) {
        draw.drawArea(ctx, this._getUpArrowPoints(), { fillStyle: lineStyle.strokeStyle });
        return;
      } else if (price <= bottom) {
        draw.drawArea(ctx, this._getDownArrowPoints(), { fillStyle: lineStyle.strokeStyle });
        return;
      }
      draw.drawLine2(ctx, lines, lineStyle);
      let height = 20;
      let textStyle = {
        textAlign: 'right',
        textBaseline: 'middle',
        font: theme.chart.baseFont,
        fillStyle: theme.chart.foregroundColor,
        maxWidth: labelWidth - 6
      };
      forEach(lines, ([p1, p2]) => {
        let rect = {
          left: p2.x - labelWidth,
          top: p2.y - height / 2,
          width: labelWidth,
          height,
        };
        draw.drawRect2(ctx, [rect], { fillStyle: lineStyle.strokeStyle });
        draw.drawText(ctx, label, p2.x - 4, p2.y, textStyle);
      });
    });
  }

  drawYArea(ctx: Context2D) {
    let { yWidth: width } = this.context.sizeOption;
    let { getScaleX, getScaleY } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let [bottom, top] = scaleY.domain();
    let prices = this._getPositionSummary();
    if (!prices) {
      return;
    }
    let theme = getTheme(this.context.theme);
    let tuple: Array<[number[], any]> = [];
    let yTickFormat = this.createYTickFormat();
    let tickSize = 2;
    let tickPadding = 6;
    let textStyle = {
      textAlign: 'right',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
      maxWidth: width - tickSize - tickPadding,
    };
    if (prices.sell) {
      tuple.push([[prices.sell], { fillStyle: theme.sellPositionSummaryLine.color }]);
    }
    if (prices.buy) {
      tuple.push([[prices.buy], { fillStyle: theme.buyPositionSummaryLine.color }]);
    }
    forEach(tuple, ([prices, style]) => {
      forEach(prices, price => {
        if (price >= top || price <= bottom) return;
        let pointY = Math.round(scaleY(price));
        let text = yTickFormat(price);
        let height = 20;
        let rect = {
          left: 0.5,
          top: pointY - height / 2,
          width,
          height,
        };
        draw.drawRect2(ctx, [rect], style);
        draw.drawText(ctx, text, width - tickPadding, pointY, textStyle);
      });
    });
  }
}
