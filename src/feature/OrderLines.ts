import * as draw from '../canvas/draw';
import { getI18n, getTheme } from '../config/index';
import { IChartContext, BoundingBox } from '../lib';
import { yTickFormat } from '../util';
import { forEach } from '../util/array';
import { isEmpty } from '../util/is';
import { Feature } from './Feature';
import EventLabel from './EventLabel';

export class OrderLines extends Feature {
  isEventable = true;
  private labels: EventLabel[] = [];
  constructor(context: IChartContext) {
    super(context);
  }

  createYTickFormat() {
    let { precisionScale } = this.context;
    return yTickFormat(precisionScale);
  }

  shouldDisplay() {
    let { displayOption, symbol, chartDataModel } = this.context;
    let symbolId = chartDataModel.orders && chartDataModel.orders.symbolId;
    return displayOption.orderLine && symbol.id === symbolId;
  }
  getEventlabels(): EventLabel[] {
    return this.labels;
  }

  _getOrderPrices() {
    let chartDataModel = this.context.chartDataModel;
    return chartDataModel.orders;
  }

  _getUpArrowPoints() {
    let { chartWidth } = this.context;
    let x = chartWidth - 20,
      y = 10;
    let p0 = { x, y };
    let p1 = { x: x - 8, y: y + 12 };
    let p2 = { x: x + 8, y: y + 12 };
    return [p0, p1, p2];
  }

  _getDownArrowPoints() {
    let { chartWidth, chartHeight } = this.context;
    let x = chartWidth - 20,
      y = chartHeight - 10;
    let p0 = { x, y };
    let p1 = { x: x - 8, y: y - 12 };
    let p2 = { x: x + 8, y: y - 12 };
    return [p0, p1, p2];
  }

  draw(ctx: Context2D): void {
    this.labels = [];
    let { getScaleX, getScaleY } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let [bottom, top] = scaleY.domain();
    let [left, right] = scaleX.domain();
    let prices = this._getOrderPrices();
    let x1, x2;
    let tuple: Array<[number[], string, any]>;
    let t, theme;
    if (!prices) {
      return;
    }
    t = getI18n(this.context.lang).t;
    theme = getTheme(this.context.theme);

    tuple = [];
    if (prices.sell) {
      tuple.push([prices.sell, t('lines.sellOrder'), draw.getCanvasLineStyle(theme.sellOrderLine)]);
    }
    if (prices.buy) {
      tuple.push([prices.buy, t('lines.buyOrder'), draw.getCanvasLineStyle(theme.buyOrderLine)]);
    }
    x1 = Math.round(scaleX(left));
    x2 = Math.round(scaleX(right));
    // set canvas font first
    ctx.font = theme.chart.baseFont;
    forEach(tuple, ([prices, label, lineStyle]) => {
      let lines = [];
      let hasUp = false,
        hasDown = false;
      let labelWidth = ctx.measureText(label).width + 12;
      forEach(prices, price => {
        if (isEmpty(price)) {
          return console.warn(`invalid order : value is empty: ${price}! `);
        }
        let pointY = scaleY(price);
        pointY = Math.round(pointY);
        if (price <= bottom) hasDown = true;
        else if (price >= top) hasUp = true;
        else lines.push([{ x: x1, y: pointY }, { x: x2, y: pointY }]);
      });
      if (hasUp) {
        draw.drawArea(ctx, this._getUpArrowPoints(), { fillStyle: lineStyle.strokeStyle });
      }
      if (hasDown) {
        draw.drawArea(ctx, this._getDownArrowPoints(), { fillStyle: lineStyle.strokeStyle });
      }
      draw.drawLine2(ctx, lines, lineStyle);
      let height = 20;
      let textStyle = {
        textAlign: 'right',
        textBaseline: 'middle',
        font: theme.chart.baseFont,
        fillStyle: theme.chart.foregroundColor,
        maxWidth: labelWidth - 6
      };
      forEach(lines, ([p1, p2]) => {
        let rect = {
          left: p2.x - labelWidth,
          top: p2.y - height / 2,
          width: labelWidth,
          height,
        };
        draw.drawRect2(ctx, [rect], { fillStyle: lineStyle.strokeStyle });
        draw.drawText(ctx, label, p2.x - 4, p2.y, textStyle);
        this.labels.push(new EventLabel(this.context, rect, {
          name: 'order',
          type: 'todo'
        }));
      });
    });
  }

  drawYArea(ctx: Context2D) {
    let { yWidth: width } = this.context.sizeOption;
    let { getScaleX, getScaleY } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let [bottom, top] = scaleY.domain();
    let prices = this._getOrderPrices();
    let theme;
    let textStyle;
    let tuple: Array<[number[], any]>;
    let yTickFormat;
    let tickSize = 2;
    let tickPadding = 6;
    if (!prices) {
      return;
    }
    theme = getTheme(this.context.theme);
    textStyle = {
      textAlign: 'right',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
      maxWidth: width - tickSize - tickPadding,
    };
    tuple = [];
    if (prices.sell) {
      tuple.push([prices.sell, { fillStyle: theme.sellOrderLine.color }]);
    }
    if (prices.buy) {
      tuple.push([prices.buy, { fillStyle: theme.buyOrderLine.color }]);
    }
    yTickFormat = this.createYTickFormat();
    forEach(tuple, ([prices, style]) => {
      forEach(prices, price => {
        if (isEmpty(price)) return;
        if (price >= top || price <= bottom) return;
        let pointY = Math.round(scaleY(price));
        let text = yTickFormat(price);
        let height = 20;
        let rect = {
          left: 0.5,
          top: pointY - height / 2,
          width,
          height,
        };
        draw.drawRect2(ctx, [rect], style);
        draw.drawText(ctx, text, width - tickPadding, pointY, textStyle);
      });
    });
  }
}
