import { DataItem, IChartContext, IDrawable, Point, Layout, Domain, Side } from '../lib';
import { Feature } from './Feature';
import { xTickFormat, yTickFormat, numberFormat } from '../util';
import { dataAccessor } from '../util/data';
import { getTheme } from '../config/index';
import * as draw from '../canvas/draw';

export class CurrentQuote extends Feature {
  constructor(context: IChartContext) {
    super(context);
  }

  createYTickFormat() {
    let { precisionScale } = this.context;
    return yTickFormat(precisionScale);
  }

  _getLastPrice() {
    let { allData, yAccessor } = this.context;
    let { getScaleY } = this.context;
    let scaleY = getScaleY();
    let [bottom, top] = scaleY.domain();
    let quote = null;
    for (let i = allData.length - 1; i >= 0; i--) {
      let dataItem = yAccessor(allData[i]);
      if (dataItem.data) {
        quote = dataItem.data.close;
        break;
      }
    }
    if (quote) {
      if (quote > bottom || quote < top) {
        return quote;
      } else {
        return null;
      }
    }
    return quote;
  }

  shouldDisplay() {
    let currentLine = this.context.displayOption.currentLine;
    return currentLine;
  }

  draw(ctx: Context2D): void {
    let theme = getTheme(this.context.theme);
    let { chartSide, getScaleX, getScaleY } = this.context;
    // let lineColor = chartSide === Side.BID ? theme.candle.downColor : theme.candle.upColor;
    let lineColor = theme.currentPriceLine.color;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let [left, right] = scaleX.domain();
    let quote = this._getLastPrice();
    let pointY;
    let lines;
    if (!quote) {
      return;
    }

    pointY = scaleY(quote);
    lines = (() => {
      pointY = Math.round(pointY);
      left = Math.round(scaleX(left));
      right = Math.round(scaleX(right));
      let coords = [[{ x: left, y: pointY }, { x: right, y: pointY }]];
      return coords;
    })();
    let style = {
      ...theme.currentPriceLine,
      color: lineColor
    };
    draw.drawLine2(ctx, lines, draw.getCanvasLineStyle(style));
  }

  drawYArea(ctx: Context2D) {
    let theme = getTheme(this.context.theme);
    let { yWidth: width } = this.context.sizeOption;
    let { chartSide, getScaleX, getScaleY } = this.context;
    // let fillStyle = chartSide === Side.BID ? theme.candle.downColor : theme.candle.upColor;
    let fillStyle = theme.currentPriceLine.color;
    let quote = this._getLastPrice();
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let [left, right] = scaleX.domain();
    if (!quote) {
      return;
    }

    let pointY = scaleY(quote);
    let tickSize = 2;
    let tickPadding = 6;

    let yTickFormat = this.createYTickFormat();
    let text = yTickFormat(quote);
    let height = 20;
    let rect = {
      left: 0.5,
      top: pointY - height / 2,
      width,
      height,
    };
    let textStyle = {
      textAlign: 'right',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
      maxWidth: width - tickSize - tickPadding,
    };
    draw.drawRect2(ctx, [rect], { fillStyle });

    draw.drawText(ctx, text, width - tickPadding, pointY, textStyle);
  }
}
