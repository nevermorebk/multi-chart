import { DataItem, IChartContext, IDrawable, Point, Rect, DrawOpt, IMouseXY, ChartType } from '../lib';
import { dataAccessor } from '../util/data';
import { isDefined } from '../util/is';
import { numberFormat } from '../util/format';
import { getTheme, getI18n } from '../config';
import * as draw from '../canvas/draw';
import { forEach } from '../util/array';
import { dom } from '../util/index';
import { DomFeature } from './DomFeature';
import Chart from '../chart/Chart';
import { createFontIcon, SpritesMap } from '../style/cssSprite';

export class CrosshairOHLC extends DomFeature {
  mouse: IMouseXY;
  theme: string;
  lang: string;
  container: HTMLElement;
  chartWidth: number;

  shouldDisplay() {
    let display = super.shouldDisplay() && this.context.displayOption.crosshair;
    return display && this.context.isMainChart(this.context.chartId) && this._isDisplayMouse(this.context.mouse());
  }

  shouldRedraw(drawOpt: DrawOpt): boolean {
    let context = this.context;
    let theme = context.theme;
    let lang = context.lang;
    let mouse = context.mouse();
    let chartWidth = context.sizeOption.width;
    if (mouse !== this.mouse || theme !== this.theme || lang !== this.lang || chartWidth !== this.chartWidth) {
      this.mouse = mouse;
      this.theme = theme;
      this.lang = lang;
      this.chartWidth = chartWidth;
      return true;
    }
    return false;
  }

  _isDisplayMouse(mouse: IMouseXY): boolean {
    if (mouse && mouse.closest) return true;
    return false;
  }

  drawHTML(): void {
    let context = this.context;
    let panel = this.panel;
    let $panel = dom(panel);
    let width = context.sizeOption.width;
    let theme = getTheme(context.theme);
    let t = getI18n(context.lang).t;
    let mouse = context.mouse();
    let ohlcOrPrice = dataAccessor(mouse.closest);
    let $ohlc = $panel.find('.chart-panel-crosshair');
    if (!ohlcOrPrice) return;
    if ($ohlc) {
      $ohlc
        .css({
          color: `${theme.chart.fontColor}`,
        })
        .empty();
    } else {
      $ohlc = dom
        .create('div')
        .addClass('chart-panel-crosshair')
        .css({
          position: 'absolute',
          top: 0,
          margin: '4px 6px',
          color: `${theme.chart.fontColor}`,
        })
        .appendTo(panel);

      this.container = $ohlc.node();
    }
    // draw ohlc
    let text = '';
    let fmt = numberFormat(context.precisionScale);
    if (width > 600) {
      text += `${context.symbol.name} (${t('chartType.' + ChartType[context.chartType])}) `;
    }
    if (context.chartType === ChartType.TICK) {
      text += `${t('price')}: ${fmt(ohlcOrPrice.close)}`;
    } else {
      let sliceLength = width <= 450 ? 1 : undefined;
      text += `
      ${t('open').slice(0, sliceLength)}: ${fmt(ohlcOrPrice.open)}
      ${t('high').slice(0, sliceLength)}: ${fmt(ohlcOrPrice.high)}
      ${t('low').slice(0, sliceLength)}: ${fmt(ohlcOrPrice.low)}
      ${t('close').slice(0, sliceLength)}: ${fmt(ohlcOrPrice.close)}
      `;
    }

    dom
      .create('span')
      .addClass('chart-panel-crosshair-ohlc')
      .html(text)
      .appendTo($ohlc.node());
  }

  hide() {
    let container = this.container;
    if (container) {
      dom(container).empty();
    }
  }

  destroy() {
    let container = this.container;
    if (container) {
      dom(container).remove();
    }
  }

  registerEvent(panel: HTMLElement): void {}
}
