import EventAdapter from '../event/EventAdapter';
import { BoundingBox, IChartContext, IDrawable, ISelection, Point, Rect, SelectionState } from '../lib';
import { contains } from '../util/geometry';
const { HIDE, HIGHLIGHT, SHOW, SELECTED } = SelectionState;

export default class EventLabel extends EventAdapter implements ISelection, IDrawable {
  context: IChartContext;
  state: SelectionState;
  isEditing = false;
  rect: Rect;
  payload: any;
  constructor(context: IChartContext, rect: Rect, payload: any) {
    super();
    this.context = context;
    this.rect = rect;
    this.payload = payload;
    this.state = SHOW;
  }

  getBoundingBox(): BoundingBox {
    let x1 = this.rect.left;
    let y1 = this.rect.top;
    let x2 = x1 + this.rect.width;
    let y2 = y1 + this.rect.height;
    return [x1, y1, x2, y2];
  }
  setState(state: SelectionState): void {
    this.state = state;
  }
  getState(): SelectionState {
    return this.state;
  }
  isHover(xy: Point): boolean {
    return contains(xy, this.rect);
  }
  highlight(): void {
    this.setState(HIGHLIGHT);
  }
  select(): void {
    this.setState(SELECTED);
  }
  normal(): void {
    this.setState(HIDE);
  }

  draw(ctx: Context2D): void {
    // do nothing
    // draw.drawRect2(ctx, [this.rect], this.style);
  }
}
