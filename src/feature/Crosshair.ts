import { DataItem, IChartContext, IDrawable, Point, Layout, ChartType, DrawedLineDashedStyle } from '../lib';
import { Feature } from './Feature';
import { xTickFormat, yTickFormat } from '../util';
import { dataAccessor } from '../util/data';
import { getTheme, getI18n } from '../config/index';
import * as draw from '../canvas/draw';
import { timeFormat2, timeFormat } from '../util/format';

const MIN_COORD = -10;

export class Crosshair extends Feature {
  createXTickFormat() {
    let { chartType, tzOffset } = this.context;
    return xTickFormat(chartType, tzOffset);
  }

  createYTickFormat() {
    let { precisionScale } = this.context;
    return yTickFormat(precisionScale, false);
  }

  createXLabelTimeFormat() {
    let { chartType, tzOffset } = this.context;
    return timeFormat2(chartType, tzOffset);
  }

  shouldDisplay() {
    return this.context.displayOption.crosshair;
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY, chartHeight, chartWidth, state } = this.context;
    let { chartId: currChartId } = this.context;
    let theme = getTheme(this.context.theme);
    let mouse = this.context.mouse();
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let { x: pointX, y: pointY, chartId, closestX } = mouse;

    if (pointX < 0 && pointY < 0) return;
    if (chartId !== currChartId ||
      pointY < 8 || chartHeight - pointY < 8 || state.resizing
    ) pointY = MIN_COORD;

    let lines = (() => {
      let x = closestX > 0 ? closestX : -10;
      let y = pointY;
      let [top, bottom, left, right] = [0, chartHeight, 0, chartWidth];
      let coords = [
        [{ x: x, y: bottom }, { x: x, y: top }], [{ x: left, y: y }, { x: right, y: y }]
      ];
      if (x < 0 && y > 0) {  coords = [coords[1]];  }
      if (y < 0 && pointX > 0) {  coords = [coords[0]];  }
      return coords;
    })();

    draw.drawLine2(ctx, lines, draw.getCanvasLineStyle(theme.crosshair));
  }

  drawXArea(ctx: Context2D) {
    let { data, sizeOption: { xHeight: height } } = this.context;
    let theme = getTheme(this.context.theme);
    let mouse = this.context. mouse();
    let { x, closestX, closest } = mouse;
    if (x < 0 || closestX < 0 || !closest) return;

    let xTickFormat = this.createXTickFormat();
    let labelText = xTickFormat(closest.x, data, this.createXLabelTimeFormat());
    let width = labelText.length * theme.chart.baseFontSize * 0.7; // for number rect width
    let padding = 10;
    let rect = {
      left: closestX - width / 2,
      top: 0.5,
      width,
      height,
    };
    let textStyle = {
      textAlign: 'center',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
      // bgColor: 'rgb(255, 165, 0)',
    };

    draw.drawRect2(ctx, [rect], { fillStyle: 'rgb(255, 165, 0)' });
    draw.drawText(ctx, labelText, closestX, height / 2, textStyle);
  }

  drawYArea(ctx: Context2D) {
    let { chartId: currChartId, chartHeight, state } = this.context;
    let { yWidth: width } = this.context.sizeOption;
    let theme = getTheme(this.context.theme);
    let yScale = this.context.getScaleY();
    let mouse = this.context. mouse();
    let { y: pointY, chartId } = mouse;
    if (pointY < 0) return;
    if (chartId !== currChartId ||
      pointY < 8 || chartHeight - pointY < 8 || state.resizing
    ) return;

    let yTickFormat = this.createYTickFormat();
    let text = yTickFormat(yScale.invert(pointY));
    let tickSize = 2;
    let tickPadding = 6;
    let height = 20;
    let rect = {
      left: 0.5,
      top: pointY - height / 2,
      width,
      height,
    };
    let textStyle = {
      textAlign: 'right',
      textBaseline: 'middle',
      font: theme.chart.baseFont,
      fillStyle: theme.chart.foregroundColor,
      maxWidth: width - tickSize - tickPadding,
    };

    draw.drawRect2(ctx, [rect], { fillStyle: 'rgb(255, 165, 0)' });
    draw.drawText(ctx, text, width - tickPadding, pointY, textStyle);
  }
}
