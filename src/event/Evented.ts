import { IEvented } from '../lib';
import { forEach } from '../util/array';

export type IEventMap = {
  [key: string]: Function;
}
// TODO:  support namespace & logger
export default class Evented<M extends IEventMap> implements IEvented {
  private _listeners: {
    [P in keyof M]?: Function[];
  };

  constructor() {
    this._listeners = {};
  }

  destroy() {
    this._listeners = null;
  }

  on<K extends keyof M>(name: K, callback: any) {
    let _listeners = this._listeners;
    if (!_listeners) return;
    let callbacks = _listeners[name];
    if (!callbacks) {
      callbacks = _listeners[name] = [];
    }
    callbacks.push(callback);
    return this;
  }

  once<K extends keyof M>(name: K, callback: any) {
    let wrapper: any = (...args) => {
      this.off(name, callback);
      callback.apply(null, args);
    };
    wrapper._origin = callback;
    return this.on(name, wrapper);
  }

  off<K extends keyof M>(name: K, callback?: M[K]) {
    let _listeners = this._listeners;
    if (!_listeners) return;
    const callbacks = _listeners[name];
    if (!callbacks) {
      return this;
    }
    if (callback) {
      for (let i = callbacks.length - 1; i >= 0; i--) {
        let cb = callbacks[i] || {} as any;
        if (cb === callback || cb._origin === callback) {
          callbacks.splice(i, 1);
          break;
        }
      }
    } else {
      _listeners[name] = null;
    }
    return this;
  }

  trigger<K extends keyof M>(name: K, ...args: any[]) {
    let _listeners = this._listeners;
    if (!_listeners) return;
    const callbacks = _listeners[name];
    if (!callbacks) {
      return;
    }
    forEach([...callbacks], cb => cb.apply(null, args));
  }
}
