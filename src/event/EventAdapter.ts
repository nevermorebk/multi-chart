import { IUIEventListener, Point, ScaleOpts, DragPoint, ZoomPoint, PinchPoint } from '../lib';
export default class EventAdapter implements IUIEventListener {
  onMouseDown(point: Point, event?: MouseEvent): void {}
  onMouseMove(point: Point, event?: MouseEvent): void {}
  onMouseUp(point: Point, event?: MouseEvent): void {}
  onClick(point: Point, event?: MouseEvent): void {}
  onDblClick(point: Point): void {}
  onWheel(point: ZoomPoint): void {}
  onKeyUp(event: KeyboardEvent): void {}

  onDragStart(point: DragPoint, event?: MouseEvent | TouchEvent): void {}
  onDrag(point: DragPoint, event?: MouseEvent | TouchEvent): void {}
  onDragCancel(point: DragPoint, event?: MouseEvent | TouchEvent): void {}
  onDragEnd(point: DragPoint, event?: MouseEvent | TouchEvent): void {}
  onPinch(point: PinchPoint, event?: TouchEvent): void {}
}
