import { identity as d3ZoomIdentity } from 'd3-zoom/src/transform';
import { dom, timeout, isRightButton } from '../util';
import { IChartContext, IComponent, ScaleOpts, DragPoint, Point, TouchPoint, DrawOpt } from '../lib';
import { noevent, eventPoint, eventPoints, cloneEventObject, dispatchEvent, createMouseEvent } from '../util/event';
import { roundUp, roundDown } from '../util/math';
import { forEach } from '../util/array';
import Evented from './Evented';
import { isTouchDevice } from '../util';

type TouchedPoint = TouchPoint & {
  dx?: number;
  dy?: number;
};

type DragStartPoint = {
  clientX: number;
  clientY: number;
  offset: { clientX: number; clientY: number };
} & { source: HTMLElement };

/**
 * todo:
 * 1, to gesture lib
 * 2, for mobile: support long tap (long tap yaxia : show crosshair y line)
 *
 */
export default class EventCapturer extends Evented<any> {
  container: HTMLElement;
  readonly context: IChartContext;
  private dragMouseStartPoint: DragStartPoint;
  private touchPoints: TouchedPoint[] & { diffX?: number };
  private dragDelay = 500;
  private touchStarting: number;
  private touchEnding: number;
  private mouseMove: boolean;
  private mouseDown: boolean;
  private mouseDrag: boolean;
  private tapDelay = 250;
  private tapLasttime = 0;
  private _zoom: any;

  constructor(context: IChartContext, container: HTMLElement) {
    super();
    this.context = context;
    this.container = container;
    this.touchPoints = [];
    this.init();
  }

  init() {
    let container = this.container;
    let element = dom(container);
    element
      // .on('.event', null)
      .on('mousedown', this._handleMouseDown)
      .on('mousemove', this._handleMouseMove)
      .on('mouseup', this._handleMouseUp)
      .on('mouseleave', this._handleMouseLeave)
      .on('click', this._handleClick)
      .on('touchstart', this._handleTouchStart)
      .on('touchmove', this._handleTouchMove)
      .on('touchend', this._handleTouchEnd)
      .on('touchcancel', this._handleTouchEnd)
      .on('dblclick', this._handleDblClick)
      .on('wheel', this._handleWheel)
      .on('keyup', this._handleKeyUp);
  }

  destroy() {
    super.destroy();
    let element = dom(this.container);
    element
      .off('mousedown', this._handleMouseDown)
      .off('mousemove', this._handleMouseMove)
      .off('mouseup', this._handleMouseUp)
      .off('mouseleave', this._handleMouseLeave)
      .off('click', this._handleClick)
      .off('touchstart', this._handleTouchStart)
      .off('touchmove', this._handleTouchMove)
      .off('touchend', this._handleTouchEnd)
      .off('touchcancel', this._handleTouchCancel)
      .off('dblclick', this._handleDblClick)
      .off('wheel', this._handleWheel)
      .off('keyup', this._handleKeyUp);
  }

  shouldIgnoreTouch(dx: number, dy: number) {
    const { drawed, drawedDragging } = this.context.state;
    if (isTouchDevice() && (!drawed && !drawedDragging)) {
      if (Math.abs(dy) > Math.abs(dx)) {
        return true;
      }
    }
    return false;
  }
  _handleKeyUp = (event?: KeyboardEvent) => {
    this.trigger('keyup', event);
  };

  _handleWheel = event => {
    if (this.mouseMove === undefined) return;
    let { x, y } = eventPoint(event);
    let { deltaX, deltaY, deltaMode } = event;
    deltaX = Math.floor(deltaX);
    deltaY = Math.floor(deltaY);
    let dirX = Math.abs(deltaX) > Math.abs(deltaY);
    let max = dirX ? deltaX : deltaY;
    if (dirX) {
      let delta = -deltaX;
      this.trigger('wheel', { x, y, dx: delta, dy: 0, k: 0 });
    } else {
      let k = Math.pow(2, (-max * (deltaMode ? 120 : 1)) / 300);
      // TODO: make k to step value
      // k = k > 1? 1.15: 0.85;
      this.trigger('wheel', { x, y, dx: 0, dy: 0, k });
    }
    noevent(event);
  };

  _handleClick = (event?: MouseEvent) => {
    if (!this.mouseMove && !this.mouseDrag) {
      this.trigger('click', eventPoint(event), event);
    }
    this.mouseDown = false;
    this.mouseMove = false;
  };

  _handleMouseDown = (event?: MouseEvent) => {
    this.trigger('mousedown', eventPoint(event), event);
    // start drag
    if ((event as any)._custom) return;
    this.mouseMove = false;
    this.mouseDown = true;
    let currentTarget = event.currentTarget;
    let partEvent = cloneEventObject<MouseEvent>(event);
    timeout(() => {
      if (this.mouseDown && !isRightButton(partEvent)) {
        this._handleMouseDragStart(partEvent);
      }
    }, 0);
  };

  _handleMouseLeave = (event?: MouseEvent) => {
    this.trigger('mouseleave');
    this.mouseDown = undefined;
    this.mouseMove = undefined;
    this.mouseDrag = undefined;
  };

  _handleMouseUp = (event?: MouseEvent) => {
    if (this.mouseDrag) return;
    this.trigger('mouseup', eventPoint(event), event);
    this.mouseDown = false;
    this.mouseMove = false;
  };

  _handleMouseMove = (event?: MouseEvent) => {
    if (this.mouseDrag) return;
    this.trigger('mousemove', eventPoint(event), event);
    this.mouseMove = true;
  };

  // touch
  _handleTouchStart = (event?: TouchEvent) => {
    let points = eventPoints(event);
    let point = points[0];
    this._zoom = d3ZoomIdentity;
    // support pinch zoom / pan
    const touchPoints = this.touchPoints;
    let started;
    forEach(points, point => {
      if (!touchPoints[0]) {
        touchPoints[0] = point;
        started = true;
      } else if (!touchPoints[1]) {
        touchPoints[1] = point;
      }
    });

    if (this.touchStarting) {
      if (!touchPoints[1]) {
        // dblclick
        window.clearTimeout(this.touchStarting);
        this.touchStarting = null;
        return;
      }
    }

    if (started) {
      let target = event.target as HTMLElement;
      dispatchEvent(target, createMouseEvent('mousedown', event.changedTouches[0]));

      this.touchStarting = window.setTimeout(() => (this.touchStarting = null), this.dragDelay);
      this.trigger('dragstart', point, event);
      // this.context.redraw(DrawOpt.overlay);
    }
  };

  _handleTouchCancel = (event?: TouchEvent) => {
    if (this.touchStarting) {
      let points = eventPoints(event);
      let point = points && points[0];
      window.clearTimeout(this.touchStarting);
      this.touchStarting = null;
      this.trigger('dragcancel', point, event);
    }
    if (this.touchEnding) {
      window.clearTimeout(this.touchEnding);
      this.touchEnding = null;
    }
  };

  _handleTouchMove = (event?: TouchEvent) => {
    const touchPoints = this.touchPoints;
    let preTouchPoints = [...touchPoints];
    let points = eventPoints(event);
    if (this.touchStarting) {
      window.clearTimeout(this.touchStarting);
      this.touchStarting = null;
    }
    forEach(points, point => {
      if (touchPoints[0] && touchPoints[0].identifier === point.identifier) {
        touchPoints[0] = point;
      } else if (touchPoints[1] && touchPoints[1].identifier === point.identifier) {
        touchPoints[1] = point;
      }
    });

    // noevent(event);
    // event.preventDefault();
    let [point0, point1] = touchPoints;
    let isPinch = !!point1;
    let diff = 0,
      preDiff = 0;
    // if (isPinch) {
    //   // check if pinch or pan
    //   diff = Math.abs(point0.x - point1.x);
    //   preDiff = touchPoints.diffX;
    //   isPinch = Math.abs(preDiff - diff) > 6;
    //   if (!isPinch) {
    //     // save diff
    //   }
    //   touchPoints.diffX = diff;
    // }

    let start = preTouchPoints[0];
    let [x, y] = [point0.x, point0.y];
    let [dx, dy] = [point0.x - start.x, point0.y - start.y];

    if (!isTouchDevice() || isPinch) {
      event.preventDefault();
    }
    if (!isPinch) {
      if (this.shouldIgnoreTouch(dx, dy)) {
        return;
      } else if (isTouchDevice()) {
        event.preventDefault();
      }
      // pan
      touchPoints[0].dx = dx;
      touchPoints[0].dy = dy;
      this.trigger('drag', { x, y, dx, dy }, event);
    } else {
      // zoom
      // let transform = this._zoom;
      let dp, dl;
      let p0 = touchPoints[0],
        l0 = preTouchPoints[0],
        p1 = touchPoints[1],
        l1 = preTouchPoints[1];
      // tslint:disable-next-line:ban-comma-operator
      (dp = (dp = p1.x - p0.x) * dp + (dp = p1.y - p0.y) * dp), (dl = (dl = l1.x - l0.x) * dl + (dl = l1.y - l0.y) * dl);
      let k = Math.sqrt(dp / dl);
      // let t = k === transform.k ? transform : d3ZoomIdentity.scale(k).translate(transform.x, transform.y);
      let p = [(p0.x + p1.x) / 2, (p0.y + p1.y) / 2];
      let l = [(l0.x + l1.x) / 2, (l0.y + l1.y) / 2];
      let dx = p[0] * k - l[0];
      let dy = p[1] * k - l[1];
      let x = ((p0.x + l0.x) / 2 + (p1.x + l1.x) / 2) / 2;
      let y = ((p0.y + l0.y) / 2 + (p1.y + l1.y) / 2) / 2;
      this.trigger('pinch', { x, y, dx, dy, k }, event);
      // console.log((p[0] | 0) + ',' + (l[0] | 0) );

      // let [ prePoint0, prePoint1 ] = preTouchPoints;
      // let deltax = Math.abs(diff - preDiff);
      // let deltay = Math.abs(diff - deltax);
      // let k;
      // dx = 0; dy = 0;
      // x = (point0.x + point1.x) / 2;
      // y = (point0.y + point1.y) / 2;
      // if (prePoint0.x === point0.x) {
      //   x = point0.x;
      // } else if (prePoint1.x === point1.x) {
      //   x = point1.x;
      // }
      // k = Math.pow(2, (diff > preDiff ? 1 : -1) * (deltax === 0 ? 0 : deltax / deltay));
      // this.trigger('pinch', { x, y, dx, dy, k });
    }
  };

  _handleTouchEnd = (event?: TouchEvent) => {
    const touchPoints = this.touchPoints;
    let points = eventPoints(event);
    let point = points[0];
    let tapPoint = touchPoints[0],
      single = !touchPoints[1];

    if (this.touchEnding) {
      window.clearTimeout(this.touchEnding);
      this.touchEnding = null;
    }

    forEach(points, point => {
      if (touchPoints[0] && touchPoints[0].identifier === point.identifier) touchPoints[0] = null;
      else if (touchPoints[1] && touchPoints[1].identifier === point.identifier) touchPoints[1] = null;
    });

    if (!touchPoints[0]) {
      touchPoints[0] = touchPoints[1];
      touchPoints[1] = null;
    }

    if (!touchPoints[1]) {
      touchPoints.diffX = null;
    }

    if (!touchPoints[0]) {
      this.trigger('dragend', point, event);
      this._zoom = d3ZoomIdentity;
      // tap doubletap
      // todo dragend will trigger tap once !
      if (single) {
        if (tapPoint && (tapPoint.dx || 0) < 20 && (tapPoint.dy || 0) < 20) {
          noevent(event);
          let delta = Date.now() - this.tapLasttime;
          let target = event.target as HTMLElement;
          if (delta > 0 && delta <= this.tapDelay) {
            dispatchEvent(target, createMouseEvent('dblclick', event.changedTouches[0]));
          } else {
            dispatchEvent(target, createMouseEvent('mouseup', event.changedTouches[0]));
            dispatchEvent(target, createMouseEvent('click', event.changedTouches[0]));
          }
          this.tapLasttime = Date.now();
        }
      }
    }
    this.touchEnding = window.setTimeout(() => (this.touchEnding = null), this.dragDelay);
  };

  _handleDblClick = (event?: MouseEvent) => {
    this.trigger('dblclick', eventPoint(event));
  };

  _handleMouseDragStart = (event?: MouseEvent) => {
    if (!event) return;
    let moved = false;
    this.mouseDrag = true;
    let point = eventPoint(event);
    let clientXY = { clientX: event.clientX, clientY: event.clientY };
    let source = event.currentTarget as HTMLElement;
    this.dragMouseStartPoint = Object.assign(clientXY, { offset: clientXY }, { source });
    let dragPoint = { ...point, dx: 0, dy: 0, offsetX: 0, offsetY: 0 };
    this.trigger('dragstart', dragPoint, event);

    let view = dom(window);
    let handleMouseDrag = (event: MouseEvent) => {
      moved = true;
      this._handleMouseDrag(event);
    };
    view.on('mousemove', handleMouseDrag, true);
    view.one(
      'mouseup',
      (event: MouseEvent) => {
        if (moved) this._handleMouseDragEnd(event);
        else this._handleMouseDragCancel(event);
        this._handleMouseUp(event);
        view.off('mousemove', handleMouseDrag, true);
        moved = false;
      },
      true
    );
  };

  _handleMouseDrag = (event?: MouseEvent) => {
    let start = this.dragMouseStartPoint;
    if (!start) return;
    let source = start.source;
    // noevent(event);
    event.preventDefault();
    let point = eventPoint(event, source);
    let clientXY = { clientX: event.clientX, clientY: event.clientY };
    let { x, y } = point;
    let [dx, dy] = [event.clientX - start.clientX, event.clientY - start.clientY];
    let [offsetX, offsetY] = [start.offset.clientX - event.clientX, start.offset.clientY - event.clientY];
    this.dragMouseStartPoint = Object.assign(clientXY, { offset: start.offset }, { source });
    if (dx !== 0 || dy !== 0) {
      let dragPoint = { x, y, dx, dy, offsetX, offsetY };
      this.trigger('drag', dragPoint, event);
    }
  };

  _handleMouseDragCancel = (event?: MouseEvent) => {
    let start = this.dragMouseStartPoint;
    let [offsetX, offsetY] = [0, 0];
    if (start) {
      [offsetX, offsetY] = [start.offset.clientX - event.clientX, start.offset.clientY - event.clientY];
    }
    this.mouseDrag = false;
    if (!start) return;
    this.dragMouseStartPoint = null;
    let point = eventPoint(event, start.source);
    let dragPoint = { ...point, dx: 0, dy: 0, offsetX, offsetY };
    this.trigger('dragcancel', dragPoint, event);
  };
  _handleMouseDragEnd = (event?: MouseEvent) => {
    let start = this.dragMouseStartPoint;
    let [offsetX, offsetY] = [0, 0];
    if (start) {
      [offsetX, offsetY] = [start.offset.clientX - event.clientX, start.offset.clientY - event.clientY];
    }
    this.mouseDrag = false;
    if (!start) return;
    this.dragMouseStartPoint = null;
    let point = eventPoint(event, start.source);
    let dragPoint = { ...point, dx: 0, dy: 0, offsetX, offsetY };
    this.trigger('dragend', dragPoint, event);
  };
}
