// import '@babel/polyfill'; // useBuiltIns : usage
import { CapacityMonitor } from './Monitor';
import { IMultiChartProps, MultiChart } from './MultiChart';
import { ToolLineType, Command, ChartType, ChartStyle } from './lib';
import { TemplateConfig } from './model/Template';
import { setDefaultConfig, setTheme, setI18n } from './config';


// factory method
export function create(props: IMultiChartProps, template: TemplateConfig, data?: any[]): MultiChart {
  let mchart = new MultiChart(props, template, data);
  mchart.initialize();
  new CapacityMonitor(mchart, props.capacity).start();
  return mchart;
}

export const enums = {
  Tool: ToolLineType,
  Command,
  ChartType,
  ChartStyle,
};

export const serializeVersion = __VERSION__ || 1;

export default {
  create,
  setDefaultConfig,
  setTheme,
  setI18n,
  enums,
  serializeVersion
};
