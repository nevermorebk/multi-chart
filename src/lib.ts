import Chart from './chart/Chart';
import ITool from './tool/ITool';
import { Feature } from './feature/Feature';
import MainChart from './chart/MainChart';
import Template from './model/Template';
import { IndicatorConfig } from './model/Template';
import { IMetaInfoCfg } from './config/meta';
import ChartData from './model/ChartData';
import { DataSourceAdapter } from './model/DataLoader';
import Canvas from './canvas/Canvas';

// util
export type PropertyNames<T> = { [K in keyof T]: K }[keyof T];

export type FunctionPropertyNames<T> = { [K in keyof T]: T[K] extends Function ? K : never }[keyof T];

export type ReturnType<T> = T extends (...args: any[]) => infer R ? R : any;

export type Nullable<T> = { [P in keyof T]: T[P] | null | undefined };

export type PowerPartial<T> = { [U in keyof T]?: T[U] extends object ? PowerPartial<T[U]> : T[U] };

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

//
export type NumberTuple = [number, number];

export type Domain = NumberTuple;

export type IAccessor<T, U> = (datum: T, index?: number, array?: T[]) => U | undefined | null;

export type DataConverter = (d: any, i: number, start?: number) => DataItem;

export type DomainValue = number | undefined | null;

export type OHLC = {
  open: number;
  high: number;
  low: number;
  close: number;
};

export type PriceDataItem = {
  price: number;
};

export type DataItem = {
  x: number;
  date: number;
  data: OHLC;
  id: string;
};

export type DrawOpt = {
  isOverlay?: boolean;
  isAll?: boolean;
};

export namespace DrawOpt {
  export const overlay = { isOverlay: true };
  export const all = { isAll: true };
}

export interface Scale {
  (value: number): number;
  invert(value: number): number;
  domain(): NumberTuple;
  domain(domain: NumberTuple): this;
  range(): NumberTuple;
  range(range: NumberTuple): this;

  ticks(count?: number): number[];
  copy(): this;
}

export interface IDrawable {
  draw(ctx?: Context2D): void;
}

export interface EventHandle {
  detach: Function;
}

export interface IEvented extends Object {
  on(name: string, callback: Function): void;
  once(name: string, callback: Function): void;
  off(name: string, callback?: Function): void;
  trigger(name: string, ...args: any[]): void;
  destroy(): void;
}

export enum Side {
  BID = -1,
  ASK = 1
}

export enum KEY_CODE {
  BACKSPACE = 8,
  DELETE = 46,
  ESCAPE = 27,
}

// tslint:disable-next-line:max-line-length
export type MouseEventType = 'mousedown' | 'mousemove' | 'mouseup' | 'mouseleave' | 'mouseenter' | 'wheel' | 'click' | 'dblclick';
export type DragEventType = 'dragstart' | 'drag' | 'dragend';
export type GestureEventType = 'pinch';
export type EventType = MouseEventType | DragEventType | GestureEventType | string;

export interface IUIEventListener {
  onMouseDown(point: Point, event?: MouseEvent): void;
  onMouseMove(point: Point, event?: MouseEvent): void;
  onMouseUp(point: Point, event?: MouseEvent): void;
  onClick(point: Point, event?: MouseEvent): void;
  onDblClick(point: Point): void;
  onWheel(point: ZoomPoint): void;
  onKeyUp(event: KeyboardEvent): void;

  // high order event
  onDragStart(point: Point, event?: MouseEvent | TouchEvent): void;
  onDrag(point: DragPoint, event?: MouseEvent | TouchEvent): void;
  onDragCancel(point: Point, event?: MouseEvent | TouchEvent): void;
  onDragEnd(point: Point, event?: MouseEvent | TouchEvent): void;
  // touch  gesture ?
  onPinch(point: PinchPoint, event?: TouchEvent): void;
}

export interface IComponent extends IDrawable {
  update(props?: any): void; // todo merged to redraw
  destroy(): void;
  redraw(drawOpt?: DrawOpt): void;
}

export enum IndicatorDrawStyle {
  LINE = 0,
  POINT,
  // AREA,
  // CROSS
}

export namespace IndicatorDrawStyle {
  export const options = [
    { label: 'line', value: IndicatorDrawStyle.LINE },
    { label: 'point', value: IndicatorDrawStyle.POINT },
    // {label: 'area', value: IndicatorDrawStyle.AREA},
    // {label: 'cross', value: IndicatorDrawStyle.CROSS},
  ];
}

export type IndicatorLineMetaInfo = {
  color: string;
  width: number;
  style: IndicatorDrawStyle | number;
  styleFixed?: boolean;
};

export namespace IndicatorLineMetaInfo {
  export const DefaultLineStyle = {
    color: '#ddd',
    width: 0.5,
    style: IndicatorDrawStyle.LINE,
  };
  export const DefaultPointLineStyle = {
    color: '#ddd',
    width: 0.5,
    style: IndicatorDrawStyle.POINT,
  };
  // export const DefaultAreaLineStyle = {
  //   color: '#ddd', width: 1, style: IndicatorDrawStyle.AREA
  // };

  export const DefaultFixedLineStyle = {
    ...IndicatorLineMetaInfo.DefaultLineStyle,
    styleFixed: true,
  };

  export const DefaultFixedPointStyle = {
    ...IndicatorLineMetaInfo.DefaultPointLineStyle,
    styleFixed: true,
  };
}

export type ColorMetaInfo = string;

export type AlphaMetaInfo = number;

export type LevelColorMetaInfo = {
  levels: ReadonlyArray<{
    color: string;
    val: number;
    label?: string;
    disable?: boolean;
  }>;
  step?: number;
  precision?: number;
  oneColor?: false | string;
};
export type BarMetaInfo = {
  positive: string;
  negative: string;
};

export namespace BarMetaInfo {
  export const DefaultBarStyle = {
    positive: '#bf1032',
    negative: '#01abed',
  };
}

export type ISeriesStyle = {
  name: string;
  val: IndicatorLineMetaInfo | BarMetaInfo | ColorMetaInfo;
};

export interface ISeries extends IDrawable {
  order: number; // as same as z-index
}
export interface SeriesConstructor {
  prototype: ISeries;
  new(context: IChartContext, key: string, style?: ISeriesStyle | ISeriesStyle[]): ISeries;
}

export type Size = {
  width: number;
  height: number;
  yWidth?: number;
};

export type Point = {
  x: number;
  y: number;
};

export type TouchPoint = Point & {
  identifier: number;
};

export type DragPoint = Point & {
  dx: number;
  dy: number;
  offsetX?: number;
  offsetY?: number;
};

export type ZoomPoint = DragPoint & {
  k: number;
};

export type PinchPoint = ZoomPoint;

export type RelationPoint = Point & {
  dx?: number;
  dy?: number;
};

export type Rect = {
  left: number;
  top: number;
  width: number;
  height: number;
};

export type Position = {
  left: number | string;
  top: number | string;
  right: number | string;
  bottom: number | string;
};

export type BoundingBox = [number, number, number, number];

export type SeriesParams = {
  name: string;
  dataKey: string;
  seriesType: SeriesConstructor[];
  accessor: IAccessor<DataItem, DomainValue>;
  config?: IndicatorConfig;
  id: number;
};

export type ScaleOpts = {
  scaleX?: Function;
  scaleY?: Function;
};

export enum Layout {
  TOP,
  BOTTOM,
  LEFT,
  RIGHT,
  FILL,
}

export interface IArea extends IUIEventListener, IComponent {
  layout: Layout;
  contains(point: Point): boolean;
  addFeature(feature: Feature): void;
  removeFeature(feature: Feature): void;
  getRelationPoint(point: Point): Point;
}

export enum CursorStyle {
  DEFAULT = 'default',
  CROSSHAIR = 'crosshair',
  POINTER = 'pointer',
  MOVE = 'grabbing',
  NS_RESIZE = 'ns-resize',
  EW_RESIZE = 'ew-resize',
  ROW_RESIZE = 'row-resize',
  // and some custom styles
}

export type ContextState = {
  resizing?: boolean; // resize chart
  touching?: ContextState.Touching; // touching for touch device
  dragging?: boolean; // dragging chart
  wheeling?: boolean; // wheeling chart
  loading?: boolean; // loading data
  cmdProcing?: boolean; // tool/cmd process
  preventMore?: boolean; // more data or no data
  pauseRedraw?: boolean; // stop draw when window is not visible
  drawed?: number | undefined; // lock / hidden (permanent state!!)
  drawedDragging?: ContextState.DrawedDragging; // dragging drawed object
};

export namespace ContextState {
  export function Init() {
    return {};
  }

  export enum Touching {
    END = 0,
    START = 1,
    MOVE = 2,
  }

  export enum DrawedDragging {
    EMPTY = 0,
    CTRL_POINT = 1,
    OBJECT = 2,
  }

  export enum Drawed {
    NORMAL = 0,
    HIDDEN = 1 << 0,
    LOCKED = 1 << 1,
  }
}

export interface IBaseContext extends IConfig, IEvented {
  chartDataModel: ChartData;
  data: DataItem[];
  allData: DataItem[];
  template: Template;
  xAccessor: IAccessor<DataItem, DomainValue>;
  chartWidth: number;
  state: ContextState;
  tryUpdateZoom(k: number): number;
  updateZoom(k: number): void;
  zoomIn(): void;
  zoomOut(): void;
  resetZoom(): void;
  getBandWidth(factor?: number): number;
  isAutoShowLast(): boolean;
  showLast(prop?: { resetZoom?: boolean }): boolean;
  getScaleX(): Scale;
  updateScaleX(scale: Scale): void;
  loading(loading: boolean): void;
  resetState(): void;
  mouse(mouse?: { x: number; y: number; chartId: number }): IMouseXY;
  redraw(drawOpt?: DrawOpt): void;
  resizeChart(chart: Chart, point?: DragPoint): void;
  canResizeChart(chart: Chart, origin: string, point?: DragPoint): boolean;
  toggleMaximizeChart(chartId: number): void;
  moveUpChart(chartId: number): void;
  moveDownChart(chartId: number): void;
  isMainChart(chartId: number): boolean;
  getChart(chartId: number): Chart;
  getNextChart(currChartId: number): Chart;
  getPrevChart(currChartId: number): Chart;
  getTimelineChart(): Chart;
  getMainChart(): MainChart;

  appendIndicator(appendIndicatorConfig: IndicatorConfig): void;
  removeIndicator(indicatorId: number): void;
  updateIndicator(indicatorId: number, config?: IndicatorConfig): void;
  clearIndicators(): void;

  setCurTool(tool: ITool): void;
  getCurTool(): ITool;
  getChartMode(): ChartMode;
  setChartMode(mode: ChartMode): void;
  setCursor(cursorStyle: CursorStyle, dom?: HTMLElement): void;

  selectedLine: IToolLine;
}

export interface IChartContext extends IBaseContext {
  timestamp: number;
  chartId: number;
  chartHeight: number;
  container: HTMLElement;
  precisionScale: number;
  series: SeriesParams[];
  canvas: Canvas;
  overlayCanvas: Canvas;
  updateTimestamp: () => void;
  yAccessor: IAccessor<DataItem, DataItem>;
  flipY: (flag?: boolean) => boolean | this;
  rezoom: (transform: any, scale: ScaleOpts) => void;
  // rezoomed: (dir: string) => void;
  getScaleY(): Scale;
  updateScaleY(scale: Scale): void;
}

export interface IMouseXY {
  x: number;
  y: number;
  chartId: number;
  closest?: DataItem;
  closestX?: number;
}

export enum ChartStyle {
  Candle = 'candle',
  Line = 'line',
  Avg = 'avg',
}

export enum ChartType {
  TICK,
  M1,
  M5,
  M10,
  M15,
  M30,
  H1,
  H2,
  H4,
  D1,
  W1,
  MN1 = 16,
}

export namespace ChartType {
  export const IndicatorOptions = [
    { label: 'D1', value: ChartType.D1 },
    { label: 'W1', value: ChartType.W1 },
    { label: 'MN1', value: ChartType.MN1 },
  ];
  // only for generate future data
  export function deltaTime(chartType: ChartType, time: number): number {
    switch (chartType) {
      case ChartType.M1:
        return 1 * 60 * 1000;
      case ChartType.M5:
        return 5 * 60 * 1000;
      case ChartType.M10:
        return 10 * 60 * 1000;
      case ChartType.M15:
        return 15 * 60 * 1000;
      case ChartType.M30:
        return 30 * 60 * 1000;
      case ChartType.H1:
        return 60 * 60 * 1000;
      case ChartType.H2:
        return 1 * 60 * 60 * 1000;
      case ChartType.H4:
        return 4 * 60 * 60 * 1000;
      case ChartType.D1:
        return 24 * 60 * 60 * 1000;
      case ChartType.W1:
        return 7 * 24 * 60 * 60 * 1000;
      case ChartType.MN1: {
        if (time) {
          let date = new Date(time);
          date.setMonth(date.getMonth() + 1);
          return date.getTime() - time;
        } else return 30 * 24 * 60 * 60 * 1000;
      }
      default:
        return 0;
    }
  }
}

// draw tool api

export enum SelectionState {
  HIDE,
  SHOW,
  SELECTED,
  HIGHLIGHT,
}

export enum ChartMode {
  NORMAL = 'normal',
  DRAWTOOL = 'drawtool',
}

export enum CommandState {
  BEFORE,
  PROCESS,
  FINISH,
}

export interface ISelection extends IUIEventListener {
  context: IChartContext;
  state: SelectionState;
  isEditing: boolean;
  setState(state): void;
  getState(): SelectionState;
  isHover(xy: Point): boolean;
  highlight(): void;
  select(): void;
  normal(): void;
  getBoundingBox(): BoundingBox;
  draw(ctx: Context2D): void;
}

export type ICoord = {
  index: number;
  displayIndex: number;
  date: number;
  value: number;
  data: OHLC;
};

export interface ICtrlPoint extends ISelection {
  coord: ICoord;
  line: IToolLine;
  getPoint(): Point;
  move(coord: ICoord): void;
  draw(ctx: Context2D, point?: Point): void;
  toJSON(): object;
}

export interface IDrawLineTool {
  toolLine: IToolLine;
  setPoint(point: Point, fromIndex: number): boolean;
}

export interface IToolLine extends ISelection, ISerializable {
  id: number;
  owner: Chart;
  // metaCfg: IMetaInfoCfg; // use getter/setter instead
  ctrlPoints: ICtrlPoint[];
  ctrlPointSize: number;
  drawingIndex: number;
  setMetaCfg(cfg: Readonly<IMetaInfoCfg>): void;
  getMetaCfg(): Readonly<IMetaInfoCfg>;
  setCtrlPoint(ctrlPoint: ICtrlPoint, index: number): void;
  hasCtrlPoint(ctrlPoint: ICtrlPoint): boolean;
  move(coords: ICoord[]): void;
  moveCtrlPoint(coord: ICoord, index: number): void;
  refresh(): void;
  startEdit(): void;
  stopEdit(): void;
}

export enum ToolLineType {
  TREND_LINE = 'TREND_LINE',
  RAY_LINE = 'RAY_LINE',
  HORIZONTAL_LINE = 'HORIZONTAL_LINE',
  HORIZONTAL_RAY_LINE = 'HORIZONTAL_RAY_LINE',
  VERTICAL_LINE = 'VERTICAL_LINE',
  PARALLEL_LINE = 'PARALLEL_LINE',
  FIBONACCI_LINE = 'FIBONACCI_LINE',
  FIBONACCI_TREND_BASED_LINE = 'FIBONACCI_TREND_BASED_LINE',
  FIBONACCI_TIMEZONE_LINE = 'FIBONACCI_TIMEZONE_LINE',
  FIBONACCI_TIMEZONE_TREND_BASED_LINE = 'FIBONACCI_TIMEZONE_TREND_BASED_LINE',
  GANN_FAN_LINE = 'GANN_FAN_LINE',
  LONG_POSITION_LINE = 'LONG_POSITION_LINE',
  SHORT_POSITION_LINE = 'SHORT_POSITION_LINE',
  DATE_RANGE_LINE = 'DATE_RANGE_LINE',
  PRICE_RANGE_LINE = 'PRICE_RANGE_LINE',
}

export enum Tool { }

export enum Command {
  ZOOM_IN = 'ZOOM_IN',
  ZOOM_OUT = 'ZOOM_OUT',
  ZOOM_RESET = 'ZOOM_RESET',
  SHOW_LAST = 'SHOW_LAST',
  DELETE_LINE = 'DELETE_LINE',
  CLEAR_LINE = 'CLEAR_LINE',
  TOGGLE_LINE_VISIBLE = 'TOGGLE_LINE_VISIBLE',
  TOGGLE_LINE_LOCKED = 'TOGGLE_LINE_LOCKED',
  CLEAR_INDICATORS = 'CLEAR_INDICATORS',
  FLIP_Y = 'FLIP_Y',
}

// config

export interface ISerializable {
  toJSON(): object;
  fromJSON(json: any, meta?: Readonly<IMetaInfoCfg>): void;
}

export interface IConfig {
  lang: string;
  theme: string;
  dpr: number;
  tzOffset: number;
  // can null when init
  symbol: {
    id: number;
    name: string;
    scale: number;
  };
  chartType: number;
  chartStyle: string;
  chartSide?: Side;

  capacity: number;
  magnetMode: boolean;
  magnetDistance: number;
  barFactor: number;
  zoomLevels: number;
  initZoomLevel: number;
  defaultBandWidth: number;
  disableZoom: boolean;

  sizeOption: SizeOption; // width height 独立
  displayOption: DisplayOption;
  dataConverter?: DataConverter;
  ds: DataSourceAdapter;
  setIndicatorConfig?: (param: SetConfigParam<IndicatorConfig>) => void;
  setDrawedConfig?: (param: SetConfigParam<Readonly<IMetaInfoCfg>>) => void;
  placeOrder?: (type: string, price: number) => void;
}

export type SetConfigParam<T> = {
  id?: number;
  cfg: T;
  update: (cfg?: T, preview?: boolean) => void;
};

export type SizeOption = {
  width: number;
  height: number;
  xHeight?: number;
  yWidth?: number;
};

export type DisplayOption = {
  crosshair: boolean;
  gridline: boolean;
  tooltip: boolean;
  indicatorLabel: boolean;
  currentLine: boolean;
  orderLine: boolean;
  positionSummaryLine: boolean;
  magnifier?: boolean;
};

// theme
export enum DrawedLineDashedStyle {
  Solid = 1,
  Dashed1,
  Dashed2,
  Dashed3,
  // solid , [4,4], [2,2], [3,2,1]
}

export namespace DrawedLineDashedStyle {
  export const options = [
    { label: 'solid', value: DrawedLineDashedStyle.Solid },
    { label: 'dashed1', value: DrawedLineDashedStyle.Dashed1 },
    { label: 'dashed2', value: DrawedLineDashedStyle.Dashed2 },
    { label: 'dashed3', value: DrawedLineDashedStyle.Dashed3 },
  ];
  export const DASHED_LINE_VALUES = [undefined, undefined, [4, 4], [2, 2], [3, 2, 1]];
}

export interface DrawedLineMetaInfo {
  color?: string;
  weight?: number;
  style?: DrawedLineDashedStyle | number;
}

export interface ITheme {
  chart: {
    baseFontSize: number;
    bigFontSize: number;
    baseFont: string;
    bigFont: string;
    fontColor: string;
    foregroundColor: string;
    backgroundColor: string;
    padding: number[];
    border: DrawedLineMetaInfo;
    seriesMargin: number[];
  };
  candle: {
    upColor: string;
    upFrameColor: string;
    downColor: string;
    downFrameColor: string;
  };
  gridLine: DrawedLineMetaInfo;
  crosshair: DrawedLineMetaInfo;
  currentPriceLine: DrawedLineMetaInfo;
  buyOrderLine: DrawedLineMetaInfo;
  sellOrderLine: DrawedLineMetaInfo;
  buyPositionSummaryLine: DrawedLineMetaInfo;
  sellPositionSummaryLine: DrawedLineMetaInfo;
  tooltip: {
    color: string;
    borderColor: string;
    backgroundColor: string;
    width: number;
    offset: number;
  };
  loadingbar: {
    color: string;
    borderColor: string;
    backgroundColor: string;
  };
  label: {
    color: string;
    backgroundColor: string;
  };
  tickLine: {
    color: string;
    width: number;
  }
}
