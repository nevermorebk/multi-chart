// tslint:disable:max-file-line-count

import Chart from './chart/Chart';
import Evented from './event/Evented';
import MainChart from './chart/MainChart';
import TimelineChart from './chart/TimelineChart';
import { DragPoint, DrawOpt, SizeOption, ContextState, Scale, Side } from './lib';
import { ChartMode, CursorStyle, DataConverter, IConfig, ToolLineType } from './lib';
import { IBaseContext, IComponent, IMouseXY, EventHandle } from './lib';
import { xAccessor, first, last, copy, isEmpty } from './util';
import { defaultMapToStandardConverter, OUT_MOUSE_XY } from './util';
import { dom, isDefined } from './util';
import { getTheme, mergeConfig } from './config';
import { timeout, calcClosestData, deepMergeObject } from './util/index';
import { identity as d3ZoomIdentity } from 'd3-zoom/src/transform';
import Tooltip from './view/tooltip';
import LoadingBar from './view/LoadingBar';
import { forEach } from './util/array';
import { ICommand, ITool } from './tool';
import Template, { TemplateChangeType } from './model/Template';
import { IndicatorProps, IndicatorConfig } from './model/Template';
import { TemplateConfig } from './model/Template';
import { getIconfontStyle } from './style/cssSprite';
import * as Tool from './tool';
import { log2 } from './util/math';
import { PropertyDialog } from './view/property-dialog/index';
import ChartData from './model/ChartData';
import DataLoader, { Loading } from './model/DataLoader';
import defaultConfig from './config/defaultConfig';
import MagnifierView from './view/MagnifierView';
export type IMultiChartProps = Partial<IConfig>;

export class MultiChart extends Evented<any> implements IComponent {
  dataLoader: DataLoader;
  props: IMultiChartProps;
  container: HTMLElement;
  readonly context: IBaseContext;
  readonly template: Template;

  public zoomLevel: number;
  private bandFullWidth: number;
  private _mouseXY = OUT_MOUSE_XY;

  private _drawed: boolean;

  private domCursorCache = new WeakMap<HTMLElement, string>();
  private _chartMode: ChartMode;
  private _currentTool: ITool;
  private _tools: ITool[];
  private _commands: ICommand[];
  private _eventHandles: EventHandle[];
  private _tooltip: Tooltip;
  private _loadingbar: Loading;
  private _magnifierView: MagnifierView;
  private _propertyDialog: PropertyDialog;

  private _ready: boolean;
  private _destroyed: boolean;

  constructor(props: IMultiChartProps, template: TemplateConfig, data: any[] = []) {
    super();
    this.props = mergeConfig(defaultConfig as any, props);

    this._eventHandles = [];
    this._initZoom();
    this.context = this._initContext();
    this.context.chartDataModel.initOrUpdateStandardData(data);
    this.context.chartDataModel.initScaleX();
    this.context.chartDataModel.updateViewportData(true);
    this.template = this._initChartTemplate(template);
    this.dataLoader = new DataLoader(this.context, props.ds);
  }

  initialize() {
    this.container = this._initContainer();
    this.template.initCharts();
    this._initTools();
    this._initEventListeners();
    this._initTooltip();
    this._initMagnifierView();
    this._initCSSStyle();
    this._initPropertyDialog();
    this._initLoadingBar();
    this.dataLoader.start();
  }

  ready(fn: Function, scope?) {
    if (this._ready) return fn.call(scope, this);
    else {
      this.on('ready', () => fn.call(scope, this));
    }
  }

  draw() {
    let charts = this.template.charts;
    forEach(charts, (chart: Chart) => chart.draw());
    timeout(() => {
      this.context.trigger('ready');
      this._ready = true;
    }, 0);
  }

  redraw(drawOpt?: DrawOpt) {
    let context = this.context;
    if (context.state.pauseRedraw) return;
    drawOpt = drawOpt || DrawOpt.all; // default
    forEach(this.template.charts, chart => chart.redraw(drawOpt));
    if (drawOpt.isOverlay) {
      this.drawTooltip();
    }
    if (context.displayOption.magnifier) {
      this.drawMagnifierView();
    }
  }

  attachTo(parent: HTMLElement) {
    parent.appendChild(this.container);
    if (!this._drawed) {
      this.draw();
      this._drawed = true;
    } else {
      this.redraw();
    }
    return this;
  }

  destroy() {
    if (this._destroyed) return;
    super.destroy();
    let handles = this._eventHandles;
    this._eventHandles = [];
    if (handles && handles.length) {
      forEach(handles, handle => {
        handle.detach();
      });
    }
    this.dataLoader.stop();
    this._loadingbar && this._loadingbar.hide();
    this._tooltip && this._tooltip.destroy();
    this._propertyDialog && this._propertyDialog.destroy();
    this._magnifierView && this._magnifierView.destroy();
    dom(this.container).remove();
    this.trigger('destroy');
    this._destroyed = true;
  }

  getDataConverter(): DataConverter {
    return this.context.chartDataModel.getDataConverter();
  }

  // region zoom

  _initZoom() {
    let zoomLevels = this.props.zoomLevels;
    if (zoomLevels % 2 === 0) {
      console.warn('recommend using odd number for "zoomLevels"!');
    }
    let defaultZoomLevel = ((zoomLevels + 1) / 2) | 0;
    let defaultBandWidth = this.props.defaultBandWidth;
    let initZoomLevel = this.props.initZoomLevel;
    let zoomLevel = defaultZoomLevel;
    let delta;
    if (initZoomLevel > 0 && initZoomLevel < zoomLevels) {
      zoomLevel = initZoomLevel;
    }
    delta = Math.pow(2, zoomLevel - defaultZoomLevel);

    this.zoomLevel = zoomLevel;
    this.bandFullWidth = defaultBandWidth * delta;
  }

  _updateZoom(k = 1) {
    let diff = log2(k);
    let zoomLevel = this.zoomLevel + diff;
    let max = this.props.zoomLevels;

    if (k === 1) return;
    if (zoomLevel < 1) zoomLevel = 1;
    else if (zoomLevel > max) zoomLevel = max;
    this.zoomLevel = zoomLevel;
    this.bandFullWidth *= k;

    timeout(() => {
      if (zoomLevel === this.zoomLevel) this.trigger('zoomChanged', this.zoomLevel);
    }, 800);
  }

  setZoomLevel(zoomLevel: number) {
    let max = this.context.zoomLevels;
    if (!(zoomLevel >= 1 && zoomLevel <= max)) {
      console.warn(`invalid zoom level : ${zoomLevel}! value range: [1, ${max}]`);
      return;
    }
    let k = Math.pow(2, zoomLevel - this.zoomLevel);
    if (k) {
      this._zoom(k);
      this.redraw();
    }
  }

  zoomIn() {
    let delta = 2;
    if (this._canZoom(delta).result) {
      this._zoom(delta);
      this.redraw();
    } else {
      this.setZoomLevel(this.context.zoomLevels);
    }
  }

  zoomOut() {
    let delta = 0.5;
    if (this._canZoom(delta).result) {
      this._zoom(delta);
      this.redraw();
    } else {
      this.setZoomLevel(1);
    }
  }

  resetZoom() {
    let zoomLevels = this.props.zoomLevels as number;
    let defaultZoomLevel = ((zoomLevels + 1) / 2) | 0;
    let k = Math.pow(2, defaultZoomLevel - this.zoomLevel);
    let offsetX;
    let data = this.context.data;
    if (data && data.length) {
      let scaleX = this.getScaleX();
      let rangeX = scaleX.range();
      let centerX = (rangeX[1] - rangeX[0]) / 2;
      let firstX = scaleX(xAccessor(first(data)));
      let lastX = scaleX(xAccessor(last(data)));
      if (firstX >= centerX || lastX <= centerX) {
        offsetX = (lastX + firstX) / 2;
      }
    }
    this._zoom(k, offsetX);
    this.redraw();
    if (data.length === 0) {
      this.showLast({ resetZoom: false });
    }
  }

  _canZoom(k: number): { result: boolean; k?: number } {
    let min, max;
    let defaultZoomLevel = ((this.props.zoomLevels + 1) / 2) | 0;
    let k1 = Math.pow(2, this.props.zoomLevels - defaultZoomLevel);
    let k2 = Math.pow(2, this.zoomLevel - defaultZoomLevel);
    min = 1 / k1 / k2;
    max = k1 / k2;

    let zoomin = k > 1;
    let result = false;
    let newK = null;
    if (k === 1) {
      result = true;
    } else {
      result = zoomin ? max !== 1 && max / k >= 1 : min !== 1 && min / k <= 1;
      if (!result && ((zoomin && max !== 1) || (!zoomin && min !== 1))) {
        newK = zoomin ? max : min;
      }
    }
    return { result, k: newK };
  }

  _zoom(k: number, offsetX?: number) {
    let scaleX = this.getScaleX();
    if (!isDefined(offsetX)) {
      let [, width] = scaleX.range();
      offsetX = width / 2;
    }
    let transform = d3ZoomIdentity
      .translate(offsetX, 0)
      .scale(k)
      .translate(-offsetX, 0);

    let newScale = transform.rescaleX(scaleX);
    this.updateScaleX(newScale);
    this._updateZoom(k);
  }

  tryUpdateZoom(k: number): number {
    if (k === 1) return k;
    let cancel = false;
    let canZoom = this._canZoom(k);
    if (!canZoom.result) {
      if (canZoom.k) k = canZoom.k;
      else cancel = true;
    }
    if (cancel) return 1;
    return k;
  }

  // endregion

  // scalex helper
  getScaleX(): Scale {
    return this.context.chartDataModel.getScaleX();
  }

  updateScaleX(scale) {
    this.context.chartDataModel.updateScaleX(scale);
  }

  _getBandWidth(factor?: number) {
    factor = factor || this.props.barFactor;
    return this.bandFullWidth * factor;
  }

  _initEventListeners() {
    // dpr listener
    if (window.matchMedia) {
      let dprChangeListener = () => {
        if (!this.context) return;
        const devicePixelRatio = window.devicePixelRatio;
        let dpr = this.props.dpr;
        let dpr2 = devicePixelRatio >= 2 ? 2 : devicePixelRatio;
        if (dpr2 !== dpr) {
          this.props.dpr = dpr2;
          this.template._resizeCharts();
        }
        this.redraw();
      };
      let mediaQuery = window.matchMedia('screen and (min-resolution: 2dppx), screen and (-webkit-min-device-pixel-ratio: 2)');
      if (mediaQuery) {
        mediaQuery.addListener(dprChangeListener);
        this._eventHandles.push({
          detach: () => mediaQuery.removeListener(dprChangeListener),
        });
      }
    }
    // visibility listener
    let visibilityListener = () => {
      this.context.state.pauseRedraw = document.visibilityState === 'hidden';
      if (!this.context.state.pauseRedraw) {
        this.context.redraw();
      }
    };
    document.addEventListener('visibilitychange', visibilityListener);
    // TODO: dom.on() return {detach}
    this._eventHandles.push({
      detach: () => document.removeEventListener('visibilitychange', visibilityListener),
    });
    // leave listener
    let leaveChartListener = () => this.mouse(null);
    dom(this.container).on('mouseleave', leaveChartListener);
    this._eventHandles.push({
      detach: () => dom(this.container).off('mouseleave', leaveChartListener),
    });
  }

  // region about dom view

  _initTooltip() {
    let container = this.container;
    let parent = dom
      .create('div')
      .addClass('multi-chart-tooltip')
      .appendTo(container)
      .node();
    this._tooltip = new Tooltip(this.context, parent);
  }

  drawTooltip(drawOpt?: DrawOpt) {
    this._tooltip && this._tooltip.redraw();
  }

  _initMagnifierView() {
    let container = this.container;
    let parent = dom
      .create('div')
      .addClass('multi-chart-magnifierview')
      .appendTo(container)
      .node();
    this._magnifierView = new MagnifierView(this.context, parent);
  }

  drawMagnifierView(drawOpt?: DrawOpt) {
    this._magnifierView && this._magnifierView.redraw();
  }

  _initLoadingBar() {
    let container = this.container;
    let parent = dom
      .create('div')
      .addClass('multi-chart-loadingbar')
      .appendTo(container)
      .node();
    if (this.props.ds && this.props.ds.loadingBar) {
      this._loadingbar = this.props.ds.loadingBar;
    } else {
      this._loadingbar = new LoadingBar(this.context, parent);
    }
  }

  _initCSSStyle() {
    let container = this.container;
    dom(getIconfontStyle()).appendTo(container);
  }

  _initPropertyDialog() {
    let container = this.container;
    let modals = dom
      .create('div')
      .addClass('multi-chart-modals')
      .appendTo(container)
      .node();
    const propertyDialog = new PropertyDialog(this.context, modals, container);
    this._propertyDialog = propertyDialog;
    if (!this.props.setIndicatorConfig) {
      this.props.setIndicatorConfig = cfg => propertyDialog.open(cfg);
    }
    if (!this.props.setDrawedConfig) {
      this.props.setDrawedConfig = cfg => propertyDialog.open(cfg);
    }
  }

  _updatePropertyDialogStyle() {
    if (this._propertyDialog && this._propertyDialog.isShow) {
      this._propertyDialog.redraw();
    }
  }

  _initContainer() {
    let container = dom
      .create('div')
      .addClass('multi-chart-container')
      .node();
    this._setContainerStyle(container);
    return container;
  }

  _setContainerStyle(container?: HTMLElement) {
    let size = this.getSize();
    let theme = getTheme(this.context.theme);
    let padding = theme.chart.padding;
    let styles = {
      position: 'relative',
      'box-sizing': 'border-box',
      overflow: 'hidden',
      margin: '0',
      'user-select': 'none',
      width: `${size.chartWidth + size.yWidth + padding[1] + padding[3]}px`,
      padding: `${padding[0]}px ${padding[1]}px ${padding[2]}px ${padding[3]}px `,
      'background-color': `${theme.chart.backgroundColor}`,
      font: `${theme.chart.bigFont}`,
      // '-webkit-font-smoothing': 'antialiased',
      // '-moz-osx-font-smoothing': 'grayscale'
    };
    container = container || this.container;
    dom(container).css(styles);
  }

  setCursor(style: CursorStyle, node: HTMLElement = document.body) {
    if (style === this.domCursorCache.get(node)) return;
    if (style === 'grabbing') {
      node.style.cursor = `${dom.cssPrefix}${style}`;
    }
    node.style.cursor = style;
    this.domCursorCache.set(node, style);
  }

  // endregion

  _initContext(): IBaseContext {
    let that = this;
    let context = {
      xAccessor,
      state: ContextState.Init(),
      mouse: this.mouse.bind(this),
      redraw: this.redraw.bind(this),
      loading: this.loading.bind(this),
      resetState: this.resetState.bind(this),
      // scale
      getScaleX: this.getScaleX.bind(this),
      updateScaleX: this.updateScaleX.bind(this),
      // zoom
      zoomIn: this.zoomIn.bind(this),
      zoomOut: this.zoomOut.bind(this),
      resetZoom: this.resetZoom.bind(this),
      tryUpdateZoom: this.tryUpdateZoom.bind(this),
      updateZoom: this._updateZoom.bind(this),
      getBandWidth: this._getBandWidth.bind(this),
      isAutoShowLast: this.isAutoShowLast.bind(this),
      showLast: this.showLast.bind(this),
      // chart & resize
      resizeChart: this._resizeChart.bind(this),
      canResizeChart: this._canResizeChart.bind(this),
      isMainChart: this.isMainChart.bind(this),
      getTimelineChart: this.getTimelineChart.bind(this),
      getMainChart: this.getMainChart.bind(this),
      getChart: this.getChart.bind(this),
      getPrevChart: this.getPrevChart.bind(this),
      getNextChart: this.getNextChart.bind(this),
      toggleMaximizeChart: this.toggleMaximizeChart.bind(this),
      moveUpChart: this.moveUpChart.bind(this),
      moveDownChart: this.moveDownChart.bind(this),
      // indicators
      updateIndicator: this.updateIndicator.bind(this),
      appendIndicator: this.appendIndicator.bind(this),
      removeIndicator: this.removeIndicator.bind(this),
      clearIndicators: this.clearIndicators.bind(this),
      // tool
      setCurTool: this.setCurTool.bind(this),
      getCurTool: this.getCurTool.bind(this),
      getChartMode: this.getChartMode.bind(this),
      setChartMode: this.setChartMode.bind(this),
      setCursor: this.setCursor.bind(this),
      // event
      on: this.on.bind(this),
      off: this.off.bind(this),
      once: this.once.bind(this),
      trigger: this.trigger.bind(this),

      get selectedLine() {
        return (that as any).selectedLine;
      },

      set selectedLine(line) {
        (that as any).selectedLine = line;
      },

      chartDataModel: null, // temp ?
      // data
      get data() {
        return chartDataModel.viewportData;
      },
      get allData() {
        return chartDataModel.allData;
      },
      get chartWidth() {
        let size = that.getSize();
        return size.chartWidth;
      },
      get template() {
        return that.template;
      },
    } as any;

    let chartDataModel = new ChartData(context, this.props);
    context.chartDataModel = chartDataModel;
    Object.setPrototypeOf(context, this.props);

    if (__DEV__) {
      if (Object.seal) Object.seal(context);
    }

    return context as IBaseContext;
  }

  resetState() {
    this.context.state = ContextState.Init();
    this.mouse(null);
    if (this.template) {
      this.template.charts.forEach(chart => {
        chart.flipY(false);
      });
    }
  }

  reset() {
    // loader data
    this.clearGroup();
    this.resetState();
    this.dataLoader.stop();
    this.redraw();
    this.dataLoader.start();
  }

  pause() {
    this.context.state.pauseRedraw = true;
    this.dataLoader.stop();
  }

  resume() {
    this.resetState();
    this.context.state.pauseRedraw = false;
    this.dataLoader.start();
  }

  getSize() {
    let leftPadding = getTheme(this.props.theme).chart.padding[3];
    let { width, height, yWidth } = this.props.sizeOption;
    return {
      width,
      height,
      yWidth,
      chartWidth: width - yWidth - leftPadding,
    };
  }

  mouse(mouse?: { x: number; y: number; chartId: number } | null): IMouseXY {
    /* 😩 */
    if (!isDefined(mouse)) {
      // getter
      let _mouseXY = this._mouseXY;
      // update closestX
      if (_mouseXY && _mouseXY.closest) {
        let scaleX = this.getScaleX();
        let closestX = scaleX(xAccessor(_mouseXY.closest));
        _mouseXY.closestX = closestX;
      }
      return _mouseXY;
    }

    if (mouse === null) {
      // reset
      if (this._mouseXY !== OUT_MOUSE_XY) {
        this._mouseXY = OUT_MOUSE_XY;
        this.redraw(DrawOpt.overlay); // TODO need only draw crosshair
      }
    } else {
      // setter
      let _mouseXY = this._mouseXY;
      let { touching, dragging, drawedDragging, wheeling } = this.context.state;
      let { x, y, chartId } = mouse;
      let chart = this.getChart(chartId);
      let height = chart && chart.getRect().height;
      if (y < 0 || y > height) {
        // when drag across chart
        let targetChart!: Chart;
        let targetHeight;
        if (y < 0) {
          targetChart = this.getPrevChart(chartId);
        }
        if (y > height) {
          targetChart = this.getNextChart(chartId);
        }
        if (targetChart && !targetChart.isTimelineChart) {
          targetHeight = targetChart.getRect().height;
          y = y < 0 ? targetHeight + y : y - height;
          chartId = targetChart.chartId;
        }
      }
      if (!wheeling && _mouseXY.x === x && _mouseXY.y === y && _mouseXY.chartId === chartId) {
        // do nothing
      } else {
        let { chartWidth, allData, data } = this.context;
        let scaleX;
        let closest, closestX;
        if (!allData.length) {
          return this.mouse(null);
        }
        scaleX = this.getScaleX();
        if (dragging && (!touching || touching === ContextState.Touching.MOVE) && !drawedDragging && _mouseXY.closest) {
          closest = _mouseXY.closest;
        } else {
          closest = calcClosestData(scaleX, data, xAccessor, x);
        }
        closestX = closest ? scaleX(xAccessor(closest)) : -10;
        if (closestX > chartWidth) {
          return this.mouse(null);
        } else {
          this._mouseXY = { chartId, x, y, closest, closestX };
        }
      }
    }
    return this._mouseXY;
  }

  loading(loading = true) {
    this.context.state.loading = !!loading;
    let loadingbar = this._loadingbar;
    if (loading) loadingbar.show();
    else loadingbar.hide();
  }

  // region about template

  _initChartTemplate(templateConfig: TemplateConfig) {
    let template = new Template(this, templateConfig);
    return template;
  }

  loadTemplate(template) {
    if (!template) return;
    this.template.loadTemplate(template);
    this.redraw();
  }

  exportTemplate() {
    return this.template.exportTemplate();
  }

  // endregion

  // region command / tool & mode

  // TODO (2018/03/16 11:49) register tool/cmd api
  _initTools() {
    let context = this.context;
    // TODO: lazy init linetool
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.TREND_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.RAY_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.HORIZONTAL_RAY_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.HORIZONTAL_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.VERTICAL_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.PARALLEL_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.FIBONACCI_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.GANN_FAN_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.LONG_POSITION_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.SHORT_POSITION_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.FIBONACCI_TREND_BASED_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.FIBONACCI_TIMEZONE_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.FIBONACCI_TIMEZONE_TREND_BASED_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.DATE_RANGE_LINE));
    this.registerTool(new Tool.DrawLineTool(context, ToolLineType.PRICE_RANGE_LINE));

    this.registerCommand(new Tool.ZoomInCmd(context));
    this.registerCommand(new Tool.ZoomOutCmd(context));
    this.registerCommand(new Tool.ZoomResetCmd(context));
    this.registerCommand(new Tool.ShowLastCmd(context));
    this.registerCommand(new Tool.DeleteLineCmd(context));
    this.registerCommand(new Tool.ClearLineCmd(context));
    this.registerCommand(new Tool.ClearLineCmd(context));
    this.registerCommand(new Tool.ToggleLineVisibleCmd(context));
    this.registerCommand(new Tool.ToggleLineLockedCmd(context));
    this.registerCommand(new Tool.ClearIndicatorsCmd(context));
    this.registerCommand(new Tool.FlipYCmd(context));

    this.setChartMode(ChartMode.NORMAL);
  }

  setCurTool(toolname: string) {
    let tool: ITool = null;
    if (toolname) {
      tool = this._tools.find(t => t.name === toolname);
      if (!tool) {
        throw new TypeError(`invalid tool: ${toolname}`);
      }
    }
    let prev = this._currentTool;
    this._currentTool = tool;
    if (prev) {
      if (prev.name === toolname) return;
      prev.deactive();
    }
    if (tool) {
      tool.active();
    }
    if (prev !== tool) this.trigger('toolChanged', { tool, name: toolname });
  }

  getCurTool() {
    return this._currentTool;
  }

  execCommand(cmdname: string) {
    let cmd: ICommand;
    if (cmdname) {
      cmd = this._commands.find(c => c.name === cmdname);
      if (!cmd) {
        throw new TypeError(`invalid command: ${cmdname}`);
      }
    }

    if (cmd) {
      cmd.active();
    }
  }

  setChartMode(mode: ChartMode) {
    this._chartMode = mode;
    timeout(() => {
      if (this._chartMode === mode) this.trigger('modeChanged', mode);
    }, 0);
  }

  getChartMode() {
    return this._chartMode;
  }

  registerTool(tool: ITool) {
    if (!this._tools) {
      this._tools = [];
    }
    if (!this._tools.find(t => t.name === tool.name)) {
      this._tools.push(tool);
    }
  }

  registerCommand(cmd: ICommand) {
    if (!this._commands) {
      this._commands = [];
    }
    if (!this._commands.find(c => c.name === cmd.name)) {
      this._commands.push(cmd);
    }
  }

  // endregion

  // region update props & data apis

  update(props: IMultiChartProps = {}): this {
    let changedChartType = isDefined(props.chartType) && props.chartType !== this.props.chartType;
    let changedSymbol = isDefined(props.symbol);
    let changedSide = isDefined(props.chartSide);
    if (changedSymbol) {
      // check
      let { id, name, scale } = props.symbol;
      if (isEmpty(id) || isEmpty(name) || isEmpty(scale)) {
        throw new Error('error props: symbol');
      }
    }
    if (props.displayOption) {
      props.displayOption = Object.assign({}, this.props.displayOption, props.displayOption);
    }
    if (props.sizeOption) {
      props.sizeOption = Object.assign({}, this.props.sizeOption, props.sizeOption);
    }

    // override props
    forEach(Object.keys(props), key => {
      if (isDefined(props[key])) {
        this.props[key] = props[key];
      }
    });

    // update
    if (props.displayOption) {
      // TODO (2018/05/23 10:42)
      this.template && this.template.triggerTemplateChanged(TemplateChangeType.DisplayOption);
    }
    if (props.sizeOption) {
      this._updateSize();
    }
    if (props.theme || props.lang) {
      this._setContainerStyle();
      this._updatePropertyDialogStyle();
    }
    if (changedChartType) {
      this.template && this.template.refreshChartConfigs();
    }
    if (changedSymbol || changedChartType || changedSide) {
      this.reset();
    }
    return this;
  }

  _updateSize() {
    let size = this.getSize();
    this.context.chartDataModel.updateScaleXRange([0, size.chartWidth]);
    this.context.chartDataModel.updateViewportData();
    // update dom size
    dom(this.container).css({ width: `${size.width}px`, height: `${size.height}px` });
    this.template && this.template._resizeCharts();
    this.context.trigger('sizeChanged');
  }

  resize(sizeOption?: SizeOption) {
    this.props.sizeOption = deepMergeObject<SizeOption>(this.props.sizeOption, sizeOption);
    this._updateSize();
    this.redraw();
  }

  // endregion

  // region template delegate methods

  _canResizeChart(chart: Chart, resizeOrigin: string, point?: DragPoint) {
    return this.template._canResizeChart(chart, resizeOrigin, point);
  }

  _resizeChart(chart: Chart, point: DragPoint) {
    this.template._resizeChart(chart, point);
  }

  getChart(id: number): Chart {
    let charts = this.template.charts;
    if (!charts) return null;
    return charts.find(chart => chart.chartId === id);
  }

  getMainChart(): MainChart {
    let charts = this.template.charts;
    if (!charts) return null;
    return charts.find(chart => chart.isMainChart) as MainChart;
  }

  getNextChart(currId: number): Chart {
    let charts = this.template.charts;
    if (!charts) return null;
    for (let i = 0; i < charts.length; i++) {
      if (charts[i].chartId === currId) {
        return charts[i + 1] || null;
      }
    }
    return null;
  }

  getPrevChart(currId: number): Chart {
    let charts = this.template.charts;
    if (!charts) return null;
    for (let i = 0; i < charts.length; i++) {
      if (charts[i].chartId === currId) {
        return charts[i - 1] || null;
      }
    }
    return null;
  }

  getTimelineChart(): TimelineChart {
    return this.template.charts.find(chart => chart.isTimelineChart) as TimelineChart;
  }

  isMainChart(chartId: number): boolean {
    let chart = this.getChart(chartId);
    return chart && chart.isMainChart;
  }

  appendIndicator(appendIndicatorConfig: IndicatorConfig) {
    let autoShowLast = this.context.isAutoShowLast();
    let result = this.template.appendIndicator(appendIndicatorConfig);
    if (result === true) {
      if (autoShowLast) this.showLast();
      this.redraw();
    }
    return result;
  }

  removeIndicator(indicatorId: number) {
    let autoShowLast = this.context.isAutoShowLast();
    if (this.template.removeIndicator(indicatorId)) {
      if (autoShowLast) this.showLast();
      this.redraw();
    }
  }

  clearIndicators() {
    let autoShowLast = this.context.isAutoShowLast();
    if (this.template.clearIndicators()) {
      if (autoShowLast) this.showLast();
      this.redraw();
    }
  }

  updateIndicator(indicatorId: number, config?: IndicatorConfig, preview?: boolean) {
    if (this.template.updateIndicator(indicatorId, config, preview)) {
      this.redraw();
    }
  }

  _getIndicatorProps(): IndicatorProps[] {
    return this.template.getIndicatorProps();
  }

  toggleMaximizeChart(chartId: number) {
    if (this.template.toggleMaximizeChart(chartId)) {
      this.redraw();
    }
  }

  moveUpChart(chartId: number) {
    if (this.template.moveUpChart(chartId)) {
      this.redraw();
    }
  }

  moveDownChart(chartId: number) {
    if (this.template.moveDownChart(chartId)) {
      this.redraw();
    }
  }

  // endregion

  // region  show last
  _getShowLastBlankMargin() {
    let { getBandWidth } = this.context;
    let blankSize = 1;
    let unitWidth = getBandWidth(1);
    let indicatorConfigs = this._getIndicatorProps();
    let ikh = indicatorConfigs.find(config => config.name === 'ikh');
    if (ikh) {
      let period = ikh.config.inputsObj.kl;
      let extend = period ? period : 0; // TODO:
      blankSize += extend;
    }
    return blankSize * unitWidth;
  }

  isAutoShowLast() {
    let { getScaleX, allData, state } = this.context;
    let lastItem = last(allData);
    if (!lastItem) return false;
    let scaleX = getScaleX();
    let lastX = scaleX(xAccessor(lastItem));
    let [, width] = scaleX.range();
    let blankMargin = this._getShowLastBlankMargin();
    let indicatorConfigs = this._getIndicatorProps();
    let ikh = indicatorConfigs.find(config => config.name === 'ikh');
    if (ikh) {
      lastX += blankMargin;
      blankMargin = this.context.getBandWidth(1);
    }

    if (Math.floor(Math.abs(width - lastX)) <= Math.ceil(blankMargin)) {
      let { dragging, resizing, cmdProcing, wheeling } = state;
      if (!dragging && !wheeling && !resizing && !cmdProcing) {
        return true;
      }
    }
    return false;
  }

  showLast(prop = { resetZoom: false }): boolean {
    prop.resetZoom && this.resetZoom();

    let { getScaleX, updateScaleX, allData, redraw } = this.context;
    let lastItem = last(allData);
    if (!lastItem) return false;
    let scaleX = getScaleX();
    let right = scaleX(xAccessor(lastItem));
    let [, width] = scaleX.range();
    let blankMargin = this._getShowLastBlankMargin();
    let dx = width - right - blankMargin;
    let dy = 0;

    let transform = d3ZoomIdentity.translate(dx, dy);
    let newScaleX = transform.rescaleX(scaleX);

    if (prop.resetZoom) {
      this.template.charts.forEach((chart) => {
        let flipY = chart.context.flipY;
        if (flipY()) flipY(false);
      });
    }
    updateScaleX(newScaleX);
    redraw();
    return true;
  }

  // endregion

  // region  draw line

  getDrawedObjects() {
    let mainChart = this.context.getMainChart();
    if (!mainChart) return;
    let mainArea = mainChart.mainArea;
    let json = mainArea.saveLines();
    return {
      key: {
        symbolId: this.context.symbol.id,
        chartType: this.context.chartType,
        chartSide: this.context.chartSide || Side.BID
      },
      json,
    };
  }

  setDrawObjects(json: any) {
    let mainChart = this.context.getMainChart();
    if (!mainChart) return;
    let mainArea = mainChart.mainArea;
    mainArea.loadLines(json);
  }

  clearGroup() {
    let mainChart = this.context.getMainChart();
    mainChart.mainArea.clearGroup();
  }

  // endregion
}
