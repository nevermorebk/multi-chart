import { ContextState, IBaseContext } from '../lib';
import { getTheme } from '../config/index';
import Canvas from '../canvas/Canvas';
import { dom } from '../util/dom';
import MainChart from '../chart/MainChart';
import { MainArea } from '../area/index';
import * as draw from '../canvas/draw';
import IView from './IView';


export default class MagnifierView extends IView {
  private chart: MainChart;
  private mainArea: MainArea;
  public canvas: Canvas;

  constructor(context: IBaseContext, parentNode: HTMLElement) {
    super(context, parentNode);
    let realRedraw = this.redraw.bind(this);
    this.redraw = () => { };
    context.once('ready', () => {
      this.chart = context.getMainChart();
      this.mainArea = this.chart.mainArea;
      this.createCanvasView();
      this.redraw = realRedraw;
    });
  }

  redraw() {
    let { state, theme, displayOption } = this.context;
    let chartId = this.mainArea.context.chartId;
    let { DrawedDragging, Touching } = ContextState;
    let mouse = this.context.mouse();
    let { drawedDragging, touching } = state;
    if (drawedDragging === DrawedDragging.CTRL_POINT) {
      if ((displayOption.magnifier || touching === Touching.MOVE) &&
        mouse.chartId === chartId && mouse.closestX > 0 && mouse.y > 0
      ) {
        this.setCanvasThemeStyle(theme);
        this.visible(true);
        this.resizeCanvas();
        this.resetPositionView();
        this.redrawMagnifier();
        this.isShow = true;
      }
    } else {
      if (this.isShow) {
        this.visible(false);
        this.isShow = false;
      }
    }

  }

  private createCanvasView() {
    let chartSize = this.chart.getSize();
    let size = {
      width: chartSize.width / 5,
      height: chartSize.height / 3,
    };
    this.canvas = new Canvas(this.context, size);
    this.canvas.element.setAttribute('tabindex', '-1');
    this.setCanvasThemeStyle();
    this.parentNode.appendChild(this.canvas.element);
  }

  private resizeCanvas() {
    let chartSize = this.chart.getSize();
    this.canvas.resize({
      width: chartSize.width / 5,
      height: chartSize.height / 3,
    });
  }

  private visible(show: boolean) {
    dom(this.canvas.element).css({
      visibility: show ? 'visible' : 'hidden',
    });
  }

  private setCanvasThemeStyle(theme?: string) {
    // TODO (2018/05/25 10:58) vdom
    let view = this as any;
    if (view.theme === theme) return;
    view.theme = theme;
    let themeRef = getTheme(theme);
    let element = this.canvas.element;
    dom(element).css({
      border: `1px solid ${themeRef.chart.border.color}`,
      'background-color': `${themeRef.chart.backgroundColor}`,
      'top': `${themeRef.chart.padding[0]}px`,
      'z-index': 100,
    });
  }

  private resetPositionView() {
    let view = this as any;
    let mouse = this.context.mouse();
    let magnifierCanvas = this.canvas;
    let rect = this.mainArea.getRect();
    let { sizeOption: { yWidth } } = this.context;
    let ctx = magnifierCanvas.ctx;
    let magnifierWidth = magnifierCanvas.size.width;
    let magnifierHeight = magnifierCanvas.size.height;
    let css = {} as any;
    let position = null;
    if (magnifierWidth + mouse.closestX + 50 > rect.width && mouse.y < magnifierHeight + 50) {
      position = 'left';
    }
    if (mouse.closestX < magnifierWidth + 50 && mouse.y < magnifierHeight + 50) {
      position = 'right';
    }
    if (position === view.position) {
      return;
    }

    view.position = position;
    if (position === 'left') {
      css.left = '0px';
      css.right = 'auto';
      dom(magnifierCanvas.element).css(css);
    } else {
      css.left = 'auto';
      css.right = `${yWidth - 1}px`;
      dom(magnifierCanvas.element).css(css);
    }
  }

  private redrawMagnifier() {
    let mouse = this.context.mouse();
    this.mainArea.context.updateTimestamp(); // invalid cached line points !!
    let { dpr } = this.context;
    let rect = this.mainArea.getRect();
    let scale = 2; // magnifier view zoomin 1.5 times
    let magnifierCanvas = this.canvas;
    let ctx = magnifierCanvas.ctx;
    let magnifierWidth = magnifierCanvas.size.width;
    let magnifierHeight = magnifierCanvas.size.height;
    let width = magnifierWidth;
    let height = magnifierHeight;
    let theme = getTheme(this.context.theme);


    let move = {
      x: -mouse.closestX + magnifierWidth / scale / 2,
      y: -mouse.y + magnifierHeight / scale / 2,
    };
    let moveRect = {
      left: Math.max(-move.x, 0),
      top: Math.max(-move.y, 0),
      width: rect.width + move.x,
      height: rect.height + move.y
    };
    // pre
    ctx.save();
    ctx.scale(scale, scale);
    ctx.clearRect(0, 0, width, height);
    ctx.translate(move.x, move.y);

    // clip in area
    ctx.save();
    ctx.beginPath();
    ctx.rect(moveRect.left, moveRect.top, moveRect.width, moveRect.height);
    ctx.clip();
    // draw
    this.mainArea.drawGridline(ctx);
    this.mainArea.drawSeries(ctx);
    this.mainArea.drawFeatures(ctx);
    this.mainArea.drawFeatures(ctx, true);
    this.mainArea.drawLines(ctx);
    ctx.restore();

    draw.drawRect2(ctx, [rect], draw.getCanvasLineStyle(theme.chart.border));

    // post
    ctx.restore();
  }

  destroy() {
    if (this.canvas) this.canvas.destroy();
  }

}
