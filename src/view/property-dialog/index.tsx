import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { IBaseContext, SetConfigParam } from '../../lib';
import ReactPropertyDialog from './ReactPropertyDialog';
import { IndicatorConfig } from '../../model/Template';
import { ContextContainer } from '../ContextContainer';
import { IMetaInfoCfg } from '../../config/meta';
import { getI18n } from '../../config';
import IView from '../IView';

class PropertyDialog extends IView {
  appElement: HTMLElement;
  params?: SetConfigParam<IndicatorConfig | IMetaInfoCfg>;
  title: string;

  constructor(
    context: IBaseContext,
    modalContainer: HTMLElement,
    appElement: HTMLElement
  ) {
    super(context, modalContainer);
    this.appElement = appElement;

    // Webpack Hot Module Replacement API
    if (module.hot) {
      module.hot.accept('./ReactPropertyDialog', () => {
        if (this.isShow) render(this);
      });
    }
  }

  open = (params) => {
    this.params = params;
    if (this.isShow !== true) {
      this.isShow = true;
      this.redraw();
    }
  };

  private close = () => {
    if (this.isShow) {
      this.isShow = false;
      this.redraw();
    }
  };

  redraw() {
    if (this.params) {
      let t = getI18n(this.context.lang).t;
      let key = 'label.dialog.';
      let subKey;
      if (this.params.cfg.type === 'drawline') {
        key += 'drawlines.title';
        subKey = `drawlines.name.${this.params.cfg.name}`;
      } else {
        // indicator
        key += 'indicators.title';
        subKey = `indicators.${this.params.cfg.name}.name`;
      }
      this.title = t(key);
      if (subKey) {
        this.title = `${this.title} - ${t(subKey)}`;
      }
    }
    render(this);
  }

  destroy() {
    if (this.isShow) {
      this.isShow = false;
      // render(this);
      ReactDOM.unmountComponentAtNode(this.parentNode);
    }
  }
}

const render = (props) => {
  ReactDOM.render(
    <ContextContainer context={props.context}>
      <If condition={props.isShow}>
        <ReactPropertyDialog
          title={props.title}
          isOpen={props.isShow}
          appElement={props.appElement}
          params={props.params}
          close={props.close}
        />
      </If>
    </ContextContainer>,
    props.parentNode
  );
};

export { PropertyDialog };
