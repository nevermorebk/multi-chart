

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactModal from 'react-modal';
import * as CSSModules from 'react-css-modules';
import * as PropTypes from 'prop-types';
import { IndicatorConfig } from '../../model/Template';
import PropertySettingPanel from './PropertySettingPanel';
import { IChartContext, SetConfigParam } from '../../lib';
import { LimitedDraggable } from './component';

import dialogStyles from './dialog.css';
import { IMetaInfoCfg } from '../../config/meta';
import { getI18n } from '../../config';

export interface IProps {
  title?: string;
  isOpen?: boolean;
  params?: SetConfigParam<IndicatorConfig | IMetaInfoCfg>;
  appElement?: HTMLElement;
  close?: Function;
}

export interface IState {
  indicatorConfig: IndicatorConfig;
}
@CSSModules(dialogStyles)
export default class ReactPropertyDialog extends React.Component<IProps, IState> {
  context: {
    chartContext: IChartContext;
  };
  static contextTypes = {
    chartContext: PropTypes.any,
  };
  static defaultProps: IProps = {
    isOpen: false
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      indicatorConfig: props.params.cfg,
    };
    ReactModal.setAppElement(props.appElement);
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.indicatorConfig !== nextProps.params.cfg) {
      this.setState({
        indicatorConfig: nextProps.params.cfg,
      });
    }
  }

  cancel = () => {
    let {cfg, update } = this.props.params;
    update(cfg, true); // make restore previous template
    this.props.close();
  };

  submit = () => {
    let { update } = this.props.params;
    update(null, false); // make template changed (event)
    this.props.close();
  };

  reset() {

  }

  render() {
    let theme = this.context.chartContext.theme;
    let t = getI18n(this.context.chartContext.lang).prefix('label.common');
    return (
      <ReactModal
        isOpen={this.props.isOpen}
        contentLabel='onRequestClose Example'
        onRequestClose={this.cancel}
        shouldCloseOnOverlayClick={false}
        className='chart-modal'
        bodyOpenClassName='chart-modal-open'
        overlayClassName='chart-modal-overlay'
      >
        <LimitedDraggable bounds='body' handle='.chart-modal-header' >
          <div className={`chart-modal-dialog chart-theme-${theme}`} styleName='chart-modal-dialog'>
            <div className='chart-modal-content' styleName='chart-modal-content'>
              <div className='chart-modal-header' styleName='chart-modal-header'>
                <button
                  type='button'
                  className='close'
                  styleName='close'
                  aria-label='Close'
                  onClick={this.cancel}
                >
                  <span aria-hidden='true'>×</span>
                </button>
                <h4>{this.props.title}</h4>
              </div>
              <div className='chart-modal-body' styleName='chart-modal-body'>
              <PropertySettingPanel
                config={this.state.indicatorConfig}
                update={this.props.params.update}
              />
              </div>
              <div className='chart-modal-footer' styleName='chart-modal-footer'>
                <button className='chart-btn chart-btn-cancel' styleName='chart-btn-cancel' onClick={this.cancel} >{t('cancel')}</button>
                <button className='chart-btn chart-btn-save' styleName='chart-btn-save' onClick={this.submit} >{t('save')}</button>
              </div>
            </div>
          </div>
        </LimitedDraggable>
      </ReactModal>
    );
  }
}
