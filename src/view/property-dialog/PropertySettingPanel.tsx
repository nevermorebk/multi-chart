import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactModal from 'react-modal';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import { IChartContext, ChartType } from '../../lib';
import { IMetaInfo, IMetaInfoCfg, MetaInfo, MetaType, MetaInfoUnit, KV } from '../../config/meta';
import { Tab, Tabs, BasicInput, IndicatorLineStyleInput, DrawedLineStyleInput, BarInput, LevelColorInput } from './component';

import panelStyles from './panel.css';
import { getI18n } from '../../config';
import Select from './component/Select';

export interface IProps {
  config: Readonly<IMetaInfoCfg>;
  update: (cfg: Readonly<IMetaInfoCfg>, preview?: boolean) => void;
}

export interface IState {
  config: Readonly<IMetaInfoCfg>;
  meta: IMetaInfo;
}

@CSSModules(panelStyles)
export default class PropertySettingPanel extends React.Component<IProps, IState> {
  context: {
    chartContext: IChartContext;
  };
  static contextTypes = {
    chartContext: PropTypes.any,
  };
  //  static defaultProps: IProps = { };

  constructor(props: IProps) {
    super(props);
    let name = props.config.name;
    let type = props.config.type;
    let meta = MetaInfo.getDefault(type, name);
    this.state = {
      config: props.config,
      meta: meta,
    };
  }

  componentWillReceiveProps(next) {
    // if (next.config) {
    //   this.setState({config: next.config});
    // }
  }

  updateProperty = ({ name: inputName, value: newValue }) => {
    let [key, name] = inputName.split('.');
    let { inputs, styles, ...rest } = this.state.config;
    let updateKey = key === 'inputs' ? 'inputsObj' : 'stylesObj';
    let cfg = { ...rest } as IMetaInfoCfg;
    if (cfg[updateKey][name] === newValue) return;
    // update inputsObj/stylesObj
    cfg[updateKey] = {
      ...cfg[updateKey],
      [name]: newValue,
    };

    this.setState({
      config: cfg,
    });
    this.props.update(cfg, true);
  };

  _getCurrent(array: KV[], name: string): any {
    let found = array.find(item => item.name === name);
    return found;
  }

  _renderFields(key: string) {
    let { meta, config } = this.state;
    let { name } = meta;
    let units: MetaInfoUnit[] = meta[key];
    let current: any = config[key + 'Obj'];
    return units
      .filter(input => !input.readonly)
      .map(unit => {
        let { type, name, def } = unit;
        let curr = current[name];
        let input;
        let val = curr === undefined ? def : curr;
        if (type === MetaType.Integer) {
          let min = (unit && unit.min) || 0,
            max = unit && unit.max,
            step = unit && unit.step,
            precision = unit && unit.precision;
          input = (
            <BasicInput
              type='number'
              name={`${key}.${name}`}
              min={min}
              max={max}
              step={step}
              precision={precision}
              value={val}
              onChange={this.updateProperty}
            />
          );
        } else if (type === MetaType.Bool) {
          input = <BasicInput type='checkbox' name={`${key}.${name}`} value={val} onChange={this.updateProperty} />;
        } else if (type === MetaType.Color) {
          input = <BasicInput type='color' name={`${key}.${name}`} value={val} onChange={this.updateProperty} />;
        } else if (type === MetaType.Alpha) {
          input = <BasicInput type='alpha' name={`${key}.${name}`} value={val} onChange={this.updateProperty} />;
        } else if (type === MetaType.IndicatorLine) {
          input = <IndicatorLineStyleInput name={`${key}.${name}`} value={val} onChange={this.updateProperty} />;
        } else if (type === MetaType.DrawedLine) {
          input = <DrawedLineStyleInput name={`${key}.${name}`} value={val} onChange={this.updateProperty} />;
        } else if (type === MetaType.Bar) {
          input = <BarInput name={`${key}.${name}`} value={val} onChange={this.updateProperty} />;
        } else if (type === MetaType.LevelColor) {
          input = <LevelColorInput name={`${key}.${name}`} value={val} defaultValue={def} onChange={this.updateProperty} />;
        } else if (type === MetaType.ChartType) {
          input = (
            <div className='input-groups'>
              <div className='input-item' style={{ display: 'inline-block', width: '172px' }}>
                <Select
                  name={`${key}.${name}`}
                  value={val}
                  i18n={true}
                  i18nPrefix='chartType'
                  options={ChartType.IndicatorOptions}
                  onChange={this.updateProperty}
                />
              </div>
            </div>
          );
        }

        return {
          input,
          name,
          type,
          label: `${key}.${name}`,
        };
      });
  }

  _renderDrawlineFields() {
    let inputsFields = this._renderFields('inputs');
    let stylesFields = this._renderFields('styles');
    stylesFields.push(...inputsFields);
    return stylesFields;
  }

  render() {
    let inputsFields = [];
    let stylesFields = [];
    let tabPrefix = 'label.';
    let paramPrefix = '';
    let i18n = getI18n(this.context.chartContext.lang);
    let tabI18n, paramI18n;
    if (this.props.config.type === 'drawline') {
      stylesFields = this._renderDrawlineFields();
      tabPrefix += 'dialog.drawlines';
      paramPrefix += 'drawlines';
    } else {
      // indicator line
      inputsFields = this._renderFields('inputs');
      stylesFields = this._renderFields('styles');
      tabPrefix += 'dialog.indicators';
      paramPrefix += `indicators.${this.props.config.name}`;
    }
    tabI18n = i18n.prefix(tabPrefix);
    paramI18n = i18n.prefix(paramPrefix);
    let tabs = [];
    if (inputsFields.length > 0) {
      tabs.push(
        <Tab title={tabI18n('inputs')} key='input'>
          <table className='chart-setting-table'>
            <tbody>
              {inputsFields.map(({ input, name, label }) => (
                <tr key={name}>
                  <td className='label-name'>
                    <div>{paramI18n(label)}</div>
                  </td>
                  <td className='label-value'>
                    <div>{input}</div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </Tab>
      );
    }
    if (stylesFields.length > 0) {
      tabs.push(
        <Tab title={tabI18n('styles')} key='style'>
          <table className='chart-setting-table'>
            <tbody>
              {stylesFields.map(({ input, name, label }) => (
                <tr key={name}>
                  <td className='label-name'>
                    <div>{paramI18n(label)}</div>
                  </td>
                  <td className='label-value'>
                    <div>{input}</div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </Tab>
      );
    }
    return (
      <div className='chart-setting-panel'>
        <Tabs defaultActiveIndex={0}>{tabs}</Tabs>
        {/* <div style={{ whiteSpace: 'pre', maxHeight: '100px' }}>{JSON.stringify(this.state.config, null, 4)}</div> */}
      </div>
    );
  }
}
