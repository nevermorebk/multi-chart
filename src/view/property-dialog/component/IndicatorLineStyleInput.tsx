import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactModal from 'react-modal';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import { IChartContext, IndicatorLineMetaInfo, IndicatorDrawStyle } from '../../../lib';
import BasicInput from './BasicInput';
import Select from './Select';

import inputStyles from './inputs.css';


const styleOptions = IndicatorDrawStyle.options;
const widthOptions = [0.5, 1, 2, 3 ].map(o => ({label: o, value: o}));

export interface IProps {
  name: string;
  value: IndicatorLineMetaInfo;
  onChange: Function;
}

export interface IState {
  value: IndicatorLineMetaInfo;
  styleOptions: any[];
  widthOptions: any[];
}

@CSSModules(inputStyles)
export default class IndicatorLineStyleInput extends React.Component<IProps, IState> {
  context: {
    chartContext: IChartContext;
  };
  static contextTypes = {
    chartContext: PropTypes.any,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      value: this.props.value,
      styleOptions,
      widthOptions
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  handleColorChange = ({value:color, name}) => {
    let {color: c, ...rest} = this.state.value;
    let value = {...rest, color};
    this.setState({
      value
    });
    this.props.onChange({name, value});
  }

  handleWidthChange = ({value: width, name}) => {
    let {width: w, ...rest} = this.state.value;
    let value = {...rest,  width};
    this.setState({
      value
    });
    this.props.onChange({name, value});
  }

  handleStyleChanged = ({value: style, name}) => {
    let {style: s, ...rest} = this.state.value;
    let value = {...rest, style};
    this.setState({
      value
    });
    this.props.onChange({name, value});
  }

  render() {
    let value = this.state.value;
    let {color, width, style, styleFixed} = value;
    if (width === undefined) width = IndicatorLineMetaInfo.DefaultLineStyle.width;
    if (style === undefined) style = IndicatorLineMetaInfo.DefaultLineStyle.style;
    return (
      <div styleName='input-groups'>
        <div styleName='input-item'>
          <BasicInput type='color' name={this.props.name} value={color} onChange={this.handleColorChange} />
        </div>
        <div styleName='input-item'>
          <Select
            className='chart-select-control-type-width'
            options={this.state.widthOptions}
            value={width}
            name={this.props.name}
            onChange={this.handleWidthChange}
          />
        </div>
        <div styleName='input-item'>
          <Select
            options={this.state.styleOptions}
            value={style}
            name={this.props.name}
            disabled={!!styleFixed}
            i18n={true}
            onChange={this.handleStyleChanged}
          />
        </div>
      </div>
    );
  }
}
