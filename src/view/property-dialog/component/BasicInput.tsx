import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactModal from 'react-modal';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import NumberInput from './NumberInput';
import ColorInput from './ColorInput';
import AlphaInput from './AlphaInput';

import inputStyles from './inputs.css';

export interface IProps {
  className?: string;
  name: string;
  type: string;
  value: any;
  max?: number;
  min?: number;
  step?: number;
  precision?: number;
  disableAlpha?: boolean;
  onChange: Function;
}

export interface IState {
  value: any;
}

@CSSModules(inputStyles)
export default class BasicInput extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      value: this.props.value
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  handleNumberInputChange = (number, string, input) => {
    let value = number;
    let name = input.name;
    this.setState({ value });
    this.props.onChange({
      name, value
    });
  }

  handleColorChange = (value, name) => {
    this.setState({ value });
    this.props.onChange({
      name, value
    });
  }

  handleAlphaChange = (value, name) => {
    this.setState({ value });
    this.props.onChange({
      name, value
    });
  }
  handleChange = (e) => {
    let value = e.target.value;
    let type = e.target.type;
    if (type === 'checkbox') {
      value = e.target.checked;
    }
    this.setState({ value });
    this.props.onChange({
      name: this.props.name,
      value,
    });
  }

  render() {
    let value = this.state.value;
    return (
      <Choose>
        <When condition={this.props.type === 'number'}>
          <NumberInput
            noStyle={true}
            name={this.props.name}
            min={this.props.min || 0}
            max={this.props.max}
            step={this.props.step || 1}
            precision={this.props.precision || 0}
            value={value}
            strict={false}
            onChange={this.handleNumberInputChange}
          />
        </When>
        <When condition={this.props.type === 'color'}>
          <ColorInput
            name={this.props.name}
            disableAlpha={this.props.disableAlpha}
            value={value}
            onChange={this.handleColorChange}
          />
        </When>
        <When condition={this.props.type === 'checkbox'}>
          <label className='chart-checkbox-control'>
            <input type={this.props.type} name={this.props.name} checked={value} onChange={this.handleChange} />
          </label>
        </When>
        <When condition={this.props.type === 'alpha'}>
          <AlphaInput
            name={this.props.name}
            value={value}
            onChange={this.handleAlphaChange}
          />
        </When>
        <Otherwise>
          <input type={this.props.type} name={this.props.name} value={value} onChange={this.handleChange} />
        </Otherwise>
      </Choose>
    );
  }
}
