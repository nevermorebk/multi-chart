import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import { AlphaPicker } from 'react-color';

import inputStyles from './inputs.css';

export type IAlphaProps = {
  name: string;
  value: number;
  onChange: Function;
};

export type IAlphaState = {
  value: number;
};


const styles = {
  width: '12px',
  height: '12px',
  borderRadius: '50%',
  transform: 'translate(-9px, -1px)',
  backgroundColor: 'rgb(248, 248, 248)',
  boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.37)',
};
const AlphaPointer = ({ direction }) => {
  return (
    <div className='chart-alpha-input-control-pointer' style={styles} />
  )
}
@CSSModules(inputStyles)
export default class AlphaInput extends React.Component<IAlphaProps, IAlphaState> {

  private pointer: any;
  constructor(props: IAlphaProps) {
    super(props);
    this.state = {
      value: this.props.value,
    };
    this.pointer = AlphaPointer;
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  handleChange = (data) => {
    let value = data.rgb.a;
    this.setState({ value });
    this.props.onChange(value, this.props.name);
  }

  render() {
    let color = `rgba(0, 0, 0, ${this.state.value})`;
    return (
        <AlphaPicker
          className='chart-alpha-input-control'
          width={100}
          height={10}
          pointer={this.pointer}
          color={color}
          onChangeComplete={this.handleChange}
        />
    );
  }
}
