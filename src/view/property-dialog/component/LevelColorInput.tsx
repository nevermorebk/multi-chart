import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactModal from 'react-modal';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import { IChartContext, LevelColorMetaInfo } from '../../../lib';
import BasicInput from './BasicInput';
import Select from './Select';

import inputStyles from './inputs.css';
import { getI18n } from '../../../config';
import NumberInput from './NumberInput';

export interface IProps {
  name: string;
  value: LevelColorMetaInfo;
  defaultValue?: LevelColorMetaInfo;
  onChange: Function;
}

export interface IState {
  prevValue: LevelColorMetaInfo;
  value: LevelColorMetaInfo;
}

@CSSModules(inputStyles)
export default class LevelColorInput extends React.Component<IProps, IState> {
  context: {
    chartContext: IChartContext;
  };
  static contextTypes = {
    chartContext: PropTypes.any,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      prevValue: this.props.value,
      value: this.props.value,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  handleLevelDisableChange = (event) => {
    let input = event.target;
    if (!input) return;
    let val =  !!input.checked;
    let index = +input.name;

    let { levels, ...rest } = this.state.value;
    let newLevels = [...levels];
    newLevels[index] = {...newLevels[index]};
    newLevels[index].disable = !val;
    let value: LevelColorMetaInfo = {...rest, levels: newLevels};
    this.setState({value});
    this.props.onChange({name: this.props.name, value});
  }

  handleLevelValChange = (number, string, input) => {
    let val = number;
    let index = +input.name;

    let { levels, ...rest } = this.state.value;
    let newLevels = [...levels];
    newLevels[index] = {...newLevels[index]};
    newLevels[index].val = val;
    if (newLevels[index].label !== undefined) newLevels[index].label = `${val}`;
    let value: LevelColorMetaInfo = {...rest, levels: newLevels};
    this.setState({value});
    this.props.onChange({name: this.props.name, value});
  }

  handleLevelColorChange = ({value:color, name}) => {
    let index = +name;
    let { levels, ...rest } = this.state.value;
    let newLevels = [...levels];
    newLevels[index] = {...newLevels[index]};
    newLevels[index].color = color;
    let value: LevelColorMetaInfo = {...rest, levels: newLevels, oneColor: false};
    this.setState({value});
    this.props.onChange({name: this.props.name, value});
  }

  handleOneColorCheckChange = (event) => {
    let input = event.target;
    if (!input) return;
    let checked = input.checked;
    let { levels, ...rest } = this.state.value;
    let oneColor;
    if (checked) {
      let newLevels = [...levels];
      oneColor = levels[0].color;
      newLevels.forEach((level, i) => {
        newLevels[i] = {...level};
        newLevels[i].color = oneColor;
      });
      levels = newLevels;
    } else {
      let defaultLevels = this.state.prevValue.levels;
      let hasDiff = defaultLevels.find(({color}) => color !== defaultLevels[0].color);
      if (!hasDiff) { // use default metainfo
        defaultLevels = this.props.defaultValue && this.props.defaultValue.levels;
      }
      levels = defaultLevels.map((level, i) => ({...levels[i], color: level.color}));
      oneColor = checked;
    }
    let value: LevelColorMetaInfo = {...rest, levels, oneColor};
    this.setState({value});
    this.props.onChange({name: this.props.name, value});
  }

  handleOneColorChange = ({value:color, name}) => {
    let {levels, ...rest} = this.state.value;
    let newLevels = [...levels];
    newLevels.forEach((level, i) => {
      newLevels[i] = {...level};
      newLevels[i].color = color;
    });
    let value = {...rest, levels: newLevels, oneColor: color};
    this.setState({value});
    this.props.onChange({name: this.props.name, value});
  }


  render() {
    let value = this.state.value;
    let {levels, oneColor, step, precision } = value;
    // 一つの色を使って下さい
    let t = getI18n(this.context.chartContext.lang).prefix('drawlines.styles');
    return (
      <div className='chart-level-color-wrapper'>
        <ul>
          {levels.map((level, i) => (
              <li key={i}>
                <label className='chart-checkbox-control'>
                  <input type='checkbox' name={`${i}`} checked={!level.disable} onChange={this.handleLevelDisableChange}/>
                </label>
                <If condition={level.label === undefined}>
                  {/* <input type='number' min={0} step={step} name={`${i}`} value={level.val} onChange={this.handleLevelValChange}/> */}
                  <NumberInput
                    noStyle={true}
                    name={i}
                    min={0}
                    step={step || 1}
                    precision={precision || 0}
                    value={level.val}
                    strict={true}
                    onChange={this.handleLevelValChange}
                  />
                </If>
                <If condition={level.label !== undefined}>
                  <input type='text' disabled={true}  value={level.label}/>
                </If>
                <BasicInput type='color' disableAlpha={true} name={`${i}`} value={level.color} onChange={this.handleLevelColorChange} />
              </li>
          ))}
        </ul>
        <div className='chart-level-color-one'>
          <label className='chart-checkbox-control'>
            <input type='checkbox' checked={!!oneColor} onChange={this.handleOneColorCheckChange}/>
            <span>{t('oneColor')}</span>
          </label>
          <If condition={!!oneColor}>
            <BasicInput type='color' disableAlpha={true} name='oneColor' value={oneColor} onChange={this.handleOneColorChange} />
          </If>
        </div>
      </div>
    );
  }
}
