import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import { SketchPicker } from 'react-color';

import inputStyles from './inputs.css';

export type IProps = {
  name: string;
  value: string;
  disableAlpha?: boolean;
  onChange: Function;
};

export type IState = {
  displayColorPicker?: boolean;
  value: string;
};

@CSSModules(inputStyles)
export default class ColorInput extends React.Component<IProps, IState> {

  private popover: HTMLElement;

  private cover: HTMLElement;
  constructor(props: IProps) {
    super(props);
    let color = this.props.value;
    this.state = {
      displayColorPicker: false,
      value: color,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker }, () => {
      this.resetPosition();
    });
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  handleChange = ({ rgb, hex }, event) => {
    let value = rgb.a !== 1 ? `rgba(${rgb.r},${rgb.g},${rgb.b},${rgb.a})` : hex;
    this.setState({ value });
    this.props.onChange(value, this.props.name);
  };

  resetPosition() {
    if (!this.cover) return;
    let wh = window.innerHeight;
    let popoverRect = this.popover.getBoundingClientRect();
    // let coverRect = this.cover.getBoundingClientRect();
    let delta = popoverRect.top + popoverRect.height - wh;
    if (delta > 0) {
      this.popover.style.top = `${-delta}px`;
    }
  }

  render() {
    return (
      <div className='chart-color-input-control-wrapper'>
        <div className='chart-color-input-control' onClick={this.handleClick}>
          <div
            className='chart-color-input'
            style={{ backgroundColor: this.state.value }}
          />
        </div>
        {this.state.displayColorPicker ? (
          <div className='chart-color-input-popover' ref={(popover) => {this.popover = popover;}}>
            <div className='chart-color-input-cover' ref={(cover) => {this.cover = cover;}} onClick={this.handleClose} />
            <SketchPicker
              width={240}
              className='chart-color-picker'
              color={this.state.value}
              onChangeComplete={this.handleChange}
              disableAlpha={this.props.disableAlpha}
            />
          </div>
        ) : null}
      </div>
    );
  }
}
