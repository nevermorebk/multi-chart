import * as React from 'react';
import * as CSSModules from 'react-css-modules';

// import tabsStyle from './tabs.css';

// @CSSModules(tabsStyle)
export default class Tab extends React.Component<any, any> {
  constructor(props, context) {
    super(props, context);
  }

  handleClick = (event) => {
    event.preventDefault();
    this.props.onClick(this.props.tabIndex);
  }

  render() {
    return (
      <li
        className={`chart-nav-item ${this.props.isActive ? 'active' : ''}`}
        onClick={this.handleClick}
      >
        {this.props.title}
      </li>
    );
  }
}
