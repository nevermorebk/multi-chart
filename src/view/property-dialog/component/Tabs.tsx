import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';

import tabsStyle from './tabs.css';
import { Fragment } from 'react';

@CSSModules(tabsStyle)
export default class Tabs extends React.Component<any, any> {
  constructor(props, context) {
    super(props, context);
    this.state = {
      activeIndex: this.props.defaultActiveIndex,
    };
  }

  handleTabClick = tabIndex => {
    this.setState({
      activeIndex: tabIndex,
    });
  };

  renderNavItems() {
    return React.Children.map(this.props.children, (child, index) => {
      return React.cloneElement(child as React.ReactElement<any>, {
        onClick: this.handleTabClick,
        tabIndex: index,
        isActive: index === this.state.activeIndex,
      });
    });
  }

  renderActiveTabContent() {
    const { children } = this.props;
    const { activeIndex } = this.state;
    if (children[activeIndex]) {
      return children[activeIndex].props.children;
    }
  }

  render() {
    return (
      <Fragment>
        <ul className='chart-nav-tabs'>{this.renderNavItems()}</ul>
        <div className='chart-nav-content'>{this.renderActiveTabContent()}</div>
      </Fragment>
    );
  }
}
