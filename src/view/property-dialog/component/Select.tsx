// based on https://github.com/fraserxu/react-dropdown

import * as React from 'react';
import classNames from 'classnames';
import * as PropTypes from 'prop-types';

import * as CSSModules from 'react-css-modules';

import inputStyles from './inputs.css';
import { IChartContext } from '../../../lib';
import { getI18n } from '../../../config';
import { dom } from '../../../util';
export type IProps = {
  className?: string;
  baseClassName?: string;
  value: string | number;
  options: any[];
  name: string;
  i18n?: boolean;
  i18nPrefix?: string;
  disabled?: boolean;
  onFocus?: Function;
  onChange?: Function;
};

export type IState = {
  selected: { label: string; value: string };
  isOpen: boolean;
};

@CSSModules(inputStyles)
export default class Select extends React.Component<IProps, IState> {
  static defaultProps = {
    baseClassName: 'chart-select-control',
    i18nPrefix: 'label.options.indicatorLineStyle',
  };
  context: {
    chartContext: IChartContext;
  };
  static contextTypes = {
    chartContext: PropTypes.any,
  };

  private container: HTMLElement;
  private menu: HTMLElement;
  constructor(props) {
    super(props);

    let value = this.props.value;
    let selected =
      value !== undefined
        ? this._findByValue(value)
        : {
            label: '',
            value: '',
          };
    this.state = { selected, isOpen: false };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.value !== undefined && newProps.value !== this.state.selected.value) {
      let value = newProps.value;
      let selected = this._findByValue(value);
      this.setState({ selected });
    }
  }

  componentDidMount() {
    document.addEventListener('click', this.handleDocumentClick, false);
    document.addEventListener('touchend', this.handleDocumentClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleDocumentClick, false);
    document.removeEventListener('touchend', this.handleDocumentClick, false);
  }

  handleMouseDown = event => {
    if (this.props.onFocus && typeof this.props.onFocus === 'function') {
      this.props.onFocus(this.state.isOpen);
    }
    if (event.type === 'mousedown' && event.button !== 0) return;
    event.stopPropagation();
    event.preventDefault();

    if (!this.props.disabled) {
      this.setState(
        {
          isOpen: !this.state.isOpen,
        },
        () => {
          this.resetPosition();
        }
      );
    }
  };

  resetPosition() {
    if (!this.menu) return;
    let wh = window.innerHeight;
    let menuRect = this.menu.getBoundingClientRect();
    let delta = menuRect.top + menuRect.height - wh;
    if (delta > 0) {
      this.menu.style.top = `${-delta}px`;
    }
  }

  _findByValue(value) {
    let options = this.props.options;
    let selected = options.find(option => {
      if (option && option.value !== undefined) return option.value === value;
      return option === value;
    });
    selected = selected && selected.label ? selected : { label: selected, value: selected };
    return selected;
  }
  setValue(value, label) {
    let newState = {
      selected: this._findByValue(value),
      isOpen: false,
    };
    this.fireChangeEvent(newState);
    this.setState(newState);
  }

  fireChangeEvent = newState => {
    if (newState.selected.value !== this.state.selected.value && this.props.onChange) {
      this.props.onChange({
        value: newState.selected.value,
        name: this.props.name,
      });
    }
  };

  renderOption(option) {
    let optionClass = classNames({
      [`${this.props.baseClassName}-option`]: true,
      'is-selected': option === this.state.selected,
    });

    let value = option.value !== undefined ? option.value : option;
    let label = option.label !== undefined ? option.label : option;
    let t = getI18n(this.context.chartContext.lang).prefix(this.props.i18nPrefix);
    if (this.props.i18n) label = t(label);
    return (
      <div
        key={value}
        className={optionClass}
        onMouseDown={this.setValue.bind(this, value, label)}
        onClick={this.setValue.bind(this, value, label)}
        data-value={value}
      >
        {label}
      </div>
    );
  }

  buildMenu() {
    let { options, baseClassName } = this.props;
    let ops = options.map(option => this.renderOption(option));

    return ops.length ? ops : <div className={`${baseClassName}-noresults`} />;
  }

  handleDocumentClick = event => {
    if (this.container) {
      if (!this.container.contains(event.target)) {
        if (this.state.isOpen) {
          this.setState({ isOpen: false });
        }
      }
    }
  };

  render() {
    const { baseClassName, className } = this.props;
    let t = getI18n(this.context.chartContext.lang).prefix(this.props.i18nPrefix);
    const disabledClass = this.props.disabled ? 'chart-select-disabled' : '';
    let label = this.state.selected.label;
    const selectedValue = this.state.selected.value;
    if (this.props.i18n) label = t(label);
    let value = (
      <div className={`${baseClassName}-label`} data-value={selectedValue}>
        {label}
      </div>
    );
    let menu = this.state.isOpen ? (
      <div
        ref={menu => {
          this.menu = menu;
        }}
        className={`${baseClassName}-menu`}
      >
        {this.buildMenu()}
      </div>
    ) : null;

    let selectClass = classNames({
      [className]: !!className,
      [`${baseClassName}`]: true,
      'is-open': this.state.isOpen,
      hairlines: dom.isSupportHairline(),
    });

    return (
      <div
        className={selectClass}
        ref={ref => {
          this.container = ref;
        }}
      >
        <div className={`chart-select-control-inner ${disabledClass}`} onMouseDown={this.handleMouseDown} onTouchEnd={this.handleMouseDown}>
          {value}
          <span className={`${baseClassName}-arrow`} />
        </div>
        {menu}
      </div>
    );
  }
}
