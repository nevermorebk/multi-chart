import * as React from 'react';
import * as PropTypes from 'prop-types';
import Draggable from 'react-draggable';
import { debounce } from '../../../util';

export default class LimitedDraggable extends React.Component<any, any> {
  contentRef: HTMLElement;

  setContentRef = (element) => {
    this.contentRef = element;
  };

  constructor(props) {
    super(props);
    this.state = { bounds: false, position: null };
    this.handleWindowResized = debounce(
      this.handleWindowResized.bind(this),
      500
    );
  }
  componentDidMount() {
    this.handleWindowResized();
    window.addEventListener('resize', this.handleWindowResized);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResized);
  }

  handleWindowResized() {
    let content = this.contentRef;
    if (content) {
      let position;
      let winWidth = window.innerWidth;
      let winHeight = window.innerHeight;
      let { left, top, width, height } = content.getBoundingClientRect();
      if (top + 20 > winHeight || left + 20 > winWidth) {
        position = {
          x: 0,
          y: 0,
        };
        this.setState({ position });
        return this.handleWindowResized();
      }
      if (this.state.position) {
        position = null;
      }
      this.setState({
        position,
        bounds: {
          left: -left,
          top: -top,
          right: winWidth - width - left,
          bottom: winHeight - height - top,
        },
      });
    }
  }

  render() {
    return (
      <Draggable
        position={this.state.position}
        bounds={this.state.bounds}
        handle={this.props.handle}
        grid={[10, 10]}
      >
        {React.Children.map(this.props.children, (node) => {
          if (React.isValidElement(node)) {
            return React.cloneElement(node, {
              ref: this.setContentRef,
            });
          }
        })}
      </Draggable>
    );
  }
}
