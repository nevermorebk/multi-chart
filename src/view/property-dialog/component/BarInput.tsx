import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import { IChartContext, BarMetaInfo } from '../../../lib';
import ColorInput from './ColorInput';

import inputStyles from './inputs.css';

export interface IProps {
  name: string;
  value: BarMetaInfo;
  onChange: Function;
}

export interface IState {
  value: BarMetaInfo;
}

@CSSModules(inputStyles)
export default class BarInput extends React.Component<IProps, IState> {
  context: {
    chartContext: IChartContext;
  };
  static contextTypes = {
    chartContext: PropTypes.any,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      value: this.props.value,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }


  handleColorChange = (color, name) => {
    let { positive, negative } = this.state.value;
    let value = Object.assign({positive, negative}, {
      [name]: color
    });
    this.setState({ value });
    this.props.onChange({
      name: this.props.name,
      value,
    });
  };

  render() {
    let value = this.state.value;
    let { positive, negative } = value;
    return (
      <div styleName='input-groups'>
        <div styleName='input-item'>
          <ColorInput name='positive' value={positive} onChange={this.handleColorChange} />
        </div>
        <div styleName='input-item'>
          <ColorInput name='negative' value={negative} onChange={this.handleColorChange} />
        </div>
      </div>
    );
  }
}
