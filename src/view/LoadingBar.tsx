import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as ReactDOM from 'react-dom';
import { ContextContainer } from './ContextContainer';
import { getTheme, getI18n } from '../config';
import IView from './IView';
import { Loading } from '../model/DataLoader';
export default class LoadingBar extends IView implements Loading {
  show(): void {
    this.isShow = true;
    render(this);
  }
  hide(): void {
    this.isShow = false;
    render(this);
  }
  toggle(): void {
    this.isShow = !this.isShow;
    render(this);
  }

  redraw() {
    let showLoading = this.context.state.loading;
    if (!showLoading) {
      if (this.isShow) {
        this.isShow = false;
        render(this);
      }
    } else {
      this.isShow = true;
      render(this);
    }
  }

  destroy() {
    if (this.isShow) {
      this.isShow = false;
      // render(this);
      ReactDOM.unmountComponentAtNode(this.parentNode);
    }
  }
}

type IProps = {
  isShow: boolean;
};

const styles: any = {
  bounding: {
    position: 'absolute',
    zIndex: 101,
    left: '50%',
    top: '50%',
    height: 'auto',
    transform: 'translate3d(-50%, -50%, 0)',
    pointerEvents: 'none',
    willChange: 'transform',
  },
  content: {
    display: 'block',
    boxSizing: 'border-box',
    margin: 0,
    padding: '5px 10px',
    // color: `${theme.loadingbar.color}`,
    // border: `1px solid ${theme.loadingbar.borderColor}`,
    // 'background-color': `${theme.loadingbar.backgroundColor}`,
    borderRadius: '3px',
    fontSize: '12px',
    overflow: 'hidden',
  },
};

const LoadingBarPanel: React.SFC<IProps> = (props, context) => {
  if (!props.isShow) return null;
  let theme = getTheme(context.chartContext.theme);
  let t = getI18n(context.chartContext.lang).t;
  styles.content = {
    ...styles.content,
    color: `${theme.loadingbar.color}`,
    border: `1px solid ${theme.loadingbar.borderColor}`,
    backgroundColor: `${theme.loadingbar.backgroundColor}`,
  };
  return (
    <div className="chart-loadingbar-panel" style={styles.bounding}>
      <div style={styles.content}>{t('loading.text')}</div>
    </div>
  );
};

LoadingBarPanel.contextTypes = {
  chartContext: PropTypes.any,
};

const render = (props) => {
  ReactDOM.render(
    <ContextContainer context={props.context}>
      <LoadingBarPanel isShow={props.isShow} />
    </ContextContainer>,
    props.parentNode
  );
};
