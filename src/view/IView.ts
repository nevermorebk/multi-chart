import { IBaseContext } from '../lib';



export default abstract class IView {
  context: IBaseContext;
  parentNode: HTMLElement;
  isShow: boolean;

  constructor(context: IBaseContext, parentNode: HTMLElement) {
    this.context = context;
    this.parentNode = parentNode;
    this.isShow = false;
  }

  abstract redraw();

  abstract destroy();
}
