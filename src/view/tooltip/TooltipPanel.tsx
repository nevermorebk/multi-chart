import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as CSSModules from 'react-css-modules';
import {
  IChartContext,
  OHLC,
  ChartType,
  SeriesParams,
  Position,
} from '../../lib';
import * as indicator from '../../indicator';
import tooltipStyles from './tooltip.css';
import { getTheme, getI18n } from '../../config';
import { priceFormat, timeCustomFormat, forEach, flatten } from '../../util';

export type IProps = {
  isShow: boolean;
};

@CSSModules(tooltipStyles)
export default class ToolTipPanel extends React.Component<IProps> {
  private boundingBox: HTMLElement;

  private contentBox: HTMLElement;

  private boundingStyle: any;

  context: {
    chartContext: IChartContext;
  };

  static contextTypes = {
    chartContext: PropTypes.any,
  };

  private calcStyles() {
    const chartContext = this.context.chartContext;
    let theme = getTheme(chartContext.theme);
    let {
      sizeOption: { xHeight, yWidth },
      chartWidth,
      mouse,
    } = this.context.chartContext;
    let { closestX } = mouse();
    let tooltipWidth = theme.tooltip.width;
    let offset = 4; // theme.tooltip.offset;
    let position: Partial<Position> = {
      top: 'auto',
      bottom: `${xHeight + offset}px`,
    };
    if (!this.boundingStyle) {
      this.boundingStyle = {
        width: `${theme.tooltip.width}px`,
        left: `${offset + theme.chart.padding[3]}px`,
      };
    }
    if (tooltipWidth + closestX + 50 > chartWidth - yWidth) {
      let left = offset + theme.chart.padding[3];
      position.left = `${left}px`;
      position.right = 'auto';
    }
    if (closestX < tooltipWidth + 50) {
      let right = yWidth + offset;
      position.left = 'auto';
      position.right = `${right}px`;
    }

    let bounding = {
      ...this.boundingStyle,
      ...position,
    };
    let content = {
      color: `${theme.tooltip.color}`,
      borderColor: `${theme.tooltip.borderColor}`,
      backgroundColor: `${theme.tooltip.backgroundColor}`,
    };
    this.boundingStyle = bounding;
    return { bounding, content };
  }

  // TODO: dataKey need contains template
  private getDataKeyByIndicatorId(id: number): string {
    let charts = this.context.chartContext.template.charts;
    let series: SeriesParams[] = flatten<SeriesParams>(
      charts.map((chart) => chart.context.series)
    );
    let found = series.find((s) => s.id === id);
    if (found) {
      return found.dataKey;
    }
    return null;
  }
  private renderItems() {
    const chartContext = this.context.chartContext;
    let {
      sizeOption: { xHeight, yWidth },
      lang,
    } = chartContext;
    let mouse = chartContext.mouse();
    let { closest } = mouse;
    let t = getI18n(lang).t;

    let items: Array<{
      label?: string;
      title?: { __html: string };
      value?: any;
      color?: string;
    }> = [];
    let indicatorProps = chartContext.template.getIndicatorProps();
    let indicatorKeys = [{ name: 'data' }, ...indicatorProps];
    let scale = chartContext.symbol.scale;
    for (let i = 0; i < indicatorKeys.length; i++) {
      let indicatorConfig = indicatorKeys[i] as any;
      let { name, primary } = indicatorConfig;
      if (name === 'data') {
        let ohlc = closest.data || ({} as OHLC);
        let date = closest.date;
        const priceFormatter = priceFormat(scale);
        if (date) {
          const dateFormatter = timeCustomFormat(
            'yyyy/MM/dd',
            chartContext.tzOffset
          );
          const timeFormatter = timeCustomFormat(
            'HH:mm:ss',
            chartContext.tzOffset
          );
          items.push({ label: `${t('date')}`, value: dateFormatter(date) });
          items.push({ label: `${t('time')}`, value: timeFormatter(date) });
        }

        if (chartContext.chartType === ChartType.TICK) {
          items.push({
            label: `${t('close')}`,
            value: priceFormatter(ohlc.close),
          });
        } else {
          items.push({
            label: `${t('open')}`,
            value: priceFormatter(ohlc.open),
          });
          items.push({
            label: `${t('high')}`,
            value: priceFormatter(ohlc.high),
          });
          items.push({ label: `${t('low')}`, value: priceFormatter(ohlc.low) });
          items.push({
            label: `${t('close')}`,
            value: priceFormatter(ohlc.close),
          });
        }
      } else if (!indicatorConfig.config.hidden) {
        let inputScale = indicatorConfig.config.inputsObj.scale;
        let scale2 = inputScale || (primary ? scale : 3); // TODO:
        let indicatorId = indicatorConfig.id;
        let dataKey = this.getDataKeyByIndicatorId(indicatorId);
        let currIndicator = indicator[name];
        if (currIndicator && currIndicator.tooltip && dataKey) {
          let indicatorItems: Array<{
            label?: string;
            value?: string;
            color?: string;
          }>;
          try {
            indicatorItems = currIndicator.tooltip(
              closest[dataKey],
              scale2,
              indicatorConfig.config
            );
          } catch (error) {
            if (__DEV__)
              console.error(`fail in get ${name} "s tooltip data`, error);
          }
          if (indicatorItems) {
            let labelI18nPrefix = `indicators.${name}.styles.`;
            items.push({
              // title: `indicators.${name}.name`,
              title: {
                __html:
                  '<div>' +
                  t(`indicators.${name}.name`) +
                  '</div>' +
                  '<span>' +
                  currIndicator.label(indicatorConfig.config, chartContext) +
                  '</span>',
              },
            });
            forEach(indicatorItems, (item) =>
              items.push({
                ...item,
                label: t(`${labelI18nPrefix}${item.label}`),
              })
            );
          }
        }
      }
    }
    if (!items.length) return null;
    return items.map((item, i) => {
      return (
        <li key={i}>
          <If condition={!!item.title}>
            <div
              styleName="chart-tooltip-title"
              dangerouslySetInnerHTML={item.title}
            />
          </If>
          <If condition={!item.title}>
            <span styleName="label">{item.label}</span>
            <span styleName="value" style={{ color: item.color || 'unset' }}>
              {item.value}
            </span>
          </If>
        </li>
      );
    });
  }

  render() {
    if (!this.props.isShow) return null;
    const chartContext = this.context.chartContext;
    if (!chartContext) return null;
    let {
      sizeOption: { xHeight, yWidth },
      lang,
    } = chartContext;
    let mouse = chartContext.mouse();
    let { x, y, closest, closestX, chartId } = mouse;
    let chart = chartContext.getChart(chartId);
    if (x < 0 || !closest || !chart) return null;

    let styles = this.calcStyles();
    return (
      <div
        styleName="chart-tooltip-container"
        className="chart-tooltip-container"
        style={styles.bounding}
        ref={(boundingBox) => (this.boundingBox = boundingBox)}
      >
        <ul
          styleName="chart-tooltip-content"
          style={styles.content}
          ref={(contentBox) => (this.contentBox = contentBox)}
        >
          {this.renderItems()}
        </ul>
      </div>
    );
  }
}
