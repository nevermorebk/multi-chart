import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { IBaseContext } from '../../lib';
import TooltipPanel from './TooltipPanel';
import { ContextContainer } from '../ContextContainer';
import IView from '../IView';
export default class Tooltip extends IView {
  constructor(context: IBaseContext, parentNode: HTMLElement) {
    super(context, parentNode);
    // Webpack Hot Module Replacement API
    if (module.hot) {
      module.hot.accept('./TooltipPanel', () => {
        if (this.isShow) render(this);
      });
    }
  }

  redraw() {
    let showTooltip = this.context.displayOption.tooltip;
    let enoughSpace = this.context.sizeOption.width > 250;
    const state = this.context.state;
    showTooltip = showTooltip && !state.resizing && enoughSpace;
    // showTooltip = showTooltip && this.context.chartType !== ChartType.TICK;
    if (!showTooltip) {
      if (this.isShow) {
        this.isShow = false;
        render(this);
      }
    } else {
      this.isShow = true;
      render(this);
    }
  }

  destroy() {
    if (this.isShow) {
      this.isShow = false;
      // render(this);
      ReactDOM.unmountComponentAtNode(this.parentNode);
    }
  }
}

const render = (props) => {
  ReactDOM.render(
    <ContextContainer context={props.context}>
      <TooltipPanel isShow={props.isShow} />
    </ContextContainer>,
    props.parentNode
  );
};
