

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { hot } from 'react-hot-loader';
import { IChartContext } from '../lib';

export interface IProps {
  context?: IChartContext;
}

export class Container extends React.Component<IProps> {
  static childContextTypes = {
    chartContext: PropTypes.object
  }

  getChildContext() {
    return {
      chartContext: this.props.context
    };
  }
  render() {
    return (
      <React.Fragment>
        {this.props.children}
      </React.Fragment>
    );
  }
}

export const ContextContainer = hot(module)(Container);
