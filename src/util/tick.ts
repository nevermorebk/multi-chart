import { Domain } from '../lib';
export function xTicks(width: number): number {
  let defaultXTicks = 8; // per 800px
  let defaultWidth = 800;
  let xTicks = Math.ceil((defaultXTicks * width) / defaultWidth);
  return xTicks;
}

export function yTicks(height: number, domain: Domain, scale = 3): number {
  let minTickSpace = height > 150 ? 50 : 40;
  let yTicks = Math.ceil(height / minTickSpace);
  if (domain[0] === domain[1]) yTicks = 0;
  else {
    let size = Math.floor(Math.abs(domain[1] - domain[0]) * Math.pow(10, scale));
    if (size < yTicks) yTicks = size;
  }
  return yTicks;
}
