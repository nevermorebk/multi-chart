import { forEach } from './array';

export interface IElement {
  (): HTMLElement;
  node(): HTMLElement;
  attr(attrs: object): this;
  html(html: string): this;
  css(styles: object): this;
  empty(): this;
  addClass(cls: string[] | string): this;
  removeClass(cls: string[] | string): this;
  append(...nodes: HTMLElement[]): this;
  appendTo(parent: HTMLElement): this;
  insertBefore(before: HTMLElement): this;
  find(selector: string): IElement | null;
  findAll(selector: string): HTMLElement[];
  closest(clsName: string): HTMLElement;
  remove(): void;
  on<K extends keyof HTMLElementEventMap>(type: K, listener: (ev: HTMLElementEventMap[K]) => any, useCapture?: boolean): this;
  on<K extends keyof HTMLElementEventMap>(type: K, selector: string, listener: (ev: HTMLElementEventMap[K]) => any, useCapture?: boolean): this;
  one<K extends keyof HTMLElementEventMap>(type: K, listener: (ev: HTMLElementEventMap[K]) => any, useCapture?: boolean): this;
  off<K extends keyof HTMLElementEventMap>(type: K, listener?: (ev: HTMLElementEventMap[K]) => any, useCapture?: boolean): this;
  _setCacheEventObj(type: string, listener: Function): void;
  prevObject: IElement;
  _node: HTMLElement;
}

type EventCacheList = {
  // [key: string]: Function[];
  [key in keyof HTMLElementEventMap]?: Function[];
};

const NOOP = () => { };

const EventCache = new WeakMap<HTMLElement, EventCacheList>();

const proto = {
  node() {
    return this._node;
  },

  attr(attrs: Partial<any>): IElement {
    forEach(Object.keys(attrs), (k: string) => {
      this._node.setAttribute(k, attrs[k]);
    });
    return this;
  },

  html(html: string): IElement {
    this._node.innerHTML = html;
    return this;
  },

  empty(): IElement {
    // TODO:
    this._node.innerHTML = '';
    return this;
  },

  css(styles: Partial<any>): IElement {
    forEach(Object.keys(styles), (k: string) => {
      this._node.style[k] = styles[k];
      // this._node.style.setProperty(k, styles[k]);
    });
    return this;
  },

  addClass(value: string[] | string): IElement {
    if (typeof value === 'string') {
      value = value.split(' ');
    }
    forEach(value, cls => this._node.classList.add(cls));
    return this;
  },

  removeClass(value: string[] | string = []) {
    if (typeof value === 'string') {
      value = value.split(' ');
    }
    forEach(value, cls => this._node.classList.remove(cls));
    return this;
  },

  append(...nodes: HTMLElement[]) {
    const raws = nodes;
    forEach(raws, raw => {
      if (_isHelper(raw)) {
        raw = raw();
      }
      this._node.appendChild(raw);
    });
    return this;
  },

  appendTo(parent: HTMLElement) {
    if (_isHelper(parent)) {
      parent = parent();
    }
    parent.appendChild(this._node);
    return this;
  },

  insertBefore(before: HTMLElement) {
    let parent = before.parentElement || before.parentNode;
    parent.insertBefore(this.node(), before);
    return this;
  },

  find(selector: string) {
    let selected = this._node.querySelector(selector) as HTMLElement;
    if (!selected) return null;
    let found = dom(selected);
    found.prevObject = this;
    return found;
  },

  findAll(selector: string) {
    let found = this._node.querySelectorAll(selector);
    let result = [];
    for (let i = 0; i < found.length; i++) {
      result[i] = found[i] as HTMLElement;
    }
    return result;
  },

  closest(clsName: string) {
    let cursor = this.node();
    while (cursor !== document.body) {
      if (cursor.classList) {
        if (cursor.classList.contains(clsName)) return cursor;
      } else if (cursor.nodeName.toLowerCase() === 'svg') {
        let cls = cursor.getAttribute('class') || '';
        if (cls.split(' ').indexOf(clsName) > -1) return cursor;
      }
      cursor = cursor.parentElement || (cursor.parentNode as HTMLElement);
    }
    return null;
  },

  remove() {
    let cacheObj = EventCache.get(this._node);
    if (cacheObj) {
      Object.keys(cacheObj).forEach(key => this.off(key));
    }
    if (this._node.remove) {
      this._node.remove();
    } else {
      // IE
      this._node.parentNode.removeChild(this._node);
    }
    this._node = null;
  },

  on(type: string, listener: (ev: Event) => any, capture?: boolean) {
    let selector = null;
    if (typeof listener === 'string' && typeof capture === 'function') {
      selector = listener;
      listener = capture;
      capture = false;
    }
    if (!selector) {
      this._node.addEventListener(type, listener, capture);
      this._setCacheEventObj(type, listener);
    } else {
      // TODO: cache  type selector
      this.on(type, event => {
        let target = event.target as HTMLElement;
        let foundAll = dom(event.currentTarget as HTMLElement).findAll(selector);
        let match = false;
        while (target && foundAll.length) {
          match = foundAll.indexOf(target) > -1;
          if (match) break;
          target = target.parentNode as HTMLElement;
          if (target === this._node) target = null;
        }
        if (match) {
          listener.call(target, event);
        }
      },
        capture
      );
    }

    return this;
  },

  one(type: string, listener: (ev: Event) => any, capture?: boolean) {
    let fn = ev => {
      this._node.removeEventListener(type, fn, capture);
      listener(ev);
    };
    this._node.addEventListener(type, fn, capture);
    return this;
  },

  off(type: string, listener?: (ev: Event) => any, capture?: boolean) {
    if (listener) {
      capture = capture === true ? true : false;
      this._node.removeEventListener(type, listener, capture);
    } else {
      let cacheObj = EventCache.get(this._node);
      if (cacheObj) {
        let listeners = cacheObj[type] || [];
        listeners.forEach(listener => this.off(type, listener));
      }
    }

    return this;
  },

  _setCacheEventObj(type: string, listener: Function) {
    let cacheObj = EventCache.get(this._node);
    if (!cacheObj) {
      cacheObj = Object.create(null);
      EventCache.set(this._node, cacheObj);
    }
    let listeners = cacheObj[type];
    if (!listeners) {
      listeners = cacheObj[type] = [];
    }
    listeners.push(listener);
  },
} as IElement;

/**
 * todo: event listeners with namespace
 */
function dom(source: string | any): IElement {
  let node: HTMLElement;

  if (typeof source === 'string') {
    node = document.querySelector(source) as HTMLElement;
  } else {
    node = source;
  }

  if (!node) {
    throw new ReferenceError('Not found Element');
  }

  // define Helper Fun
  const delegate = (() => node) as IElement;

  delegate.constructor = NOOP;
  delegate._node = node;
  Object.setPrototypeOf(delegate, proto);

  return delegate;
}

namespace dom {
  export function create(tagName: string): IElement {
    const node = document.createElement(tagName);
    return dom(node);
  }
  export function fromString(htmlStr: string): IElement {
    let wrap = document.createElement('div');
    wrap.innerHTML = htmlStr;
    return dom(wrap.lastElementChild as HTMLElement);
  }

  export const cssPrefix = (() => {
    let styles: any = window.getComputedStyle(document.documentElement, '');
    let core = (Array.prototype.slice
      .call(styles)
      .join('')
      .match(/-(moz|webkit)-/) || (styles.OLink === '' && ['', 'o']))[1];
    return '-' + core + '-';
  })();

  export const isSupportHairline = (() => {
    let support;
    let dpr = window.devicePixelRatio || 1;
    return () => {
      if (dpr !== window.devicePixelRatio) {
        support = undefined;
      }
      if (support === undefined) {
        support = detectHairlines();
      }
      return support;
    };
  })();
}

export { dom };

function _isHelper(raw: HTMLElement | IElement): raw is IElement {
  return typeof raw === 'function' && raw.constructor === NOOP;
}

function _isWindow(node: any): node is Window {
  return node.window === node && !!node.document;
}

function detectHairlines() {
  let support = false;
  if (window.devicePixelRatio && devicePixelRatio >= 2) {
    let testElem = document.createElement('div');
    testElem.style.border = '.5px solid transparent';
    document.body.appendChild(testElem);
    if (testElem.offsetHeight === 1) {
      support = true;
    }
    document.body.removeChild(testElem);
  }
  return support;
}
