import { isFinite, forEach } from '../util';
import { Point, Rect, BoundingBox } from '../lib';
import { LinearEquation, Vector } from './math';

export function getBoundingBox(center: Point, margin = 0): BoundingBox {
  let x = center.x | 0;
  let y = center.y | 0;
  return [x - margin, y - margin, x + margin, y + margin];
}

export function getBoundingBox2(points: Point[], margin = 0): BoundingBox {
  let arrX = points.map(p => p.x);
  let arrY = points.map(p => p.y);
  let [x1, x2] = [Math.min.apply(null, arrX) - margin, Math.max.apply(null, arrX) + margin];
  let [y1, y2] = [Math.min.apply(null, arrY) - margin, Math.max.apply(null, arrY) + margin];
  return [x1, y1, x2, y2];
}

export function contains(xy: Point, rect: Rect, border = false): boolean {
  if (!xy) return false;
  let { x, y } = xy;
  let { left, top, width, height } = rect;
  if (border) {
    if (x >= left && x <= left + width && (y >= top && y <= top + height)) {
      return true;
    }
  } else {
    if (x > left && x < left + width && (y > top && y < top + height)) {
      return true;
    }
  }
  return false;
}

export function contains2(xy: Point, box: BoundingBox, border = false): boolean {
  if (!xy) return false;
  let { x, y } = xy;
  let [x1, y1, x2, y2] = box;
  if (border) {
    if (x >= x1 && x <= x2 && (y >= y1 && y <= y2)) {
      return true;
    }
  } else {
    if (x > x1 && x < x2 && (y > y1 && y < y2)) {
      return true;
    }
  }
  return false;
}

export function intersectionOnBoundingBox(p1: Point, p2: Point, box: BoundingBox, isRay = false): Point[] {
  let [x1, y1, x2, y2] = box;
  let line = new LinearEquation(p1, p2);
  let topPoint = { x: line.x(y1), y: y1 };
  let bottomPoint = { x: line.x(y2), y: y2 };
  let leftPoint = { x: x1, y: line.y(x1) };
  let rightPoint = { x: x2, y: line.y(x2) };

  let interPoints = [topPoint, bottomPoint, leftPoint, rightPoint].filter(p => isValidPoint(p) && contains2(p, box, true));
  forEach(interPoints, point => {
    point.x = Math.round(point.x);
    point.y = Math.round(point.y);
  });
  if (interPoints.length < 2) return interPoints;
  if (isRay) {
    if (p2.x === p1.x) {
      interPoints.sort((i1, i2) => i1.y - i2.y);
      return [p1.y > p2.y ? interPoints[0] : interPoints[1]];
    }
    interPoints.sort((i1, i2) => i1.x - i2.x);
    return [p1.x > p2.x ? interPoints[0] : interPoints[1]];
  }
  return interPoints;
}

// tslint:disable:jsdoc-format
/**
p1-----------------------p2
|                        |
|                        |
|                        |
X-----------P3-----------X
 */
export function getParallelPoints(p1: Point, p2: Point, distance: number): [Point, Point] {
  let py;
  return [
    {
      x: p1.x,
      y: p1.y + distance,
    },
    {
      x: p2.x,
      y: p2.y + distance,
    },
  ];
}

export function distanceToLine(p1: Point, p2: Point, xy: Point): number {
  return Vector.distance(p1, p2, xy);
}

export function distanceBetweenPoint(p1: Point, p2: Point): number {
  return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
}

export function maxDistance(origin: Point, box: BoundingBox): number {
  let [x, y, w, h] = box;
  let lt = { x: x, y: y };
  let lb = { x: x, y: h };
  let rt = { x: w, y: y };
  let rb = { x: w, y: h };
  return Math.round(Math.max(
    distanceBetweenPoint(origin, lt),
    distanceBetweenPoint(origin, lb),
    distanceBetweenPoint(origin, rt),
    distanceBetweenPoint(origin, rb),
  ));
}

export function getAngle(origin: Point, a: Point, b: Point): number {
  let Va = Vector.from(origin, a);
  let Vb = Vector.from(origin, b);
  let Sab = Va.x * Vb.y - Va.y * Vb.x;
  let Ma = Math.sqrt(Va.x * Va.x + Va.y * Va.y);
  let Mb = Math.sqrt(Vb.x * Vb.x + Vb.y * Vb.y);
  let sin = Sab / (Ma * Mb);
  if (sin < -1) sin = -1;
  else if (sin > 1) sin = 1;
  let angle = Math.asin(sin);
  return angle;
}

export function isCrossLine([a, b]: [Point, Point], p: Point): boolean {
  return Vector.from(a, b).isVerticalWith(Vector.from(p, a));
}

export function getCenterPoint(p1: Point, p2: Point): Point {
  return {
    x: (p1.x + p2.x) / 2,
    y: (p1.y + p2.y) / 2,
  };
}

export function getPointInLine(points: [Point, Point], x: number, byY = false): Point {
  let p;
  if (byY) {
    p = Vector.pointByY(points[0], points[1], x);
  } else {
    p = Vector.pointByX(points[0], points[1], x);
  }
  return p;
}

export function isValidPoint(point: Point): boolean {
  return isFinite(point.x) && isFinite(point.y);
}

export function getDistancePointByAngle(start: Point, end: Point, distanceToStart: number, angle = 0): Point {
  let { x: sx, y: sy } = start;
  let { x: ex, y: ey } = end;
  let dis = Math.sqrt((sx - ex) * (sx - ex) + (sy - ey) * (sy - ey));
  let dx = 0;
  let dy = 0;
  if (dis) {
    let a = Math.atan((sx - ex) / (sy - ey));
    let sin = (ey - sy) / dis;
    let cos = (ex - sx) / dis;
    let newCos = Math.cos(Math.acos(cos) - angle);
    if (a < 0) angle = -angle;
    let newSin = Math.sin(Math.asin(sin) - angle);

    dx = distanceToStart * newCos;
    dy = distanceToStart * newSin;
  }

  return { x: sx + dx, y: sy + dy };
}
