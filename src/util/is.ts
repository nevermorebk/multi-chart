import { memoize } from './memoize';

export function isObject(arg): boolean {
  return typeof arg === 'object' && arg !== null;
}

export function isFunction(arg): boolean {
  return typeof arg === 'function';
}

export function isWindow(win: any): win is Window {
  return win.window === win && !!win.document;
}

export function isEmpty(value): boolean {
  return value === undefined || value === null || value === '';
}

export function isEmptyValue(value: any | any[]): boolean {
  if (Array.isArray(value)) {
    let notNull = value.some(v => !isEmpty(v));
    return !notNull;
  }
  return isEmpty(value);
}

export function isNotEmpty(value): boolean {
  return !isEmpty(value);
}

export function isDefined(value): boolean {
  return value !== undefined;
}

export function isValidNumber(value): boolean {
  return typeof value === 'number' && !isNaN(value) &&
    value !== Infinity && value !== -Infinity;
}

export function isNumber(value: any): value is number {
  return typeof value === 'number' || value instanceof Number;
}

export const isArray =
  Array.isArray ||
  function _isArray(arr: any): arr is any[] {
    return Object.prototype.toString.call(arr) === '[object Array]';
  };


export const isFinite: (v: any) => boolean =
  Number.isFinite ||
  function _isFinite(value: any) {
    return typeof value === 'number' && (window as any).isFinite(value);
  };

// for env
export const isTouchDevice = memoize(function _isTouchDevice() {
  const canTouch = 'ontouchstart' in document;
  return canTouch && /mobile|tablet|ip(ad|hone|od)|android/i.test(navigator.userAgent);
});
