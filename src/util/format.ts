import { isFinite } from '../util/is';
import { ChartType, DataItem } from '../lib';
import { xAccessor, dateAccessor } from './data';
import { calcOffsetDate } from './data';
import { memoize } from './memoize';
import { trunc } from './math';


const REG_NEGATIVE_ZERO = /\-(0(?:\.0*)?)$/;

function _checkNegativeZero(number: number, zeroPoint: number, result: string): string {
  if (number < 0 && number >= zeroPoint) {
    let match = result.match(REG_NEGATIVE_ZERO);
    if (match) {
      result = match[1];
    }
  }
  return result;
}

// time format  helper

const timeFormatter = (function _() {
  const tokenFunctions = {};
  const fmtFunctions = {};
  const fmtTokens = /(MM|dd|yyyy|HH|mm|ss|.)/g;

  function addToken(token: string, fn: (date: Date) => string | number) {
    tokenFunctions[token] = date => fill(fn(date), token.length);
  }

  function makeFmtFunction(fmt: string) {
    let matches = fmt.match(fmtTokens).map(match => {
      let tokenFunc = tokenFunctions[match];
      if (tokenFunc) return tokenFunc;
      else return () => match;
    });
    return (date: Date) => matches.reduce((out: string, fn: Function) => out + fn(date), '');
  }

  function fill(value, length) {
    value = '' + value;
    let fillLength = length - value.length;
    return Array(Math.max(0, fillLength) + 1).join('0') + value;
  }

  addToken('yyyy', date => date.getFullYear());
  addToken('MM', date => date.getMonth() + 1);
  addToken('dd', date => date.getDate());
  addToken('HH', date => date.getHours());
  addToken('mm', date => date.getMinutes());
  addToken('ss', date => date.getSeconds());

  return function timeFormatter(date: Date, fmt: string) {
    fmtFunctions[fmt] = fmtFunctions[fmt] || makeFmtFunction(fmt);
    return fmtFunctions[fmt](date);
  };
})();

export type FnNumberFormatter = (date: number) => string;

export type FnNumberOrStringFormatter = (num: number | string) => string;

export type FnTickFormatter = (v: number, data?: DataItem[], format?: Function) => string;


function _timeFormat(type: ChartType, tzOffset = 0): FnNumberFormatter {
  let fmt;
  switch (type) {
    case ChartType.TICK:
      fmt = 'HH:mm:ss';
      break;
    case ChartType.M1:
    case ChartType.M5:
    case ChartType.M10:
    case ChartType.M15:
    case ChartType.M30:
      fmt = 'HH:mm';
      break;
    case ChartType.H1:
    case ChartType.H2:
    case ChartType.H4:
      fmt = 'MM/dd HH:mm';
      break;
    case ChartType.D1:
    case ChartType.W1:
    case ChartType.MN1:
      fmt = 'yyyy/MM/dd';
      break;
    default:
      fmt = 'MM/dd HH:mm:ss';
  }

  let result = function format(date: number): string {
    return timeFormatter(calcOffsetDate(new Date(date), tzOffset), fmt);
  };
  return result;
}

function _timeFormat2(type: ChartType, tzOffset = 0): FnNumberFormatter {
  let fmt;
  switch (type) {
    case ChartType.TICK:
      fmt = 'MM/dd HH:mm:ss';
      break;
    case ChartType.M1:
    case ChartType.M5:
    case ChartType.M10:
    case ChartType.M15:
    case ChartType.M30:
      fmt = 'MM/dd HH:mm';
      break;
    case ChartType.H1:
    case ChartType.H2:
    case ChartType.H4:
      fmt = 'MM/dd HH:mm';
      break;
    case ChartType.D1:
    case ChartType.W1:
    case ChartType.MN1:
      fmt = 'yyyy/MM/dd';
      break;
    default:
      fmt = 'yyyy/MM/dd HH:mm:ss';
  }

  let result = function format(date: number): string {
    return timeFormatter(calcOffsetDate(new Date(date), tzOffset), fmt);
  };
  return result;
}

function _timeCustomFormat(fmt: string, tzOffset = 0): FnNumberFormatter {

  let result = function format(date: number): string {
    return timeFormatter(calcOffsetDate(new Date(date), tzOffset), fmt);
  };
  return result;
}

function _numberFormat(scale = 3, round = true): FnNumberOrStringFormatter {
  let zeroPoint = - 1 / Math.pow(10, scale);
  let result = function format(num: number | string): string {
    let result = '';
    if (typeof num === 'string') {
      num = parseFloat(num);
    }
    // if (sign && num >= 0) result = '\xA0';
    if (round) {
      result += num.toFixed(scale);
    } else {
      let f = Math.pow(10, scale);
      result += (trunc(num * f) / f).toFixed(scale);
    }

    result = _checkNegativeZero(num, zeroPoint, result);
    return result;
  };
  return result;
}

function _priceFormat(scale = 3): FnNumberFormatter {
  let fmt = numberFormat(scale);
  let result = function format(price: number): string {
    if (!isFinite(price)) return '-';
    return fmt(price);
  };
  return result;
}

function _xTickFormat(chartType, tzOffset): FnTickFormatter {

  let defaultFormat = timeFormat(chartType, tzOffset);
  let result = (v, data, format) => {
    // TODO:  lodash
    // TODO: perf cache
    let index = -1;
    for (let i = 0, len = data.length; i < len; i++) {
      if (xAccessor(data[i]) === v) {
        index = i;
        break;
      }
    }
    if (index > -1) {
      let item = data[index];
      let date = dateAccessor(item);
      if (date) {
        return (format || defaultFormat)(date as number);
      }
    }
    return '';
  };
  return result;
}

export function _yTickFormat(scale, round = true): FnNumberFormatter {
  let format = numberFormat(scale, round);
  let result = d => format(d);
  return result;
}

enum StandardDeltaTime {
  DAY = 24 * 3600 * 1000,
  HOUR = 3600 * 1000,
  MINUTE = 60 * 1000,
  SECOND = 1000,
}
export function getHumanDeltaTime(delta: number, t: Function /* i18n */) {
  let { DAY, HOUR, MINUTE, SECOND } = StandardDeltaTime;
  if (delta < 0) {
    return '-' + getHumanDeltaTime(-delta, t);
  }
  let text = '';
  if (delta >= DAY) {
    let days = Math.floor(delta / DAY);
    text += `${days} ${t('label.common.d')} `;
    delta = delta % DAY;
  }
  if (delta >= HOUR) {
    let hours = Math.floor(delta / HOUR);
    text += `${hours} ${t('label.common.h')} `;
    delta = delta % HOUR;
  }
  if (delta >= MINUTE) {
    let mins = Math.floor(delta / MINUTE);
    text += `${mins} ${t('label.common.m')} `;
    delta = delta % MINUTE;
  }
  if (delta >= SECOND) {
    let seconds = Math.floor(delta / SECOND);
    text += `${seconds} ${t('label.common.s')}`;
    delta = delta % SECOND;
  }
  return text;
}


export const timeFormat = memoize(_timeFormat);
export const timeFormat2 = memoize(_timeFormat2);
export const timeCustomFormat = memoize(_timeCustomFormat);
export const numberFormat = memoize(_numberFormat);
export const priceFormat = memoize(_priceFormat);
export const xTickFormat = memoize(_xTickFormat);
export const yTickFormat = memoize(_yTickFormat);
