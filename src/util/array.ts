// TODO: cb: value: T

export type ArrayCallback<T> = (value: T, index?: number, array?: T[]) => any;

// for perf
export function forEach<T>(array: T[], cb: ArrayCallback<T>) {
  for (let i = 0, len = array.length; i < len; i++) {
    cb(array[i], i, array);
  }
}

export function forEach2<T>(array: T[], cb: ArrayCallback<T>) {
  for (let i = 0, len = array.length; i < len; i++) {
    if (cb(array[i], i, array) === false) return i;
  }
}

export function keys<T, K extends keyof T>(obj: T): K[] {
  return Object.keys(obj) as K[];
}

export function lastIndexOf<T>(array: T[], cb: ArrayCallback<T>) {
  for (let i = array.length - 1; i >= 0; i--) {
    if (cb(array[i], i, array) === true) return i;
  }
  return -1;
}

export function remove<T>(array: T[], removed: T[]): T[] {
  let rest = [];
  for (let i = 0; i <= array.length - 1; i++) {
    if (removed.indexOf(array[i]) === -1) rest.push(array[i]);
  }
  return rest;
}

export function flatten<T>(array: any, selector?: (item: any) => T[]): T[] {
  let result = [];
  forEach(array, item => {
    let flatted: T[] = item as T[];
    if (selector) flatted = selector(item);
    if (flatted) result.push(...flatted);
  });
  return result;
}

export function extent(array: any[], accessor: Function = null): [number, number] {
  let n = array.length,
    i = -1,
    value,
    min,
    max;

  if (accessor === null) {
    while (++i < n) {
      if ((value = array[i]) !== null && value >= value) {
        min = max = value;
        while (++i < n) {
          if ((value = array[i]) !== null) {
            if (min > value) min = value;
            if (max < value) max = value;
          }
        }
      }
    }
  } else {
    while (++i < n) {
      if ((value = accessor(array[i], i, array)) !== null && value >= value) {
        min = max = value;
        while (++i < n) {
          if ((value = accessor(array[i], i, array)) !== null) {
            if (min > value) min = value;
            if (max < value) max = value;
          }
        }
      }
    }
  }

  return [min, max];
}

export function max(array: number[]) {
  if (array && array.length) {
    let max = Number.NEGATIVE_INFINITY;
    forEach(array, val => {
      if (max < val) max = val;
    });
    return max;
  }
  return undefined;
}

export function min(array: number[]) {
  if (array && array.length) {
    let min = Number.POSITIVE_INFINITY;
    forEach(array, val => {
      if (min > val) min = val;
    });
    return min;
  }
  return undefined;
}

export function map(array: any[], cb: Function) {
  if (array && array.length) {
    let map = [];
    forEach(array, val => {
      map.push(cb(val));
    });
    return map;
  }
  return [];
}

export function union<T>(array: T[]): T[] {
  let set = new Set<T>();
  let result: T[] = [];
  for (let i = 0, len = array.length; i < len; i++) {
    let item = array[i];
    if (set.has(item)) continue;
    result.push(item);
    set.add(item);
  }
  return result;
}
