import { default as d3Bisector } from 'd3-array/src/bisector';
import { extent, forEach } from './array';
import { IBaseContext } from '../lib';
import {
  ICoord,
  IAccessor,
  DataItem,
  Domain,
  DomainValue,
  OHLC,
  PriceDataItem,
  DataConverter,
  IMouseXY,
  NumberTuple,
  ChartType,
} from '../lib';
import { isEmptyValue } from './is';

export const defaultYAccessor: IAccessor<DataItem, DataItem> = d => d;

export const xAccessor: IAccessor<DataItem, DomainValue> = d => d && d.x;

export const dataAccessor: IAccessor<DataItem, OHLC> = d => d && d.data;

export const dateAccessor: IAccessor<DataItem, number> = d => d && d.date;

export const priceAccessor: IAccessor<DataItem, DomainValue> = d => {
  if (d && d.data) {
    let data: any = d.data;
    if (data.close) return data.close;
    if (data.price) return data.price;
  }
};

export function mapToStandard(data: any[], convertor: DataConverter, startX = 0): DataItem[] {
  return data.map((d, i) => convertor(d, i, startX));
}

export function defaultMapToStandardConverter(d: any[], i: number, start = 0) {
  let open = +d[1];
  let high = +d[2];
  let low = +d[3];
  let close = +d[4];
  if (!close) {
    high = low = close = open;
  }
  return {
    x: i + start,
    date: +d[0],
    data: {
      open,
      high,
      low,
      close,
    },
    id: d[d.length - 1],
  };
}

export function mapToPriceConverter(d: any[], i: number, start: number) {
  return {
    x: i + start,
    date: +d[0],
    data: {
      price: +d[4] || +d[1],
    },
    id: d[d.length - 1],
  };
}

export function mergedToStandard(
  standard: DataItem[],
  key: string,
  merge: any[],
  startX: number,
  context: IBaseContext
): IAccessor<DataItem, any> {
  startX = startX || 0;
  let fromX: number = startX;
  let fromDate: number; // for merge when no standard data
  if (standard.length === 0) {
    let lastItem = last(context.allData);
    let lastX = context.xAccessor(lastItem);
    fromDate = dateAccessor(lastItem);
    if (fromDate) {
      for (let i = Math.abs(startX - lastX) - 1; i > 0; i--) {
        fromDate = getFutureStockTime(fromDate, context);
      }
    }
  } else {
    fromDate = standard[0].date;
  }
  for (let i = 0, len = merge.length; i < len; i++) {
    let item = standard[i];
    if (!standard[i]) {
      item = standard[i] = {
        x: fromX,
        date: getFutureStockTime(fromDate, context),
        id: null,
        data: null,
      };
    }

    item[key] = merge[i];

    fromDate = item.date;
    fromX = item.x + 1;
  }
  return d => d[key];
}

// temp for pp line
export function mergedToStandard2(standard: DataItem[], key: string, refData: DataItem[]): IAccessor<DataItem, any> {
  let index = refData.length - 1;
  let i = standard.length - 1;
  for (; i >= 0 && index >= 0; i--) {
    let item = standard[i];
    let refItem = refData[index];
    if (item.date < refItem.date) {
      index--;
      i++;
      continue;
    }
    if (item.date >= refItem.date) {
      if (item.data && !isEmptyValue(refItem[key])) item[key] = refItem[key];
    }
  }
  for (; i >= 0; i--) {
    standard[i][key] = null;
  }
  return d => d[key];
}

export function calcOffsetDate(date: Date, offset: number): Date {
  let utc = date.getTime() + date.getTimezoneOffset() * 60000;
  return new Date(utc + 3600000 * offset);
}

export function getFutureStockTime(fromDate: number, context: IBaseContext): number {
  // except weeked days and new year's day (just for jp market)
  let { chartType, tzOffset } = context;
  if (!fromDate) return 0;
  const delta = ChartType.deltaTime(chartType, fromDate);
  let date = fromDate + delta;
  let next = new Date(date);
  let tzNext = calcOffsetDate(next, tzOffset);
  let tzDeltaTime = tzNext.getTime() - next.getTime();
  const isStockTime = date => {
    let [w, m, d] = [date.getDay(), date.getMonth() + 1, date.getDate()];
    return w !== 6 && w !== 0 && !(m === 1 && d === 1);
  };

  while (!isStockTime(tzNext)) {
    tzNext = calcOffsetDate(new Date(date), tzOffset);
    tzNext.setDate(tzNext.getDate() + 1);
    date = tzNext.getTime() - tzDeltaTime;
  }
  return date;
}

// tslint:disable-next-line:max-line-length
export function calcYDomain(standard: DataItem[], keys: string[], isSymmetrical = false): Domain {
  let result: any[] = [];
  forEach(standard, s =>
    forEach(keys, key => {
      let val: any = s[key];
      if (!val) return;

      if (key === 'data') {
        if (val.price !== undefined) {
          result.push(val.price);
        }
        if (val.low !== undefined && val.high !== undefined) {
          result.push(val.low);
          result.push(val.high);
        }
      } else if (val.length) {
        // has null value
        for (let i = 0; i < val.length; i++) {
          let v = val[i];
          if (v !== null) {
            if (v.length) {
              result.push(...v);
            } else {
              result.push(v);
            }
          }
        }
      } else {
        result.push(val);
      }
    })
  );

  let domain = extent(result);
  if (isSymmetrical) {
    let max = Math.max.apply(null, domain.map(Math.abs));
    let min = -max;
    domain = [min, max];
  }
  return domain;
}

export function getClosestItemIndex(array, accessor, value) {
  return _getClosestItemIndex(array, accessor, value, true);
}

export const OUT_MOUSE_XY: IMouseXY = { x: -10, y: -10, chartId: -1, closest: null, closestX: -10 };

export function calcClosestData<T extends DataItem>(
  scaleX: any, data: T[], xAccessor: IAccessor<T, DomainValue>, value: number, strict = true
): T | null {
  if (data.length === 0) return null;
  let x = scaleX.invert(value);
  let { left, right } = _getClosestItemIndexes(data, xAccessor, x);
  let pointX0 = scaleX(xAccessor(data[left]));
  let pointX1 = scaleX(xAccessor(data[right]));

  if (left === right) {
    if (strict) {
      if (value - pointX1 > 0) return null;
      if (value - pointX0 < 0) return null;
    }
    return data[left];
  }
  if (strict) {
    if (pointX1 - pointX0 < pointX0 - value) return null;
  }
  let closest = value - pointX0 < pointX1 - value ? data[left] : data[right];
  return closest;
}

export function cutInDomain(data: DataItem[], domain: Domain, accessor: Function): any[] {
  return data.filter(d => accessor(d) >= domain[0] && accessor(d) <= domain[1]);
}

export function getDomain(data: DataItem[], accessor: IAccessor<DataItem, DomainValue>): [DomainValue, DomainValue] {
  return extent(data, accessor);
}

export function getGapDomainY(domain: Domain, rangeSize: number, margin: number[]): Domain {
  let [top, , bottom] = margin;
  const factor = (rangeSize + (top + bottom)) / rangeSize;
  const delta = Math.abs(domain[0] - domain[1]);
  const space = delta * factor - delta;
  const topSpace = space * top / (top + bottom);
  const bottomSpace = space * bottom / (top + bottom);
  const newDomain = [domain[0] - bottomSpace, domain[1] + topSpace] as Domain;
  return newDomain;
}

export function niceExtent(extent: [number, number]): NumberTuple {
  return [Math.floor(extent[0]), Math.ceil(extent[1])];
}

export function filterDataByDomain(data: DataItem[], accessor: IAccessor<DataItem, DomainValue>, domain: Domain, forward = 0) {
  let bisector = d3Bisector(accessor);
  let indexRange = niceExtent(domain);
  let left = bisector.left(data, indexRange[0]);
  let right = bisector.right(data, indexRange[1]);
  if (left >= forward) left -= forward;
  else left = 0;
  return data.slice(left, right);
}

export function getDataItemByX(data: DataItem[], x: number): DataItem {
  let firstX = data[0].x;
  let index = x - firstX;
  if (index > data.length - 1) index = data.length - 1;
  return data[index];
}

export function getDataItemIndexByX(data: DataItem[], x: number): number {
  let first = data[0];
  if (!first) return -1;
  let firstX = first.x;
  let index = x - firstX;
  if (index > data.length - 1) index = data.length - 1;
  else if (index < 0)
    //
    index = 0;
  return index;
}

export function getMagnetValue(data: OHLC, scaleY: any, pointY: number, distance: number) {
  let values = []; // domainValue, pixelValue, pixelDiff
  data.high && values.push([data.high]);
  data.low && values.push([data.low]);
  data.open && values.push([data.open]);
  data.close && values.push([data.close]);

  values = values
    .map(value => {
      let v = value[0];
      value[1] = scaleY(v);
      value[2] = Math.abs(value[1] - pointY);
      return value;
    })
    .filter(v => v[2] <= distance)
    .sort((v1, v2) => v1[2] - v2[2]);

  if (values[0]) {
    return values[0][0];
  }

  return null;
}

// tslint:disable-next-line:max-line-length
export function calcPositionEntryICoord(data: DataItem[], entryPrice: number): ICoord {
  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    let stock = dataAccessor(item);
    if (entryPrice <= stock.high && entryPrice >= stock.low) {
      return {
        value: entryPrice,
        index: item.x,
        displayIndex: item.x,
        date: item.date,
        data: item.data,
      };
    }
  }
  return null;
}

export function calcPositionEndICoord(data: DataItem[], profitLevel: number, stopLevel: number): ICoord {
  let position = profitLevel > stopLevel ? 'long' : 'short';
  let max = position === 'long' ? profitLevel : stopLevel;
  let min = position === 'long' ? stopLevel : profitLevel;
  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    let stock = dataAccessor(item);
    let maxCoord: ICoord, minCoord: ICoord;
    if (min >= stock.low && min <= stock.high) {
      minCoord = {
        value: min,
        index: item.x,
        displayIndex: item.x,
        date: item.date,
        data: item.data,
      };
    }
    if (max <= stock.high && max >= stock.low) {
      maxCoord = {
        value: max,
        index: item.x,
        displayIndex: item.x,
        date: item.date,
        data: item.data,
      };
    }
    if (minCoord && maxCoord) {
      return position === 'long' ? minCoord : maxCoord;
    }
    if (minCoord) return minCoord;
    if (maxCoord) return maxCoord;
  }

  let lastItem = data[data.length - 1];
  if (lastItem) {
    let last = {
      value: dataAccessor(lastItem).close,
      index: lastItem.x,
      displayIndex: lastItem.x,
      date: lastItem.date,
      data: lastItem.data,
    };
    if (last.value <= max && last.value >= min) return last;
  }
  return null;
}

export function first(data: any[]) {
  return data[0];
}

export function last(data: any[]) {
  return data[data.length - 1];
}

export function sliceDataByX(data: DataItem[], range: number[]): DataItem[] {
  let firstX = data[0].x;
  range = [range[0] - firstX, range[1] - firstX + 1];
  return data.slice(range[0], range[1]);
}

// private

function _generateMergedAccessor(key: string) {
  return d => d[key];
}

function _getClosestItemIndexes(array, accessor, value) {
  let left = d3Bisector(accessor).left(array, value);
  left = Math.max(left - 1, 0);
  let right = Math.min(left + 1, array.length - 1);
  // let item = accessor(array[left]);
  // if (item >= value && item <= value) right = left;
  if (accessor(array[left]) > value) {
    right = left;
  } else if (accessor(array[right]) < value) {
    left = right;
  }
  return { left, right };
}

function _getClosestItemIndex(array, accessor, value, strict = false) {
  let len = array.length;
  let bisector = d3Bisector(accessor);
  let index = bisector.left(array, value);
  if (strict && (index === 0 || index === len - 1)) {
    let firstValue = accessor(array[0]);
    let lastValue = accessor(array[len - 1]);
    if (value < firstValue || value > lastValue) return -1;
  }
  return index;
}

// snap point
