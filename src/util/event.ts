import { Point, TouchPoint, KEY_CODE } from '../lib';

export function isWheelEvent(sourceEvent: Event): sourceEvent is WheelEvent {
  return sourceEvent instanceof WheelEvent;
}

export function isMouseEvent(event: Event): boolean {
  return event instanceof MouseEvent;
}

export function isTouchEvent(event: Event): boolean {
  if ((window as any).TouchEvent) {
    return event instanceof TouchEvent;
  } else {
    return !!(event as any).touches;
  }
}

export function isPanEvent(sourceEvent: Event): sourceEvent is MouseEvent | TouchEvent {
  return !isWheelEvent(sourceEvent) && (isMouseEvent(sourceEvent) || isTouchEvent(sourceEvent));
}

export function isRightButton(event: MouseEvent) {
  return !!(event && event.button === 2);
}

export function isEscKey(event: KeyboardEvent) {
  return !!(event && event.keyCode === KEY_CODE.ESCAPE);
}

export function noevent(event) {
  if (event.preventDefault) event.preventDefault();
  if (event.stopImmediatePropagation) event.stopImmediatePropagation();
}

export function eventPoint(event: MouseEvent, node?: HTMLElement): Point {
  node = node || (event.currentTarget as HTMLElement) || document.body;
  if (node === (window as any)) {
    node = document.body;
  }
  // TODO: perf ?
  // if use offsetXY , make target not reflow (delegate drag in window/body)
  let rect = node.getBoundingClientRect();
  let { clientX, clientY } = event;
  let [clientLeft, clientTop] = [node.clientLeft || 0, node.clientTop || 0];
  return {
    x: clientX - rect.left - clientLeft,
    y: clientY - rect.top - clientTop,
  };
}

export function eventPoints(event: TouchEvent, node?: HTMLElement): TouchPoint[] {
  node = node || (event.currentTarget as HTMLElement) || document.body;
  if (node === (window as any)) {
    node = document.body;
  }
  let rect = node.getBoundingClientRect();
  let touches = event.changedTouches || [];
  let points = [];
  let [clientLeft, clientTop] = [node.clientLeft || 0, node.clientTop || 0];
  for (let i = 0; i < touches.length; i++) {
    let { clientX, clientY, identifier } = touches[i];
    points[i] = {
      x: clientX - rect.left -  clientLeft,
      y: clientY - rect.top - clientTop,
      identifier,
    };
  }
  return points;
}

export function cloneEventObject<T extends Event>(event: T): T {
  let obj = {} as T;
  try {
    // tslint:disable-next-line:forin
    for (let k in event) {
      obj[k] = event[k];
    }
  } catch {}

  return obj;
}

export function createEvent(type) {
  let event = document.createEvent('HTMLEvents');
  event.initEvent(type, true, true);
  (event as any)._custom = true;
  return event;
}

export function createMouseEvent(type, touch: Touch): MouseEvent {
  let event = document.createEvent('MouseEvents');
  event.initMouseEvent(
    type,
    true,
    true,
    window,
    1,
    touch.screenX,
    touch.screenY,
    touch.clientX,
    touch.clientY,
    false,
    false,
    false,
    false,
    0,
    null
  );
  (event as any)._custom = true;
  return event;
}

export function dispatchEvent(target: HTMLElement, event: Event | string) {
  if (typeof event === 'string') {
    event = createEvent(event);
  }
  target.dispatchEvent(event);
}
