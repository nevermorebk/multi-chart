import { isFinite } from './is';
export type MPoint = {
  x: number;
  y: number;
};

export class LinearEquation {
  k: number;
  m: number;
  p: MPoint;
  constructor(p1?: MPoint, p2?: MPoint) {
    if (!p1 && !p2) return;
    this.p = p1;
    this.k = (p1.y - p2.y) / (p1.x - p2.x);
    this.m = p1.y - this.k * p1.x;
  }

  x(y: number) {
    if (!isFinite(this.k)) return this.p.x;
    return (y - this.m) / this.k;
  }

  y(x: number) {
    if (!isFinite(this.k)) return this.k; // Infinity
    return this.k * x + this.m;
  }

}

export class Vector {
  x: number;
  y: number;
  origin: MPoint = {x: 0, y: 0};
  constructor(x: number, y: number, origin?: MPoint) {
    this.x = x;
    this.y = y;
    if (origin) this.origin = origin;
  }

  isVerticalWith(v: Vector): boolean {
    return this.x * v.x + this.y * v.y === 0;
  }

  end(): MPoint {
    return {
      x: this.x + this.origin.x,
      y: this.y + this.origin.y
    };
  }

  static distance(p1: MPoint, p2: MPoint, xy: MPoint): number {
    let AB = Vector.from(p1, p2);
    let a = AB.y;
    let b = -AB.x;
    let c = -(a * p1.x + b * p1.y);
    return Math.abs(a * xy.x + b * xy.y + c) / Math.sqrt(a * a + b * b);
  }

  static from(a: MPoint, b?: MPoint): Vector {
    if (b) return new Vector(b.x - a.x, b.y - a.y, a);
    else return new Vector(a.x, a.y);
  }

  static pointByX(p1: MPoint, p2: MPoint, x: number): MPoint {
    let y = (p1.y + p1.y) / 2;
    let v = Vector.from(p1, p2);
    if (v.x !== 0 ) y = p1.y + (x - p1.x) *  v.y / v.x;
    return {x, y};
  }
  static pointByY(p1: MPoint, p2: MPoint, y: number): MPoint {
    let x = (p1.x + p2.x) / 2;
    let v = Vector.from(p1, p2);
    if (v.y !== 0 ) x = p1.x + (y - p1.y) *  v.x / v.y;
    return {x, y};
  }
}

export function clamp(value, min, max): number {
  return min < max
    ? (value < min ? min : value > max ? max : value)
    : (value < max ? max : value > min ? min : value)
}

export function roundUp(value, scale): number {
  let ex = Math.pow(10, scale);
  let extending = value * ex;
  return Math.round(extending) / ex;
}

export function roundDown(value, scale): number {
  let ex = Math.pow(10, scale);
  let extending = value * ex;
  return Math.floor(extending) / ex;
}

export function log2(x): number {
  return Math.log(x) * Math.LOG2E;
}


export const trunc = Math.trunc || function _trunc(v: number): number {
  v = +v;
	return (v - v % 1)   ||   (!isFinite(v) || v === 0 ? v : v < 0 ? -0 : 0);
}
