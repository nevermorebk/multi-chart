import { forEach, keys } from './array';
import { isArray, isObject, isFunction } from './is';
import * as aspect from './aspect';
import { PropertyNames } from '../lib';

export * from './array';

export * from './data';

export * from './dom';

export * from './event';

export * from './is';

export * from './format';

export * from './tick';

export * from './geometry';

export * from './color';

export * from './memoize';

export { aspect };

export function uuidGenerator(start = 1): () => number {
  let i = start;
  return () => i++;
}

export const uuid = uuidGenerator();

const win: any = window;
const requestAnimationFrame =
  win.requestAnimationFrame ||
  win.mozRequestAnimationFrame ||
  win.webkitRequestAnimationFrame ||
  win.msRequestAnimationFrame ||
  win.oRequestAnimationFrame;

const setFrame = typeof requestAnimationFrame === 'function' ? requestAnimationFrame.bind(win) : fn => fn();

export function timeout(fn: Function, delay?: number) {
  if (typeof delay === 'number') {
    return setTimeout(fn, delay);
  }
  return setFrame(fn);
}

export function cancel(handle: number, hasDelay = false) {
  if (hasDelay) clearTimeout(handle);
  else (cancelAnimationFrame || webkitCancelAnimationFrame)(handle);
}

/**
 * only for redraw ..., (arguments length <= 1)
 */
export function optimizeRenderProcess(fn: Function, context?: object) {
  let requested = false;
  let timer = null;
  let optimizer: any = function _optimizeRenderProcess(drawOpt) {
    if (requested && (drawOpt && drawOpt.isAll)) {
      requested = false;
      if (timer) cancel(timer);
    }
    if (!requested) {
      requested = true;
      // if (drawOpt) requested = false;
      timer = timeout(time => {
        fn.call(context, drawOpt);
        requested = false;
        timer = null;
      });
    }
  };
  optimizer._origin = fn.bind(context);
  return optimizer;
}

export function debounce<U extends Function, R extends (U & {cancel: () => void})>(func: U, wait, immediate = false): R {
  let timeout;
  let fn = function _debounce() {
    let context = this,
      args = arguments;
    let later = function _later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    let callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  } as any as R;
  fn.cancel = () => {
    timeout && clearTimeout(timeout);
  };
  return fn;
}

export function applyMixins(derivedCtor: any, baseCtors: any[]) {
  forEach(baseCtors, baseCtor => {
    forEach(Object.getOwnPropertyNames(baseCtor.prototype), name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}

export function copy(obj) {
  if (isArray(obj)) {
    return obj.map((item) => copy(item));
  } else if (isFunction(obj)) {
    return obj;
  } else if (isObject(obj)) {
    if (isFunction(obj.copy)) {
      return obj.copy();
    }
    return Object.keys(obj).reduce((acc, k) => {
      acc[k] = copy(obj[k]);
      return acc;
    }, {});
  } else {
    return obj;
  }
}

export function deepMergeObject<T>(target: T, source: Partial<T>, replace?: Array<PropertyNames<T>>): T {
  let dest = {} as T;
  target = target || {} as T;
  replace = replace || [];

  if (isArray(source) || isFunction(source)) {
    return copy(source);
  }

  if (isObject(target)) {
    forEach(Object.keys(target), k => dest[k] = copy(target[k]));
  }

  if (isObject(source)) {
    forEach(keys(source), k => {
      let needReplace = replace.indexOf(k) > -1;
      let entry = needReplace ? source[k] : copy(source[k]);
      if (isObject(entry) && !needReplace) {
        dest[k] = deepMergeObject(dest[k], entry);
      } else {
        dest[k] = entry;
      }
    })
  }

  return dest;
}
