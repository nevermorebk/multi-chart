import * as color2rgba from 'color-rgba';
import invert from 'invert-color';
import { memoize } from './memoize';

// TODO (2018/05/06 13:58) 根据背景自适应前景色
// TODO (2018/05/06 13:58) 调整色彩明度、亮度



function _invertColor(hex: string) {
  return invert(hex);
}


function _rgba(rgbaString: string, alpha = -1) {
  // TODO (2018/03/07 12:56) cache 100 lfu?
  let [r, g, b, a] = color2rgba(rgbaString);
  if (alpha < 0 || alpha > 1) a = (a + 1) / 2;
  else a = alpha;
  return `rgba(${r}, ${g}, ${b}, ${a})`;
}

export const invertColor = memoize(_invertColor);

export const rgba = memoize(_rgba);


export function isDark(rgb: string): boolean {
  let [r, g, b] = color2rgba(rgb);
  return (299 * r + 587 * g + 114 * b) / 1e3 < 128;
}
