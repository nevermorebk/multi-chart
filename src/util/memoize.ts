
// memoize

function identityFn(): string {
  let args = [].slice.call(arguments);
  return args.map(arg => arg.toString()).join('_');
}



export function memoize<U extends Function>(fn: U, identity?: () => string): U {
  return memoizeTimes(fn, identity);
}


export function memoizeOnce<U extends Function>(fn: U, identity?: () => string): U {
  return memoizeTimes(fn, identity, 1);
}

export function memoizeTimes<U extends Function>(fn: U, identity?: () => string, size?: number): U {
  let count = 0;
  let cache = new Map();
  if (!identity) identity = identityFn.bind(null, fn.name || '');
  function _memoize(...args: any) {
    let key = identity.apply(null, args);
    let val = cache.get(key);
    if (val === undefined) {
      val = fn.apply(null, arguments);

      if (size === 1) cache.clear();
      else if (size > 1 && ++count > size) {
        cache.clear();
        count = 0;
      }
      cache.set(key, val);
    }
    return val;
  }

  return _memoize as any;
}
