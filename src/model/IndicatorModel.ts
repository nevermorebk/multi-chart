import { IMetaInfoCfg } from '../config/meta';
import { IChartContext } from '../lib';

export class IndicatorModel {
  context: IChartContext;
  protected metaCfg: Readonly<IMetaInfoCfg>;

  constructor(context: IChartContext, metaCfg: Readonly<IMetaInfoCfg>) {
    this.context = context;
    this.metaCfg = metaCfg;
  }

  calc() {}

  render() {

  }

}
