import Evented from '../event/Evented';
import { DataItem, IBaseContext, DataConverter, Scale, NumberTuple, ChartType, Side } from '../lib';
import { xAccessor, defaultMapToStandardConverter, mapToStandard, filterDataByDomain, priceAccessor, last, getDomain, isNumber } from '../util';
import { IMultiChartProps } from '../MultiChart';
import { default as d3ScaleLinear } from 'd3-scale/src/linear';
import { OrderResponse, SummaryResponse } from './DataLoader';

const DefaultScaleX = d3ScaleLinear() as any;
export type ChartDataEventListener = {
  onLoad: () => any;
  onAppend: (data: DataItem[]) => any;
  onPrepend: (data: DataItem[]) => any;
};

export default class ChartData extends Evented<ChartDataEventListener> {
  dataConverter: DataConverter;
  private context: IBaseContext;
  allData: DataItem[];
  viewportData: DataItem[];
  refData?: DataItem[];
  refCharType: ChartType;

  orders: OrderResponse;
  positionSummary: SummaryResponse;
  private _scaleX: Scale = DefaultScaleX;

  // loader: DataLoader;

  constructor(context: IBaseContext, props: IMultiChartProps) {
    super();
    this.context = context;
    this.dataConverter = props.dataConverter;
    // this.loader = new DataLoader(context, {});
  }

  // region about scalex
  initScaleX() {
    if (this._scaleX !== DefaultScaleX) {
      // add check scale domin [0, 0] // data.length == 1
      let xDomain = this._scaleX.domain();
      if (xDomain[0] || xDomain[1]) return;
    }
    // init zoom data
    let scale, range, domain;
    let bandFullWidth = this.context.getBandWidth(1);
    let { allData, chartWidth } = this.context;
    let initNeedDataSize = Math.floor(chartWidth / bandFullWidth);
    let data = allData.slice(-initNeedDataSize);
    if (!data || !data.length) return;
    scale = d3ScaleLinear();
    range = [0, (chartWidth * data.length) / initNeedDataSize];
    domain = getDomain(data, xAccessor);
    scale.range(range).domain(domain);
    this._scaleX = scale;

    if (initNeedDataSize > data.length) {
      this.updateScaleXRange([0, chartWidth]);
    }
  }

  updateScaleXRange(range: NumberTuple) {
    // range: [0, x]
    if (range[0] !== 0) {
      console.warn('new range must start from 0!');
    }
    let scaleX = this.getScaleX();
    let oldRange = scaleX.range();
    let domain = scaleX.domain();
    let newDomain0 = scaleX.invert(oldRange[1] - range[1]);
    scaleX.range(range).domain([newDomain0, domain[1]]);
  }

  getScaleX(): Scale {
    return this._scaleX;
  }

  updateScaleX(newScale: Scale) {
    this._scaleX = newScale;
    // up data
    this.updateViewportData();
  }

  // endregion

  updateOrders(orders: OrderResponse) {
    this.orders = orders;
  }

  updatePositionSummary(summary: SummaryResponse) {
    this.positionSummary = summary;
  }

  updateData(data: any[] = [], limit?: number) {
    this.initOrUpdateStandardData(data);
    this.initScaleX();
    this.updateViewportData(true);
  }

  updateRefData(data: any[] = [], type: ChartType, limit?: number) {
    if (!isNumber(type)) type = -1;
    this.initOrUpdateRefData(data, type);
  }

  loadMoreData(prependData: any[], limit: number) {
    prependData = prependData || [];
    limit = limit || prependData.length;
    if (prependData.length === 0 || prependData.length < limit) {
      // dont trigger loadmore again
      this.context.state.preventMore = true;
      if (prependData.length === 0) return;
    }
    this.initOrUpdateStandardData(prependData, prependData.length);
    this.updateViewportData(true);
    this.context.trigger('moreDataLoaded');
  }

  appendData(item: any): this {
    let lastX, removed, converter, toAppend;
    let standardLast = last(this.allData);
    if (!standardLast) return this;

    converter = this.getDataConverter();
    toAppend = converter(item, 0);
    if (toAppend.date <= standardLast.date) {
      // update
      let i = this.allData.length - 1;
      while (i >= 0) {
        if (toAppend.date === standardLast.date) break;
        standardLast = this.allData[--i];
      }
      if (i === -1) {
        console.warn(`not found quote: ${item}`);
        return;
      }
      lastX = this.context.xAccessor(standardLast);
      this.allData[i] = converter(item, lastX);
    } else {
      // append
      lastX = this.context.xAccessor(standardLast);
      this.allData.push(converter(item, lastX, 1));
    }
    this.updateViewportData();
  }

  initOrUpdateStandardData(origin: any[], prependSize = 0) {
    let standardData = [];

    if (origin.length) {
      let startX = 0;
      if (this.allData && this.allData.length) {
        let firstX = xAccessor(this.allData[0]) as number;
        startX = -1 * prependSize + firstX;
      }
      standardData = mapToStandard(origin, this.getDataConverter(), startX);
    }

    if (prependSize > 0) {
      // loadmore data
      // TODO (2018/05/08 19:33) warn: 验证是否有重复数据
      standardData = standardData.concat(this.allData);
    } else if (prependSize < 0) {
      // clip overflow data
      standardData = this.allData.slice(prependSize);
    }
    this.allData = standardData;
  }

  updateViewportData(forceUpdate = false) {
    let standardData = this.allData;
    let xScale = this.context.getScaleX();
    if (standardData.length === 0) {
      this.viewportData = [];
    } else {
      let domain = xScale.domain();
      let forward = 0;
      let data = filterDataByDomain(standardData, xAccessor, domain, forward);
      let update = false;
      if (forceUpdate) update = true;
      else {
        update = this._isViewportDataChanged(this.viewportData, data);
      }
      if (update) this.viewportData = data;
    }
  }

  _isViewportDataChanged(source: DataItem[], target: DataItem[]) {
    let changed = false;
    if (target.length !== source.length) changed = true;
    else if (target.length === 0 || xAccessor(target[0]) !== xAccessor(source[0])) changed = true;
    else {
      let last = priceAccessor(target[target.length - 1]);
      let _last = priceAccessor(source[source.length - 1]);
      if (last !== _last) changed = true;
    }
    return changed;
  }

  initOrUpdateRefData(ref: any[], type: ChartType, prependSize = 0) {
    let refData;
    if (ref && ref.length) {
      refData = mapToStandard(ref, this.getDataConverter(), 0);
    }
    this.refData = refData;
    this.refCharType = type;
  }

  appendRefData(item: any) {
    // TODO (2018/05/07 16:38) same as appendData
    let lastX, removed, converter, toAppend;
    let refDataLast = this.refData && last(this.refData);
    if (!refDataLast) return this;

    converter = this.getDataConverter();
    toAppend = converter(item, 0);
    if (toAppend.date <= refDataLast.date) {
      // update
      let i = this.refData.length - 1;
      while (i >= 0) {
        if (toAppend.date === refDataLast.date) break;
        refDataLast = this.refData[--i];
      }
      if (i === -1) {
        console.warn(`not found quote: ${item}`);
        return;
      }
      lastX = this.context.xAccessor(refDataLast);
      this.refData[i] = converter(item, lastX);
    } else {
      // append
      lastX = this.context.xAccessor(refDataLast);
      this.refData.push(converter(item, lastX, 1));
    }
  }
  getDataConverter(): DataConverter {
    if (this.dataConverter) return this.dataConverter;
    return defaultMapToStandardConverter as any;
  }
  calcIndicators() { }

  convertToAvg() { }
}
