import { DataItem, DrawOpt, IBaseContext, Side } from '../lib';
import { debounce, niceExtent, timeout, xAccessor } from '../util';
import { TemplateChangeType } from './Template';

export declare type FetchResponse = {
  symbolId: number;
  data: DataItem[];
  limit: number;
};

export type ChartResponse = {
  symbolId: number;
  chartType: number;
  side: Side;
  data: DataItem;
};

export type OrderResponse = {
  symbolId: number;
  sell: number[];
  buy: number[];
};

export type SummaryResponse = {
  symbolId: number;
  sell: number;
  buy: number;
};

export interface Subscription {
  unsubscribe(): void;
}

export interface Loading {
  show(): void;
  hide(): void;
  toggle?(): void;
}

export interface DataSourceAdapter {
  loadingBar?: Loading;
  fetch(symbolId, chartType, side, limit, from): Promise<FetchResponse>;

  subscribeChart(symbolId, chartType, side, callback: (result: ChartResponse) => void): void;

  unsubscribeChart(symbolId, chartType, side, callback): void;

  subscribeOrders(symbolId, callback: (orders: OrderResponse) => void): void;

  unsubscribeOrders(symbolId, callback): void;

  subscribePositionSummary(symbolId, callback: (ps: SummaryResponse) => void): void;

  unsubscribePositionSummary(symbolId, callback): void;
}

export default class DataLoader {
  context: IBaseContext;
  ds: DataSourceAdapter;
  private fetchHandlesMap: Map<any, any>;
  private subscribed: Subscription[];

  static LoadSizeLimit = 2048 / 2;
  constructor(context: IBaseContext, ds: DataSourceAdapter) {
    this.context = context;
    this.ds = ds;
    this.fetchHandlesMap = new Map();
    this.subscribed = [];
    this.initEvents();
  }

  initEvents() {
    // events
    // TODO: gesture support (contain direction info in event point)
    let lastDrag;
    this.context.on('pan', point => (lastDrag = point));
    this.context.on('panend', () => this.checkLoadMore(lastDrag));
    this.context.on('wheelend', zoomPoint => this.checkLoadMore(zoomPoint));
    this.context.on('dataUpdated', () => this.checkLoadMore());
    this.context.on('zoomChanged', () => this.checkLoadMore());
    this.context.on('sizeChanged', () => this.checkLoadMore());
    this.context.on('moreDataLoaded', debounce(() => this.checkLoadMore(), 500));
    this.context.on('templateChanged', type => {
      if (type === TemplateChangeType.Indicator || type === TemplateChangeType.DisplayOption) {
        this.fetchRefDataIfNeed();
      }
    });
  }

  start(): void {
    this.fetchBasicData();
    this.fetchRefDataIfNeed();
  }

  stop(): void {
    // how to avoid ajax handler invoke
    this.subscribed.forEach(subscription => subscription.unsubscribe());
    this.subscribed = [];
    this.fetchHandlesMap.clear();
    this.context.chartDataModel.updateData([]);
    this.context.chartDataModel.updateRefData([], null);
  }

  private updateBasicData(data: DataItem[], limit?: number) {
    limit = limit || data.length;
    if (data.length === 0 || data.length < limit) {
      // dont trigger loadmore again
      this.context.state.preventMore = true;
    }

    this.context.chartDataModel.updateData(data, limit);

    this.context.showLast();
    timeout(() => {
      this.context.trigger('dataUpdated');
    }, 300);
  }

  private appendBasicData(data) {
    this.context.chartDataModel.appendData(data);
    if (this.context.isAutoShowLast()) {
      this.context.showLast();
    } else {
      // dont redraw when last is outside viewport
      let needDrawAll = this.shouldRedrawAllWhenAppendData();
      let needDrawOverlay = this.context.displayOption.currentLine;
      if (needDrawAll || needDrawOverlay) {
        this.context.redraw(needDrawAll ? DrawOpt.all : DrawOpt.overlay);
      }
    }
  }

  private fetchBasicData() {
    this.context.loading(true);
    let {
      chartType,
      chartSide: side,
      symbol: { id },
    } = this.context;
    let options = { symbolId: id, chartType, side };
    this.fetch(options, ({ data, limit }) => {
      this.context.loading(false);
      this.updateBasicData(data, limit);
      this.subscribeChart();
      this.subscribeOrders();
      this.subscribePositionSummary();
    });
  }

  private fetchRefDataIfNeed() {
    let context = this.context;
    let {
      symbol: { id },
      chartSide: side,
      chartDataModel,
      template,
    } = context;
    let refData = chartDataModel.refData;
    let empty = !refData || refData.length === 0;
    let indicators = template.getRefIndicatorProps();
    if (indicators.length === 0) {
      // todo mv
      chartDataModel.updateRefData([], null);
    } else {
      // now only support one refData
      let refIndicatorProps = indicators[0];
      let { refChartType, refDataLength } = refIndicatorProps.config.inputsObj;
      if (!empty) {
        // change ref chart interval
        if (chartDataModel.refCharType === refChartType) return;
        chartDataModel.updateRefData([], null);
      }
      refDataLength = 20 + 1; // TODO (2018/05/30 16:14) set max value
      let options = { symbolId: id, chartType: refChartType, side };
      this.fetch(options, ({ data }) => {
        data = data.slice(-refDataLength);
        chartDataModel.updateRefData(data, refChartType);
        context.redraw(); // need force redraw
      });
    }
  }

  private loadHistoryBasicData(firstQuoteId, needLoadSize) {
    let context = this.context;
    let {
      chartType,
      chartSide: side,
      symbol: { id },
    } = context;
    let options = { symbolId: id, chartType, side, first: firstQuoteId, limit: needLoadSize };
    context.state.loading = true; // mark cancel loadmore work immediately
    context.loading(true);
    this.fetch(options, ({ data, limit }) => {
      context.loading(false);
      context.chartDataModel.loadMoreData(data, limit);
      context.redraw();
    });
  }

  private fetch(dataOptions, callback: (resp: FetchResponse) => void) {
    let { symbolId, chartType, side, limit, first } = dataOptions;
    this.fetchHandlesMap.set(dataOptions, callback);
    this.ds.fetch(symbolId, chartType, side, limit, first).then((...args) => {
      let callback = this.fetchHandlesMap.get(dataOptions);
      this.fetchHandlesMap.delete(dataOptions);
      if (callback) callback(...args);
    });
  }

  private subscribeChart() {
    let context = this.context;
    let {
      chartType,
      chartSide: side,
      symbol: { id },
    } = context;
    this._subscribe('chart', { symbolId: id, chartType, side }, (result: ChartResponse) => {
      if (result.symbolId === context.symbol.id && result.chartType === context.chartType && result.side === context.chartSide)
        this.appendBasicData(result.data);
    });
  }

  private subscribeOrders() {
    let context = this.context;
    this._subscribe('order', { symbolId: context.symbol.id }, (result: OrderResponse) => {
      if (context.symbol.id === result.symbolId) {
        context.chartDataModel.updateOrders(result);
        if (context.displayOption.orderLine) {
          context.redraw();
        }
      }
    });
  }

  private subscribePositionSummary() {
    let context = this.context;
    this._subscribe('positionSummary', { symbolId: context.symbol.id }, (result: SummaryResponse) => {
      if (context.symbol.id === result.symbolId) {
        context.chartDataModel.updatePositionSummary(result);
        if (context.displayOption.positionSummaryLine) {
          context.redraw();
        }
      }
    });
  }

  private _subscribe(type, options, callback) {
    let unsubscribe;
    switch (type) {
      case 'chart': {
        let { symbolId, chartType, side } = options;
        this.ds.subscribeChart(symbolId, chartType, side, callback);
        unsubscribe = () => {
          this.ds.unsubscribeChart(symbolId, chartType, side, callback);
        };
        break;
      }
      case 'order': {
        this.ds.subscribeOrders(options.symbolId, callback);
        unsubscribe = () => {
          this.ds.unsubscribeOrders(options.symbolId, callback);
        };
        break;
      }
      case 'positionSummary': {
        this.ds.subscribePositionSummary(options.symbolId, callback);
        unsubscribe = () => {
          this.ds.unsubscribePositionSummary(options.symbolId, callback);
        };
        break;
      }
    }
    this.subscribed.push({ unsubscribe });
  }

  // region loadmore

  checkLoadMore(point?: { dx?: number; k?: number }) {
    let forward: number = Math.max.apply(null, [0, ...this.context.template.forwards]) || 0;
    let { dragging, wheeling, loading, preventMore, pauseRedraw } = this.context.state;
    let cancel = !!(pauseRedraw || dragging || wheeling || loading || preventMore);
    if (!cancel && (!point || point.dx > 0 || Number(point.k || 0) < 1)) {
      let allData = this.context.allData;
      let loadmore = forward >= allData.length;
      if (!loadmore) {
        let checkData = allData[forward] || allData[0];
        loadmore = checkData && this.context.getScaleX()(xAccessor(checkData)) > 0;
      }
      if (loadmore) {
        let quoteId = allData[0] && allData[0].id;
        if (quoteId) {
          let needLoadSize = this._calcNeedLoadMore(forward);
          this.loadHistoryBasicData(quoteId, needLoadSize);
        }
      }
    }
  }

  private _calcNeedLoadMore(forward = 0): number {
    let allData = this.context.allData;
    if (allData) {
      let first = allData[0];
      let firstX = this.context.xAccessor(first);
      let scaleX = this.context.getScaleX();
      let leftX = Math.floor(scaleX.domain()[0]);
      return Math.min(DataLoader.LoadSizeLimit, Math.abs(firstX - leftX) + forward);
    }
    return -1;
  }

  // endregion

  shouldRedrawAllWhenAppendData() {
    const allData = this.context.allData;
    const lastIndex = allData.length - 1;
    const scaleX = this.context.getScaleX();
    const [left, right] = niceExtent(scaleX.domain());
    let backwards = this.context.template.backwards;
    let x = this.context.xAccessor(allData[lastIndex]);
    if (x >= left && x <= right) return true;
    if (x > right) {
      for (let i = backwards.length - 1; i >= 0; i--) {
        let backward = backwards[i];
        let item = allData[lastIndex - backward];
        x = this.context.xAccessor(item);
        if (x > left && x < right) return true;
      }
    }
    return false;
  }
}
