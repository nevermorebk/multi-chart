import { IBaseContext, ChartType, DragPoint, DisplayOption, DrawOpt } from '../lib';
import { MultiChart } from '../MultiChart';
import Chart from '../chart/Chart';
import { flatten } from '../util/array';
import { getTheme, getI18n } from '../config/index';
import MainChart from '../chart/MainChart';
import TimelineChart from '../chart/TimelineChart';
import { isNotEmpty } from '../util/is';
import { uuidGenerator, timeout } from '../util/index';
import * as indicator from '../indicator';
import { MetaInfo, IMetaInfoCfg } from '../config/meta';

export type TemplateConfig = {
  charts: ReadonlyArray<SerializableChartConfig>;
  displayOption?: DisplayOption;
};

export type IndicatorConfig = IMetaInfoCfg & {
  hidden?: boolean;
};

export type IndicatorProps = {
  name: string;
  id: number;
  primary: boolean;
  singleton: boolean;
  config: IndicatorConfig;
};

export type ChartConfig = {
  indicators: IndicatorProps[];
  height?: number;
  maximize?: boolean;
};

export type SerializableChartConfig = {
  config: IndicatorConfig[];
  height?: number;
};

export enum TemplateChangeType {
  Size,
  Order,
  Indicator,
  DisplayOption,
}

const getIndicatorID = uuidGenerator(1000);

const EmptyTemplateConfig = {
  charts: [
    {
      config: [],
      height: 1,
    },
  ],
};

export default class Template {
  private mchart: MultiChart;
  private context: IBaseContext;

  private readonly ConfigMap = new WeakMap<Chart, ChartConfig>();
  private chartConfigs: ChartConfig[]; // 与charts顺序强关联

  public forwards: number[];
  public backwards: number[];

  public charts: Chart[]; // 目前需 保持 timeline 在最后

  constructor(mchart: MultiChart, templateConfig?: TemplateConfig) {
    this.mchart = mchart;
    this.context = mchart.context;
    let savedChartConfigs = templateConfig && templateConfig.charts;
    let displayOption = templateConfig && templateConfig.displayOption;
    if (savedChartConfigs) {
      let chartConfigs = this._convertToChartConfigs(savedChartConfigs);
      this.forwards = this._initChartForwards(chartConfigs);
      this.backwards = this._initChartBackwards(chartConfigs);
      this.chartConfigs = this._restoreChartConfigs(chartConfigs);
    } else {
      this.forwards = [];
      this.backwards = [];
      this.chartConfigs = this._initChartConfigs();
    }
    if (displayOption) {
      this.mchart.update({ displayOption });
    }
  }

  loadTemplate(template: TemplateConfig) {
    let savedChartConfigs = template.charts;
    if (savedChartConfigs) {
      let chartConfigs = this._convertToChartConfigs(savedChartConfigs);
      this.forwards = this._initChartForwards(chartConfigs);
      this.backwards = this._initChartBackwards(chartConfigs);
      this.chartConfigs = this._restoreChartConfigs(chartConfigs);
    } else {
      this.forwards = [];
      this.backwards = [];
      this.chartConfigs = this._initChartConfigs();
    }
    let displayOption = template.displayOption;
    if (displayOption) {
      this.mchart.update({ displayOption });
    }
    this._reflowCharts();
    // this.triggerTemplateChanged();
  }

  exportTemplate(): TemplateConfig {
    let chartConfigs = this.chartConfigs;
    let serializableChartConfigs: SerializableChartConfig[] = chartConfigs.map(config => ({
      height: config.height,
      config: config.indicators.map(props => props.config),
    }));
    let displayOption = this.mchart.context.displayOption;
    return {
      charts: serializableChartConfigs,
      displayOption,
    };
  }

  triggerTemplateChanged(type: TemplateChangeType) {
    timeout(() => {
      this.context.trigger('templateChanged', type, this.exportTemplate());
    }, 0);
  }

  _convertToChartConfigs(savedChartConfigs: ReadonlyArray<SerializableChartConfig>): ChartConfig[] {
    if (!savedChartConfigs) return null;
    let chartConfigs = savedChartConfigs.reduce((result, chartConfig) => {
      let { config, ...rest } = chartConfig;
      // filter singleton indicator
      config = config.reduce(
        (prev, curr) => {
          if (curr.singleton) {
            if (prev.flag === true) return prev;
            prev.flag = true;
          }
          prev.result.push(curr);
          return prev;
        },
        { result: [], flag: false }
      ).result;
      result.push({
        indicators: config.map((iconfig: IndicatorConfig) => this._indicatorPropsWrapper(iconfig)),
        ...rest,
      });
      return result;
    }, []);
    return chartConfigs;
  }

  _indicatorPropsWrapper(indicatorConfig: IndicatorConfig, id?: number): IndicatorProps {
    if (typeof indicatorConfig === 'string') {
      indicatorConfig = { name: indicatorConfig } as IndicatorConfig;
    }
    id = id || getIndicatorID();
    let primary = false;
    let singleton = false;
    let config;
    let { name, ...rest } = indicatorConfig;
    let meta = MetaInfo.indicator(name);
    if (rest && (rest.inputsObj || rest.stylesObj)) config = rest;
    // from indicator _config // TODO (2018/03/07 18:17) remove
    else if (indicator.config[name]) config = indicator.config[name];
    config = meta.merge(config);
    primary = config.primary;
    singleton = config.singleton;
    return { id, name, primary, singleton, config };
  }

  _initIndicatorsProps(indicatorConfigs: ChartConfig[] = []): IndicatorProps[] {
    let indicators = indicatorConfigs.reduce((result, config) => {
      result.push(...config.indicators);
      return result;
    }, []);
    return indicators;
  }

  _initChartForwards(chartConfigs: ChartConfig[]) {
    let forwards = [];
    let indicators = flatten<IndicatorProps>(chartConfigs, chartConfig => chartConfig.indicators);
    indicators.forEach(({ config }) => {
      let { name, inputsObj } = config;
      let f = indicator[name] && indicator[name](inputsObj);
      if (f && f.forward) {
        let forward = f.forward();
        forward > 0 && forwards.push(forward);
      }
    });
    return forwards;
  }

  _initChartBackwards(chartConfigs: ChartConfig[]) {
    let backwards = [];
    let indicators = flatten<IndicatorProps>(chartConfigs, chartConfig => chartConfig.indicators);
    indicators.forEach(({ config }) => {
      let { name, inputsObj } = config;
      let f = indicator[name] && indicator[name](inputsObj);
      if (f && f.backward) {
        let backward = f.backward();
        backward > 0 && backwards.push(backward);
      }
    });
    return backwards;
  }

  _updateChartForwards() {
    this.forwards = this._initChartForwards(this.chartConfigs);
  }

  _updateChartBackwards() {
    this.backwards = this._initChartBackwards(this.chartConfigs);
  }

  _initChartConfigs(indicators: IndicatorProps[] = []) {
    let primaryIndicatorIndex = indicators.findIndex(prop => prop.primary);
    let chartConfigs = [];
    if (primaryIndicatorIndex > -1) {
      let sorted = indicators.filter(props => !props.primary).map(props => ({ indicators: [props] }));
      let primary = { indicators: indicators.filter(props => props.primary) };
      sorted.splice(primaryIndicatorIndex - 1, 0, primary);
      chartConfigs = sorted;
    } else {
      chartConfigs = indicators.map(props => ({ indicators: [props] }));
      chartConfigs.unshift({ indicators: [] });
    }
    this._reAllocateHeightOfChartConfigs(chartConfigs, true);
    this._updateVisibleOfChartConfigsForTick(chartConfigs);
    return chartConfigs;
  }

  _restoreChartConfigs(chartConfigs: ChartConfig[]) {
    if (!chartConfigs.find(config => this._isMainChartConfig(config))) {
      // maybe this is a invalid config  , use default config
      chartConfigs.unshift({ indicators: [] });
    }
    if (!this._isValidHeightOfChartConfigs(chartConfigs)) {
      console.warn('reallocate chart height. ', chartConfigs);
      this._reAllocateHeightOfChartConfigs(chartConfigs, true);
    }
    this._updateVisibleOfChartConfigsForTick(chartConfigs);
    return chartConfigs;
  }

  _reAllocateHeightOfChartConfigs(chartConfigs: ChartConfig[], reset?: boolean) {
    let unit = 8;
    let minHeight = 1 / unit;
    let leaveHeight = 1;
    let mainConfig = chartConfigs.find(this._isMainChartConfig);
    if (reset) {
      // for calc default height
      chartConfigs.filter(config => !this._isMainChartConfig(config)).forEach(config => {
        leaveHeight -= minHeight;
        config.height = minHeight;
      });
      mainConfig.height = leaveHeight;
    } else {
      // appendindicator : appended config has height === -1, otherwise remove indicator
      let appendConfig = chartConfigs.find(config => config.height === -1);
      let restConfigs = chartConfigs.filter(config => config.height > 0);
      if (appendConfig) {
        appendConfig.height = minHeight;
        if (mainConfig.height > 0.5) {
          mainConfig.height -= minHeight;
        } else {
          restConfigs.forEach(config => (config.height -= config.height * minHeight));
        }
      } else {
        let total = chartConfigs.reduce((sum, config) => (sum += config.height), 0);
        if (mainConfig.height < 0.5) {
          mainConfig.height += 1 - total;
        } else {
          chartConfigs.forEach(config => (config.height /= total));
        }
      }
    }
  }

  _updateVisibleOfChartConfigsForTick(chartConfigs: ChartConfig[]) {
    let tick = this.context.chartType === ChartType.TICK;
    // up maximize when type is tick
    chartConfigs.forEach(config => (config.maximize = false));
    let mainChartConfig = this.getMainChartConfig(chartConfigs);
    if (mainChartConfig) {
      mainChartConfig.maximize = tick;
    }
  }

  _updateHeightOfChartConfig(chart: Chart, chartHeight: number) {
    let chartConfigs = this.chartConfigs;
    let charts = this.charts;
    let index = charts.indexOf(chart);
    let totalHeight = this._getTotalHeightWithoutXChart();
    chartConfigs[index].height = chartHeight / totalHeight;
  }

  refreshChartConfigs() {
    this._updateVisibleOfChartConfigsForTick(this.chartConfigs);
    this._reflowCharts();
    this.triggerTemplateChanged(TemplateChangeType.Size);
  }

  getIndicatorProps(all?: boolean): IndicatorProps[] {
    let configs = this.chartConfigs;
    let maximizeConfig = this.getMaximizeChartConfig(configs);
    if (!all && maximizeConfig) {
      configs = [maximizeConfig];
    }
    let indicators = flatten<IndicatorProps>(configs, config => config.indicators);
    return indicators;
  }

  hasIndicator(name: string): boolean {
    let indicators = flatten<IndicatorProps>(this.chartConfigs, config => config.indicators);
    return !!indicators.find(indicator => indicator.name === name);
  }

  getRefIndicatorProps(): IndicatorProps[] {
    let indicators = flatten<IndicatorProps>(this.chartConfigs, config => config.indicators);
    return indicators.filter(indicator => indicator.config.inputsObj && indicator.config.inputsObj.useRefData);
  }

  hasManyConfigCharts() {
    return this.charts.length - 1 > 1 && this.context.chartType !== ChartType.TICK;
  }

  _setChartConfig(chart, chartConfig) {
    this.ConfigMap.set(chart, chartConfig);
  }

  getChartConfig(chart): ChartConfig {
    return this.ConfigMap.get(chart);
  }

  getMaximizeChartConfig(configs?: ChartConfig[]) {
    configs = configs || this.chartConfigs;
    return configs.find(config => config.maximize);
  }

  getMainChartConfig(configs?: ChartConfig[]) {
    configs = configs || this.chartConfigs;
    return configs.find(this._isMainChartConfig);
  }

  _isMainChartConfig(config: ChartConfig): boolean {
    let indicators = config.indicators;
    if (indicators) {
      if (indicators.length) return indicators[0].primary;
      else return true;
    }
    return false;
  }

  _getChartByChartConfig(config: ChartConfig, charts?: Chart[]): Chart {
    let chart;
    charts = (charts || this.charts).filter(c => !c.isTimelineChart);
    let indicators = config.indicators;
    let indicatorId = indicators[0] && indicators[0].id;
    if (indicatorId) {
      chart = charts.find(chart => chart.hasIndicator(indicatorId));
    }
    if (!chart) chart = charts.find(chart => chart.isMainChart);
    return chart;
  }

  _isValidHeightOfChartConfigs(configs: ChartConfig[]): boolean {
    // tslint:disable-next-line:ban-comma-operator
    let height = configs.reduce((sum, config) => ((sum += config.height || 0), sum), 0);
    return Math.abs(1 - height) < 0.0001;
  }

  _getTotalHeightWithoutXChart() {
    let context = this.context;
    let sizeOption = context.sizeOption;

    let themePadding = getTheme(context.theme).chart.padding;
    let topPadding = themePadding[0] || 0;
    let totalHeight = sizeOption.height - sizeOption.xHeight - topPadding;
    return totalHeight;
  }

  appendIndicator(indicatorConfig: IndicatorConfig): boolean | string {
    let indicatorProps = this._indicatorPropsWrapper(indicatorConfig);
    let mainConfig = this.getMainChartConfig();

    if (this.context.chartType === ChartType.TICK) return false;
    if ((!indicatorProps.primary && this.chartConfigs.length >= 8) || (indicatorProps.primary && mainConfig.indicators.length >= 8)) {
      console.warn('too many chart! maximize chart count is 8.');
      // return false;
      return getI18n(this.context.lang).t('error.tooManyIndicator');
    }
    if (indicatorProps.singleton) {
      if (this.getIndicatorProps().find(prop => prop.name === indicatorProps.name)) {
        console.warn('this indicator only allow append once!', indicatorProps.name);
        // return false;
        let t = getI18n(this.context.lang).t;
        return t('error.onlyAllowOneIndicator', {
          key: t(`indicators.${indicatorProps.name}.name`),
        });
      }
    }
    if (indicatorProps.primary) {
      mainConfig.indicators = [...mainConfig.indicators, indicatorProps]; // must assign a new array!
    } else {
      this.chartConfigs.push({ indicators: [indicatorProps], height: -1 });
    }

    // toggle maximize
    let maximizeConfig = this.getMaximizeChartConfig();
    if (maximizeConfig) {
      let maxChart = this._getChartByChartConfig(maximizeConfig);
      if (maxChart && maxChart.isMainChart && indicatorProps.primary) {
        // do nothing
      } else {
        maxChart && this.toggleMaximizeChart(maxChart.chartId);
      }
    }
    this._updateChartForwards();
    this._updateChartBackwards();
    this._reAllocateHeightOfChartConfigs(this.chartConfigs);
    this._reflowCharts();
    this.triggerTemplateChanged(TemplateChangeType.Indicator);
    return true;
  }

  updateIndicator(indicatorId: number, indicatorConfig?: IndicatorConfig, preview?: boolean): boolean {
    if (!indicatorConfig || preview === false) {
      // on submit in property edit view
      this.triggerTemplateChanged(TemplateChangeType.Indicator);
      return true;
    }
    let chartConfig: ChartConfig;
    let indicators: IndicatorProps[];
    let indicatorProp = this._indicatorPropsWrapper(indicatorConfig, indicatorId);
    // up chartConfigs
    chartConfig = this.chartConfigs.find(config => !!config.indicators.find(prop => prop.id === indicatorId));
    if (!chartConfig) return false;
    indicators = chartConfig.indicators;
    let foundIndex = indicators.findIndex(prop => prop.id === indicatorId);
    indicators.splice(foundIndex, 1, indicatorProp);

    chartConfig.indicators = [...indicators]; // must assign a new array!
    this._updateChartForwards();
    this._updateChartBackwards();
    // update chart
    this._reflowCharts();
    if (!preview) this.triggerTemplateChanged(TemplateChangeType.Indicator);
    return true;
  }

  removeIndicator(indicatorId: number): boolean {
    let chartConfigs = this.chartConfigs;
    let indicators = flatten<IndicatorProps>(chartConfigs, config => config.indicators);
    let removeIndicator = indicators.find(indicator => indicator.id === indicatorId);
    if (!removeIndicator) return false;
    if (removeIndicator.primary) {
      let config = this.getMainChartConfig();
      let removeIndex = config.indicators.findIndex(indicator => indicator.id === indicatorId);
      if (removeIndex > -1) {
        let indicators = config.indicators;
        indicators.splice(removeIndex, 1);
        config.indicators = [...indicators]; // must assign a new array value!
      }
    } else {
      let removeIndex = chartConfigs.findIndex(config => config.indicators[0] && config.indicators[0].id === indicatorId);
      if (removeIndex > -1) {
        chartConfigs.splice(removeIndex, 1);
      }
    }

    this._updateChartForwards();
    this._updateChartBackwards();
    this._reAllocateHeightOfChartConfigs(this.chartConfigs);
    this._reflowCharts();
    this.triggerTemplateChanged(TemplateChangeType.Indicator);
    return true;
  }

  clearIndicators(): boolean {
    let currentTemplate = this.exportTemplate();
    let charts = currentTemplate.charts;
    if (charts.length === 1) {
      let config = charts[0].config;
      if (config.length === 0) return false;
    }
    this.loadTemplate(EmptyTemplateConfig);
    this.triggerTemplateChanged(TemplateChangeType.Indicator);
    return true;
  }

  toggleMaximizeChart(chartId: number): boolean {
    let chart = this.context.getChart(chartId);
    if (!chart || chart.isTimelineChart) return false;
    let seriesCharts = this.charts.filter(c => !c.isTimelineChart);
    if (seriesCharts.length === 1 && seriesCharts[0] === chart) return;
    let config = this.getChartConfig(chart);
    let maximize = config.maximize;
    this.chartConfigs.forEach(config => (config.maximize = false));
    config && (config.maximize = !maximize);
    this._reflowCharts();
    this.triggerTemplateChanged(TemplateChangeType.Size);
    return true;
  }

  moveUpChart(chartId: number): boolean {
    let chart = this.context.getChart(chartId);
    let prevChart = this.context.getPrevChart(chartId);
    return this._moveChart(chart, prevChart);
  }

  moveDownChart(chartId: number): boolean {
    let chart = this.context.getChart(chartId);
    let nextChart = this.context.getNextChart(chartId);
    return this._moveChart(chart, nextChart);
  }

  _moveChart(chart1: Chart, chart2: Chart): boolean {
    if (!chart1 || chart1.isTimelineChart || !chart2 || chart2.isTimelineChart) return false;

    let chartConfigs = this.chartConfigs;
    let config1 = this.getChartConfig(chart1);
    let config2 = this.getChartConfig(chart2);
    let index1 = chartConfigs.indexOf(config1);
    let index2 = chartConfigs.indexOf(config2);
    chartConfigs[index1] = config2;
    chartConfigs[index2] = config1;
    this._reflowCharts();
    this.triggerTemplateChanged(TemplateChangeType.Order);
    return true;
  }

  initCharts() {
    let charts: Chart[];
    let context = this.context;
    let chartConfigs = this.chartConfigs;
    let totalHeight = this._getTotalHeightWithoutXChart();
    let maxConfig = this.getMaximizeChartConfig();
    if (maxConfig) {
      chartConfigs = [maxConfig];
    }
    let indicatorCharts = chartConfigs.reduce((charts, config, i) => {
      let indicators = config.indicators;
      let chartHeight = (maxConfig ? 1 : config.height) * totalHeight;
      let chart;
      if (this._isMainChartConfig(config)) {
        chart = new MainChart(context, { indicators, chartHeight });
      } else {
        chart = new Chart(context, { indicators, chartHeight });
      }
      charts.push(chart);
      this._setChartConfig(chart, config);
      return charts;
    }, []);

    this.charts = charts = [...indicatorCharts];

    let timelineChart = new TimelineChart(context, { chartHeight: context.sizeOption.xHeight });
    charts.push(timelineChart);

    charts.forEach(chart => chart.initialize());
    // render
    charts.forEach(chart => chart.attachTo(this.mchart.container));
  }

  _reflowCharts() {
    let context = this.context;
    let currCharts = this.charts;
    let nextCharts = (this.charts = [] as Chart[]);
    let totalHeight = this._getTotalHeightWithoutXChart();
    let chartConfigs = this.chartConfigs;
    let timelineChart = currCharts.find(chart => chart.isTimelineChart);
    let indicatorCharts = currCharts.filter(chart => !chart.isTimelineChart);
    let chartContainer = this.mchart.container;
    let maximizeConfig = chartConfigs.find(config => config.maximize);

    if (maximizeConfig) {
      let maxChart: Chart | undefined = this._getChartByChartConfig(maximizeConfig, currCharts);
      let chartHeight = totalHeight;
      let indicators = maximizeConfig.indicators;
      if (maxChart) {
        indicatorCharts.forEach(chart => {
          if (chart === maxChart) {
            chart.update({ indicators, chartHeight });
          } else {
            chart.update({ chartHeight: 0 });
          }
          nextCharts.push(chart);
        });
      } else {
        if (this._isMainChartConfig(maximizeConfig)) {
          maxChart = new MainChart(context, { indicators, chartHeight });
        } else {
          maxChart = new Chart(context, { indicators, chartHeight });
        }
        maxChart.initialize();
        nextCharts.push(maxChart);
        this._setChartConfig(maxChart, maximizeConfig);
        // render
        maxChart.attachTo(chartContainer, timelineChart && timelineChart.container);
      }
    } else {
      // toggle max or add / delete chart
      let mainChart: Chart | undefined = currCharts.find(chart => chart.isMainChart);
      let tailCharts = indicatorCharts;
      if (mainChart) tailCharts = indicatorCharts.filter(chart => !chart.isMainChart);
      chartConfigs.forEach(config => {
        let chart;
        let indicators = config.indicators;
        let chartHeight = config.height * totalHeight;
        if (this._isMainChartConfig(config)) {
          if (mainChart) {
            chart = mainChart;
            chart.update({ indicators, chartHeight });
          } else {
            chart = new MainChart(context, { indicators, chartHeight });
            chart.initialize();
          }
        } else {
          let existedChart = this._getChartByChartConfig(config, tailCharts);
          if (existedChart) {
            chart = existedChart;
            chart.update({ indicators, chartHeight });
            tailCharts.splice(tailCharts.indexOf(existedChart), 1);
          } else {
            chart = new Chart(context, { indicators, chartHeight });
            chart.initialize();
          }
        }
        nextCharts.push(chart);
        this._setChartConfig(chart, config);
        // render
        chart.attachTo(chartContainer, timelineChart && timelineChart.container);
      });
      tailCharts.forEach(chart => chart.destroy());
    }
    // append timeline
    if (timelineChart) nextCharts.push(timelineChart);
  }

  _resizeCharts() {
    let context = this.context;
    let totalHeight = this._getTotalHeightWithoutXChart();
    let chartConfigs = this.chartConfigs;

    let timelineChart = this.charts.find(chart => chart.isTimelineChart);
    let indicatorCharts = this.charts.filter(chart => !chart.isTimelineChart);

    let hasMaximizeChart = !!chartConfigs.find(config => config.maximize);
    indicatorCharts.forEach((chart, i) => {
      let chartConfig = this.getChartConfig(chart);
      let { height, indicators } = chartConfigs[i];
      let chartHeight = height * totalHeight;
      if (hasMaximizeChart) {
        if (chartConfig.maximize) {
          chartHeight = totalHeight;
        } else {
          chartHeight = 0;
        }
      }
      chart.update({ chartHeight });
    });
    if (timelineChart) {
      timelineChart.update({ chartHeight: context.sizeOption.xHeight });
    }
  }

  _canResizeChart(chart: Chart, resizeOrigin: string, point?: DragPoint) {
    let charts = this.charts;
    if (!charts) return false;
    let maximized = !!this.getMaximizeChartConfig();
    if (maximized) return false;
    let index = charts.indexOf(chart);
    let [prev, next]: [Chart, Chart] = [charts[index - 1], charts[index + 1]];
    let can = false;
    if (resizeOrigin === 'top') {
      can = prev && !prev.isTimelineChart;
    } else {
      // 'bottom'
      can = next && !next.isTimelineChart;
    }
    return can;
  }

  _resizeChart(chart: Chart, point: DragPoint = null) {
    let resizeOrigin = (chart as any)._resizeOrigin;
    let originHeight = (chart as any)._resizeOriginHeight;
    let relatedOriginHeight = (chart as any)._resizeRelatedOriginHeight;
    let relatedChart = (chart as any)._resizeRelatedChart;

    if (resizeOrigin === null) {
      // end resize
      this.triggerTemplateChanged(TemplateChangeType.Size);
      return;
    }
    if (chart.isTimelineChart || (relatedChart && relatedChart.isTimelineChart)) return;
    let offsetY = point.offsetY;
    let charts;
    if (resizeOrigin === 'top') {
      charts = [[relatedChart, relatedOriginHeight], [chart, originHeight]];
    } else {
      charts = [[chart, originHeight], [relatedChart, relatedOriginHeight]];
    }
    let tuples: Array<[Chart, number]> = [[charts[0][0], charts[0][1] - offsetY], [charts[1][0], charts[1][1] + offsetY]];
    let enough = tuples.every(([chart, newHeight]) => newHeight >= chart.minHeight);
    if (enough) {
      tuples.forEach(([chart, chartHeight]) => {
        let optimizedRedraw = (chart.redraw as any)._origin;
        chart.update({ chartHeight });
        this._updateHeightOfChartConfig(chart, chartHeight);
        if (optimizedRedraw) optimizedRedraw(DrawOpt.all);
        else chart.redraw(DrawOpt.all);
      });
    }
  }
}
