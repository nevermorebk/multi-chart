import { IDrawable, DataItem, IChartContext } from '../lib';
import { xTicks, yTicks } from '../util/tick';
import { getTheme } from '../config/index';
import * as draw from '../canvas/draw';

type Point = {
  [index: number]: number;
  [Symbol.iterator](): IterableIterator<number>;
};

const MIN_PADDING = 8;

// TODO: to feature
export class Gridline implements IDrawable {
  static defaultStyles = {
    color: '#ccc',
    width: 1,
    dashed: 2,
  };
  readonly context: IChartContext;
  ctx: Context2D;
  constructor(context: IChartContext, ctx: Context2D) {
    this.context = context;
    this.ctx = ctx;
  }

  draw(): void {
    let showGridline = this.context.displayOption.gridline;
    if (!showGridline) return;
    let { getScaleX, getScaleY, precisionScale } = this.context;
    let { chartWidth: width, chartHeight: height } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    // TODO (2018/03/20 11:32) same as xAxis/yAxis
    let _xTicks = scaleX.ticks(xTicks(width)).map(scaleX);
    let _yTicks = scaleY.ticks(yTicks(height, scaleY.domain(), precisionScale)).map(scaleY);
    let theme = getTheme(this.context.theme);
    let lineStyle = draw.getCanvasLineStyle(theme.gridLine || Gridline.defaultStyles);

    let lines = [
      ..._xTicks.map((x: number) => {
        return [
          {
            x, y: 0,
          },
          {
            x, y: height,
          },
        ];
      }),
      ..._yTicks.map((y: number) => {
          return [
            {
              x: 0, y,
            },
            {
              x: width, y,
            },
          ];
        })
        .filter(([p1]) => !(p1.y < MIN_PADDING || p1.y + MIN_PADDING > height)),
    ];

    draw.drawLine2(this.ctx, lines, lineStyle);
  }
}
