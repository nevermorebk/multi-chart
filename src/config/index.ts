import { IConfig, ITheme } from '../lib';
import dark from './theme/dark';
import white from './theme/white';
import en from './locale/en';
import ja from './locale/ja';
import defaultConfig from './defaultConfig';
import { forEach } from '../util/array';
import { isDefined } from '../util/is';
import { deepMergeObject } from '../util';

export function setDefaultConfig(config: IConfig) {
  forEach(Object.keys(config), key => (defaultConfig[key] = config[key]));
}

export function mergeConfig(origin: IConfig, part: Partial<IConfig>): IConfig {
  return deepMergeObject<IConfig>(origin, part, ['symbol', 'ds']);
}

// theme
export const themes = {
  dark,
  white,
};

export function setTheme(name: string, theme: ITheme) {
  let origin = themes[name];
  forEach(Object.keys(theme), cate => {
    let kv = theme[cate];
    let oKv = origin[cate];
    if (!isDefined(oKv)) {
      console.warn(`invalid theme params: ${cate}`);
      return;
    }
    if (typeof oKv !== 'object' || oKv === null) {
      origin[cate] = kv;
      return;
    }
    forEach(Object.keys(kv), key => {
      oKv[key] = kv[key];
    });
  });
}

export function getTheme(name: string): ITheme {
  let theme = themes[name];
  if (!theme) {
    throw new TypeError(`missing theme: ${name}`);
  }
  return theme;
}

// i18n
const i18n = {
  t: function t(lang: string, label: string, data?: any): string {
    let i18n: I18nFunction = locales[lang];
    if (typeof i18n === 'function') {
      return i18n(label, data);
    }
    let nodes = label.split('.');
    let node = i18n as any;
    for (let i = 0; i < nodes.length; i++) {
      let key = nodes[i];
      if (node[key]) {
        node = node[key];
      } else {
        console.warn(`missing i18n[${label}]`);
        return label;
      }
    }
    if (data) {
      return template(node, data);
    } else return node;
  },
};
export const locales = {
  en,
  ja,
};

export function setI18n(lang: string, i18n: I18nFunction) {
  locales[lang] = i18n;
}

export function getI18n(lang: string) {
  function t(label: string, data?: any) {
    return i18n.t(lang, label, data);
  }
  function prefix(prefix: string) {
    return (label: string) => t(`${prefix}.${label}`);
  }
  return {
    t,
    prefix,
  };
}

function template(tpl: string, data: any) {
  let text = '';
  Object.keys(data).forEach(key => {
    let val = data[key];
    let placeholder = `{{${key}}}`;
    text += tpl.replace(placeholder, val);
  });
  return text;
}
