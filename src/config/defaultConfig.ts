import { ChartStyle, ChartType, IConfig, Side } from '../lib';
import { isTouchDevice } from '../util';

const devicePixelRatio = window.devicePixelRatio;
// TODO 提取部分 context 参数
const defaultConfig: Partial<IConfig> = {
  lang: 'en',
  theme: 'white',
  //
  symbol: null,
  chartType: ChartType.M1,
  chartStyle: ChartStyle.Candle,
  chartSide: Side.BID,
  sizeOption: {
    width: 800,
    height: 600,
    yWidth: 50,
    xHeight: 20,
  },

  capacity: 4096,
  tzOffset: 9,
  dpr: devicePixelRatio >= 2 ? 2 : devicePixelRatio, // for perf
  magnetMode: true,
  magnetDistance: 30,
  barFactor: 0.66,
  zoomLevels: 7,
  // initZoomLevel: 4,
  defaultBandWidth: 10, // itemWidth
  disableZoom: false,

  displayOption: {
    crosshair: true,
    gridline: true,
    tooltip: true,
    indicatorLabel: true,
    currentLine: false,
    orderLine: false,
    positionSummaryLine: false,
    magnifier: isTouchDevice()
  },
};

export default defaultConfig;
