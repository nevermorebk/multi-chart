export default {
  open: '始値',
  high: '高値',
  low: '安値',
  close: '終値',
  date: '日付',
  time: '時間',
  price: '価格',
  chartType: {
    TICK: 'Tick',
    M1: '1 分足',
    M5: '5 分足',
    M10: '10 分足',
    M15: '15 分足',
    M30: '30 分足',
    H1: '1 時間足',
    H2: '2 時間足',
    H4: '4 時間足',
    D1: '日足',
    W1: '週足',
    MN1: '月足',
  },
  error: {
    tooManyIndicator: '表示できる指標数に達しました。',
    onlyAllowOneIndicator: '指標: {{key}} は1つのみ表示可能です。',
  },
  indicators: {
    sma: {
      name: '単純移動平均線',
      inputs: {
        period: '期間',
      },
      styles: {
        sma: '移動平均',
      },
    },

    ema: {
      name: '指数平滑移動平均線',
      inputs: {
        period: '期間',
      },
      styles: {
        ema: '移動平均',
      },
    },

    wma: {
      name: '加重移動平均線',
      inputs: {
        period: '期間',
      },
      styles: {
        wma: '移動平均',
      },
    },

    ikh: {
      name: '一目均衡表',
      inputs: {
        tl: '転換線',
        kl: '基準線',
        s: '先行スパン 2',
      },
      styles: {
        tl: '転換線',
        kl: '基準線',
        cs: '遅行スパン',
        sa: '先行スパン 1',
        sb: '先行スパン 2',
        sc: '透明度',
      },
    },
    bollinger_band: {
      name: 'ボリンジャーバンド',
      period: '期間',
      div: '標準偏差',
      ma: '移動平均',
      band: 'バンド',
      inputs: {
        period: '期間',
        variance: '標準偏差',
      },
      styles: {
        upper: 'バンド上昇色',
        ma: '移動平均',
        lower: 'バンド下降色',
      },
    },

    super_bollinger: {
      name: 'スーパーボリンジャー',
      inputs: {
        period: '期間',
        latePeriod: '遅行スパン期間',
        variance1: '標準偏差 1',
        variance2: '標準偏差 2',
        variance3: '標準偏差 3',
      },
      styles: {
        ma: '移動平均',
        late: '遅行スパン',
        lower1: '標準偏差 1 下降',
        lower2: '標準偏差 2 下降',
        lower3: '標準偏差 3 下降',
        upper1: '標準偏差 1 上昇',
        upper2: '標準偏差 2 上昇',
        upper3: '標準偏差 3 上昇',
      },
    },

    pivot_point: {
      name: 'ピボットポイント',
      inputs: {
        refChartType: '足',
        refDataLength: '表示日数',
      },
      styles: {
        r4: 'R4',
        r3: 'R3',
        r2: 'R2',
        r1: 'R1',
        p: 'P',
        s1: 'S1',
        s2: 'S2',
        s3: 'S3',
        s4: 'S4',
      },
    },
    envelope: {
      name: 'エンベロープ',
      inputs: {
        period: '期間',
        deviation: '乖離',
      },
      styles: {
        upper: '上バンド',
        lower: '下バンド',
        ma: '移動平均',
      },
    },
    parabolic_sar: {
      name: 'パラボリックSAR',
      factor1: '因数 1',
      factor2: '因数 2',
      inputs: {
        up: '因数 1',
        down: '因数 2',
      },
      styles: {
        up: '上昇',
        down: '下降',
      },
    },
    span_model: {
      name: 'スパンモデル',
      inputs: {
        period1: '転換線期間',
        period2: '基準線期間',
        period3: '先行スパン2',
      },
      styles: {
        span1: '先行スパン1',
        span2: '先行スパン2',
        span3: '透明度',
        late: '遅行スパン',
      },
    },
    macd: {
      name: 'MACD',
      inputs: {
        short: '短期',
        long: '長期',
        signal: 'シグナル',
      },
      styles: {
        macd: 'MACD',
        signal: 'シグナル',
        osci: 'ヒストグラム',
      },
    },
    rci: {
      name: 'RCI',
      inputs: {
        period: '期間',
      },
      styles: {
        rci: 'RCI',
      },
    },
    kdj: {
      name: 'スローストキャスティクス',
      inputs: {
        k: 'K',
        d: 'D',
        sd: 'SD',
      },
      styles: {
        k: 'K',
        d: 'D',
        sd: 'SD',
      },
    },
    hv: {
      name: 'ヒストリカル・ボラティリティ',
      inputs: {
        period: '期間',
        total: '全体期間',
      },
      styles: {
        hv: 'HV',
      },
    },
    ma: {
      name: '移動平均乖離率',
      inputs: {
        period: '期間',
      },
      styles: {
        ma: '移動平均',
      },
    },
    momentum: {
      name: 'モメンタム',
      inputs: {
        period: '期間',
      },
      styles: {
        momentum: 'モメンタム',
      },
    },
    atr: {
      name: 'ATR',
      inputs: {
        period: '期間',
      },
      styles: {
        atr: 'ATR',
      },
    },
    dmi: {
      name: 'DMI',
      inputs: {
        period: '期間',
      },
      styles: {
        diplus: '+DI',
        diminus: '-DI',
        adx: 'ADX',
      },
    },
    rsi: {
      name: 'RSI',
      inputs: {
        period: '期間',
      },
      styles: {
        rsi: 'RSI',
      },
    },
    adx: {
      name: 'ADX',
      inputs: {
        period: '期間',
      },
      styles: {
        adx: 'ADX',
      },
    },
  },
  drawlines: {
    name: {
      TREND_LINE: 'トレンドライン',
      HORIZONTAL_RAY_LINE: '水平レイライン',
      HORIZONTAL_LINE: '水平ライン',
      VERTICAL_LINE: '垂直ライン',
      RAY_LINE: 'レイライン',
      PARALLEL_LINE: '平行ライン',
      FIBONACCI_LINE: 'フィボナッチ・リトレースメント',
      FIBONACCI_TREND_BASED_LINE: 'フィボナッチ・トレンドベース',
      GANN_FAN_LINE: 'ギャンファン',
      LONG_POSITION_LINE: 'ロング・ターゲットライン',
      SHORT_POSITION_LINE: 'ショート・ターゲットライン',
      FIBONACCI_TIMEZONE_LINE: 'フィボナッチ・タイムゾーン',
      FIBONACCI_TIMEZONE_TREND_BASED_LINE: 'フィボナッチ・トレンド・タイムゾーン',
      DATE_RANGE_LINE: 'タイムレンジライン',
      PRICE_RANGE_LINE: 'プライスレンジライン',
    },
    inputs: {
      accountSize: 'アカウントのサイズ',
      riskSize: 'リスク',
    },
    styles: {
      line: 'ライン',
      showArrow: 'ショーアロー',
      trendLine: 'トレンドライン',
      arrowLine: '矢印線',
      area: 'エリア',
      fibos: 'レベル',
      ratios: '比',
      background: 'バックグラウンド',
      label: 'ラベル',
      text: 'テキスト',
      oneColor: '一つの色',
      displayPrice: '表示価格',
      extendLine: '拡張ライン',
      reverse: 'リバース',
      target: 'ターゲット',
      stop: '逆指値',
    },
  },
  lines: {
    buyOrder: 'KOレベル',
    sellOrder: 'KOレベル',
    buyPositionSummary: '買サマリ',
    sellPositionSummary: '売サマリ',
  },
  loading: {
    text: 'ローディング中...',
  },
  label: {
    indicatorLabel: {
      setting: '編集',
      visible: '表示/非表示',
      remove: '削除',
    },
    switch: {
      up: '上に移動',
      down: '下に移動',
      toggle: 'チャート最大化トグル',
    },
    common: {
      target: 'ターゲット',
      amount: '量',
      stop: '逆指値',
      open: '開始',
      close: '終了',
      pnl: '利益&損失',
      qty: '数量',
      rrratio: 'リスク/リワード比',
      bar: 'バー',
      d: '日',
      h: '時間',
      m: '分',
      s: '秒',
      save: '保存',
      cancel: '取消',
    },
    options: {
      indicatorLineStyle: {
        line: 'ライン',
        point: 'ポイント',
      },
    },
    dialog: {
      indicators: {
        title: 'テクニカル指標設定',
        inputs: '入力',
        styles: 'スタイル',
      },
      drawlines: {
        title: '描画ツール設定',
        styles: 'スタイル',
      },
    },
  },
};
