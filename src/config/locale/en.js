export default {
  open: 'Open',
  high: 'High',
  low: 'Low',
  close: 'Close',
  date: 'Date',
  time: 'Time',
  price: 'Price',
  chartType: {
    TICK: 'TICK',
    M1: 'M1',
    M5: 'M5',
    M10: 'M10',
    M15: 'M15',
    M30: 'M30',
    H1: 'H1',
    H2: 'H2',
    H4: 'H4',
    D1: 'D1',
    W1: 'W1',
    MN1: 'MN1',
  },
  error: {
    tooManyIndicator: 'It has reached the number of indicators that can be displayed.',
    onlyAllowOneIndicator: 'Indicator: {{key}} only support to be added once!',
  },
  indicators: {
    sma: {
      name: 'SMA',
      inputs: {
        period: 'Period',
      },
      styles: {
        sma: 'SMA',
      },
    },
    ema: {
      name: 'EMA',
      inputs: {
        period: 'Period',
      },
      styles: {
        ema: 'EMA',
      },
    },

    wma: {
      name: 'WMA',
      inputs: {
        period: 'Period',
      },
      styles: {
        wma: 'WMA',
      },
    },

    ikh: {
      name: 'IKH',
      inputs: {
        tl: 'Tenkan',
        kl: 'Kijun',
        s: 'Senkou2',
      },
      styles: {
        tl: 'Tenkan',
        kl: 'Kijun',
        cs: 'Chikou',
        sa: 'Senkou1',
        sb: 'Senkou2',
        sc: 'Kumo',
      },
    },

    bollinger_band: {
      name: 'Bollinger Band',
      inputs: {
        period: 'Period',
        variance: 'Variance',
      },
      styles: {
        upper: 'Band Up',
        ma: 'MA',
        lower: 'Band Down',
      },
    },

    super_bollinger: {
      name: 'Super Bollinger',
      inputs: {
        period: 'Period',
        latePeriod: 'Late Period',
        variance1: 'Variance1',
        variance2: 'Variance2',
        variance3: 'Variance3',
      },
      styles: {
        ma: 'MA',
        late: 'Late',
        lower1: 'Variance1 Down',
        lower2: 'Variance2 Down',
        lower3: 'Variance3 Down',
        upper1: 'Variance1 Up',
        upper2: 'Variance2 Up',
        upper3: 'Variance3 Up',
      },
    },

    pivot_point: {
      name: 'Pivot Point',
      inputs: {
        refChartType: 'Interval',
        refDataLength: 'Number',
      },
      styles: {
        r4: 'R4',
        r3: 'R3',
        r2: 'R2',
        r1: 'R1',
        p: 'P',
        s1: 'S1',
        s2: 'S2',
        s3: 'S3',
        s4: 'S4',
      },
    },
    envelope: {
      name: 'Envelope',
      inputs: {
        period: 'Period',
        deviation: 'Deviation',
      },
      styles: {
        upper: 'Upper',
        lower: 'Lower',
        ma: 'MA',
      },
    },
    parabolic_sar: {
      name: 'Parabolic SAR',
      inputs: {
        up: 'Factor 1',
        down: 'Factor 2',
      },
      styles: {
        up: 'Up',
        down: 'Down',
      },
    },
    span_model: {
      name: 'Span Model',
      inputs: {
        period1: 'Period1',
        period2: 'Period2',
        period3: 'Period3',
      },
      styles: {
        span1: 'Span1',
        span2: 'Span2',
        span3: 'Background',
        late: 'Late',
      },
    },
    macd: {
      name: 'MACD',
      inputs: {
        short: 'Short',
        long: 'Long',
        signal: 'Signal',
      },
      styles: {
        macd: 'MACD',
        signal: 'Signal',
        osci: 'Osci',
      },
    },
    rci: {
      name: 'RCI',
      inputs: {
        period: 'Period',
      },
      styles: {
        rci: 'RCI',
      },
    },
    kdj: {
      name: 'KDJ',
      inputs: {
        k: 'K',
        d: 'D',
        sd: 'SD',
      },
      styles: {
        k: 'K',
        d: 'D',
        sd: 'SD',
      },
    },
    hv: {
      name: 'HV',
      inputs: {
        period: 'Period',
        total: 'Total',
      },
      styles: {
        hv: 'HV',
      },
    },
    ma: {
      name: 'MA',
      inputs: {
        period: 'Period',
      },
      styles: {
        ma: 'MA',
      },
    },
    momentum: {
      name: 'Momentum',
      inputs: {
        period: 'Period',
      },
      styles: {
        momentum: 'Momentum',
      },
    },
    atr: {
      name: 'ATR',
      inputs: {
        period: 'Period',
      },
      styles: {
        atr: 'ATR',
      },
    },

    dmi: {
      name: 'DMI',
      inputs: {
        period: 'Period',
      },
      styles: {
        diplus: '+DI',
        diminus: '-DI',
        adx: 'ADX',
      },
    },
    rsi: {
      name: 'RSI',
      inputs: {
        period: 'Period',
      },
      styles: {
        rsi: 'RSI',
      },
    },
    adx: {
      name: 'ADX',
      inputs: {
        period: 'Period',
      },
      styles: {
        adx: 'ADX',
      },
    },
  },
  drawlines: {
    name: {
      TREND_LINE: 'Trend Line',
      HORIZONTAL_RAY_LINE: 'Horizontal Ray',
      HORIZONTAL_LINE: 'Horizontal Line',
      VERTICAL_LINE: 'Vertical Line',
      RAY_LINE: 'Ray',
      PARALLEL_LINE: 'Parallel Channel',
      FIBONACCI_LINE: 'Fib Retracement',
      FIBONACCI_TREND_BASED_LINE: 'Trend-Based Fib Extension',
      FIBONACCI_TIMEZONE_LINE: 'Fib Time Zone',
      FIBONACCI_TIMEZONE_TREND_BASED_LINE: 'Trend-Based Fib Time',
      GANN_FAN_LINE: 'Gann Fan',
      LONG_POSITION_LINE: 'Long Position',
      SHORT_POSITION_LINE: 'Short Position',
      DATE_RANGE_LINE: 'Date Range',
      PRICE_RANGE_LINE: 'Price Range',
    },
    inputs: {
      accountSize: 'AccountSize',
      riskSize: 'RiskSize',
    },
    styles: {
      line: 'Line',
      showArrow: 'ShowArrow',
      trendLine: 'TrendLine',
      arrowLine: 'ArrowLine',
      area: 'Area',
      fibos: 'Levels',
      ratios: 'Ratios',
      background: 'Background',
      label: 'Label',
      text: 'Text',
      oneColor: 'OneColor',
      displayPrice: 'DisplayPrice',
      extendLine: 'ExtendLine',
      reverse: 'Reverse',
      target: 'Target',
      stop: 'Stop',
    },
  },
  lines: {
    buyOrder: 'KO Level',
    sellOrder: 'KO Level',
    buyPositionSummary: 'Buy Position Summary',
    sellPositionSummary: 'Sell Position Summary',
  },
  loading: {
    text: 'Loading...',
  },
  label: {
    indicatorLabel: {
      setting: 'Setting',
      visible: 'Show/Hide',
      remove: 'Delete',
    },
    switch: {
      up: 'Move Up',
      down: 'Move Down',
      toggle: 'Toggle Maximize',
    },
    common: {
      target: 'Target',
      amount: 'Amount',
      stop: 'Stop',
      open: 'Open',
      close: 'Closed',
      pnl: 'P&L',
      qty: 'Qty',
      rrratio: 'Risk/Reward Ratio',
      bar: 'bars',
      d: 'd',
      h: 'h',
      m: 'm',
      s: 's',
      save: 'Save',
      cancel: 'Cancel',
    },
    options: {
      indicatorLineStyle: {
        line: 'Line',
        point: 'Point',
      },
    },
    dialog: {
      indicators: {
        title: 'Technical Indicator Edit',
        inputs: 'Inputs',
        styles: 'Styles',
      },
      drawlines: {
        title: 'Line Edit',
        styles: 'Styles',
      },
    },
  },
};
