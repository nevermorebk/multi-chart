import { ToolLineType, IndicatorLineMetaInfo, BarMetaInfo, ChartType } from '../lib';
import { isDefined } from '../util/is';
import { remove, copy, isArray } from '../util';
import { isObject } from '../util';

export enum MetaType {
  Integer = 'integer',
  Bool = 'bool',
  Color = 'color',
  Alpha = 'alpha',
  DrawedLine = 'drawed-line',
  IndicatorLine = 'indicator-line',
  Bar = 'bar', // macd bar style
  LevelColor = 'level-color',
  // LevelDrawedLine = 'level-drawed-line',
  ChartType = 'chart-interval',
}

export type MetaInfoUnit = {
  name: string;
  type: MetaType | string;
  def: any;
  max?: number;
  min?: number;
  step?: number;
  precision?: number;
  readonly?: boolean;
};

export type KV = {
  name: string;
  val: any;
};
export type IMetaInfoCfg = {
  name: string;
  type?: string;
  primary?: boolean;
  singleton?: boolean;
  inputs?: ReadonlyArray<KV>;
  styles?: ReadonlyArray<KV>;
  inputsObj?: any;
  stylesObj?: any;
};

export type IMetaInfo = {
  readonly name: string;
  readonly inputs: ReadonlyArray<MetaInfoUnit>;
  readonly styles: ReadonlyArray<MetaInfoUnit>;
};
export type IIndicatorMetaInfo = {
  primary: boolean;
  singleton?: boolean;
} & IMetaInfo;

export type IDrawedMetaInfo = {
  type: string;
} & IMetaInfo;

export class MetaInfo {
  type?: string;
  primary?: boolean;
  singleton?: boolean;
  name: string;
  readonly inputs: ReadonlyArray<MetaInfoUnit>;
  readonly styles: ReadonlyArray<MetaInfoUnit>;
  constructor(meta: IIndicatorMetaInfo | IDrawedMetaInfo) {
    this.name = meta.name;
    this.inputs = meta.inputs;
    this.styles = meta.styles;
    this.primary = (meta as IIndicatorMetaInfo).primary;
    this.singleton = (meta as IIndicatorMetaInfo).singleton;
    this.type = (meta as IDrawedMetaInfo).type;
  }

  merge(mergeConfig?: IMetaInfoCfg): Readonly<IMetaInfoCfg> {
    let result: IMetaInfoCfg = {
      name: this.name,
    };
    let inputsObj = {},
      stylesObj = {};
    let mergeInputsObj = {},
      mergeStylesObj = {};
    let inputs, styles;
    let otherKeys = [];
    if (mergeConfig) otherKeys = remove(Object.keys(mergeConfig), ['inputs', 'styles', 'inputsObj', 'stylesObj']);
    if (mergeConfig) {
      if (mergeConfig.inputsObj) {
        mergeInputsObj = mergeConfig.inputsObj;
      }
      if (mergeConfig.stylesObj) {
        mergeStylesObj = mergeConfig.stylesObj;
      }
    }

    if (isDefined(this.type)) {
      result.type = this.type;
    }
    if (isDefined(this.primary)) {
      result.primary = this.primary;
    }
    if (isDefined(this.singleton)) {
      result.singleton = this.singleton;
    }
    if (mergeConfig) otherKeys.forEach(key => (result[key] = mergeConfig[key]));

    inputs = this.inputs.reduce((convert, item) => {
      let name = item.name;
      let val = mergeInputsObj[name];
      if (val === undefined || val === null) val = item.def;
      else if (isObject(val) && !isArray(val)) val = { ...item.def, ...val };
      convert.push({
        name,
        val,
      });
      inputsObj[name] = val;
      return convert;
    }, []);
    styles = this.styles.reduce((convert, item) => {
      let name = item.name;
      let val = mergeStylesObj[name];
      if (val === undefined) val = item.def;
      else if (isObject(val) && !isArray(val)) val = { ...item.def, ...val };
      convert.push({
        name,
        val,
      });
      stylesObj[name] = val;
      return convert;
    }, []);

    result.inputsObj = inputsObj;
    result.stylesObj = stylesObj;
    // set help property  (not store)
    Object.defineProperty(result, 'inputs', {
      value: inputs,
      writable: false,
      enumerable: false,
    });
    Object.defineProperty(result, 'styles', {
      value: styles,
      writable: false,
      enumerable: false,
    });

    return result;
  }
}

export namespace MetaInfo {
  export function indicator(name: string): MetaInfo {
    // copy metainfo
    let meta = copy(IndicatorCfgs[name]);
    return new MetaInfo(meta);
  }

  export function drawed(name: string): MetaInfo {
    // copy metainfo
    const drawed = DrawedCfgs[name];
    if (!drawed) return null;
    let meta = copy(drawed);
    return new MetaInfo(meta);
  }

  export function getDefault(type: string, name: string): MetaInfo {
    let meta;
    if (type === 'drawline') {
      meta = MetaInfo.drawed(name);
    } else {
      meta = MetaInfo.indicator(name);
    }
    return meta;
  }

  export function tick(): MetaInfo {
    let meta = copy(TickMetaInfoObj);
    return new MetaInfo(meta);
  }
}

const IndicatorCfgs = {
  ma: {
    primary: false,
    name: 'ma',
    inputs: [{ type: MetaType.Integer, def: 7, min: 2, max: 120, name: 'period' }, { type: MetaType.Integer, def: 2, name: 'scale', readonly: true }],
    styles: [{ type: MetaType.IndicatorLine, name: 'ma', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  sma: {
    primary: true,
    name: 'sma',
    inputs: [{ type: MetaType.Integer, def: 10, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'sma', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  ema: {
    primary: true,
    name: 'ema',
    inputs: [{ type: MetaType.Integer, def: 12, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'ema', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  wma: {
    primary: true,
    name: 'wma',
    inputs: [{ type: MetaType.Integer, def: 10, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'wma', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  bollinger_band: {
    primary: true,
    name: 'bollinger_band',
    inputs: [{ type: MetaType.Integer, def: 20, min: 2, max: 120, name: 'period' }, { type: MetaType.Integer, def: 2, name: 'variance' }],
    styles: [
      { type: MetaType.IndicatorLine, name: 'upper', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'ma', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'lower', def: IndicatorLineMetaInfo.DefaultLineStyle },
    ],
  },
  envelope: {
    primary: true,
    name: 'envelope',
    inputs: [
      { type: MetaType.Integer, def: 15, min: 2, max: 120, name: 'period' },
      { type: MetaType.Integer, def: 0.002, step: 0.001, precision: 3, name: 'deviation' },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'upper', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'ma', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'lower', def: IndicatorLineMetaInfo.DefaultLineStyle },
    ],
  },
  parabolic_sar: {
    primary: true,
    name: 'parabolic_sar',
    inputs: [
      { type: MetaType.Integer, def: 0.02, step: 0.01, precision: 2, name: 'up' },
      { type: MetaType.Integer, def: 0.2, step: 0.01, precision: 2, name: 'down' },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'up', def: IndicatorLineMetaInfo.DefaultFixedPointStyle },
      { type: MetaType.IndicatorLine, name: 'down', def: IndicatorLineMetaInfo.DefaultFixedPointStyle },
    ],
  },
  span_model: {
    primary: true,
    name: 'span_model',
    inputs: [
      { type: MetaType.Integer, def: 9, min: 2, max: 120, name: 'period1' },
      { type: MetaType.Integer, def: 20, min: 2, max: 120, name: 'period2' },
      { type: MetaType.Integer, def: 52, min: 2, max: 120, name: 'period3' },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'late', def: { color: '#00ff00', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'span1', def: { color: '#8c8c03', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'span2', def: { color: '#ff0000', width: 0.5, style: 0 } },
      { type: MetaType.Color, name: 'span3', def: 'rgba(0, 255, 0, 0.3)' },
    ],
  },
  pivot_point: {
    primary: true,
    name: 'pivot_point',
    singleton: true,
    inputs: [
      { type: MetaType.Bool, name: 'useRefData', def: true, readonly: true },
      { type: MetaType.ChartType, name: 'refChartType', def: ChartType.D1 },
      { type: MetaType.Integer, name: 'refDataLength', def: 2, max: 120, min: 2 },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'r4', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 'r3', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 'r2', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 'r1', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 'p', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 's1', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 's2', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 's3', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
      { type: MetaType.IndicatorLine, name: 's4', def: IndicatorLineMetaInfo.DefaultFixedLineStyle },
    ],
  },
  super_bollinger: {
    primary: true,
    name: 'super_bollinger',
    inputs: [
      { name: 'period', type: MetaType.Integer, def: 21, min: 2, max: 120 },
      { name: 'latePeriod', type: MetaType.Integer, def: 21, min: 2, max: 120 },
      { name: 'variance1', type: MetaType.Integer, def: 1, min: 1 },
      { name: 'variance2', type: MetaType.Integer, def: 2, min: 2 },
      { name: 'variance3', type: MetaType.Integer, def: 3, min: 2 },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'upper3', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'upper2', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'upper1', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'ma', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'lower1', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'lower2', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'lower3', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'late', def: IndicatorLineMetaInfo.DefaultLineStyle },
    ],
  },
  ikh: {
    primary: true,
    name: 'ikh',
    inputs: [
      { type: MetaType.Integer, def: 9, name: 'tl', min: 2, max: 120 },
      { type: MetaType.Integer, def: 16, name: 'kl', min: 2, max: 120 },
      { type: MetaType.Integer, def: 52, name: 's', min: 2, max: 120 },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'tl', def: { color: '#00ff00', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'kl', def: { color: '#ff0000', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'cs', def: { color: '#00ffff', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'sa', def: { color: 'rgb(255,191,0)', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'sb', def: { color: 'rgb(255,191,0)', width: 0.5, style: 0 } },
      { type: MetaType.Color, name: 'sc', def: 'rgba(0, 255, 0, 0.3)' },
    ],
  },
  rsi: {
    primary: false,
    name: 'rsi',
    inputs: [{ type: MetaType.Integer, def: 14, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'rsi', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  rci: {
    primary: false,
    name: 'rci',
    inputs: [{ type: MetaType.Integer, def: 9, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'rci', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  kdj: {
    primary: false,
    name: 'kdj',
    inputs: [
      { type: MetaType.Integer, def: 15, name: 'k', min: 2, max: 120 },
      { type: MetaType.Integer, def: 5, name: 'd', min: 2, max: 120 },
      { type: MetaType.Integer, def: 5, name: 'sd', min: 2, max: 120 },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'k', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'd', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'sd', def: IndicatorLineMetaInfo.DefaultLineStyle },
    ],
  },
  hv: {
    primary: false,
    name: 'hv',
    inputs: [{ type: MetaType.Integer, def: 7, min: 2, max: 120, name: 'period' }, { type: MetaType.Integer, def: 250, name: 'total' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'hv', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  momentum: {
    primary: false,
    name: 'momentum',
    inputs: [{ type: MetaType.Integer, def: 10, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'momentum', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  atr: {
    primary: false,
    name: 'atr',
    inputs: [{ type: MetaType.Integer, def: 10, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'atr', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  adx: {
    primary: false,
    name: 'adx',
    inputs: [{ type: MetaType.Integer, def: 10, min: 2, max: 120, name: 'period' }],
    styles: [{ type: MetaType.IndicatorLine, name: 'adx', def: IndicatorLineMetaInfo.DefaultLineStyle }],
  },
  dmi: {
    primary: false,
    name: 'dmi',
    inputs: [{ type: MetaType.Integer, def: 9, min: 2, max: 120, name: 'period' }],
    styles: [
      { type: MetaType.IndicatorLine, name: 'adx', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'diplus', def: IndicatorLineMetaInfo.DefaultLineStyle },
      { type: MetaType.IndicatorLine, name: 'diminus', def: IndicatorLineMetaInfo.DefaultLineStyle },
    ],
  },
  macd: {
    primary: false,
    name: 'macd',
    inputs: [
      { type: MetaType.Integer, def: 12, name: 'short', min: 2, max: 120 },
      { type: MetaType.Integer, def: 26, name: 'long', min: 2, max: 120 },
      { type: MetaType.Integer, def: 9, name: 'signal', min: 2, max: 120 },
      { type: MetaType.Integer, def: 4, name: 'scale', readonly: true },
      { type: MetaType.Bool, def: true, name: 'symmetric', readonly: true },
    ],
    styles: [
      { type: MetaType.IndicatorLine, name: 'macd', def: { color: '#ff0000', width: 0.5, style: 0 } },
      { type: MetaType.IndicatorLine, name: 'signal', def: { color: 'green', width: 0.5, style: 0 } },
      { type: MetaType.Bar, name: 'osci', def: BarMetaInfo.DefaultBarStyle },
    ],
  },
};

const {
  TREND_LINE,
  RAY_LINE,
  HORIZONTAL_LINE,
  HORIZONTAL_RAY_LINE,
  VERTICAL_LINE,
  PARALLEL_LINE,
  FIBONACCI_LINE,
  FIBONACCI_TIMEZONE_LINE,
  FIBONACCI_TIMEZONE_TREND_BASED_LINE,
  FIBONACCI_TREND_BASED_LINE,
  GANN_FAN_LINE,
  LONG_POSITION_LINE,
  SHORT_POSITION_LINE,
  DATE_RANGE_LINE,
  PRICE_RANGE_LINE,
} = ToolLineType;

const DrawedCfgs = {
  [TREND_LINE]: {
    type: 'drawline',
    name: TREND_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } },
      { type: MetaType.Bool, def: true, name: 'showArrow' },
    ],
  },
  [RAY_LINE]: {
    type: 'drawline',
    name: RAY_LINE,
    inputs: [],
    styles: [{ type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } }],
  },
  [HORIZONTAL_LINE]: {
    type: 'drawline',
    name: HORIZONTAL_LINE,
    inputs: [],
    styles: [{ type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } }],
  },
  [HORIZONTAL_RAY_LINE]: {
    type: 'drawline',
    name: HORIZONTAL_RAY_LINE,
    inputs: [],
    styles: [{ type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } }],
  },
  [VERTICAL_LINE]: {
    type: 'drawline',
    name: VERTICAL_LINE,
    inputs: [],
    styles: [{ type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } }],
  },
  [PARALLEL_LINE]: {
    type: 'drawline',
    name: PARALLEL_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } },
      { type: MetaType.Color, name: 'area', def: 'rgba(255, 0, 0, 0.3)' },
    ],
  },
  [FIBONACCI_LINE]: {
    type: 'drawline',
    name: FIBONACCI_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'trendLine', def: { color: 'rgb(218, 116, 116)', weight: 0.5, style: 2 } },
      { type: MetaType.DrawedLine, name: 'line', def: { weight: 0.5, style: 1 } },
      {
        type: MetaType.LevelColor,
        name: 'fibos',
        def: {
          levels: [
            { color: 'rgb( 160, 107, 0)', val: 0 },
            { color: 'rgb( 105, 158, 0)', val: 0.236 },
            { color: 'rgb( 0, 155, 0)', val: 0.382 },
            { color: 'rgb( 0, 153, 101)', val: 0.5 },
            { color: 'rgb( 128, 128, 128)', val: 0.618 },
            { color: 'rgb(0, 101, 153)', val: 0.786 },
            { color: 'rgb( 189, 92, 250)', val: 1 },
            { color: 'rgb( 216, 6, 215)', val: 1.618 },
            { color: 'rgb( 165, 0, 0)', val: 2.618 },
            { color: 'rgb( 149, 204, 40)', val: 3.618 },
            { color: 'rgb( 40, 204, 149)', val: 4.236 },
          ],
          step: 0.001,
          precision: 3,
          oneColor: false,
        },
      },
      { type: MetaType.Bool, name: 'extendLine', def: false },
      { type: MetaType.Bool, name: 'reverse', def: false },
      { type: MetaType.Alpha, name: 'background', def: 0.4 },
      { type: MetaType.Bool, name: 'displayPrice', def: true },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [FIBONACCI_TREND_BASED_LINE]: {
    type: 'drawline',
    name: FIBONACCI_TREND_BASED_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'trendLine', def: { color: 'rgb(218, 116, 116)', weight: 0.5, style: 2 } },
      { type: MetaType.DrawedLine, name: 'line', def: { weight: 0.5, style: 1 } },
      {
        type: MetaType.LevelColor,
        name: 'fibos',
        def: {
          levels: [
            { color: 'rgb( 160, 107, 0)', val: 0 },
            { color: 'rgb( 105, 158, 0)', val: 0.236 },
            { color: 'rgb( 0, 155, 0)', val: 0.382 },
            { color: 'rgb( 0, 153, 101)', val: 0.5 },
            { color: 'rgb( 128, 128, 128)', val: 0.618 },
            { color: 'rgb(0, 101, 153)', val: 0.786 },
            { color: 'rgb( 189, 92, 250)', val: 1 },
            { color: 'rgb( 216, 6, 215)', val: 1.618 },
            { color: 'rgb( 165, 0, 0)', val: 2.618 },
            { color: 'rgb( 149, 204, 40)', val: 3.618 },
            { color: 'rgb( 40, 204, 149)', val: 4.236 },
          ],
          step: 0.001,
          precision: 3,
          oneColor: false,
        },
      },
      { type: MetaType.Bool, name: 'extendLine', def: false },
      { type: MetaType.Bool, name: 'reverse', def: false },
      { type: MetaType.Alpha, name: 'background', def: 0.4 },
      { type: MetaType.Bool, name: 'displayPrice', def: true },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [FIBONACCI_TIMEZONE_LINE]: {
    type: 'drawline',
    name: FIBONACCI_TIMEZONE_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'trendLine', def: { color: 'rgb(218, 116, 116)', weight: 0.5, style: 2 } },
      { type: MetaType.DrawedLine, name: 'line', def: { weight: 0.5, style: 1 } },
      {
        type: MetaType.LevelColor,
        name: 'fibos',
        def: {
          levels: [
            { color: 'rgb( 160, 107, 0)', val: 0 },
            { color: 'rgb( 105, 158, 0)', val: 1 },
            { color: 'rgb( 0, 155, 0)', val: 2 },
            { color: 'rgb( 0, 153, 101)', val: 3 },
            { color: 'rgb( 128, 128, 128)', val: 5 },
            { color: 'rgb(0, 101, 153)', val: 8 },
            { color: 'rgb( 189, 92, 250)', val: 13 },
            { color: 'rgb( 216, 6, 215)', val: 21 },
            { color: 'rgb( 165, 0, 0)', val: 34 },
            { color: 'rgb( 149, 204, 40)', val: 55 },
            { color: 'rgb( 40, 204, 149)', val: 89 },
          ],
          oneColor: false,
        },
      },
      { type: MetaType.Alpha, name: 'background', def: 0.4 },
      { type: MetaType.Bool, name: 'label', def: true },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [FIBONACCI_TIMEZONE_TREND_BASED_LINE]: {
    type: 'drawline',
    name: FIBONACCI_TIMEZONE_TREND_BASED_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'trendLine', def: { color: 'rgb(218, 116, 116)', weight: 0.5, style: 2 } },
      { type: MetaType.DrawedLine, name: 'line', def: { weight: 0.5, style: 1 } },
      {
        type: MetaType.LevelColor,
        name: 'fibos',
        def: {
          levels: [
            { color: 'rgb( 160, 107, 0)', val: 0 },
            { color: 'rgb( 105, 158, 0)', val: 0.382 },
            { color: 'rgb( 0, 155, 0)', val: 0.5 },
            { color: 'rgb( 0, 153, 101)', val: 0.618 },
            { color: 'rgb( 128, 128, 128)', val: 1 },
            { color: 'rgb(0, 101, 153)', val: 1.382 },
            { color: 'rgb( 189, 92, 250)', val: 1.618 },
            { color: 'rgb( 216, 6, 215)', val: 2 },
            { color: 'rgb( 165, 0, 0)', val: 2.382 },
            { color: 'rgb( 149, 204, 40)', val: 2.618 },
            { color: 'rgb( 40, 204, 149)', val: 3 },
          ],
          step: 0.001,
          precision: 3,
          oneColor: false,
        },
      },
      { type: MetaType.Alpha, name: 'background', def: 0.4 },
      { type: MetaType.Bool, name: 'label', def: true },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [GANN_FAN_LINE]: {
    type: 'drawline',
    name: GANN_FAN_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { weight: 0.5, style: 1 } },
      {
        type: MetaType.LevelColor,
        name: 'ratios',
        def: {
          levels: [
            { color: 'rgb( 160, 107, 0)', val: 1 / 8, label: '8/1' },
            { color: 'rgb( 105, 158, 0)', val: 1 / 4, label: '4/1' },
            { color: 'rgb( 0, 155, 0)', val: 1 / 3, label: '3/1' },
            { color: 'rgb( 0, 153, 101)', val: 1 / 2, label: '2/1' },
            { color: 'rgb( 128, 128, 128)', val: 1, label: '1/1' },
            { color: 'rgb(0, 101, 153)', val: 2, label: '1/2' },
            { color: 'rgb( 189, 92, 250)', val: 3, label: '1/3' },
            { color: 'rgb( 216, 6, 215)', val: 4, label: '1/4' },
            { color: 'rgb( 165, 0, 0)', val: 8, label: '1/8' },
          ],
          step: 0.001,
          precision: 3,
          oneColor: false,
        },
      },
      { type: MetaType.Alpha, name: 'background', def: 0.4 },
      { type: MetaType.Bool, name: 'label', def: true },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [LONG_POSITION_LINE]: {
    type: 'drawline',
    name: LONG_POSITION_LINE,
    inputs: [
      { type: MetaType.Integer, def: 1000, name: 'accountSize' },
      { type: MetaType.Integer, def: 0.2, step: 0.1, precision: 1, name: 'riskSize' },
    ],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } },
      { type: MetaType.Color, name: 'target', def: 'rgba(0, 128, 0, 0.3)' },
      { type: MetaType.Color, name: 'stop', def: 'rgba(255, 0, 0, 0.3)' },
      { type: MetaType.DrawedLine, name: 'arrowLine', readonly: true, def: { color: '#333', weight: 0.5, style: 2 } },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [SHORT_POSITION_LINE]: {
    type: 'drawline',
    name: SHORT_POSITION_LINE,
    inputs: [
      { type: MetaType.Integer, def: 1000, name: 'accountSize' },
      { type: MetaType.Integer, def: 0.2, step: 0.1, precision: 1, name: 'riskSize' },
    ],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } },
      { type: MetaType.Color, name: 'target', def: 'rgba(0, 128, 0, 0.3)' },
      { type: MetaType.Color, name: 'stop', def: 'rgba(255, 0, 0, 0.3)' },
      { type: MetaType.DrawedLine, name: 'arrowLine', readonly: true, def: { color: '#333', weight: 0.5, style: 2 } },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [DATE_RANGE_LINE]: {
    type: 'drawline',
    name: DATE_RANGE_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } },
      { type: MetaType.Color, name: 'area', def: 'rgba(255, 0, 0, 0.3)' },
      { type: MetaType.DrawedLine, name: 'arrowLine', readonly: true, def: { color: '#333', weight: 0.5, style: 2 } },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
  [PRICE_RANGE_LINE]: {
    type: 'drawline',
    name: PRICE_RANGE_LINE,
    inputs: [],
    styles: [
      { type: MetaType.DrawedLine, name: 'line', def: { color: 'red', weight: 0.5, style: 1 } },
      { type: MetaType.Color, name: 'area', def: 'rgba(255, 0, 0, 0.3)' },
      { type: MetaType.DrawedLine, name: 'arrowLine', readonly: true, def: { color: '#333', weight: 0.5, style: 2 } },
      // {type: 'font', name: 'text', def: {fontSize: 12, fontFamily: '', color: ''} }
    ],
  },
};

// candle metainfo (optional)

export const candleMetaInfoObj = {
  primary: true,
  name: 'candle',
  inputs: [
    {
      type: 'option',
      def: 'candle',
      name: 'chartStyle',
      options: [{ name: 'candle', val: 'candle' }, { name: 'line', val: 'line' }, { name: 'avg', val: 'avg' }],
    },
  ],
  styles: [
    { type: MetaType.Color, name: 'upBackground', def: '' },
    { type: MetaType.Color, name: 'downBackground', def: '' },
    { type: MetaType.Color, name: 'upBorderColor', def: '' },
    { type: MetaType.Color, name: 'upBorderColor', def: '' },
  ],
};

export const TickMetaInfoObj = {
  primary: true,
  name: 'tick',
  inputs: [
    { type: MetaType.Bool, name: 'isBid', def: true }, // from context..chartSide
  ],
  styles: [
    // TODO (2020/07/31 16:49) support config params placeholder
    { type: MetaType.Color, name: 'bid', def: 'blue' },
    { type: MetaType.Color, name: 'ask', def: 'red' },
  ]
};
