import { ITheme, DrawedLineDashedStyle } from '../../lib';

const white: ITheme = {
  chart: {
    padding: [5, 0, 0, 5],
    seriesMargin: [20, 15, 5, 15],
    baseFontSize: 11,
    bigFontSize: 12,
    baseFont: '11px Arial, sans-serif',
    bigFont: '12px Verdana, sans-serif',
    fontColor: '#555',
    foregroundColor: '#333',
    backgroundColor: '#fff',
    border: {
      color: '#c3c3c3',
      style: DrawedLineDashedStyle.Solid,
      weight: window.devicePixelRatio >= 2 ? 2 : 1,
    },
  },
  candle: {
    upColor: '#bf1032',
    upFrameColor: '#bf1032',
    downColor: '#01abed',
    downFrameColor: '#01abed',
  },
  gridLine: {
    color: '#a7a7a7',
    style: DrawedLineDashedStyle.Dashed1,
    weight: 0.5,
  },
  crosshair: {
    color: '#000',
    style: DrawedLineDashedStyle.Dashed1,
    weight: 0.5,
  },
  currentPriceLine: {
    color: '#e6002d',
    style: DrawedLineDashedStyle.Solid,
    weight: 1,
  },
  buyOrderLine: {
    color: '#bf1032',
    style: DrawedLineDashedStyle.Solid,
    weight: 1,
  },
  sellOrderLine: {
    color: '#01abed',
    style: DrawedLineDashedStyle.Solid,
    weight: 1,
  },
  buyPositionSummaryLine: {
    color: '#bf1032',
    style: DrawedLineDashedStyle.Solid,
    weight: 1,
  },
  sellPositionSummaryLine: {
    color: '#01abed',
    style: DrawedLineDashedStyle.Solid,
    weight: 1,
  },
  tooltip: {
    color: '#333',
    borderColor: '#999',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    width: 200,
    offset: 4,
  },
  loadingbar: {
    color: '#333',
    borderColor: '#999',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },
  label: {
    color: '#333',
    backgroundColor: 'rgba(230, 230, 230, 0.75)',
  },
  tickLine: {
    color: '#333',
    width: 2,
  }
};

export default white;
