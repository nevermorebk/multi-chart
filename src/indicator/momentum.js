import { priceFormat } from '../util/format';

function momentum(inputsObj = {}) {
  let period = inputsObj.period;
  let accessor;

  function momentum(data) {
    return calc(data, accessor, period);
  }

  momentum.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return momentum;
  };

  momentum.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return momentum;
  };

  momentum.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  momentum.key = function() {
    return `momentum_${period}`;
  };

  return momentum;
}

momentum.label = function(config = {}) {
  let {inputsObj, stylesObj} = config;
  let period = inputsObj.period;
  let color = stylesObj.momentum.color;
  return `(<span style="color:${color}">${period}</span>)`;
};

momentum.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    const { inputsObj, stylesObj } = config;
    const period = inputsObj.period;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'momentum',
        value: formatter(record),
        color: stylesObj.momentum.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;

  var y = null;
  var momentum = [];

  for (var i = 0; i < r.length; i++) {
    if (i >= period) {
      y = accessor(r[i]) - accessor(r[i - period]);
    }

    momentum.push(y);
  }

  return momentum;
}

export default momentum;
