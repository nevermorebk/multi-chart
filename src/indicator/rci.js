import { priceFormat } from '../util/format';

function sumsq(array) {
  var sum = 0;
  for (var i = 0; i < array.length; i++) {
    sum += Math.pow(array[i], 2);
  }
  return sum;
}

function rci(inputsObj = {}) {
  let period = inputsObj.period;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  indicator.key = function() {
    return `rci_${period}`;
  };

  return indicator;
}

rci.label = function(config = {}) {
  let { inputsObj, stylesObj } = config;
  let period = inputsObj.period;
  let color = stylesObj.rci.color;
  return `(<span style="color: ${color}">${period}</span>)`;
};

rci.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    const {inputsObj, stylesObj} = config;
    const period = inputsObj.period;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'rci',
        value: formatter(record),
        color: stylesObj.rci.color
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (i >= period - 1) {
      var start = i - period + 1;

      var queue = r.slice(start, start + period).map(function(a) {
        return accessor(a);
      });

      var sorted = queue.slice().sort(function(a, b) {
        return b - a;
      });

      var ranks = queue.slice().map(function(v, index) {
        return sorted.indexOf(v) + 1 - period + index;
      });

      result.push((1 - sumsq(ranks) / 120) * 100);
    } else {
      result.push(null);
    }
  }

  return result;
}

export default rci;
