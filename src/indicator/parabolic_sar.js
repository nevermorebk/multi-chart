import { priceFormat } from '../util/format';

function roundUp(value, scale) {
  let ex = Math.pow(10, scale);
  let extending = value * ex;
  return Math.round(extending) / ex;
}

function parabolic_sar(inputsObj = {}) {
  let factor = [inputsObj.up, inputsObj.down];
  let accessor;

  function indicator(data) {
    return calc(data, accessor, factor);
  }

  indicator.factor = function(x) {
    if (x === undefined) {
      return factor;
    }
    factor = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    return 100;
  };

  indicator.key = function() {
    return `parabolic_sar_${factor.join('.')}`;
  };

  return indicator;
}

parabolic_sar.label = function(config = {}) {
  let { inputsObj, stylesObj } = config;
  return `(<span style="color:${stylesObj.up.color}">${inputsObj.up}</span>,<span
  style="color:${stylesObj.down.color}">${inputsObj.down}</span>)`;
};

parabolic_sar.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length === 2) {
    const { stylesObj } = config;
    const formatter = priceFormat(scale);
    let up = formatter(record[0]);
    let down = formatter(record[1]);
    return [
      {
        label: 'up',
        value: up,
        color: stylesObj.up.color,
      },
      {
        label: 'down',
        value: down,
        color: stylesObj.down.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, factors) {
  let [factor, factor2] = factors;

  if (!data || data.length === 0) {
    return [];
  }

  var r = data.map(accessor);
  var last, last_af, last_trend, last_ep, last_parabolic;
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (!last) {
      last = r[i];
      result.push([null, null]);
      continue;
    }

    var parabolic, trend, ep, af;
    if (i === 1) {
      last_trend = trend = r[i].close > last.close ? 1 : -1;
      last_ep = trend === 1 ? r[i].high : r[i].low;
      last_af = factor;
      last_parabolic = parabolic = trend === 1 ? last.high : last.low;
    } else {
      trend = last_trend === 1 ? (last_parabolic > r[i].low ? -1 : 1) : last_parabolic < r[i].high ? 1 : -1;
      ep =
        trend === 1
          ? last_parabolic > r[i].low
            ? r[i].low
            : Math.max(r[i].high, last_ep)
          : last_parabolic < r[i].high
            ? r[i].high
            : Math.min(r[i].low, last_ep);
      af = roundUp(trend !== last_trend ? factor : ep !== last_ep && last_af < factor2 ? last_af + factor : last_af, 2);
      parabolic = trend === last_trend ? last_parabolic + af * (ep - last_parabolic) : last_ep;

      last_trend = trend;
      last_ep = ep;
      last_af = af;
      last_parabolic = parabolic;
    }

    if (trend > 0) {
      result.push([parabolic, null]);
    } else {
      result.push([null, parabolic]);
    }
  }

  return result;
}

export default parabolic_sar;
