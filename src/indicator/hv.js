import { priceFormat } from '../util/format';

function average(data) {
  var sum = data.reduce(function(sum, value) {
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}

function stdevp(values) {
  var avg = average(values);

  var s = 0;
  for (var i = 0; i < values.length; i++) {
    var value = values[i];
    s += Math.pow(value - avg, 2);
  }
  return Math.sqrt(s / values.length);
}

function hv(inputsObj = {}) {
  let period = inputsObj.period;
  let total = inputsObj.total;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period, total);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.total = function(x) {
    if (x === undefined) {
      return total;
    }
    total = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  indicator.key = function() {
    return `hv_${period}_${total}`;
  };

  return indicator;
}

hv.label = function(config = {}) {
  let  { inputsObj, stylesObj } = config;
  let period = inputsObj.period;
  let total = inputsObj.total;
  let color = stylesObj.hv.color;
  return `(<span style="color: ${color}">${period}, ${total}</span>)`;
};

hv.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    let  { inputsObj, stylesObj } = config;
    const period = inputsObj.period;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'hv',
        value: formatter(record),
        color: stylesObj.hv.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period, total) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;
  var last = null;
  var y = null;
  var Rs = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (!last) {
      last = r[i];
      result.push(null);
      continue;
    }

    let ln = Math.log(accessor(r[i]) / accessor(last)) * 100;
    Rs.push(ln);

    if (Rs.length === period) {
      let ld = stdevp(Rs);
      let hv = ld * Math.sqrt(total);
      result.push(hv);

      Rs.splice(0, 1);
    } else {
      result.push(null);
    }

    last = r[i];
  }

  return result;
}

export default hv;
