import { priceFormat } from '../util/format';
import { ChartType } from '../lib';
import { getI18n } from '../config';

function pivot_point(inputsObj = {}) {
  let refDataLength = inputsObj.refDataLength;
  let accessor;

  function indicator(data) {
    return calc(data, accessor);
  }

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    return 0;
  };

  indicator.key = function() {
    return `pivot_point_${refDataLength}`;
  };

  return indicator;
}

// TODO (2018/09/03 18:22)  指标增加 参数有效性判定

pivot_point.label = function(config = {}, context) {
  let { inputsObj, stylesObj } = config;
  let t = getI18n(context.lang).t;
  if (context.chartType >= inputsObj.refChartType) {
    return `<span style='color:red'> (${t('chartType.' + ChartType[inputsObj.refChartType])} ${inputsObj.refDataLength})</span>`;
  }
  return ` (${t('chartType.' + ChartType[inputsObj.refChartType])} ${inputsObj.refDataLength})`;
};

pivot_point.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length > 0) {
    const { stylesObj } = config;
    const formatter = priceFormat(scale);
    let R4 = formatter(record[0]);
    let R3 = formatter(record[1]);
    let R2 = formatter(record[2]);
    let R1 = formatter(record[3]);
    let P = formatter(record[4]);
    let S1 = formatter(record[5]);
    let S2 = formatter(record[6]);
    let S3 = formatter(record[7]);
    let S4 = formatter(record[8]);

    return [
      { label: 'r4', value: `${R4}`, color: stylesObj.r4.color },
      { label: 'r3', value: `${R3}`, color: stylesObj.r3.color },
      { label: 'r2', value: `${R2}`, color: stylesObj.r2.color },
      { label: 'r1', value: `${R1}`, color: stylesObj.r1.color },
      { label: 'p', value: `${P}`, color: stylesObj.p.color },
      { label: 's1', value: `${S1}`, color: stylesObj.s1.color },
      { label: 's2', value: `${S2}`, color: stylesObj.s2.color },
      { label: 's3', value: `${S3}`, color: stylesObj.s3.color },
      { label: 's4', value: `${S4}`, color: stylesObj.s4.color },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor) {
  if (!data || data.length === 0) {
    return [];
  }

  var d = data.map(accessor);
  var last = null;
  var result = [];

  for (var i = 0; i < d.length; i++) {
    if (!last) {
      last = d[i];
      result.push([null, null, null, null, null, null, null, null, null]);
      continue;
    }

    var H = last.high;
    var L = last.low;
    var C = last.close;

    var P = (H + L + C) / 3;

    var R1 = P + (P - L);
    var R2 = P + (H - L);
    var R3 = R2 + (H - L);
    var R4 = R3 + (H - L);

    var S1 = P - (H - P);
    var S2 = P - (H - L);
    var S3 = S2 - (H - L);
    var S4 = S3 - (H - L);

    // var D1 = H - P;
    // var D2 = P - L;
    // var D3 = H - L;

    // var R1 = P + D2;
    // var R2 = P + D3;
    // var R3 = R2 + D3;
    // var R4 = R3 + D3;

    // var S1 = P - D1;
    // var S2 = P - D3;
    // var S3 = S2 - D3;
    // var S4 = S3 - D3;

    result.push([R4, R3, R2, R1, P, S1, S2, S3, S4]);

    last = d[i];
  }

  return result;
}

export default pivot_point;
