/**
 * 加重移動平均線
 */
import { priceFormat } from '../util/format';

function wma(inputsObj = {}) {
  let period = +inputsObj.period;
  let accessor;

  function wma(data) {
    return calc(data, accessor, period);
  }

  wma.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return wma;
  };

  wma.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return wma;
  };

  wma.forward = function() {
    return period - 1;
  };

  wma.key = function() {
    return `wma_${period}`;
  };

  return wma;
}

wma.label = function(config = {}) {
  let  { inputsObj, stylesObj } = config;
  // TODO: assert()
  let period = inputsObj.period;
  let color = stylesObj.wma.color;
  return `(<span style="color: ${color}">${period}</span>)`;
};

wma.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    let  { inputsObj, stylesObj } = config;
    // TODO: assert()
    let period = inputsObj.period;
    let color = stylesObj.wma.color;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'wma',
        value: formatter(record),
        color: color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;

  var y = null;
  var wma = [];
  var periodArr = [];
  var length = r.length;
  var wAvg = function(arr) {
    var sum = 0;
    var n = arr.length;
    var d = (1 + n) * (n / 2);
    var i = n;
    while (i--) {
      sum = sum + arr[i] * ((i + 1) / d);
    }
    return sum;
  };

  for (var i = 0; i < length; i++) {
    let close = accessor(r[i]);
    periodArr.push(close);

    if (period === periodArr.length) {
      var avg = wAvg(periodArr);
      y = avg;
      periodArr.splice(0, 1);
    }
    wma.push(y);
  }
  return wma;
}

export default wma;
