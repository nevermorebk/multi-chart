/**
 * 指数平均数指标
 * 公式：  https://zhidao.baidu.com/question/417909385.html
 */

import { priceFormat } from '../util/format';

function ema(inputsObj = {}) {
  let period = +inputsObj.period;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function (x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function (x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function () {
    if (period === undefined) {
      return 0;
    }
    return 2 * period - 1;
  };

  indicator.key = function () {
    return `ema_${period}`;
  };

  return indicator;
}

ema.label = function (config = {}) {
  let { inputsObj, stylesObj } = config;
  // TODO: assert()
  let period = inputsObj.period;
  let color = stylesObj.ema.color;
  return `(<span style="color: ${color}">${period}</span>)`;
};

ema.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    let { inputsObj, stylesObj } = config;
    // TODO: assert()
    let period = inputsObj.period;
    let color = stylesObj.ema.color;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'ema',
        value: formatter(record),
        color: color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;

  var t;
  var y = null;
  var k = 2 / (period + 1);
  var ema; // exponential moving average.
  var emLine = [];
  var periodArr = [];
  var length = r.length;
  var arrayAvg = function (arr) {
    var sum = 0,
      arrLength = arr.length,
      i = arrLength;
    while (i--) {
      sum = sum + arr[i];
    }
    return sum / arrLength;
  };
  for (var i = 0; i < length; i++) {
    // if (r[i - 1]) {
    periodArr.push(accessor(r[i]));
    // }

    if (period === periodArr.length) {
      // t = r[i][1];
      if (!y) {
        y = arrayAvg(periodArr);
      } else {
        let prev = emLine[emLine.length - 1];
        let ema = prev + k * (periodArr[periodArr.length - 1] - prev);
        y = ema;
      }
      periodArr.splice(0, 1);
    }
    emLine.push(y);
  }

  return emLine;
}

export default ema;
