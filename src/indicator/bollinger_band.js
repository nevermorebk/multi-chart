//  単純移動平均線±(2×標準偏差)
//  close         high

import { priceFormat } from '../util/format';

function avgFn(arr) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i];
  }
  return sum / arrLength;
}

function stdevp(arr) {
  var avg = avgFn(arr);
  var s = 0;
  for (var i = 0; i < arr.length; i++) {
    var value = arr[i];
    s += Math.pow(value - avg, 2);
  }
  return Math.sqrt(s / arr.length);
}

function bollinger_band(inputsObj = {}) {
  let period = inputsObj.period;
  let variance = inputsObj.variance;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period, variance);
  }

  indicator.period = function (x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.variance = function (x) {
    if (x === undefined) {
      return variance;
    }
    variance = x;
    return indicator;
  };

  indicator.accessor = function (x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function () {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  indicator.key = function () {
    return `bollinger_band_${period}_${variance}`;
  };

  return indicator;
}

bollinger_band.label = function (config = {}) {
  let { inputsObj, stylesObj } = config;
  let period = inputsObj.period;
  let variance = inputsObj.variance;
  let color = stylesObj.ma.color;
  return `(<span style="color: ${color}">${period},${variance}</span>)`;
};

bollinger_band.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length === 3) {
    let { stylesObj } = config;
    const formatter = priceFormat(scale);
    let upper = formatter(record[0]);
    let ma = formatter(record[1]);
    let lower = formatter(record[2]);
    return [
      {
        label: 'upper',
        value: upper,
        color: stylesObj.upper.color,
      },
      {
        label: 'ma',
        value: ma,
        color: stylesObj.ma.color,
      },
      {
        label: 'lower',
        value: lower,
        color: stylesObj.lower.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period, variance) {
  if (!data || data.length === 0) {
    return [];
  }

  var y = null;
  var d = data.map(accessor);
  var periodArr = [];
  var result = [];

  for (var i = 0; i < d.length; i++) {
    periodArr.push(d[i]);
    if (periodArr.length === period) {
      var sigma = stdevp(periodArr);
      var ma = avgFn(periodArr);
      var upper = ma + sigma * variance;
      var lower = ma - sigma * variance;
      y = [upper, ma, lower];

      periodArr.splice(0, 1);
    } else {
      y = [null, null, null];
    }

    result.push(y);
  }

  return result;
}

export default bollinger_band;
