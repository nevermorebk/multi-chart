import { priceFormat } from '../util/format';

function average(arr) {
  var sum = 0,
    arrLength = arr.length,
    i = arrLength;
  while (i--) {
    sum = sum + arr[i];
  }
  return sum / arrLength;
}

function envelope(inputsObj = {}) {
  let period = inputsObj.period;
  let deviation = inputsObj.deviation;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period, deviation);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.deviation = function(x) {
    if (x === undefined) {
      return deviation;
    }
    deviation = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  indicator.key = function() {
    return `envelope_${period}_${deviation}`;
  };

  return indicator;
}

envelope.label = function(config = {}) {
  let  { inputsObj, stylesObj } = config;
  let period = inputsObj.period;
  let deviation = inputsObj.deviation;
  let color = stylesObj.ma.color;
  return `(<span style="color: ${color}">${period}, ${deviation}</span>)`;
};


envelope.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length === 3) {
    let  { stylesObj } = config;
    const formatter = priceFormat(scale);
    let upper = formatter(record[0]);
    let ma = formatter(record[1]);
    let lower = formatter(record[2]);
    return [
      {
        label: 'upper',
        value: upper,
        color: stylesObj.upper.color,
      },
      {
        label: 'ma',
        value: ma,
        color: stylesObj.ma.color,
      },
      {
        label: 'lower',
        value: lower,
        color: stylesObj.lower.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period, deviation) {
  if (!data || data.length === 0) {
    return [];
  }

  var y = null;
  var d = data;
  var periodArr = [];
  var result = [];

  for (var i = 0; i < d.length; i++) {
    periodArr.push(accessor(d[i]));
    if (periodArr.length === period) {
      var avg = average(periodArr);

      y = [avg + avg * deviation, avg, avg - avg * deviation];

      periodArr.splice(0, 1);
    } else {
      y = [null, null, null];
    }

    result.push(y);
  }

  return result;
}

export default envelope;
