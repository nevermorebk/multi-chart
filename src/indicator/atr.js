import { max as _max } from '../util/array';
import { priceFormat } from '../util/format';

function average(arr) {
  var sum = 0,
    arrLength = arr.length,
    i = arrLength;
  while (i--) {
    sum = sum + arr[i];
  }
  return sum / arrLength;
}

function roundUp(value, scale) {
  let ex = Math.pow(10, scale);
  let extending = value * ex;
  return Math.round(extending) / ex;
}

function atr(inputsObj = {}) {
  let period = inputsObj.period;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  indicator.key = function() {
    return `atr_${period}`;
  };

  return indicator;
}

atr.label = function(config = {}) {
  let {inputsObj, stylesObj} = config;
  let period = inputsObj.period;
  let color = stylesObj.atr.color;
  return `(<span style="color:${color}">${period}</span>)`;
};

atr.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    const { stylesObj } = config;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'atr',
        value: formatter(record),
        color: stylesObj.atr.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data.map(accessor);
  var last = null;
  let last_atr;
  var TRs = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (!last) {
      last = r[i];
      result.push(null);
      continue;
    }

    let TR = roundUp(_max([r[i].high - r[i].low, r[i].high - last.close, r[i].low - last.close]), 4);
    TRs.push(TR);

    if (TRs.length === period - 1) {
      let atr;
      if (i < period) {
        atr = roundUp(average(TRs), 5);
      } else {
        atr = roundUp(last_atr + (TR - last_atr) * 2 / (period + 1), 5);
      }

      last_atr = atr;

      result.push(atr);
      TRs.splice(0, 1);
    } else {
      result.push(null);
    }

    last = r[i];
  }

  return result;
}

export default atr;
