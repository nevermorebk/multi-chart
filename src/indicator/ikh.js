import { priceFormat } from '../util/format';
import { forEach } from '../util/index';

/**
 * 一目均衡表
 */

function mid(v1, v2) {
  return v1 > 0 && v2 > 0 ? (v1 + v2) / 2 : null;
}

function left(record, index, limit) {
  var i = index + limit;
  return i < record.length ? record[i].close : null;
}

function low(record, index, limit) {
  if (index < limit) return 0;
  // if (index > record.length - limit) {
  //   return 0;
  // }
  var r = Infinity;
  for (var i = 0; i <= limit; i++) {
    r = Math.min(r, record[index - i].low);
  }
  return r;
}

function high(record, index, limit) {
  if (index < limit) return 0;
  // if (index > record.length - limit) {
  //   return 0;
  // }
  var r = -Infinity;
  for (var i = 0; i <= limit; i++) {
    r = Math.max(r, record[index - i].high);
  }
  return r;
}

function ikh(inputsObj = {}) {
  let tl, kl, s;
  tl = +inputsObj.tl;
  kl = +inputsObj.kl;
  s = +inputsObj.s;
  let period = [tl, kl, s];
  let accessor;

  function ikh(data) {
    return calc(data, accessor, period);
  }

  ikh.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return ikh;
  };

  ikh.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return ikh;
  };

  ikh.forward = function() {
    return period[1] - 1 + period[2] - 1;
  };

  ikh.backward = function() {
    return period[1] || 0;
  };

  ikh.key = function() {
    return `ikh_${period.join('.')}`;
  };

  return ikh;
}

ikh.label = function(config = {}) {
  let {inputsObj, stylesObj} = config;
  // TODO: assert()
  let tlPeriod = +inputsObj.tl;
  let klPeriod = +inputsObj.kl;
  let sPeriod = +inputsObj.s;
  let tlColor = stylesObj.tl.color;
  let klColor = stylesObj.kl.color;
  let sbColor = stylesObj.sb.color;
  return `(<span style="color:${tlColor}">${tlPeriod}</span>,<span
  style="color:${klColor}">${klPeriod}</span>,<span
  style="color:${sbColor}">${sPeriod}</span>)`;
};

ikh.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length > 0) {

    let {inputsObj, stylesObj} = config;
    // TODO: assert()
    let tlPeriod = +inputsObj.tl;
    let klPeriod = +inputsObj.kl;
    let sPeriod = +inputsObj.s;
    let tlColor = stylesObj.tl.color;
    let klColor = stylesObj.kl.color;
    let csColor = stylesObj.cs.color;
    let saColor = stylesObj.sa.color;
    let sbColor = stylesObj.sb.color;

    const formatter = priceFormat(scale);
    let tl = formatter(record[0]);
    let kl = formatter(record[1]);
    let cs = formatter(record[2]);
    let sa = formatter(record[5][0]);
    let sb = formatter(record[5][1]);
    return [
      {
        label: 'tl',
        value: tl,
        color: tlColor
      },
      {
        label: 'kl',
        value: kl,
        color: klColor
      },
      {
        label: 'cs',
        value: cs,
        color: csColor
      },
      {
        label: 'sa',
        value: sa,
        color: saColor
      },
      {
        label: 'sb',
        value: sb,
        color: sbColor
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  let [p0, p1, p2] = period;
  let record = data.map(accessor);
  let r = [];
  let size = record.length;
  let rLen = record.length + p1;

  for (let k = -p1 + 1; k < record.length; k++) {
    r.push([null, null, null, [null, null]]);
  }

  for (let i = 0; i < size; i++) {
    r[i][0] = mid(low(record, i, p0 - 1), high(record, i, p0 - 1));
    r[i][1] = mid(low(record, i, p1 - 1), high(record, i, p1 - 1));
    r[i][2] = left(record, i, p1 - 1);
    r[i + p1 -1][3] = mid(r[i][0], r[i][1]);
    r[i + p1 -1][4] = mid(low(record, i, p2 - 1), high(record, i, p2 - 1));
    r[i + p1 - 1][5] = [r[i + p1 -1][3], r[i + p1 -1][4]];
    // r[i + p1 - 1][3] = mid(r[i][0], r[i][1]);
    // r[i + p1 - 1][4] = mid(low(record, i, p2 - 1), high(record, i, p2 - 1));
  }

  return r;
}

export default ikh;
