/**
 * 简单移动平均线
 * 公式：  https://zh.wikipedia.org/wiki/%E7%A7%BB%E5%8B%95%E5%B9%B3%E5%9D%87
 */

import { priceFormat } from '../util/format';

function sma(inputsObj = {}) {
  let period = +inputsObj.period;
  let accessor;

  function sma(data) {
    return calc(data, accessor, period);
  }

  sma.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return sma;
  };

  sma.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return sma;
  };

  sma.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  sma.key = function() {
    return `sma_${period}`;
  };

  return sma;
}

sma.label = function(config = {}) {
  let  { inputsObj, stylesObj } = config;
  // TODO: assert()
  let period = inputsObj.period;
  let color = stylesObj.sma.color;
  return `(<span style="color: ${color}">${period}</span>)`;
};

sma.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    let  { inputsObj, stylesObj } = config;
    // TODO: assert()
    let period = inputsObj.period;
    let color = stylesObj.sma.color;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'sma',
        value: formatter(record),
        color: color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;

  var y = null;
  var sma = [];
  var periodArr = [];
  var length = r.length;
  var arrayAvg = function(arr) {
    var sum = 0,
      arrLength = arr.length,
      i = arrLength;
    while (i--) {
      sum = sum + arr[i];
    }
    return sum / arrLength;
  };

  for (var i = 0; i < length; i++) {
    let close = accessor(r[i]);
    periodArr.push(close);

    if (period === periodArr.length) {
      var avg = arrayAvg(periodArr);
      y = avg;
      periodArr.splice(0, 1);
    }
    sma.push(y);
  }
  return sma;
}

export default sma;
