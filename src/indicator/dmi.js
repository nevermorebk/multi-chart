import { priceFormat } from '../util/format';

function averageWithIndex(arr, index = 0) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i][index];
  }
  return sum / arrLength;
}

function averageDI(arr) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i];
  }
  return sum / arrLength;
}

function dmi(inputsObj = {}) {
  let period = inputsObj.period;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return 2 * period - 1;
  };

  indicator.key = function() {
    return `dmi_${period}`;
  };

  return indicator;
}

dmi.label = function(config = {}) {
  let {inputsObj, stylesObj} = config;
  let period = inputsObj.period;
  let color = stylesObj.adx.color;
  return `(<span style="color:${color}">${period}</span>)`;
};

dmi.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length === 3) {
    const {stylesObj} = config;
    const formatter = priceFormat(scale);
    let adx = formatter(record[0]);
    let dip = formatter(record[1]);
    let dim = formatter(record[2]);
    return [
      {
        label: 'adx',
        value: adx,
        color: stylesObj.adx.color
      },
      {
        label: 'diplus',
        value: dip,
        color: stylesObj.diplus.color,
      },
      {
        label: 'diminus',
        value: dim,
        color: stylesObj.diminus.color
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data.map(accessor);
  var last = null;
  var periodArray = [];
  var periodArray1 = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (!last) {
      last = r[i];
      result.push([null, null, null]);
      continue;
    }

    var current = r[i];

    var DMP = current.high - last.high;
    var DMN = last.low - current.low;

    var DM_PLUS, DM_MINUS;

    if (DMP > DMN && DMP > 0) {
      DM_PLUS = DMP;
    } else {
      DM_PLUS = 0;
    }

    if (DMN > DMP && DMN > 0) {
      DM_MINUS = DMN;
    } else {
      DM_MINUS = 0;
    }

    var TR = Math.max(current.high, last.close) - Math.min(current.low, last.close);
    periodArray.push([DM_PLUS, DM_MINUS, TR]);

    if (periodArray.length === period) {
      var TRP = averageWithIndex(periodArray, 2);
      var DI_PLUS = TRP === 0 ? null : averageWithIndex(periodArray, 0) * 100 / TRP;
      var DI_MINUS = TRP === 0 ? null : averageWithIndex(periodArray, 1) * 100 / TRP;

      periodArray.splice(0, 1);
      let DX = Math.abs(DI_PLUS - DI_MINUS) / (DI_PLUS + DI_MINUS) * 100;
      periodArray1.push(DX);

      if (periodArray1.length === period) {
        var ADX = TR === 0 ? null : averageDI(periodArray1);
        periodArray1.splice(0, 1);
        result.push([ADX, DI_PLUS, DI_MINUS]);
      } else {
        result.push([null, DI_PLUS, DI_MINUS]);
      }
    } else {
      result.push([null, null, null]);
    }

    last = r[i];
  }

  return result;
}

export default dmi;
