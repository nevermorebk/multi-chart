import { priceFormat } from '../util/format';

function avg(arr, field) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i][field];
  }
  return sum / arrLength;
}

function rsi(inputsObj = {}) {
  let period = inputsObj.period;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  indicator.key = function() {
    return `rsi_${period}`;
  };

  return indicator;
}

rsi.label = function(config = {}) {
  let { inputsObj, stylesObj } = config;
  let period = inputsObj.period;
  let color = stylesObj.rsi.color;
  return `(<span style="color: ${color}">${period}</span>)`;
};

rsi.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    const {inputsObj, stylesObj} = config;
    const period = inputsObj.period;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'rsi',
        value: formatter(record),
        color: stylesObj.rsi.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;
  var last = null;
  var periodArray = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (!last) {
      last = r[i];
      result.push(null);
      continue;
    }

    let up = Math.max(accessor(r[i]) - accessor(last), 0);
    let down = Math.min(accessor(r[i]) - accessor(last), 0);

    periodArray.push([up, down]);
    if (periodArray.length === period) {
      let upAvg = avg(periodArray, 0);
      let downAvg = avg(periodArray, 1) * -1;

      if (upAvg + downAvg === 0) {
        result.push(0);
      } else {
        result.push(100 * upAvg / (upAvg + downAvg));
      }

      periodArray.splice(0, 1);
    } else {
      result.push(null);
    }

    last = r[i];
  }

  return result;
}

export default rsi;
