import { priceFormat } from '../util/format';

function avgFn(arr) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i];
  }
  return sum / arrLength;
}

function stdevp(arr) {
  var avg = avgFn(arr);
  var s = 0;
  for (var i = 0; i < arr.length; i++) {
    var value = arr[i];
    s += Math.pow(value - avg, 2);
  }
  return Math.sqrt(s / arr.length);
}

function super_bollinger(inputsObj = {}) {

  let period = [inputsObj.period, inputsObj.latePeriod];
  let variance = [inputsObj.variance1, inputsObj.variance2, inputsObj.variance3];
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period, variance);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.variance = function(x) {
    if (x === undefined) {
      return variance;
    }
    variance = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    return Math.max.apply(null, period) - 1;
  };

  indicator.backward = function() {
    return period[1] || 0;
  };

  indicator.key = function() {
    return `super_bollinger_${period.join('.')}_${variance}`;
  };

  return indicator;
}

super_bollinger.label = function(config = {}) {
  let {inputsObj} = config;
  let period = [inputsObj.period, inputsObj.latePeriod];
  let variance = [inputsObj.variance1, inputsObj.variance2, inputsObj.variance3];
  return `(${period},${variance})`;
};

super_bollinger.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length > 0) {
    const { stylesObj } = config;
    const formatter = priceFormat(scale);

    let upper3 = formatter(record[0]);
    let upper2 = formatter(record[1]);
    let upper1 = formatter(record[2]);
    let ma = formatter(record[3]);
    let lower1 = formatter(record[4]);
    let lower2 = formatter(record[5]);
    let lower3 = formatter(record[6]);
    let late = formatter(record[7]);

    return [
      { label: 'upper3', value: `${upper3}`, color: stylesObj.upper3.color },
      { label: 'upper2', value: `${upper2}`, color: stylesObj.upper2.color },
      { label: 'upper1', value: `${upper1}`, color: stylesObj.upper1.color },
      { label: 'ma', value: `${ma}`, color: stylesObj.ma.color },
      { label: 'lower1', value: `${lower1}`, color: stylesObj.lower1.color },
      { label: 'lower2', value: `${lower2}`, color: stylesObj.lower2.color },
      { label: 'lower3', value: `${lower3}`, color: stylesObj.lower3.color },
      { label: 'late', value: `${late}`, color: stylesObj.late.color },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, periods, variances) {
  let [period, latePeriod] = periods;
  let [variance1, variance2, variance3] = variances;

  if (!data || data.length === 0) {
    return [];
  }

  var y = null;
  var d = data;
  var periodArr = [];
  var result = [];

  for (var i = 0; i < d.length; i++) {
    periodArr.push(accessor(d[i]));

    var late = null;
    if (i + latePeriod - 1 < d.length) {
      late = accessor(d[i + latePeriod - 1]);
    }

    if (periodArr.length === period) {
      var sigma = stdevp(periodArr);
      var ma = avgFn(periodArr);
      var upper1 = ma + sigma * variance1;
      var lower1 = ma - sigma * variance1;

      var upper2 = ma + sigma * variance2;
      var lower2 = ma - sigma * variance2;

      var upper3 = ma + sigma * variance3;
      var lower3 = ma - sigma * variance3;

      // var late = null;
      // if (i + latePeriod - 1 < d.length) {
      //   late = accessor(d[i + latePeriod - 1]);
      // }

      y = [upper3, upper2, upper1, ma, lower1, lower2, lower3, late];
      periodArr.splice(0, 1);
    } else {
      y = [null, null, null, null, null, null, null, late];
    }

    result.push(y);
  }

  return result;
}

export default super_bollinger;
