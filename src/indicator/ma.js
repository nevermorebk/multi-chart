/**
 * 移动平均乖离率
 * 公式：  https://zh.wikipedia.org/wiki/%E4%B9%96%E9%9B%A2%E7%8E%87
 */

import { priceFormat } from '../util/format';

function ma(inputsObj = {}) {
  let period = +inputsObj.period;
  let accessor;

  function ma(data) {
    return calc(data, accessor, period);
  }

  ma.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return ma;
  };

  ma.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return ma;
  };

  ma.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period - 1;
  };

  ma.key = function() {
    return `ma_${period}`;
  };

  return ma;
}

ma.label = function(config = {}) {
  let {inputsObj, stylesObj} = config;
  let period = inputsObj.period;
  let color = stylesObj.ma.color;
  return `(<span style="color:${color}">${period}</span>)`;
};

ma.tooltip = function tooltip(record, scale, config = {}) {
  let tooltipData = null;
  if (record !== undefined) {
    const { inputsObj, stylesObj } = config;
    const period = inputsObj.period;
    const formatter = priceFormat(scale);
    const maColor = stylesObj.ma.color;
    tooltipData = [
      {
        label: 'ma',
        value: formatter(record),
        color: maColor,
      },
    ];
  }
  return tooltipData;
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data;

  var y = null;
  var ma = [];
  var periodArr = [];
  var length = r.length;
  var arrayAvg = function(arr) {
    var sum = 0,
      arrLength = arr.length,
      i = arrLength;
    while (i--) {
      sum = sum + arr[i];
    }
    return sum / arrLength;
  };

  for (var i = 0; i < length; i++) {
    let close = accessor(r[i]);
    periodArr.push(close);

    if (period === periodArr.length) {
      var avg = arrayAvg(periodArr);
      y = (close - avg) / avg * 100;
      periodArr.splice(0, 1);
    }
    ma.push(y);
  }
  return ma;
}

export default ma;
