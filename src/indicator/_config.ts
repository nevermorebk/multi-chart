import { ChartType } from '../lib';

/*
  trend/oscillator
  默认配置
  // TODO (2018/03/05 11:42) remove. (use meta instead)
*/
export const config = {
  ma: {
    name: 'ma',
    primary: false,
    inputsObj: {
      period: 7,
      scale: 2,
    },
    stylesObj: {
      ma: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
  wma: {
    name: 'wma',
    primary: true,
    inputsObj: {
      period: 10,
    },
    stylesObj: {
      wma: {
        color: '#a600ff',
        width: 0.5,
        style: 0,
      },
    },
  },
  sma: {
    name: 'sma',
    primary: true,
    inputsObj: {
      period: 10,
    },
    stylesObj: {
      sma: {
        color: '#28da49',
        width: 0.5,
        style: 0,
      },
    },
  },
  ema: {
    name: 'ema',
    primary: true,
    inputsObj: {
      period: 12,
    },
    stylesObj: {
      ema: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
  macd: {
    name: 'macd',
    primary: false,
    inputsObj: {
      short: 12,
      long: 26,
      signal: 9,
      scale: 4,
      symmetric: true,
    },
    stylesObj: {
      macd: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
      signal: {
        color: 'green',
        width: 0.5,
        style: 0,
      },
      osci: {
        positive: '#bf1032',
        negative: '#01abed',
      },
    },
  },
  ikh: {
    name: 'ikh',
    primary: true,
    inputsObj: {
      tl: 9,
      kl: 26,
      s: 52,
    },
    stylesObj: {
      tl: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
      kl: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
      cs: {
        color: '#00ffff',
        width: 0.5,
        style: 0,
      },
      sa: {
        color: 'rgb(255,191,0)',
        width: 0.5,
        style: 0,
      },
      sb: {
        color: 'rgb(255,191,0)',
        width: 0.5,
        style: 0,
      },
      sc: 'rgba(0, 255, 0, 0.3)',
    },
  },
  bollinger_band: {
    name: 'bollinger_band',
    primary: true,
    inputsObj: {
      period: 20,
      variance: 2,
    },
    stylesObj: {
      upper: {
        color: '#ffa800',
        width: 0.5,
        style: 0,
      },
      ma: {
        color: '#276eff',
        width: 0.5,
        style: 0,
      },
      lower: {
        color: '#ffa800',
        width: 0.5,
        style: 0,
      },
    },
  },
  pivot_point: {
    name: 'pivot_point',
    primary: true,
    singleton: true,
    inputsObj: {
      useRefData: true,
      refChartType: ChartType.D1,
      refDataLength: 2,
    },
    stylesObj: {
      r4: {
        color: '#ea157a',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      r3: {
        color: '#00addc',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      r2: {
        color: '#1ab39f',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      r1: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      p: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      s1: {
        color: '#a6a65b',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      s2: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      s3: {
        color: '#feb80a',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
      s4: {
        color: '#7fd13b',
        width: 0.5,
        style: 0,
        styleFixed: true,
      },
    },
  },
  envelope: {
    name: 'envelope',
    primary: true,
    inputsObj: {
      period: 15,
      deviation: 0.002,
    },
    stylesObj: {
      upper: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
      ma: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
      lower: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
    },
  },
  parabolic_sar: {
    name: 'parabolic_sar',
    primary: true,
    inputsObj: {
      up: 0.02,
      down: 0.2,
    },
    stylesObj: {
      up: {
        color: '#ff0000',
        width: 2,
        style: 1,
        styleFixed: true,
      },
      down: {
        color: '#0000ff',
        width: 2,
        style: 1,
        styleFixed: true,
      },
    },
  },
  span_model: {
    name: 'span_model',
    primary: true,
    inputsObj: {
      period1: 9,
      period2: 20,
      period3: 52,
    },
    stylesObj: {
      late: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
      span1: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
      span2: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
      span3: 'rgba(255,0,0, 0.3)',
    },
  },
  super_bollinger: {
    name: 'super_bollinger',
    primary: true,
    inputsObj: {
      period: 21,
      latePeriod: 21,
      variance1: 1,
      variance2: 2,
      variance3: 3,
    },
    stylesObj: {
      upper3: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
      upper2: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
      upper1: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
      ma: {
        color: '#00ffff',
        width: 0.5,
        style: 0,
      },
      lower1: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
      lower2: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
      lower3: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
      late: {
        color: '#00ffff',
        width: 0.5,
        style: 0,
      },
    },
  },
  rsi: {
    name: 'rsi',
    primary: false,
    inputsObj: {
      period: 14,
    },
    stylesObj: {
      rsi: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
    },
  },
  rci: {
    name: 'rci',
    primary: false,
    inputsObj: {
      period: 9,
    },
    stylesObj: {
      rci: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
    },
  },
  kdj: {
    name: 'kdj',
    primary: false,
    inputsObj: {
      k: 15,
      d: 5,
      sd: 5,
    },
    stylesObj: {
      k: {
        color: '#00ffff',
        width: 0.5,
        style: 0,
      },
      d: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
      sd: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
  hv: {
    name: 'hv',
    primary: false,
    inputsObj: {
      period: 7,
      total: 250,
    },
    stylesObj: {
      hv: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
  momentum: {
    name: 'momentum',
    primary: false,
    inputsObj: {
      period: 10,
    },
    stylesObj: {
      momentum: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
  atr: {
    name: 'atr',
    primary: false,
    inputsObj: {
      period: 10,
    },
    stylesObj: {
      atr: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
  dmi: {
    name: 'dmi',
    inputsObj: {
      period: 9,
    },
    stylesObj: {
      adx: {
        color: '#00ff00',
        width: 0.5,
        style: 0,
      },
      diplus: {
        color: '#8c8c03',
        width: 0.5,
        style: 0,
      },
      diminus: {
        color: '#00ffff',
        width: 0.5,
        style: 0,
      },
    },
  },
  adx: {
    name: 'adx',
    primary: false,
    inputsObj: {
      period: 14,
    },
    stylesObj: {
      adx: {
        color: '#ff0000',
        width: 0.5,
        style: 0,
      },
    },
  },
};
