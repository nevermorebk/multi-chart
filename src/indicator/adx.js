import { priceFormat } from '../util/format';

function sumWithIndex(arr, index = 0) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i][index];
  }
  return sum;
}

function averageWithIndex(arr, index = 0) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i][index];
  }
  return sum / arrLength;
}

function averageDI(arr) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    if (!arr[i][0] || !arr[i][1]) {
      continue;
    }
    sum = sum + Math.abs(arr[i][0] - arr[i][1]);
  }
  return sum / arrLength;
}

function adx(inputsObj = {}) {
  let period = inputsObj.period;
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return 2 * period - 1;
  };

  indicator.key = function() {
    return `adx_${period}`;
  };

  return indicator;
}

adx.label = function(config = {}) {
  let {inputsObj, stylesObj} = config;
  let period = inputsObj.period;
  let color = stylesObj.adx.color;
  return `(<span style="color:${color}">${period}</span>)`;
};

adx.tooltip = function tooltip(record, scale, config = {}) {
  if (record !== undefined) {
    const { stylesObj } = config;
    const formatter = priceFormat(scale);
    return [
      {
        label: 'adx',
        value: formatter(record),
        color: stylesObj.adx.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  if (!data || data.length === 0) {
    return [];
  }

  var r = data.map(accessor);
  var last = null;
  var periodArray = [];
  var periodArray1 = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    if (!last) {
      last = r[i];
      result.push(null);
      continue;
    }

    var current = r[i];

    var DMP = current.high - last.high;
    var DMN = last.low - current.low;

    var DM_PLUS, DM_MINUS;

    if (DMP > DMN && DMP > 0) {
      DM_PLUS = DMP;
    } else {
      DM_PLUS = 0;
    }

    if (DMN > DMP && DMN > 0) {
      DM_MINUS = DMN;
    } else {
      DM_MINUS = 0;
    }

    var TR = Math.max(current.high, last.close) - Math.min(current.low, last.close);

    periodArray.push([DM_PLUS, DM_MINUS, TR]);

    if (periodArray.length === period) {
      let sumTR = sumWithIndex(periodArray, 2);
      let DI_PLUS = sumWithIndex(periodArray, 0) / sumTR * 100;
      let DI_MINUS = sumWithIndex(periodArray, 1) / sumTR * 100;

      let DX = Math.abs((DI_PLUS - DI_MINUS) / (DI_PLUS + DI_MINUS) * 100);

      periodArray.splice(0, 1);
      periodArray1.push([DX]);

      if (periodArray1.length === period) {
        var ADX = averageWithIndex(periodArray1, 0);
        periodArray1.splice(0, 1);
        result.push(ADX);
      } else {
        result.push(null);
      }
    } else {
      result.push(null);
    }

    last = r[i];
  }

  return result;
}

export default adx;
