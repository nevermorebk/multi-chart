import { max as _max, min as _min, map as _map } from '../util/array';
import { priceFormat } from '../util/format';

function min(array) {
  return _min(
    _map(array, function(a) {
      return a.low;
    })
  );
}

function max(array) {
  return _max(
    _map(array, function(a) {
      return a.high;
    })
  );
}

function averageWithIndex(arr, index = 0) {
  var sum = 0;
  var arrLength = arr.length;
  var i = arrLength;
  while (i--) {
    sum = sum + arr[i][index];
  }
  return sum / arrLength;
}

function kdj(inputsObj = {}) {
  let period = [
    inputsObj.k,
    inputsObj.d,
    inputsObj.sd,
  ];
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    if (period === undefined) {
      return 0;
    }
    return period[0] - 1 + (period[1] - 1) + (period[2] - 1);
  };

  indicator.key = function() {
    return `kdj_${period}`;
  };

  return indicator;
}

kdj.label = function(config = {}) {
  let { inputsObj, stylesObj } = config;
  let period = [
    inputsObj.k,
    inputsObj.d,
    inputsObj.sd,
  ];
  let color = [
    stylesObj.k.color,
    stylesObj.d.color,
    stylesObj.sd.color,
  ];
  return `(<span style="color: ${color[0]}">${period[0]}</span>,<span
  style="color: ${color[1]}">${period[1]}</span>,<span
  style="color: ${color[2]}">${period[2]}</span>)`;
};

kdj.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length === 3) {
    const {stylesObj} = config;
    const formatter = priceFormat(scale);
    let k = formatter(record[0]);
    let d = formatter(record[1]);
    let sd = formatter(record[2]);
    return [
      {
        label: 'k',
        value: k,
        color: stylesObj.k.color,
      },
      {
        label: 'd',
        value: d,
        color: stylesObj.d.color,
      },
      {
        label: 'sd',
        value: sd,
        color: stylesObj.sd.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  let [p0, p1, p2] = period;

  if (!data || data.length === 0) {
    return [];
  }

  var r = data.map(accessor);
  var p0Array = [];
  var p1Array = [];
  var p2Array = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    p0Array.push(r[i]);
    if (p0Array.length === p0) {
      var H = r[i].close - min(p0Array);
      var L = max(p0Array) - min(p0Array);
      var K = 100 * H / L;

      var SS = [K, null, null];
      p1Array.push([H, L]);

      if (p1Array.length === p1) {
        SS[1] = 100 * averageWithIndex(p1Array, 0) / averageWithIndex(p1Array, 1);
        p2Array.push([SS[1]]);

        if (p2Array.length === p2) {
          SS[2] = averageWithIndex(p2Array, 0);
          p2Array.splice(0, 1);
        }
        p1Array.splice(0, 1);
      }

      result.push(SS);
      p0Array.splice(0, 1);
    } else {
      result.push([null, null, null]);
    }
  }

  return result;
}

export default kdj;
