import ema from './ema';
import { priceFormat } from '../util/format';
import { forEach } from '../util/array';

function arrayAvg(arr) {
  let sum = 0,
    arrLength = arr.length,
    i = arrLength;
  while (i--) {
    sum = sum + arr[i];
  }
  return sum / arrLength;
}

function macd(inputsObj = {}) {
  let period = [inputsObj.short, inputsObj.long, inputsObj.signal];
  let accessor;

  function macd(data) {
    return calc(data, accessor, period);
  }

  macd.period = function (x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return macd;
  };

  macd.accessor = function (x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return macd;
  };

  macd.forward = function () {
    if (period === undefined) {
      return 0;
    }
    let [short, long, signal] = period;
    return 2 * Math.max(short, long) - 1 + (signal - 1);
  };

  macd.key = function () {
    return `macd_${period.join('.')}`;
  };

  return macd;
}

macd.label = function (config = {}) {
  let { inputsObj, stylesObj } = config;
  let period = [inputsObj.short, inputsObj.long, inputsObj.signal];
  let color = [stylesObj.macd.color, stylesObj.signal.color];
  return `(<span style="color: ${color[0]}">${period[0]}</span>,<span
  style="color: ${color[0]}">${period[1]}</span>,<span style="color: ${color[1]}">${period[2]}</span>)`;
};

macd.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length === 3) {
    const { stylesObj } = config;
    const formatter = priceFormat(scale);
    let macd = formatter(record[0]);
    let signal = formatter(record[1]);
    let osci = formatter(record[2]);
    return [
      {
        label: 'macd',
        value: macd,
        color: stylesObj.macd.color,
      },
      {
        label: 'signal',
        value: signal,
        color: stylesObj.signal.color,
      },
      {
        label: 'osci',
        value: osci,
        color: osci > 0 ? stylesObj.osci.positive : stylesObj.osci.negative,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, period) {
  let p0 = period[0];
  let p1 = period[1];
  let p2 = period[2];

  let ema0 = ema([{ name: 'ema', val: p0 }])
    .period(p0)
    .accessor(accessor)(data);
  let ema1 = ema([{ name: 'ema', val: p1 }])
    .period(p1)
    .accessor(accessor)(data);

  // console.log(`ema short: ${ema0}, ema long: ${ema1}`);

  let dif = [];
  forEach(ema0, function (value, index) {
    if (ema1[index]) {
      dif.push(value - ema1[index]);
    }
  });

  // console.log(`macd: ${dif}`);

  let macdArray = [];
  let signal = [];
  let macd = dif;
  for (let i = 0; i < macd.length; i++) {
    macdArray.push(macd[i]);
    if (macdArray.length === p2) {
      signal.push([macd[i], arrayAvg(macdArray)]);
      macdArray.splice(0, 1);
    } else {
      signal.push([macd[i], null]);
    }
  }
  //
  // let dem = Ema.single(dif, 1, p2);

  forEach(signal, function (value, index) {
    if (value[0] && value[1]) {
      value[2] = value[0] - value[1];
    } else {
      value[2] = null;
    }
    // console.log(`value[0],macd: ${value[1]}, singal: ${value[2]}, osci: ${value[3]}`);
  });
  // console.log(signal);

  // fill empty data
  let empty = [null, null, null];
  for (let i = 0, len = data.length - signal.length; i < len; i++) {
    signal.unshift(empty);
  }
  return signal;
}

export default macd;
