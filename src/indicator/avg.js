/**
 * 注：目前 AVG 非指标
 */

function avg() {
  let accessor;
  let scale;

  function avg(data) {
    return calc(data, accessor, scale);
  }

  avg.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return avg;
  };

  avg.scale = function(x) {
    if (x === undefined) {
      return scale;
    }
    scale = x;
    return avg;
  };

  avg.forward = function() {
    return 0;
  };

  return avg;
}

function calc(data, accessor, scale) {
  if (!data || data.length === 0) {
    return [];
  }

  let avg = [];

  for (let i = 0, len = data.length; i < len; i++) {
    let ohlc = accessor(data[i]);
    let { open, high, low, close } = ohlc;
    if (i === 0) {
      avg.push({
        open,
        high,
        low,
        close,
      });
    } else {
      let prev = accessor(data[i - 1]);
      let newOpen = roundUp((prev.open + prev.close) / 2, scale);
      let newClose = roundUp((open + high + low + close) / 4, scale);
      let newHigh = Math.max(newOpen, high);
      let newLow = Math.min(newOpen, low);
      avg.push({ open: newOpen, close: newClose, high: newHigh, low: newLow });
    }
  }

  return avg;
}

function roundUp(value, scale) {
  let ex = Math.pow(10, scale);
  let extending = value * ex;
  return Math.round(extending) / ex;
}

export default avg;
