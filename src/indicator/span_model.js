import { max as _max, min as _min, map as _map } from '../util/array';
import { priceFormat } from '../util/format';

function max(array) {
  return _max(
    _map(array, function(a) {
      return a.high;
    })
  );
}

function min(array) {
  return _min(
    _map(array, function(a) {
      return a.low;
    })
  );
}

function span_model(inputsObj = {}) {
  let period = [
    inputsObj.period1,
    inputsObj.period2,
    inputsObj.period3,
  ];
  let accessor;

  function indicator(data) {
    return calc(data, accessor, period);
  }

  indicator.period = function(x) {
    if (x === undefined) {
      return period;
    }
    period = x;
    return indicator;
  };

  indicator.accessor = function(x) {
    if (x === undefined) {
      return accessor;
    }
    accessor = x;
    return indicator;
  };

  indicator.forward = function() {
    return Math.max.apply(null, period) - 1;
  };

  indicator.backward = function() {
    return period[1] || 0;
  };

  indicator.key = function() {
    return `span_model_${period.join('.')}`;
  };

  return indicator;
}

span_model.label = function(config = {}) {
  let { inputsObj } = config;
  let period = [
    inputsObj.period1,
    inputsObj.period2,
    inputsObj.period3,
  ];
  return `(${period})`;
};

span_model.tooltip = function tooltip(record, scale, config = {}) {
  if (record && record.length > 0) {
    const {stylesObj} = config;
    const formatter = priceFormat(scale);
    let late = formatter(record[0]);
    let span1 = formatter(record[1]);
    let span2 = formatter(record[2]);
    return [
      {
        label: 'span1',
        value: span1,
        color: stylesObj.span1.color,
      },
      {
        label: 'span2',
        value: span2,
        color: stylesObj.span2.color,
      },
      {
        label: 'late',
        value: late,
        color: stylesObj.late.color,
      },
    ];
  } else {
    return null;
  }
};

function calc(data, accessor, periods) {
  let [p0, p1, p2] = periods;

  if (!data || data.length === 0) {
    return [];
  }

  var y = null;
  var r = data.map(accessor);
  var p0Array = [];
  var p1Array = [];
  var p2Array = [];
  var result = [];

  for (var i = 0; i < r.length; i++) {
    p0Array.push(r[i]);
    p1Array.push(r[i]);
    p2Array.push(r[i]);

    var late = null;
    var span1 = null;
    var span2 = null;

    if (p0Array.length === p0) {
      var max0 = max(p0Array);
      var min0 = min(p0Array);
      // 転換値
      var trans = (max0 + min0) / 2;

      // late = i + p1 < r.length ? r[i + p1 - 1].close : null
      late = i + p1 - 1 < r.length ? r[i + p1 - 1].close : null;

      if (p1Array.length === p1) {
        var max1 = max(p1Array);
        var min1 = min(p1Array);
        var base = (max1 + min1) / 2;
        span1 = (trans + base) / 2;

        if (p2Array.length === p2) {
          var max2 = max(p2Array);
          var min2 = min(p2Array);
          span2 = (max2 + min2) / 2;
          p2Array.splice(0, 1);
        }

        p1Array.splice(0, 1);
      }

      p0Array.splice(0, 1);
    }

    result.push([late, span1, span2, [span1, span2]]);
  }

  return result;
}

export default span_model;
