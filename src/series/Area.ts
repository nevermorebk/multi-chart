import { IChartContext, ISeries, ColorMetaInfo } from '../lib';
import * as draw from '../canvas/draw';
import { forEach } from '../util/array';

export class Area implements ISeries {
  static defaultColor = 'rgba(255,255,255, 0.8)';
  style: {
    name: string;
    label?: string;
    val: ColorMetaInfo;
  };
  key: string;
  readonly context: IChartContext;
  order = 1;
  constructor(context: IChartContext, key: string, style) {
    this.context = context;
    this.key = key;
    this.style = style || {
      name: 'untitled',
      val: Area.defaultColor
    };
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let { data, xAccessor, yAccessor } = this.context;
    let { chartHeight: height } = this.context;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    if (!data.length) return;
    let line1Points = [];
    let line2Points = [];
    let areaPoints = [];
    forEach(data, d => {
      let x = scaleX(xAccessor(d));
      let item: [number, number] = yAccessor(d) as any;
      if (item) {
        let [y1, y2] = item.map(i => (i !== null ? scaleY(i) : null));
        if (y1) line1Points.push({ x, y: y1 });
        if (y2) line2Points.push({ x, y: y2 });
      }
    });
    areaPoints = line1Points.concat(line2Points.reverse());
    let color = this.style.val;
    draw.drawArea(ctx, areaPoints, { fillStyle: color });
  }
}
