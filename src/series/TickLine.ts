import * as draw from '../canvas/draw';
import { getTheme } from '../config/index';
import { IChartContext, IndicatorLineMetaInfo, ISeries, Side } from '../lib';
import { isNotEmpty } from '../util/is';


export class TickLine implements ISeries {
  key: string;
  readonly context: IChartContext;
  order: 1;
  style: {
    name: string;
    label?: string;
    val: IndicatorLineMetaInfo;
  };
  side: Side = Side.BID;
  constructor(context: IChartContext, key: string) {
    this.context = context;
    this.key = key || 'data';

    this.side = this.context.chartSide;
    this.style = {
      name: 'untitled',
      val: IndicatorLineMetaInfo.DefaultLineStyle
    };
    let theme = getTheme(this.context.theme);
    this.style.val.color = theme.tickLine.color;
    this.style.val.width = theme.tickLine.width;
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let { data, xAccessor, yAccessor } = this.context;
    if (data.length <= 1) return;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let points = data.reduce((result, d) => {
      let item = yAccessor(d) as unknown as number;
      if (isNotEmpty(item)) {
        let x = scaleX(xAccessor(d));
        let y = scaleY(item);
        result.push({ x, y });
      }
      return result;
    }, []);

    if (points.length) {
      let { width: lineWidth, color: strokeStyle } = this.style.val;
      draw.drawLine(ctx, points, { strokeStyle, lineWidth });
    }
  }
}
