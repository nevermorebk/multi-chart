import { IChartContext, ISeries, IndicatorLineMetaInfo, IndicatorDrawStyle } from '../lib';
import { isNotEmpty } from '../util/is';
import * as draw from '../canvas/draw';
import { getTheme } from '../config';

export class Line implements ISeries {
  key: string;
  style: {
    name: string;
    label?: string;
    val: IndicatorLineMetaInfo;
  };
  readonly context: IChartContext;
  order = 2;
  constructor(context: IChartContext, key: string, style) {
    this.context = context;
    this.key = key;
    this.style = style ||  {
      name: 'untitled',
      val: IndicatorLineMetaInfo.DefaultLineStyle
    };
    if (key === 'data') { // candle line style
      this.style.val.color = getTheme(context.theme).chart.fontColor;
    }
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let { data, xAccessor, yAccessor } = this.context;
    if (data.length <= 1) return;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let points = data.reduce((result, d) => {
      let item = yAccessor(d) as any as number;
      if (isNotEmpty(item)) {
        let x = scaleX(xAccessor(d));
        let y = scaleY(item);
        result.push({ x, y });
      }
      return result;
    }, []);
    if (points.length) {
      let {width: lineWidth, color: strokeStyle, style } = this.style.val;
      if (style === IndicatorDrawStyle.POINT) {
        draw.drawCircle2(ctx, points, lineWidth, { fillStyle: strokeStyle });
        // TODO:  other style
      } else { // if (style === LineStyleEnum.LINE)
        draw.drawLine(ctx, points, { strokeStyle, lineWidth });
      }
    }
  }
}
