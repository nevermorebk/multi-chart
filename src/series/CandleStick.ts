import { default as d3ScaleLinear } from 'd3-scale/src/linear';
import { IChartContext, ISeries, Point } from '../lib';
import { getTheme } from '../config/index';
import * as draw from '../canvas/draw';
import { forEach } from '../util/array';
import { isNotEmpty } from '../util/is';

// TODO: multi

export class CandleStick implements ISeries {
  key: string;
  readonly context: IChartContext;
  order: 1;
  constructor(context: IChartContext, key: string) {
    this.context = context;
    this.key = key || 'data';
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let { data, xAccessor, yAccessor } = this.context;
    let { chartHeight: height } = this.context;
    let barWidth = this.context.getBandWidth();
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let theme = getTheme(this.context.theme);
    let style = theme.candle;
    if (style.downColor === style.downFrameColor) style.downFrameColor = null;
    if (style.upColor === style.upFrameColor) style.upFrameColor = null;

    let mappedData = data.reduce((result, d) => {
      let ohlc = yAccessor(d);
      if (isNotEmpty(ohlc)) {
        result.push({ x: xAccessor(d), ...ohlc });
      }
      return result;
    }, []);

    let y1 = d3ScaleLinear()
      .domain(scaleY.domain())
      .range(scaleY.range());
    let upLines = [],
      downLines = [];
    forEach(mappedData, d => {
      let x = scaleX(d.x);
      let line = [
        {
          x,
          y: y1(d.high),
        },
        {
          x,
          y: y1(d.low),
        },
      ];
      d.open < d.close ? upLines.push(line) : downLines.push(line);
    });

    draw.drawLine2(ctx, upLines, draw.getCanvasLineStyle({ color: style.upColor }));
    draw.drawLine2(ctx, downLines, draw.getCanvasLineStyle({ color: style.downColor }));

    if (barWidth > 3 && barWidth < 4) barWidth = 4;
    if (barWidth > 3) {
      let upRects = [],
        downRects = [];
      forEach(mappedData, (d) => {
        let se = [d.open, d.close].sort();
        let left = scaleX(d.x) - barWidth / 2;
        let top = y1(se[1]);
        let width = barWidth;
        let height = Math.max(1, Math.abs(y1(se[0]) - y1(se[1])));
        let rect = { left, top, width, height };
        d.open < d.close ? upRects.push(rect) : downRects.push(rect);
      });
      draw.drawRect2(ctx, upRects, { fillStyle: style.upColor, lineWidth: 0.5, strokeStyle: style.upFrameColor });
      draw.drawRect2(ctx, downRects, { fillStyle: style.downColor, lineWidth: 0.5, strokeStyle: style.downFrameColor });
    }
  }
}
