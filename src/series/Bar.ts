import { default as d3ScaleLinear } from 'd3-scale/src/linear';
import { IChartContext, ISeries, BarMetaInfo, Point } from '../lib';
import { forEach } from '../util/array';
import { isNotEmpty } from '../util/is';
import * as draw from '../canvas/draw';

export class Bar implements ISeries {
  static defaultStyle = {
    positive: 'rgb(224,0,28)',
    negative: 'rgb(0,95,193)',
  };
  style: {
    name: string;
    label?: string;
    val: BarMetaInfo;
  };
  key: string;
  readonly context: IChartContext;
  order = 1;
  constructor(context: IChartContext, key: string, style) {
    this.context = context;
    this.key = key;
    this.style = style || {
      name: 'untitled',
      val: Bar.defaultStyle
    };
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let { data, xAccessor, yAccessor } = this.context;
    let { chartHeight: height } = this.context;
    let barWidth = Math.max(this.context.getBandWidth(), 2);
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let style = this.style;

    let mappedData: Point[] = data.reduce((result, d) => {
      let y = yAccessor(d);
      if (isNotEmpty(y)) {
        result.push({
          x: xAccessor(d),
          y,
        });
      }
      return result;
    }, []);
    let y1 = d3ScaleLinear()
      .domain(scaleY.domain())
      .range(scaleY.range());
    let positiveBars = [];
    let negativeBars = [];
    let positiveStyles = { fillStyle: style.val.positive };
    let negativeStyles = { fillStyle: style.val.negative };

    forEach(mappedData, (d) => {
      // if (barWidth <= 1) return;
      if (d.y === null || d.y === undefined) return;
      let startX = scaleX(d.x);
      let data = d.y > 0 ? [0, d.y] : [d.y, 0];
      let height = Math.max(1, Math.abs(y1(data[0]) - y1(data[1])));
      let rect = {
        left: startX - barWidth / 2,
        top: y1(data[1]),
        width: barWidth,
        height,
      };
      if (d.y > 0) {
        positiveBars.push(rect);
      } else {
        negativeBars.push(rect);
      }
    });

    if (positiveBars.length) {
      draw.drawRect2(ctx, positiveBars, positiveStyles);
    }
    if (negativeBars.length) {
      draw.drawRect2(ctx, negativeBars, negativeStyles);
    }
  }
}
