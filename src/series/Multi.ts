import { DomainValue, IAccessor, DataItem, IChartContext, ISeries, ISeriesStyle, SeriesConstructor } from '../lib';
import { isEmptyValue, forEach } from '../util';

export class Multi implements ISeries {
  key: string;
  style: ISeriesStyle[];
  readonly context: IChartContext;
  seriesList: SeriesConstructor[];
  order: 1.5;
  constructor(context: IChartContext, key: string, style?: ISeriesStyle[]) {
    this.context = context;
    this.key = key;
    this.style = style || [];
    this.seriesList = [];
  }

  draw(ctx: Context2D): void {
    // TODO: perf
    let sorted = this.seriesList.map<ISeries>(this._createSeries.bind(this)).sort((s1: ISeries, s2: ISeries) => s1.order - s2.order);
    forEach(sorted, (series: ISeries) => series.draw(ctx));
  }

  _createSeries(Series: SeriesConstructor, index: number) {
    let { data, yAccessor } = this.context;
    let style = (this.style || [])[index];
    let key = `${this.key}_${index}`;
    let seriesYAccessor = this._getIndexAccessor(yAccessor, index);
    let newContext = { yAccessor: seriesYAccessor };
    let context = Object.setPrototypeOf(newContext, this.context) as IChartContext;
    return new Series(context, key, style);
  }

  _getIndexAccessor(yAccessor, index): IAccessor<DataItem, DomainValue> {
    return d => {
      let y = yAccessor(d);
      return y ? y[index] : null;
    };
  }

  series(series: SeriesConstructor[]): ISeries {
    this.seriesList = series;
    return this;
  }
}
