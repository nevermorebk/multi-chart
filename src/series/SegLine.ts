import { DataItem, IChartContext, ISeries, ISeriesStyle, IndicatorLineMetaInfo, IndicatorDrawStyle } from '../lib';
import { forEach } from '../util/array';
import { isNotEmpty } from '../util/is';
import * as draw from '../canvas/draw';
import { getI18n } from '../config';

export class SegLine implements ISeries {
  key: string;
  style: {
    name: string;
    label?: string;
    val: IndicatorLineMetaInfo;
  };
  readonly context: IChartContext;
  order = 2;
  constructor(context: IChartContext, key: string, style) {
    this.context = context;
    this.key = key;
    this.style = style || {
      name: 'untitled',
      val: IndicatorLineMetaInfo.DefaultLineStyle
    };
  }

  draw(ctx: Context2D): void {
    let { getScaleX, getScaleY } = this.context;
    let t = getI18n(this.context.lang).prefix(`indicators.${this.style.label}.styles`);
    let { data, xAccessor, yAccessor } = this.context;
    if (data.length <= 1) return;
    let scaleX = getScaleX();
    let scaleY = getScaleY();
    let points = data.reduce((result, d) => {
      let val = yAccessor(d) as any as number;
      if(isNotEmpty(val)) {
        let x = scaleX(xAccessor(d));
        let y = scaleY(val);
        result.push({
          x, y, val
        });
      }
      return result;
    }, []);
    if (points.length > 1 ) {
      let lines = reduceByValue(points, ({val}) => val, (item, preItem) => ({
        x: item.x, y: preItem.y, val: preItem.val
      }));
      let {width: lineWidth, color: strokeStyle } = this.style.val;
      forEach(lines, (linePoints) => {
        let leftPoint = linePoints[0];
        draw.drawLine(ctx, linePoints, { strokeStyle, lineWidth });
        draw.drawText(ctx, t(this.style.name), leftPoint.x, leftPoint.y, {
          fillStyle: strokeStyle,
          textAlign: 'right'
        });
      });
    }
  }
}

function reduceByValue<T>(array: T[], valFn: Function, mapFn: (item: T, preItem: T) => any): any[] {
  let result = [];
  let group;
  result.push((group = []));
  forEach(array, (item, i) => {
    let preItem = i > 0 ? array[i - 1] : undefined;
    if (preItem && valFn(preItem) !== valFn(item)) {
      group.push(mapFn(item, preItem));
      result.push((group = [ item ]));
    } else {
      group.push(item);
    }
  });
  return result;
}
