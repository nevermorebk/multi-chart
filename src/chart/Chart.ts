// tslint:disable:max-file-line-count

import { CursorStyle, IComponent, IChartContext, IBaseContext, IArea, Layout, SeriesParams, DrawOpt, ChartType, Scale } from '../lib';
import { priceAccessor, mergedToStandard, dataAccessor, getDataItemIndexByX, last, niceExtent, mergedToStandard2 } from '../util/data';
import { xAccessor, defaultYAccessor, cutInDomain, calcYDomain, isTouchEvent, first, debounce } from '../util';
import { default as d3ScaleLinear } from 'd3-scale/src/linear';
import { Point, DragPoint, ZoomPoint, PinchPoint, ContextState } from '../lib';
import { ScaleOpts, Size } from '../lib';
import { DataItem } from '../lib';
import { dom, optimizeRenderProcess, isNotEmpty, isDefined, isValidNumber } from '../util';
import EventCapturer from '../event/EventCapturer';
import { SeriesArea, YArea, Area } from '../area';
import Canvas from '../canvas/Canvas';
import { uuidGenerator, uuid } from '../util/index';
import { Feature } from '../feature/Feature';
import { DomFeature } from '../feature/DomFeature';
import { Crosshair } from '../feature';
import { getTheme } from '../config/index';
import { forEach } from '../util/array';
import * as indicator from '../indicator';
import * as Series from '../series/index';
import { IndicatorProps } from '../model/Template';
import { SwitchChart, IndicatorLabel } from '../feature';
import { SegLine } from '../series/SegLine';
import { isTouchDevice } from '../util';

const redrawTimestamp = uuidGenerator(Date.now());

export type ChartProps = {
  // minTickSpace: number;
  yWidth: number;
  chartHeight: number;
  indicators: IndicatorProps[];
};

export type ChartPartialProps = Partial<ChartProps>;

export default class Chart implements IComponent {
  public context: IChartContext;
  public chartId: number;
  public container: HTMLElement;
  public delegateContainer: HTMLElement;
  public chartPanel: HTMLElement;
  public canvas: Canvas;
  public overlayCanvas: Canvas;
  public areas: Area[];
  public mainArea: Area;
  public isMainChart: boolean;
  public isTimelineChart: boolean;
  public supportResize: boolean;
  public minHeight: number;
  protected features: Feature[];
  protected domFeatures: DomFeature[];
  protected props: ChartProps;
  protected eventSource: EventCapturer;
  protected _scaleY: Scale;
  protected _flipY: boolean;
  protected size: Size;
  protected _indicatorProps: IndicatorProps[];
  protected _series: SeriesParams[];
  protected _data: DataItem[];
  protected _refData: DataItem[];
  protected _firstX: number;
  private _drawed!: boolean;

  constructor(baseContext: IBaseContext, props?: ChartPartialProps) {
    this.chartId = uuid();
    this.props = Object.assign({}, props) as ChartProps;
    this.size = this.getSize(baseContext);
    this.canvas = new Canvas(baseContext, this.size);
    this.overlayCanvas = new Canvas(baseContext, this.size, true);
    this.minHeight = 50;
    this.supportResize = !isTouchDevice();
    this.isTimelineChart = false;
    this.isMainChart = false;

    this.redraw = optimizeRenderProcess(this.redraw, this);
    this.context = this._initContext(baseContext);
  }

  initialize() {
    this._initFeatures();
    this._initArea();
    this.layout();
    this._initContainer();
    this._initDomFeatures();
    this._initEventListeners();
  }

  _initFeatures() {
    this.features = [new Crosshair(this.context)];
  }

  _initDomFeatures() {
    let panel = this.chartPanel;
    this.domFeatures = [new SwitchChart(this.context, panel), new IndicatorLabel(this.context, panel)];
  }

  _initEventListeners() {
    let delegateContainer = this.delegateContainer;
    let eventSource = new EventCapturer(this.context, delegateContainer);
    eventSource.on('mousedown', this.onMouseDown.bind(this));
    eventSource.on('mousemove', this.onMouseMove.bind(this));
    eventSource.on('mouseup', this.onMouseUp.bind(this));
    eventSource.on('mouseenter', this.onMouseEnter.bind(this));
    eventSource.on('mouseleave', this.onMouseLeave.bind(this));
    eventSource.on('keyup', this.onKeyUp.bind(this));
    eventSource.on('dragstart', this.onDragStart.bind(this));
    eventSource.on('drag', this.onDrag.bind(this));
    eventSource.on('dragcancel', this.onDragCancel.bind(this));
    eventSource.on('dragend', this.onDragEnd.bind(this));
    eventSource.on('dblclick', this.onDblClick.bind(this));
    eventSource.on('click', this.onClick.bind(this));
    eventSource.on('wheel', this.onWheel.bind(this));
    eventSource.on('pinch', this.onPinch.bind(this));
    this.eventSource = eventSource;
  }

  _initArea() {
    let areas = [];
    let context = this.context;
    let features = this.features;
    let seriesArea = new SeriesArea(context, { features }, Layout.FILL);
    let yArea = new YArea(context, { features }, Layout.RIGHT);

    areas.push(seriesArea);
    areas.push(yArea);
    this.mainArea = seriesArea;
    this.areas = areas;
  }

  layout() {
    let areas = this.areas;
    let context = this.context;
    areas = areas.sort((a, b) => a.layout - b.layout);
    let {
      chartWidth,
      chartHeight,
      sizeOption: { yWidth, xHeight },
    } = context;

    let top = areas.find(a => a.layout === Layout.TOP) ? xHeight : 0;
    let left = areas.find(a => a.layout === Layout.LEFT) ? yWidth : 0;
    let bottom = areas.find(a => a.layout === Layout.BOTTOM) ? xHeight : 0;
    let right = areas.find(a => a.layout === Layout.RIGHT) ? yWidth : 0;

    forEach(areas, area => {
      let rect = null;
      switch (area.layout) {
        case Layout.TOP: {
          // to check
          rect = { left: left, top: 0, width: chartWidth + yWidth, height: xHeight };
          break;
        }
        case Layout.BOTTOM: {
          // to check
          rect = { left: left, top: chartHeight - bottom, width: chartWidth + yWidth, height: xHeight };
          break;
        }
        case Layout.LEFT: {
          rect = { left: 0, top: top, width: yWidth, height: chartHeight - top - bottom };
          break;
        }
        case Layout.RIGHT: {
          rect = { left: chartWidth + left, top: top, width: yWidth, height: chartHeight - top - bottom };
          break;
        }
        case Layout.FILL: {
          rect = { left, top, width: chartWidth + yWidth - right, height: chartHeight - top - bottom };
          break;
        }
      }
      if (rect) area.setRect(rect);
    });
  }

  shouldIgnoreEvent(): boolean {
    let { loading } = this.context.state;
    return !!loading;
  }

  getRect() {
    let { chartWidth, chartHeight } = this.context;
    return {
      left: 0,
      top: 0,
      width: chartWidth,
      height: chartHeight,
    };
  }

  _initContainer() {
    let size = this.getSize();
    let container = dom
      .create('div')
      .css({
        position: 'relative',
        display: 'block',
        overflow: 'hidden',
        width: `${size.width}px`,
        height: `${size.height}px`,
      })
      .node();
    let delegateContainer = dom
      .create('div')
      .appendTo(container)
      .node();

    delegateContainer.appendChild(this.canvas.element);
    delegateContainer.appendChild(this.overlayCanvas.element);

    if (!this.isTimelineChart) {
      let width = this.context.chartWidth;
      let theme = getTheme(this.context.theme);
      this.chartPanel = dom
        .create('div')
        .css({
          position: 'absolute',
          'use-select': 'none',
          left: 0,
          top: 0,
          right: 0,
          width: `${width}px`,
          height: 0,
        })
        .addClass('multi-chart-panel')
        .appendTo(container)
        .node();
    }

    this.container = container;
    this.delegateContainer = delegateContainer;
  }

  _updateContainer(size: Size) {
    let container = dom(this.container);
    container.css({
      width: `${size.width}px`,
      height: `${size.height}px`,
    });
    if (!this.isTimelineChart) {
      let panel = dom(this.chartPanel);
      let theme = getTheme(this.context.theme);
      if (panel) {
        let width = this.context.chartWidth;
        panel.css({
          width: `${width}px`,
        });
      }
    }
  }

  _initContext(baseContext: IBaseContext): IChartContext {
    let that = this;
    let chartId = this.chartId;
    let { chartHeight } = this.props;
    let canvas = this.canvas;
    let overlayCanvas = this.overlayCanvas;
    let timestamp = redrawTimestamp();
    let updateTimestamp = () => (this.context.timestamp = redrawTimestamp());
    // TODO 可标记对象属性状态和依赖
    const context: any = {
      timestamp,
      // yTicks,
      chartId,
      // container,
      canvas,
      overlayCanvas,
      chartHeight, // to size
      yAccessor: defaultYAccessor,
      updateTimestamp: updateTimestamp,
      getScaleY: this.getScaleY.bind(this),
      updateScaleY: this.updateScaleY.bind(this),
      flipY: this.flipY.bind(this),
      rezoom: this.rezoom.bind(this),
      get precisionScale() {
        return that._calcPrecisionScale(baseContext);
      },
      get series() {
        return that.getSeries();
      },
      get container() {
        return that.container;
      },
      //
    };
    Object.setPrototypeOf(context, baseContext);
    if (__DEV__) {
      if (Object.seal) Object.seal(context);
    }
    return context as IChartContext;
  }

  _updateContext() {
    this.context.chartHeight = this.props.chartHeight;
  }

  // rm to util
  _calcPrecisionScale(baseContext: IBaseContext) {
    let precisionScale = 3;
    if (this.isMainChart) {
      return baseContext.symbol.scale;
    }
    let { indicators } = this.props;
    if (indicators) {
      let first = indicators[0];
      if (first && first.config) precisionScale = first.config.inputsObj.scale || precisionScale;
    } else {
      precisionScale = baseContext.symbol.scale;
    }
    return precisionScale;
  }

  _ifNeedRedraw() {
    const allData = this.context.allData;
    const lastIndex = allData.length - 1;
    const scaleX = this.context.getScaleX();
    const [left, right] = niceExtent(scaleX.domain());
    let backwards = this.context.template.backwards;
    let x = this.context.xAccessor(allData[lastIndex]);
    if (x >= left && x <= right) return true;
    if (x > right) {
      for (let i = backwards.length - 1; i >= 0; i--) {
        let backward = backwards[i];
        let item = allData[lastIndex - backward];
        x = this.context.xAccessor(item);
        if (x > left && x < right) return true;
      }
    }
    return false;
  }

  getSeries() {
    const { data, allData, chartDataModel, xAccessor } = this.context;
    const indicators = this.props.indicators;
    const firstX = xAccessor(first(allData));
    let changed = indicators !== this._indicatorProps || data.length < 2 || firstX !== this._firstX;
    if (!changed) {
      changed = this._refData !== chartDataModel.refData;
    }
    if (!changed) {
      if (xAccessor(first(data)) === xAccessor(first(this._data)) && data !== this._data) {
        changed = this._ifNeedRedraw();
      } else changed = data !== this._data;
    }
    if (changed) {
      // update
      this._indicatorProps = indicators;
      this._data = data;
      this._refData = chartDataModel.refData;
      this._series = this._mergeIndicatorsData(data, indicators);
      this._firstX = firstX;
    }
    return this._series;
  }

  // TODO: mv to init module / file
  _mergeIndicatorsData(data: DataItem[], indicators: IndicatorProps[]): SeriesParams[] {
    const allData = this.context.allData;
    const viewportData = data;
    let series = [];
    let { Line, Area, Bar } = Series;
    if (!allData.length) return series;
    let xdomain = this.context.getScaleX().domain();
    for (let i = 0; i < indicators.length; i++) {
      let config = indicators[i].config;
      let id = indicators[i].id;
      let name = config.name;
      let inputsObj = config.inputsObj;
      let seriesType;
      let indicatorF;
      switch (name) {
        case 'ema': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'sma': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'wma': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'macd': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line, Line, Bar];
          break;
        }
        case 'ikh': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line, Line, Line, Line, Line, Area];
          break;
        }
        case 'ma': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'bollinger_band': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line, Line, Line];
          break;
        }
        case 'pivot_point': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [SegLine, SegLine, SegLine, SegLine, SegLine, SegLine, SegLine, SegLine, SegLine];
          break;
        }
        case 'envelope': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line, Line, Line];
          break;
        }
        case 'parabolic_sar': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line, Line];
          break;
        }
        case 'span_model': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line, Line, Line, Area];
          break;
        }
        case 'super_bollinger': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line, Line, Line, Line, Line, Line, Line, Line];
          break;
        }
        case 'rsi': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'rci': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'kdj': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line, Line, Line];
          break;
        }
        case 'hv': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'momentum': {
          indicatorF = indicator[name](inputsObj).accessor(priceAccessor);
          seriesType = [Line];
          break;
        }
        case 'atr': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line];
          break;
        }
        case 'dmi': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line, Line, Line];
          break;
        }
        case 'adx': {
          indicatorF = indicator[name](inputsObj).accessor(dataAccessor);
          seriesType = [Line];
          break;
        }
        default: {
          throw new TypeError(`indicator type: ${name} is not supported!`);
        }
      }

      let startX, endX, fromIndex, toIndex;
      let indicatorData, dataKey, mergeFromX, seriesAccessor;
      let forward = indicatorF.forward();
      let backward = indicatorF.backward ? indicatorF.backward() : 0;
      [startX, endX] = niceExtent(xdomain);
      if (!viewportData.length) {
        if (backward === 0) continue;
        if (xAccessor(last(allData)) + backward < startX) continue;
      }

      let firstX = this.context.xAccessor(allData[0]);
      if (startX < firstX) startX = firstX;
      if (startX - forward < firstX) forward = startX - firstX;
      startX -= forward;
      endX += backward;

      [fromIndex, toIndex] = [startX, endX].map(x => getDataItemIndexByX(allData, x));
      // ppline calc ...
      if (name === 'pivot_point') {
        indicatorData = indicatorF(this.context.chartDataModel.refData);
      } else {
        indicatorData = indicatorF(allData.slice(fromIndex, toIndex + 1));
        indicatorData = indicatorData.slice(forward); // backward
      }
      if (!indicatorData.length) continue;

      dataKey = indicatorF.key ? indicatorF.key() : name;
      if (name === 'pivot_point') {
        let refData = this.context.chartDataModel.refData;
        let refChartType = this.context.chartDataModel.refCharType;

        if (this.context.chartType >= refChartType) {
          refData = [];
        }
        mergedToStandard(refData, dataKey, indicatorData, 0, this.context);
        let refDataLength = inputsObj.refDataLength || 0;
        seriesAccessor = mergedToStandard2(viewportData, dataKey, refData.slice(-refDataLength));
      } else {
        seriesAccessor = mergedToStandard(viewportData, dataKey, indicatorData, startX + forward, this.context);
      }
      series.push({
        id,
        config,
        name,
        dataKey,
        seriesType,
        accessor: seriesAccessor,
      });
    }

    return series;
  }

  getSize(context?: IBaseContext) {
    context = context || this.context;

    let {
      chartWidth,
      sizeOption: { width, yWidth },
    } = context;
    let { chartHeight } = this.props;
    return {
      width: chartWidth + yWidth,
      height: chartHeight,
      yWidth,
    };
  }

  // scaleY

  getScaleY() {
    let scale = this._scaleY;
    if (!scale) {
      let { chartHeight, theme } = this.context;
      let margin = getTheme(theme).chart.seriesMargin;
      let [top, , bottom] = margin;
      scale = this._scaleY = (d3ScaleLinear() as any) as Scale;
      scale.domain([0, 0]);
      scale.range([chartHeight - bottom, 0 + top]);
    }
    return scale;
  }

  updateScaleY(newScale: any) {
    this._scaleY = newScale;
  }

  _updateScaleYRange(chartHeight) {
    if (this.isTimelineChart) return;
    let scaleY = this.getScaleY();
    let { theme } = this.context;
    let margin = getTheme(theme).chart.seriesMargin;
    let [top, , bottom] = margin;
    scaleY.range([chartHeight - bottom, 0 + top]);
  }

  _updateScaleYDomain(isViewport = true) {
    if (this.isTimelineChart) return;
    let series = this.getSeries();
    let { data, getScaleX } = this.context;
    let found = data;
    if (isViewport) {
      let domain = getScaleX().domain();
      found = cutInDomain(data, domain, xAccessor);
      if (found.length <= 1) found = data;
    }
    if (series) {
      let keys = series.map<string>(s => {
        if (s) return s.dataKey || s.name;
        return undefined;
      });
      let isSymmetrical = false;
      // TODO (2018/03/20 10:13) maybe has other indicator line has symmetric param
      let macdSeries: SeriesParams = series.find(s => s.name === 'macd');
      if (macdSeries) {
        let inputsObj = macdSeries.config.inputsObj;
        isSymmetrical = !!inputsObj.symmetric;
      }
      let newDomain = calcYDomain(found, keys, isSymmetrical);
      if (newDomain[0] === newDomain[1] && newDomain[0] > 0) {
        // for  tiny domain
        let minStep = 2 / Math.pow(10, this.context.precisionScale);
        newDomain[0] = newDomain[0] - minStep;
        newDomain[1] = newDomain[1] + minStep;
      }
      if (isValidNumber(newDomain[0]) && isValidNumber(newDomain[1])) {
        this.getScaleY().domain(newDomain);
      }
    }
  }

  flipY(flag?: boolean) {
    if (!isDefined(flag)) return this._flipY;
    if (this.isMainChart && !!this._flipY !== !!flag) this.context.trigger('mainYAxisLocked', !flag);
    this._flipY = flag;
  }

  // base implements

  attachTo(parent: HTMLElement, before?: HTMLElement) {
    let container = this.container;
    if (before) {
      parent.insertBefore(container, before);
    } else {
      parent.appendChild(container);
    }
    if (!this._drawed) {
      this.draw();
      this._drawed = true;
    } else {
      this.redraw(DrawOpt.all);
    }
    return this;
  }

  draw() {
    this.context.updateTimestamp();
    if (!this._flipY) {
      // TODO MV
      this._updateScaleYDomain();
    }
    forEach(this.areas, area => {
      area.draw();
    });

    forEach(this.domFeatures, (feature: DomFeature) => {
      if (feature.shouldDisplay()) feature.draw();
    });
  }

  update(props: ChartPartialProps = {}): this {
    forEach(Object.keys(props), key => {
      if (isDefined(props[key])) {
        this.props[key] = props[key];
      }
    });

    if (isNotEmpty(props.chartHeight)) {
      this._updateScaleYRange(props.chartHeight);
    }
    this._updateContext();

    // TODO: resize check
    let size = this.getSize();
    if (size.height !== this.size.height || size.width !== this.size.width || size.yWidth !== this.size.yWidth) {
      this._updateContainer(size);
      this.size = size;
      this.layout();
    }
    // resize or dpr changed
    this.canvas.resize(size);
    this.overlayCanvas.resize(size);
    return this;
  }

  redraw(drawOpt: DrawOpt) {
    if (this.context === null) return; // destroyed
    if (this.context.chartHeight === 0) return;
    this.context.updateTimestamp();
    // console.debug('redraw Chart', this.chartId);
    if (!this._flipY) {
      // TODO MV
      this._updateScaleYDomain();
    }
    forEach(this.areas, area => {
      area.redraw(drawOpt);
    });

    forEach(this.domFeatures, (feature: DomFeature) => {
      feature.redraw(drawOpt);
    });
  }

  destroy() {
    if (this.eventSource) {
      this.eventSource
        .off('click')
        .off('mousedown')
        .off('mousemove')
        .off('mouseup')
        .off('mouseleave')
        .off('dblclick')
        .off('dragstart')
        .off('drag')
        .off('dragend')
        .off('wheel')
        .off('pinch');

      this.eventSource.destroy();
    }
    forEach(this.areas, area => area.destroy());
    forEach(this.domFeatures, feature => feature.destroy());
    this.canvas.destroy();
    this.overlayCanvas.destroy();
    dom(this.chartPanel).remove();
    dom(this.container).remove();
    this.areas = [];
    this.domFeatures = [];
    this.context = null;
  }

  // update view when drag / wheel / pinch
  rezoom(transform: any, scale: ScaleOpts): boolean {
    let { updateScaleX, updateScaleY, updateZoom } = this.context;
    if (scale.scaleX) {
      let newScaleX = transform.rescaleX(scale.scaleX);
      // limit drag to left
      let lastItem = last(this.context.allData);
      if (lastItem && transform.x < 0) {
        let scaleX = newScaleX;
        let [, width] = scaleX.range();
        let lastX = scaleX(xAccessor(lastItem));
        if (lastX < width / 3) return false;
      }
      updateScaleX(newScaleX);
    }
    if (scale.scaleY && this.flipY()) {
      let newScaleY = transform.rescaleY(scale.scaleY);
      updateScaleY(newScaleY);
    }
    if (transform.k !== 1 && transform.x !== 0) {
      updateZoom(transform.k);
    }

    this.context.redraw();
    return true;
  }

  _findContainedAreas(point: Point): IArea[] {
    let areas = this.areas;
    let found = areas.filter(area => area.contains(point));
    return found.length ? found : null;
  }

  isMinHeight() {
    let {
      minHeight,
      context: { chartHeight },
    } = this;
    return chartHeight - minHeight < 1;
  }

  hasIndicator(indicatorId: number): boolean {
    return !!this.props.indicators.find(prop => prop.id === indicatorId);
  }

  // event handlers
  onKeyUp(event: KeyboardEvent) {
    //
    forEach(this.areas, area => area.onKeyUp(event));
  }

  onClick(point: Point) {
    let found = this._findContainedAreas(point);
    found &&
      forEach(found, area => {
        let xy = area.getRelationPoint(point);
        area.onClick(xy);
      });
  }

  onMouseDown(point: Point, event) {
    let found = this._findContainedAreas(point);
    found &&
      forEach(found, area => {
        let xy = area.getRelationPoint(point);
        area.onMouseDown(xy, event);
      });
  }

  onMouseMove(point: Point, event) {
    if (this.shouldIgnoreEvent()) return;
    let context = this.context;
    if (context.state.dragging) return;

    let found = this._findContainedAreas(point);
    if (found) {
      forEach(found, area => {
        let xy = area.getRelationPoint(point);
        area.onMouseMove(xy, event);
      });
    }

    let chart = this as any;
    if (this.supportResize) {
      // todo: 封装到tool？
      if (context.state.resizing) return;
      let rect = this.getRect();
      if (point.y < 8 || rect.height - point.y < 8) {
        let origin = point.y < 8 ? 'top' : 'bottom';
        if (context.canResizeChart(chart, origin)) {
          let relatedChart = origin === 'top' ? context.getPrevChart(this.chartId) : context.getNextChart(this.chartId);
          chart._resizeOrigin = origin;
          chart._resizeOriginHeight = this.size.height;
          chart._resizeRelatedOriginHeight = relatedChart && relatedChart.size.height;
          chart._resizeRelatedChart = relatedChart;
          this.context.setCursor(CursorStyle.ROW_RESIZE);
        }
      } else {
        chart._resizeOrigin = null;
        // this.context.setCursor(CursorStyle.DEFAULT);
      }
    }
  }

  onMouseUp(point: Point, event) {
    let found = this._findContainedAreas(point);
    if (found) {
      forEach(found, area => {
        let xy = area.getRelationPoint(point);
        area.onMouseUp(xy, event);
      });
    }
  }

  onMouseEnter() { }

  onMouseLeave() {
    let context = this.context;
    // context.mouse(null);
    let chart = this as any;
    if (!context.state.resizing) {
      this.context.setCursor(CursorStyle.DEFAULT);
      chart._resizeOrigin = null;
    }
  }

  onDragStart(point: DragPoint, event: MouseEvent | TouchEvent) {
    let chart = this as any;
    let context = this.context;
    // context.mouse(null);

    if (chart._resizeOrigin) {
      context.state.resizing = true;
      context.state.dragging = true;
      return;
    }

    let found = this._findContainedAreas(point);
    chart._dragAreas = found;
    if (found) {
      if (isTouchEvent(event)) context.state.touching = ContextState.Touching.START;
      context.state.dragging = true;
      forEach(found, area => {
        let xy = area.getRelationPoint(point);
        area.onDragStart(xy);
      });
    }
  }

  onDrag(point: DragPoint, event: MouseEvent | TouchEvent) {
    if (this.shouldIgnoreEvent()) return;
    let chart = this as any;
    let context = this.context;
    // context.mouse(null);
    if (context.state.resizing) {
      if (context.canResizeChart(chart, chart._resizeOrigin, point)) {
        context.resizeChart(this, point);
      }
      context.mouse(null);
      return;
    }
    if (isTouchEvent(event)) context.state.touching = ContextState.Touching.MOVE;
    let found: Area[] = chart._dragAreas;
    if (found) {
      forEach(found, area => {
        let xy = area.getRelationPoint(point) as DragPoint;
        area.onDrag(xy);
      });
      context.trigger('pan', point);
    }
  }

  onDragCancel(point: DragPoint, event: MouseEvent | TouchEvent) {
    let chart = this as any;
    let context = this.context;

    if (isTouchEvent(event)) context.state.touching = ContextState.Touching.END;
    context.state.dragging = false;
    if (context.state.resizing) {
      chart._resizeOrigin = null;
      context.state.resizing = false;
      context.setCursor(CursorStyle.DEFAULT);
      context.resizeChart(this);
      return;
    }

    let found: Area[] = chart._dragAreas;
    chart._dragAreas = undefined;
    if (found) {
      forEach(found, area => area.onDragCancel(point));
    }
  }

  onDragEnd(point: DragPoint, event: MouseEvent | TouchEvent) {
    let chart = this as any;
    let context = this.context;

    if (isTouchEvent(event)) context.state.touching = ContextState.Touching.END;
    context.state.dragging = false;
    if (context.state.resizing) {
      chart._resizeOrigin = null;
      context.state.resizing = false;
      context.setCursor(CursorStyle.DEFAULT);
      context.resizeChart(this);
      return;
    }

    let found: Area[] = chart._dragAreas;
    chart._dragAreas = undefined;
    if (found) {
      forEach(found, area => area.onDragEnd(point));
      context.trigger('panend', point);
    }
  }

  onDblClick(point: Point) {
    let found = this._findContainedAreas(point);
    found &&
      forEach(found, area => {
        let xy = area.getRelationPoint(point);
        area.onDblClick(xy);
      });
  }

  onWheel(point: ZoomPoint) {
    let context = this.context;
    if (this.shouldIgnoreEvent()) return;
    context.state.wheeling = true;
    let found = this._findContainedAreas(point);
    if (found) {
      forEach(found, area => area.onWheel(point));
      context.trigger('wheel');
      this._onWheelEnd(point);
    }
  }

  onPinch(point: PinchPoint) {
    if (this.shouldIgnoreEvent()) return;
    let found = this._findContainedAreas(point);
    found && forEach(found, area => area.onPinch(point));
  }

  _onWheelEnd = debounce((point: ZoomPoint) => {
    this.context.state.wheeling = false;
    this.context.trigger('wheelend', point);
  }, 300);
}
