import Chart from './Chart';
import { IBaseContext, IArea, Layout } from '../lib';
import { ChartPartialProps } from './Chart';
import { XArea, EmptyArea } from '../area';
import { Crosshair } from '../feature/Crosshair';
import { ShapeRange } from '../feature/ShapeRange';

export default class TimelineChart extends Chart {
  constructor(baseContext: IBaseContext, props?: ChartPartialProps) {
    super(baseContext, props);
    this.supportResize = false;
    this.isTimelineChart = true;
  }

  _initArea() {
    let areas = [];
    let context = this.context;
    let { sizeOption: { yWidth, xHeight } } = context;
    let features = this.features;
    let xArea = new XArea(context, { features }, Layout.FILL);
    // let emptyArea = new EmptyArea(context, Layout.RIGHT);
    // areas.push(emptyArea);
    areas.push(xArea);
    this.mainArea = null;
    this.areas = areas;
  }

  _initFeatures() {
    this.features = [new ShapeRange(this.context), new Crosshair(this.context)];
  }

  _initDomFeatures() {
    this.domFeatures = [];
  }

  getSeries() {
    return [];
  }
}
