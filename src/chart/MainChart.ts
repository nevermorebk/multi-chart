import Chart from './Chart';
import { IBaseContext, Layout } from '../lib';
import { ChartType, ChartStyle, DataItem } from '../lib';
import { ChartPartialProps } from './Chart';
import { CurrentQuote, OrderLines, PositionSummaryLines } from '../feature';
import YArea from '../area/YArea';
import MainArea from '../area/MainArea';
import { Line, CandleStick, TickLine } from '../series/index';
import { mergedToStandard, priceAccessor, dataAccessor } from '../util/data';
import * as indicator from '../indicator';
import { CrosshairOHLC } from '../feature/CrosshairOHLC';
import { ShapeRange } from '../feature/ShapeRange';

export default class MainChart extends Chart {
  public mainArea: MainArea;
  private _chartType: ChartType;
  private _chartStyle: string;
  private _candleSeries: any;

  constructor(baseContext: IBaseContext, props?: ChartPartialProps) {
    super(baseContext, props);
    this.isMainChart = true;
    this.minHeight = 200;
  }

  _initArea() {
    let areas = [];
    let context = this.context;
    let features = this.features;
    let seriesArea = new MainArea(context, { features }, Layout.FILL);
    let yArea = new YArea(context, { features }, Layout.RIGHT);
    areas.push(seriesArea);
    areas.push(yArea);
    this.mainArea = seriesArea;
    this.areas = areas;
  }

  _initFeatures() {
    super._initFeatures();
    let features = this.features;
    this.features = [
      new ShapeRange(this.context),
      new OrderLines(this.context),
      new PositionSummaryLines(this.context),
      new CurrentQuote(this.context),
      ...features,
    ];
  }

  _initDomFeatures() {
    let panel = this.chartPanel;
    super._initDomFeatures();
    let domFeatures = this.domFeatures;
    this.domFeatures = [new CrosshairOHLC(this.context, panel), ...domFeatures];
  }

  getSeries() {
    let { data, chartType, chartStyle } = this.context;
    if (data !== this._data || chartType !== this._chartType || chartStyle !== this._chartStyle) {
      // this._data = data; // replace in Chart.ts!
      this._chartType = chartType;
      this._chartStyle = chartStyle;
      this._candleSeries = this._getDefaultCandleSeries(data);
    }
    let candleSeries = this._candleSeries;
    let series = super.getSeries();

    if (chartType === ChartType.TICK) {
      return [candleSeries];
    }
    return [candleSeries, ...series];
  }

  _getDefaultCandleSeries(viewportData: DataItem[]) {
    let candleConfig = {
      seriesType: [],
      accessor: null,
      name: 'data',
      config: {},
    };
    let { chartType, chartStyle, precisionScale: scale } = this.context;
    if (chartType === ChartType.TICK) {
      candleConfig.accessor = priceAccessor;
      candleConfig.seriesType = [TickLine];
    } else {
      if (chartStyle === ChartStyle.Line) {
        candleConfig.seriesType = [Line];
        candleConfig.accessor = priceAccessor;
      } else if (chartStyle === ChartStyle.Avg) {
        let avg = indicator
          .avg()
          .accessor(dataAccessor)
          .scale(scale);
        let filteredViewportData = viewportData.filter(item => !!item.data);
        let startX = this.context.xAccessor(viewportData[0]);
        let avgDataAccessor = mergedToStandard(viewportData, ChartStyle.Avg, avg(filteredViewportData), startX, this.context);
        candleConfig.seriesType = [CandleStick];
        candleConfig.accessor = avgDataAccessor;
      } else if (chartStyle === ChartStyle.Candle) {
        candleConfig.seriesType = [CandleStick];
        candleConfig.accessor = dataAccessor;
      }
    }
    return candleConfig;
  }
}
