import { Size, IBaseContext } from '../lib';
import { dom } from '../util';
import { createContext2D } from './CanvasRenderingContext2D';
export default class Canvas {
  private context: IBaseContext;
  dpr: number;
  ctx: Context2D;
  size: Size;
  element: HTMLCanvasElement;
  public isOverlay: boolean;
  constructor(context: IBaseContext, size: Size, isOverlay = false) {
    this.context = context;
    this.element = this._create(size);
    this.ctx = createContext2D(this.element.getContext('2d'), this.context.dpr);
    this.resize(size);
    this.isOverlay = isOverlay;
  }

  resize(size: Size) {
    let dpr = this.context.dpr;
    if (this.size) {
      let isSameSize = size.width === this.size.width && size.height === this.size.height;
      if (dpr === this.dpr && isSameSize) return;
    }
    dom(this.element).css({
      width: `${size.width}px`,
      height: `${size.height}px`,
    });
    this.ctx.setDPR(dpr);
    this.element.width = size.width * dpr;
    this.element.height = size.height * dpr;
    this.size = size;
    this.dpr = dpr;
  }

  private _create(size: Size) {
    let canvas = dom
      .create('canvas')
      .css({
        position: 'absolute',
        display: 'block',
        '-webkit-user-select': 'none',
        '-moz-user-select': 'none',
        '-ms-user-select': 'none',
        'user-select': 'none',
        '-webkit-tap-highlight-color': 'rgba(0, 0, 0, 0)',
        '-webkit-transform': 'translateZ(0)',
        outline: 'none',
      })
      .attr({ tabindex: 1 })
      .addClass('canvas')
      .node();
    canvas.addEventListener('contextmenu', e => {
      e.preventDefault();
      return false;
    });
    return canvas as HTMLCanvasElement;
  }

  destroy() {
    dom(this.element).remove();
  }
}
