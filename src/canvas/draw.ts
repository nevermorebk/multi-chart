import { DrawedLineDashedStyle, DrawedLineMetaInfo, Point, Rect, SelectionState } from '../lib';
import { distanceBetweenPoint, getDistancePointByAngle } from '../util/geometry';
import { forEach } from '../util/array';

export type ShadowLineStyle = { shadowOffsetX?: number; shadowOffsetY?: number; shadowBlur?: number; shadowColor?: string };
export type LineStyle = { lineWidth?: number; strokeStyle?: string } & ShadowLineStyle;

export type DashLineStyle = LineStyle & { lineDash?: number[] };

export type FillStyle = { fillStyle?: string; strokeStyle?: string; lineWidth?: number };

// tslint:disable-next-line:max-line-length
export type FontStyle = { fillStyle?: string; font?: string; textBaseline?: string; textAlign?: string } & {
  // custom style
  bgColor?: string;
  maxWidth?: number;
};

export type Style = Partial<LineStyle & DashLineStyle & FillStyle & FontStyle>;

const DEFAULT_WEIGHT = 1;

const DEFAULT_STYLE = {};

export function getCanvasLineStyle(metaInfo: DrawedLineMetaInfo, state: SelectionState = SelectionState.SHOW): DashLineStyle {
  let style = {
    lineWidth: metaInfo.weight || DEFAULT_WEIGHT,
    strokeStyle: metaInfo.color || '#333',
    lineDash: DrawedLineDashedStyle.DASHED_LINE_VALUES[metaInfo.style],
  };

  // if (state === SelectionState.HIGHLIGHT) {
  //   style.lineWidth *= 1.5;
  // } else if (state === SelectionState.SELECTED) {
  //   style.lineWidth *= 1;
  // }
  return style;
}

export function drawLine(ctx: Context2D, points: Point[], style: DashLineStyle = DEFAULT_STYLE) {
  if (points.length < 2) return;
  ctx.save();
  ctx.beginPath();
  let [first, ...tail] = points;
  ctx.moveTo(Math.round(first.x), Math.round(first.y));
  forEach(tail, d => {
    ctx.lineTo(Math.round(d.x), Math.round(d.y));
  });
  ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
  ctx.strokeStyle = style.strokeStyle;
  if (style.lineDash) {
    let factor = Math.max(ctx.lineWidth, 1);
    ctx.setLineDash(style.lineDash.map(n => n * factor));
  }
  ctx.stroke();
  ctx.restore();
}

export function drawLine2(ctx: Context2D, lines: Point[][], style: DashLineStyle = DEFAULT_STYLE) {
  forEach(lines, (points) => drawLine(ctx, points, style));
}

export function drawArrowLine(ctx: Context2D, points: Point[], style: DashLineStyle = DEFAULT_STYLE) {
  let p1 = points[points.length - 2];
  let p2 = points[points.length - 1];
  drawLine(ctx, points, style);
  if (distanceBetweenPoint(p1, p2) > 15) {
    let [arrowPoint1, arrowPoint2] = arrowLineHelper(p1, p2);
    let arrowStyle = {
      strokeStyle: style.strokeStyle,
      lineWidth: style.lineWidth,
    };
    drawLine(ctx, [arrowPoint1, p2, arrowPoint2], arrowStyle);
  }
}

// TODO: support gradient
export function drawArea(ctx: Context2D, points: Point[], style: FillStyle = DEFAULT_STYLE) {
  if (points.length < 2) return;
  ctx.save();
  ctx.beginPath();
  let [first, ...tail] = points;
  ctx.moveTo(first.x, first.y);
  forEach(tail, d => {
    ctx.lineTo(d.x, d.y);
  });
  ctx.closePath();
  ctx.fillStyle = style.fillStyle;
  ctx.fill();
  if (style.strokeStyle && style.fillStyle !== style.strokeStyle) {
    ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
    ctx.strokeStyle = style.strokeStyle;
    ctx.stroke();
  }
  ctx.restore();
}

export function drawRect(ctx: Context2D, points: Point[], style: FillStyle = DEFAULT_STYLE) {
  if (points.length !== 2) return;
  let [p1, p3] = points;
  let width = Math.abs(p3.x - p1.x);
  let height = Math.abs(p3.y - p1.y);
  let start = {
    x: Math.min(p1.x, p3.x),
    y: Math.min(p1.y, p3.y),
  };
  ctx.save();
  ctx.beginPath();
  let rect = [start.x, start.y, width, height];
  if (style.fillStyle) {
    ctx.fillStyle = style.fillStyle;
    ctx.fillRect(rect[0], rect[1], rect[2], rect[3]);
  }
  if (style.strokeStyle && style.fillStyle !== style.strokeStyle) {
    ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
    ctx.strokeStyle = style.strokeStyle;
    ctx.strokeRect(rect[0], rect[1], rect[2], rect[3]);
  }
  ctx.restore();
}

export function drawRect2(ctx: Context2D, rects: Rect[], style: FillStyle = DEFAULT_STYLE) {
  ctx.save();
  ctx.beginPath();
  if (style.fillStyle) {
    ctx.fillStyle = style.fillStyle;
  }
  if (style.strokeStyle) {
    ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
    ctx.strokeStyle = style.strokeStyle;
  }
  forEach(rects, rect => {
    let left = rect.left;
    let top = rect.top;
    let width = rect.width;
    let height = rect.height;
    if (style.fillStyle) {
      ctx.fillRect(left, top, width, height);
    }
    if (style.strokeStyle && style.fillStyle !== style.strokeStyle) {
      ctx.strokeRect(left, top, width, height);
    }
  });
  ctx.restore();
}

export function drawCircle(ctx: Context2D, point: Point, radius: number, style: FillStyle = DEFAULT_STYLE) {
  ctx.save();
  ctx.beginPath();
  ctx.arc(point.x, point.y, radius, 0, Math.PI * 2, true);

  ctx.fillStyle = style.fillStyle;
  ctx.fill();
  if (style.strokeStyle && style.fillStyle !== style.strokeStyle) {
    ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
    ctx.strokeStyle = style.strokeStyle;
    ctx.stroke();
  }
  ctx.restore();
}

export function drawCircle2(ctx: Context2D, points: Point[], radius: number, style: FillStyle = DEFAULT_STYLE) {
  ctx.save();

  ctx.fillStyle = style.fillStyle;
  forEach(points, point => {
    ctx.beginPath();
    ctx.arc(point.x, point.y, radius, 0, Math.PI * 2, true);
    ctx.fill();
  });

  if (style.strokeStyle && style.fillStyle !== style.strokeStyle) {
    ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
    ctx.strokeStyle = style.strokeStyle;
    ctx.stroke();
  }
  ctx.restore();
}

export function drawFan(ctx: Context2D, center: Point, radius: number, startAngle: number, endAngle: number,
  style: FillStyle = DEFAULT_STYLE) {
  let anticlockwise = endAngle - startAngle <= 0;
  ctx.save();
  ctx.fillStyle = style.fillStyle;
  ctx.beginPath();
  ctx.moveTo(center.x, center.y);
  ctx.arc(center.x, center.y, radius, startAngle, endAngle, anticlockwise);
  ctx.closePath();
  ctx.fill();

  if (style.strokeStyle && style.fillStyle !== style.strokeStyle) {
    ctx.lineWidth = style.lineWidth || DEFAULT_WEIGHT;
    ctx.strokeStyle = style.strokeStyle;
    ctx.stroke();
  }
  ctx.restore();
}

// TODO: multiline
export function drawText(ctx: Context2D, text: string, x: number, y: number, style: FontStyle = DEFAULT_STYLE) {
  ctx.save();
  ctx.font = style.font || '12px Verdana, sans-serif';
  let textAlign = style.textAlign || 'left';
  let textBaseline = style.textBaseline || 'middle';
  if (style.bgColor) {
    let height = 12; // default todo
    let padding = 5;
    height += 2 * 2; // paddingTop * 2
    let width = ctx.measureText(text).width + 2 * padding;
    let dx = textAlign === 'left' ? -padding : textAlign === 'right' ? -width : -width / 2;
    let dy = textBaseline === 'top' ? 0 : textBaseline === 'bottom' ? -height : -height / 2;
    let rect = [x + dx, y + dy, width, height];
    ctx.fillStyle = style.bgColor;
    ctx.fillRect(rect[0], rect[1], rect[2], rect[3]);
  }
  ctx.textAlign = textAlign as CanvasTextAlign;
  ctx.textBaseline = textBaseline as CanvasTextBaseline;
  ctx.fillStyle = style.fillStyle;
  if (style.maxWidth) {
    ctx.fillText(text, x, y, style.maxWidth);
  } else {
    // fuck IE
    ctx.fillText(text, x, y);
  }
  ctx.restore();
}


// private

function arrowLineHelper(p1: Point, p2: Point, distance = 10) {
  let pa1 = getDistancePointByAngle(p2, p1, distance, Math.PI / 8);
  let pa2 = getDistancePointByAngle(p2, p1, distance, -Math.PI / 8);
  return [pa1, pa2];
}
