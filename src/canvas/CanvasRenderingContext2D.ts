// base on https://github.com/jondavidjohn/hidpi-canvas-polyfill

// tslint:disable:only-arrow-functions
function forEach(obj, func) {
  for (let p in obj) {
    if (obj.hasOwnProperty(p)) {
      func(obj[p], p);
    }
  }
}
const ratioArgs = {
  fillRect: 'all',
  clearRect: 'all',
  strokeRect: 'all',
  moveTo: 'all',
  lineTo: 'all',
  arc: [0, 1, 2],
  arcTo: 'all',
  bezierCurveTo: 'all',
  isPointinPath: 'all',
  isPointinStroke: 'all',
  quadraticCurveTo: 'all',
  rect: 'all',
  translate: 'all',
  createRadialGradient: 'all',
  createLinearGradient: 'all',
};
const regfont = /(\d+)(px|em|rem|pt)/g;

export function createContext2D(context: CanvasRenderingContext2D, dpr: number): Context2D {
  let pixelRatio = dpr || window.devicePixelRatio;

  let ctx = context as Context2D;

  ctx.setDPR = dpr => {
    pixelRatio = dpr;
  };

  ctx.getDPR = () => pixelRatio;

  forEach(ratioArgs, function (value, key) {
    if (value === 'all') {
      ctx[key] = (function (_super) {
        return function () {
          let args = Array.prototype.slice.call(arguments);
          args = args.map(a => a * pixelRatio);
          return _super.apply(this, args);
        }
      })(ctx[key]);
    } else if (Array.isArray(value)) {
      ctx[key] = (function (_super) {
        return function () {
          let args = Array.prototype.slice.call(arguments);
          for (let i = 0, len = value.length; i < len; i++) {
            args[value[i]] *= pixelRatio;
          }
          return _super.apply(this, args);
        }
      })(ctx[key]);
    }
  });

  // Stroke lineWidth adjustment
  // make linewidth thin (0.5)
  ctx.stroke = (function (_super) {
    return function (...args: any) {
      this.lineWidth *= pixelRatio;
      _super.apply(this, args);
      this.lineWidth /= pixelRatio;
    } as any;
  })(ctx.stroke);

  // setLineDash
  ctx.setLineDash = (function (_super) {
    return function (segments) {
      if (segments) {
        segments = segments.map(i => i * pixelRatio);
        _super.call(this, segments);
      }
    };
  })(ctx.setLineDash);

  // Text
  //
  ctx.fillText = (function (_super) {
    return function (...args: any) {

      args[1] *= pixelRatio; // x
      args[2] *= pixelRatio; // y
      let maxWidth = args[3];
      if (maxWidth) {
        args[3] *= pixelRatio;
      }
      let font = this.font;
      this.font = this.font.replace(regfont, function (w, m, u) {
        return m * pixelRatio + u;
      });

      _super.apply(this, args);

      this.font = font;
    };
  })(ctx.fillText);

  ctx.strokeText = (function (_super) {
    return function (...args: any) {

      args[1] *= pixelRatio; // x
      args[2] *= pixelRatio; // y
      let maxWidth = args[3];
      if (maxWidth) {
        args[3] *= pixelRatio;
      }
      let font = this.font;
      this.font = this.font.replace(regfont, function (w, m, u) {
        return m * pixelRatio + u;
      });

      _super.apply(this, args);

      this.font = font;
    };
  })(ctx.strokeText);

  return ctx;
}
