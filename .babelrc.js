module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: ['last 2 Chrome versions', 'last 2 ChromeAndroid versions', 'last 2 iOS versions'],
        },
        forceAllTransforms: true,
        debug: process.env.NODE_ENV !== 'production',
        loose: true,
        modules: false,
      },
    ],
    '@babel/preset-react',
  ],
  plugins: [
    ['@babel/plugin-transform-runtime', {
      'corejs': 3
    }],
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    'jsx-control-statements',
    'react-hot-loader/babel',
  ],
};
