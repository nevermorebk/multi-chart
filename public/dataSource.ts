import { DataSourceAdapter, Loading } from '../src/model/DataLoader';
import { DataItem } from '../src/lib';
import WS from './libs/ws';
import * as Fetch from './libs/fetch';
import Message from './libs/message';

let hostname = location.hostname

// mock order / position line
let line = {
  order: {},
  positionSummary: {},
};

let loading = {
  show: () => {
    console.log('show loadingbar');
  },
  hide: () => {
    console.log('hide loadingbar');
  },
  toggle: () => {
    console.log('toggle loadingbar');
  },
} as Loading;

class DataSource implements DataSourceAdapter {
  ws: WS;
  chartSubscribed: Map<any, any>;
  orderSubscribed: Map<any, any>;
  positionSummarySubscribed: Map<any, any>;

  loadingBar = loading;

  constructor() {
    this.chartSubscribed = new Map();
    this.orderSubscribed = new Map();
    this.positionSummarySubscribed = new Map();

    this.mock();
    this.websocket();
  }

  websocket() {
    // ws connect
    let wsUrl = `ws://${location.host}/express/rest/realtime?_csrf_token_=${Fetch.getCookie('_csrf_token_')}`;
    let ws = new WS(wsUrl, { attempt: 3, interval: 0.5 }) as any;
    this.ws = ws;

    ws.subscribeChart = function (symbolId, chartType, side) {
      if (chartType !== 0) {
        // this.send(`subscribe:chart:${symbolId}:${chartType}:${side}`);
        let msg = new Message({
          event: 'CHART',
          action: 'subscribe',
          symbol: symbolId,
          type: chartType,
          side: side,
          snapshot: 'NOP'
        });
        this.send(msg.toString());
      }
      else {
        // this.send(`subscribe:tick:${symbolId}:${side}`);
        let msg = new Message({
          event: 'TICK',
          action: 'subscribe',
          symbol: symbolId,
          side: side,
          snapshot: 'NOP'
        });
        this.send(msg.toString());
      }
    };

    ws.unsubscribeChart = function (symbolId, chartType, side) {
      if (chartType !== 0) {
        // this.send(`unsubscribe:chart:${symbolId}:${chartType}:${side}`);
        let msg = new Message({
          event: 'CHART',
          action: 'unsubscribe',
          symbol: symbolId,
          type: chartType,
          side: side,
          snapshot: 'NOP'
        });
        this.send(msg.toString());
      }
      else {
        // this.send(`unsubscribe:tick:${symbolId}:${side}`);
        let msg = new Message({
          event: 'TICK',
          action: 'unsubscribe',
          symbol: symbolId,
          side: side,
          snapshot: 'NOP'
        });
        this.send(msg.toString());
      }
    };

    ws.onopen = () => {
      console.log('ws open');
    };
    ws.onclose = e => {
      console.log('ws closed', e && e.code);
    };
    ws.onerror = e => {
      console.log('ws error', e && e.code);
    };
    ws.onmessage = e => {
      let r = JSON.parse(e.data);
      let { data, event, symbolId, type, side } = r;
      if (event !== 'CHART' && event !== 'TICK') return;
      if (!Array.isArray(data)) {
        data = [data];
      }
      data.forEach(b => {
        let callback = this.getChartCallback(symbolId, type, side);
        callback && callback({ chartType: type, symbolId, side, data: b });
      });
    };

    let t = setInterval(() => {
      if (this.ws.ws.readyState === WebSocket.CLOSED) {
        clearInterval(t);
        return;
      }
      this.ws.send(Message.pingMessage().toString());
    }, 10000);
  }

  fetch(symbolId: any, chartType: any, side: any, limit: any, from: any): Promise<{ symbolId: number; data: DataItem[]; limit: number }> {
    let type = chartType === 0 ? 'ticks' : 'charts';
    let first = from;
    limit = limit || 200;
    limit = Math.max(200, limit);
    // return Promise.resolve({
    //   symbolId,
    //   limit,
    //   data: require('./mock.ts').default.data,
    // });
    let fetchUrl = `http://${location.host}/express/rest/pricing/chart/${type}?symbolId=${symbolId}&limit=${limit}&chartType=${chartType}&side=${side}`;
    if (first) fetchUrl += `&first=${first}`;
    return Fetch.get(fetchUrl).then(resp => {
      return {
        data: resp.data ?? [],
        symbolId,
        limit,
      };
    });
  }
  // tslint:disable-next-line: max-line-length
  subscribeChart(symbolId: any, chartType: any, side: any, callback: (result: { symbolId: number; chartType: number; side: number; data: DataItem }) => void): void {
    console.log('subscribe quote ', symbolId, chartType, side);
    this.setChartCallback(symbolId, chartType, side, callback);
    this.ws.subscribeChart(symbolId, chartType, side);
  }
  unsubscribeChart(symbolId: any, chartType: any, side, callback: any): void {
    console.log('unsubscribe quote ', symbolId, chartType, side);
    this.removeChartCallback(symbolId, chartType, side);
    this.ws.unsubscribeChart(symbolId, chartType, side);
  }
  subscribeOrders(symbolId: any, callback: (orders: { symbolId: number; sell: number[]; buy: number[] }) => void): void {
    console.log('subscribe orders ', symbolId);
    this.orderSubscribed.set(symbolId, callback);
  }
  unsubscribeOrders(symbolId: any, callback: any): void {
    console.log('unsubscribe orders ', symbolId);
    this.orderSubscribed.delete(symbolId);
  }
  subscribePositionSummary(symbolId: any, callback: (ps: { symbolId: number; sell: number; buy: number }) => void): void {
    console.log('subscribe summary ', symbolId);
    this.positionSummarySubscribed.set(symbolId, callback);
  }
  unsubscribePositionSummary(symbolId: any, callback: any): void {
    console.log('unsubscribe summary ', symbolId);
    this.positionSummarySubscribed.delete(symbolId);
  }

  mock() {
    setInterval(() => {
      if (!(window as any).mchart) return;
      let mainchart = (window as any).mchart.context.getMainChart();
      let symbolId = (window as any).mchart.context.symbol.id;
      if (!mainchart) return;
      let scaleY = mainchart.getScaleY();
      let domain = scaleY.domain();
      let order = line.order as any;
      let positionSummary = line.positionSummary as any;
      positionSummary.sell = domain[0];
      positionSummary.buy = domain[1];
      positionSummary.symbolId = symbolId;
      if (Math.random() > 0.5) {
        order.sell = scaleY.ticks(3);
      } else {
        order.buy = scaleY.ticks(2);
      }
      order.symbolId = symbolId;

      let orderCallback = this.orderSubscribed.get(symbolId);
      if (orderCallback) {
        orderCallback(line.order);
      }

      let summaryCallback = this.positionSummarySubscribed.get(symbolId);
      if (summaryCallback) {
        summaryCallback(line.positionSummary);
      }
    }, 5000);
  }

  getChartCallback(symbolId, chartType, side) {
    let key = `${symbolId}_${chartType}_${side}`;
    return this.chartSubscribed.get(key);
  }

  setChartCallback(symbolId, chartType, side, callback) {
    let key = `${symbolId}_${chartType}_${side}`;
    this.chartSubscribed.set(key, callback);
  }

  removeChartCallback(symbolId, chartType, side) {
    let key = `${symbolId}_${chartType}_${side}`;
    this.chartSubscribed.delete(key);
  }
}

export { DataSource };
