

export const READY_STATE = {
  CONNECTING: 0,
  OPEN: 1,
  CLOSING: 2,
  CLOSED: 3
};

const WebSocketCloseStatus = {
  GOING_AWAY: 1001,
  SESSION_NOT_RELIABLE: 4096
};

export default class ReconnectingWebSocket {
  constructor(url, options) {
    this.url = url;
    this.options = options;
    this.attempt = options.attempt;
    this.interval = options.interval;
    this.connect();
  }

  connectUrl() {
    let url = this.url;
    // let attempt = this.attempt;
    // let now = Date.now(); // retry time
    // url += (url.indexOf("?") > -1 ? "&" : "?") + "t=" + attempt + "_" + now;
    return url;
  }

  disconnect() {
    this.forceClose = true;
    this.ws.close();
  }

  connect() {
    this.forceClose = false;
    this.ws = new WebSocket(this.connectUrl());

    this.ws.onopen = function () {
      if (this.forceClose) return;
      this.attempt = this.options.attempt;
      this.interval = this.options.interval;
      this.start_heartbeat();
      if (this.onopen) {
        this.onopen();
      }
    }.bind(this);

    this.ws.onmessage = function (msg) {
      if (this.onmessage) {
        this.onmessage(msg);
      }
    }.bind(this);

    this.ws.onclose = function (e) {
      this.stop_heartbeat();

      if (e && e.code === WebSocketCloseStatus.SESSION_NOT_RELIABLE) {
        // session invalidated
        this.forceClose = true;
        if (this.onclose) this.onclose('loseSession');
        return;
      }

      if (!this.forceClose && this.attempt > 0) {
        setTimeout(
          function () {
            if (this.forceClose) return;
            if (this.onreconnecting) {
              this.onreconnecting();
            }
            this.attempt -= 1;
            this.connect();
          }.bind(this),
          this.interval
        );
        this.interval *= 2;
        return;
      } else {
        if (this.onclose) {
          this.onclose();
        }
      }
    }.bind(this);

    this.ws.onerror = function (e) {
      // console.error(e);
      if (this.attempt === 0) {
        if (this.onerror) {
          this.onerror(e);
        }
      }
    }.bind(this);
  }

  start_heartbeat() {
    let { heartbeat = 10000 } = this.options;
    this.hbpointer = setInterval(() => {
      this.send(''); // test
    }, heartbeat);
  }

  stop_heartbeat() {
    clearInterval(this.hbpointer);
  }

  send(msg) {
    if (this.ws && this.ws.readyState === READY_STATE.OPEN) {
      this.ws.send(msg);
    }
  }
}
