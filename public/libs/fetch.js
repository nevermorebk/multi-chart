export default function xfetch(url, options = {
  isJson: true
}) {
  // options.credentials = 'include';
  let { body, method, headers = {}, isJson } = options;
  headers['X-Csrf-Token'] = getCookie('_csrf_token_')
  if (method === 'POST') {
    // check custom params
    if (isJson) {
      headers['Content-Type'] = 'application/json';
    } else {
      headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }
  }
  options.headers = headers;
  if (typeof body === 'object') {
    if (isJson) {
      let params = Object.keys(body).reduce((a, b) => {
        let val = body[b];
        if (val !== null && val !== undefined) {
          a[b] = val;
        }
        return a;
      }, {});
      options.body = JSON.stringify(params);
    } else {
      let params = Object.keys(body).reduce((a, b) => {
        let val = body[b];
        if (val !== null && val !== undefined) {
          a.push(`${b}=${val}`);
        }
        return a;
      }, []);
      options.body = params.join('&');
    }
  }

  return fetch(url, options).then(r => {
    if (r.status === 200) {
      if (!~r.headers.get('Content-Type').indexOf('application/json')) {
        return r.text();
      } else {
        return r.json();
      }
    } else {
      return Promise.reject(r.statusText);
    }
  });
}

export function get(url, options = {}) {
  options.method = 'GET';
  return xfetch(url, options);
}

export function post(url, options = {}) {
  options.method = 'POST';
  return xfetch(url, options);
}



export function getCookie(name) {
  let match = document.cookie.match(new RegExp(name + '=([^;]+)'));
  if (match) return decodeURIComponent(match[1]);
  return null;
}
