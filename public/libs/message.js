
// websocket message
export default class Message {
  static UUID = 0;
  static subscribeMessage(channel) {
    let event, symbol, type, side, snapshot, action = 'SUBSCRIBE';
    let props;
    if (typeof channel === 'string') {
      [event, symbol, type, side] = channel.split(':');
      if (event === 'TICK') {
        side = type;
        type = undefined;
      }
    } else {
      event = channel.event;
      symbol = channel.symbol;
      type = channel.type;
      side = channel.side;
      snapshot = channel.snapshot;
    }
    if (symbol) { symbol = Number(symbol); }
    if (type) { type = Number(type); }
    if (side) { side = Number(side); }
    if (!snapshot) { snapshot = 'NOP'; }
    props = { event, symbol, type, side, snapshot, action };
    return new Message(props);
  }

  static unsubscribeMessage(channel) {
    let event, symbol, type, side, action = 'UNSUBSCRIBE';
    let props;
    if (typeof channel === 'string') {
      [event, symbol, type, side] = channel.split(':');
      if (event === 'TICK') {
        side = type;
        type = undefined;
      }
    } else {
      event = channel.event;
      symbol = channel.symbol;
      type = channel.type;
      side = channel.side;
    }
    if (symbol) { symbol = Number(symbol); }
    if (type) { type = Number(type); }
    if (side) { side = Number(side); }
    props = { event, symbol, type, side, action };
    return new Message(props);
  }

  static pingMessage(channel) {
    return new Message({ action: channel || 'PING' });
  }

  // id;
  // action;
  // event;
  // symbol;
  // snapshot;
  // type;
  // side;
  constructor(prop) {
    if (prop) { Object.assign(this, prop); }
    this.id = ++Message.UUID + '';
  }

  toString() {
    const { id, action, event, symbol, type, side, snapshot } = this;
    return JSON.stringify({ id, action, event, symbol, type, side, snapshot });
  }
}
