import MultiChart from '../../src/index';

var args = [
  `%c %c %c MultiChart ${MultiChart.serializeVersion} %c  %c  ${location.href}  %c  \n`,
  'background: #ff66a5; padding:5px 0;',
  'background: #ff66a5; padding:5px 0;',
  'color: #ff66a5; background: #030307; padding:5px 0;',
  'background: #ff66a5; padding:5px 0;',
  'background: #ffc3dc; padding:5px 0;',
  'background: #ff66a5; padding:5px 0;',
];

window.console.log.apply(console, args);


$(function () {
  var $console = document.querySelector('.console')
  var _log = console.log
  var _debug = console.debug
  document.getElementById('consoleBtn').addEventListener('click', function (e) {
    if ($console.className.indexOf('collapse') > -1) {
      $console.classList.remove('collapse')
      console.log = console.debug = print
    } else {
      $console.classList.add('collapse')
      console.log = _log
      console.debug = _debug
    }
  }, false)
});


function print() {
  var first = $console.firstElementChild;
  [].forEach.call(arguments, function (str) {
    var p = document.createElement(typeof str === 'string' ? 'span' : 'code')
    p.innerHTML = typeof str === 'string' ? str : JSON.stringify(str)
    first ? $console.insertBefore(p, first) : $console.appendChild(p)
  });
  if (first) $console.insertBefore(document.createElement('br'), first);
}

