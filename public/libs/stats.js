import Stats from '../assets/stats.js';

$(function(){
  var stats = new Stats();
    stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
    stats.dom.style.position = 'absolute';
    stats.dom.style.left = 'auto';
    stats.dom.style.right = '0px';
    stats.dom.style.top = '30px';
    stats.dom.style.display = 'none';
    stats.dom.className = 'monitor';
    document.body.appendChild( stats.dom );

    function animate() {
      if (stats.dom.style.display === 'none') {
        setTimeout(animate, 1000);
        return;
      }
      stats.begin();
      // monitored code goes here
      stats.end();
      requestAnimationFrame( animate );
    }
    requestAnimationFrame( animate );
});
