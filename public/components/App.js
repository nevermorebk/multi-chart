import React, { useState, useEffect } from 'react';
import Login from './Login';
import ChartView from './ChartView';
import './App.css';
import { get } from '../libs/fetch';

function Loading() {
  return <div className="loading" />;
}

function App(props) {
  const [isUnauthorized, setUnauthorized] = useState(props.isUnauthorized || null);
  const [bootstrap, setBootstrap] = useState(null);

  useEffect(() => {
    get('/express/rest/utility/bootstrap').then((r) => {
      if (r && r.type === 'DATA' && r.data) { return [true, r.data]; }
      return [false, null];
    }).then(([ok, result]) => {
      if (ok) {
        setBootstrap(result);
        setUnauthorized(false);
        return;
      }
      setUnauthorized(true);
    });
  }, [isUnauthorized === false]);


  const View = isUnauthorized === true ? <Login afterLogin={setUnauthorized} /> :
    (isUnauthorized === false && bootstrap ? <ChartView bootstrap={bootstrap} /> : <Loading />);

  return (
    <div className="App">
      {View}
    </div>
  );
}

export default App;




