import React, { useState, useEffect, useCallback, useMemo } from 'react';
import {
  ButtonToolbar,
  ButtonGroup,
  Button,
  DropdownButton,
  Dropdown,
} from 'react-bootstrap';
import { enums } from '../../src';
import { MultiChart } from '../../src/MultiChart';

const { Command, ChartType } = enums;

function useBootstrap(bootstrap) {
  return useMemo(() => {
    const symbols = bootstrap.product.map((product) => ({
      id: product.symbolId,
      name: product.symbolName,
      scale: product.scale,
    }));
    const chartTypes = [
      'Tick',
      'M1',
      'M5',
      'M10',
      'M15',
      'M30',
      'H1',
      'H2',
      'H4',
      'D1',
      'W1',
      'MN1',
    ];
    const chartStyles = ['candle', 'avg', 'line'];
    // TODO (2019/08/22 20:18) get ` indicators types  styles ` from chart
    return {
      symbols,
      chartTypes,
      chartStyles,
    };
  }, [bootstrap]);
}

function useToolbar(chart: MultiChart, symbols) {
  const props = chart.props;
  const [symbol, setSymbol] = useState(props.symbol);
  const [chartType, setChartType] = useState(props.chartType);
  const [chartStyle, setChartStyle] = useState(props.chartStyle);
  const [isYLocked, setYLocked] = useState(false);
  const [size, setSize] = useState(props.sizeOption);
  const [prevSize, setPrevSize] = useState(props.sizeOption);

  useEffect(() => {
    let handleLocked;
    chart.on(
      'mainYAxisLocked',
      (handleLocked = (locked) => setYLocked(locked))
    );
    return () => chart.off('mainYAxisLocked', handleLocked);
  }, [chart]);

  return {
    symbol,
    setSymbol: useCallback(
      (id) => {
        const symbol = symbols.find((s) => s.id === +id);
        symbol && setSymbol(symbol);
        chart.update({ symbol });
      },
      [chart, symbols]
    ),
    chartType,
    setChartType: useCallback(
      (chartType) => {
        setChartType(chartType);
        chart.update({ chartType });
      },
      [chart]
    ),
    chartStyle,
    setChartStyle: useCallback(
      (chartStyle) => {
        setChartStyle(chartStyle);
        chart.update({ chartStyle });
      },
      [chart]
    ),
    toggleSize: useCallback(() => {
      let fullSize = {
        width: window.innerWidth,
        height: window.innerHeight - 30,
      };
      let nextSize = size.width !== fullSize.width ? fullSize : prevSize;
      setPrevSize(size);
      setSize(nextSize);
      chart.resize(nextSize);
    }, [chart, size, prevSize]),
    isYLocked,
    // commands
    zoomIn: useCallback(() => {
      chart.execCommand(Command.ZOOM_IN);
    }, [chart]),
    zoomOut: useCallback(() => {
      chart.execCommand(Command.ZOOM_OUT);
    }, [chart]),
    resetZoom: useCallback(() => {
      chart.execCommand(Command.ZOOM_RESET);
    }, [chart]),
    lockY: useCallback(() => {
      chart.execCommand(Command.FLIP_Y);
    }, [chart]),
    showLast: useCallback(() => {
      chart.execCommand(Command.SHOW_LAST);
    }, [chart]),
  };
}

export default function Toolbar({ chart, bootstrap }) {
  const { symbols, chartTypes, chartStyles } = useBootstrap(bootstrap);
  const toolbar = useToolbar(chart, symbols);
  const {
    symbol,
    setSymbol,
    chartType,
    setChartType,
    chartStyle,
    setChartStyle,
    isYLocked,
  } = toolbar;
  const { zoomIn, zoomOut, resetZoom, lockY, showLast, toggleSize } = toolbar;

  if (!chart || !bootstrap) return null;
  return (
    <ButtonToolbar>
      <ButtonGroup size="sm">
        <Button onClick={zoomIn}>
          <i className="fxiconfont fx-icon-zoom-in"></i>
        </Button>
        <Button onClick={zoomOut}>
          <i className="fxiconfont fx-icon-zoom-out"></i>
        </Button>
        <Button onClick={resetZoom}>
          <i className="fxiconfont fx-icon-search"></i>
        </Button>

        <DropdownButton
          size="sm"
          as={ButtonGroup}
          title={symbol.name}
          id="symbol"
        >
          {symbols.map(({ id, name }) => (
            <Dropdown.Item key={id} eventKey={id} onSelect={setSymbol}>
              {name}
            </Dropdown.Item>
          ))}
        </DropdownButton>
        <DropdownButton
          size="sm"
          as={ButtonGroup}
          title={ChartType[chartType]}
          id="chartType"
        >
          {chartTypes.map((label, index) => (
            <Dropdown.Item
              key={index}
              eventKey={ChartType[label]}
              onSelect={setChartType}
            >
              {label}
            </Dropdown.Item>
          ))}
        </DropdownButton>
        <DropdownButton
          size="sm"
          as={ButtonGroup}
          title={chartStyle}
          id="chartStyle"
        >
          {chartStyles.map((label, index) => (
            <Dropdown.Item
              key={index}
              eventKey={label}
              onSelect={setChartStyle}
            >
              {label}
            </Dropdown.Item>
          ))}
        </DropdownButton>

        <Button onClick={showLast}>
          <i className="fxiconfont fx-icon-long-arrow-right"></i>
        </Button>
        <Button onClick={lockY}>
          <i
            className={`fxiconfont ${
              isYLocked ? 'fx-icon-fixed-y-axis' : 'fx-icon-movable-y-axis'
            }`}
          ></i>
        </Button>
        <Button onClick={toggleSize}>
          <i className="fxiconfont fx-icon-full"></i>
        </Button>
      </ButtonGroup>
    </ButtonToolbar>
  );
}
