import React, { useCallback, useRef } from 'react';
import { useState } from 'react';
import { Card, Form, Row, Col, Button } from 'react-bootstrap';
import { post } from '../libs/fetch';

function doLogin(body) {
  return post('/express/rest/utility/login', { body }).then((r) => {
    if (r && r.key === 'success') {
      return true;
    }
    return false;
  });
}

// eslint-disable-next-line no-unused-vars
const useInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(''),
    bind: {
      value,
      onChange: event => {
        setValue(event.target.value);
      },
    },
  };
};
export default function Login(props) {
  const loginId = 't0000033';
  // render once
  const loginIdRef = useRef(null);
  const handleLogin = useCallback(e => {
    e.preventDefault();
    doLogin({
      companyId: 1,
      loginId: loginIdRef.current.value,
      password: '',
    }).then((result) => {
      if (!result) {
        return alert('login failure');
      }
      props.afterLogin(false);
    });
  }, [props, loginIdRef]);

  return (
    <Card style={{ minWidth: '30rem' }}>
      <Card.Header as="h5">Login</Card.Header>
      <Card.Body>
        <Form>
          <Form.Group as={Row} controlId="formHorizontalLoginId">
            <Form.Label column sm={3}>
              Name
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="LoginId" defaultValue={loginId} ref={loginIdRef} />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalPassword">
            <Form.Label column sm={3}>
              Password
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="password" placeholder="Password" />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={{ span: 9, offset: 3 }}>
              <Button type="submit" onClick={handleLogin}>
                Sign in
              </Button>
            </Col>
          </Form.Group>
        </Form>
      </Card.Body>
    </Card>
  );
}
