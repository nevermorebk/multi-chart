import React, { useEffect, useRef, useState } from 'react';
import MultiChart from '../../src';
import Toolbar from './Toolbar.tsx';
import { DataSource } from '../dataSource';
import './chart.css';

function initChartConfig(bootstrap) {
  return {
    chartType: MultiChart.enums.ChartType.M1,
    lang: 'en',
    chartStyle: 'candle',
    theme: 'dark',
    // dpr: 2,
    // dpr: document.body.ontouchstart ? '1' : window.devicePixelRatio,
    // defaultViewLevel: 30,
    displayOption: {
      crosshair: true,
      gridline: true,
      tooltip: false,
      indicatorLabel: true,
      currentLine: true,
      orderLine: false,
      positionSummaryLine: false,
      magnifier: true,
    },
    sizeOption: {
      width: 400,
      height: 300,
      // yWidth: 50,
    },
    symbol: {
      id: 1,
      name: 'USDJPY',
      scale: 3,
    },
    ds: new DataSource()
  };
}

export default function ChartView(props) {
  const containerRef = useRef(null);
  // const chart = useRef(null); // chart ref
  const [chart, setChart] = useState();
  const [chartConfig] = useState(initChartConfig); // init once

  useEffect(() => {
    const instance = MultiChart.create(chartConfig).attachTo(containerRef.current);
    setChart(instance);
    window.chart = instance;
    window.MultiChart = MultiChart;
    return () => {
      instance.destroy();
    };
  }, [chartConfig]);

  return <div className="chart-view">
    <div className="chart-toolbar">
      {chart ? <Toolbar chart={chart} bootstrap={props.bootstrap} /> : null}
    </div>
    <div className="chart-container" ref={containerRef}></div>
  </div>;
}
