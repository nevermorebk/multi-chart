const path = require('path');
const releasePath = path.resolve(__dirname, '../', 'dist');
const sourcePath = path.resolve(__dirname, '../', 'src');
const nodeModulesPath = path.resolve(__dirname, '../', 'node_modules');
const TerserPlugin = require('terser-webpack-plugin');

// TODO (2020/08/18 10:26)
// - IgnorePlugin?


const config = require('./webpack-dev-server.config');

Object.assign(config, {
  entry: {
    index: [path.resolve(sourcePath, 'index.ts')]
  },
  resolve: {
    modules: [sourcePath, 'node_modules'],
    mainFields: ['jsnext:main', 'module', 'browser', 'main'],
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.json', '.html', '.less', '.css'],
    alias: {
      d3: path.join(nodeModulesPath, 'd3/index.js'),
      react: path.join(nodeModulesPath, 'react/cjs/react.production.min.js'),
    },
  },
  devtool: 'source-map',
  output: {
    path: releasePath, // Path of output file
    filename: '[name].js',
    library: 'MultiChart', // TODO (2018/03/30 12:25)
    libraryTarget: 'umd',
    libraryExport: 'default',
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          output: {
            comments: false,
          }
        },
        extractComments: false,
      }),
    ],
  },
  performance: {
    hints: false,
  },
});

module.exports = config;
