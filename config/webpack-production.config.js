const path = require('path');
const buildPath = path.resolve(__dirname, '../', 'build');
const publicPath = path.resolve(__dirname, '../', 'public');


const config = require('./webpack-dev-server.config');

Object.assign(config, {
  entry: {
    index: [path.join(publicPath, 'index.js')]
  },
  devtool: 'source-map',
  output: {
    path: buildPath, // Path of output file
    filename: '[name]-[chunkhash:8].js',
    chunkFilename: '[name].[id].[chunkhash:8].js',
  },
  performance: {
    hints: false,
  },
});

config.plugins.push(...[
  new HtmlWebpackPlugin({
    template: path.join(publicPath, 'index.ejs'),
    inject: 'head',
  }),
]);

module.exports = config;
