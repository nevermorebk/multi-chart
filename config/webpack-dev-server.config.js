const webpack = require('webpack');
const path = require('path');
const argv = require('yargs').argv;
const buildPath = path.resolve(__dirname, '../', 'build');
const contextPath = path.resolve(__dirname, '../', 'src');
const publicPath = path.resolve(__dirname, '../', 'public');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ENV = process.env.NODE_ENV;

const WatchIgnorePlugin = require('webpack').WatchIgnorePlugin;
const Visualizer = require('webpack-visualizer-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// for ts perf
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { createLogicalNot } = require('typescript');

const config = {
  cache: {
    type: 'filesystem',
    buildDependencies: {
      config: [__filename]
    }
  },
  context: contextPath,
  // Entry points to the project
  entry: {
    index: ['react-hot-loader/patch', path.join(publicPath, 'index.js')],
  },
  resolve: {
    modules: [contextPath, 'node_modules'],
    mainFields: ['jsnext:main', 'module', 'browser', 'main'],
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.jsx', '.json', '.html', '.less', '.css'],
    alias: {
    },
  },
  devtool: 'eval-cheap-source-map', // 'source-map',
  output: {
    path: buildPath, // Path of output file
    filename: 'index.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      __VERSION__: JSON.stringify(require(path.resolve(__dirname, '../package.json')).version),
      __DEV__: ENV === 'development',
      __PROD__: ENV === 'production',
      __TEST__: ENV === 'test',
      __COVERAGE__: !argv.watch && ENV === 'test',
      __BASENAME__: JSON.stringify(process.env.BASENAME || ''),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        use: ['babel-loader', {
          loader: 'ts-loader',
          options: {
            transpileOnly: true,
            experimentalWatchApi: true,
          }
        }],
        exclude: [/node_modules/],
      },
      {
        test: /\.js(x)?$/,
        loader: 'babel-loader',
        exclude: [/node_modules/],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[local]--[hash:base64:5]',
              },
              importLoaders: 1,
            },
          },
          'postcss-loader',
        ],
      },
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader'],
      },
    ],
  },
};

if (ENV === 'development') {
  config.devServer = {
    overlay: true,
    contentBase: ['./build', './public/assets'], // Relative directory for base of server
    contentBasePublicPath: ['/', '/assets'],
    inline: true,
    hot: true,
    port: 3000, // Port Number
    host: '0.0.0.0', // Change to '0.0.0.0' for external facing server
    disableHostCheck: true,
    proxy: {
      '/express/rest': {
        target: 'https://release-erebor.nextop.cn/',
        changeOrigin: true,
        ws: true,
      },
    },
    before: (app, server) => {
      app.use((req, resp, next) => {
        let _write = resp.write;
        resp.write = function () {
          let cookies = resp.get('Set-Cookie');
          if (cookies) {
            cookies = cookies.map(cookie => cookie.replace(/;\s*Secure/gi, ''))
              .map((cookie) => cookie.replace(/\/express/, '/'));
            resp.set('Set-Cookie', cookies);
          }
          return _write.apply(resp, arguments);
        };
        next();
      });
    }
  };
  config.plugins.push(...[
    // new Visualizer(),
    // new BundleAnalyzerPlugin(),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        configFile: path.resolve(__dirname, '../', 'tsconfig.json'),
      },
      eslint: {
        files: './**/*.{ts,tsx,js,jsx}' // base on context
      },
    }),
    new WatchIgnorePlugin({ paths: [path.resolve(__dirname, '../test/')] }),
    new HtmlWebpackPlugin({
      template: path.join(publicPath, 'index.ejs'),
      inject: 'body',
    }),

  ]);
}

module.exports = config;
