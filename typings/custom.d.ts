/// <reference path="./jsx-control-statement" />

declare var __VERSION__: string;
declare var __DEV__: boolean;

declare interface Context2D extends CanvasRenderingContext2D {
  setDPR(dpr: number): void;
  getDPR(): number;
}

declare interface I18nFunction {
  (label: string, data?: any): string;
}

// for webpack import (not ts modules)
declare module '*.svg';

declare module '*.css';

declare module '*.less';
